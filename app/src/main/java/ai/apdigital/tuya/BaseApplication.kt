package ai.apdigital.tuya/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2021 Tuya Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *//*


package ai.apdigital.smartworld

import ai.apdigital.smartworld.base.prefModule
import ai.apdigital.smartworld.di.*
import android.app.Application
import android.util.Log
import com.facebook.drawee.backends.pipeline.Fresco
import com.tuya.smart.api.MicroContext
import com.tuya.smart.api.service.RedirectService
import com.tuya.smart.commonbiz.bizbundle.family.api.AbsBizBundleFamilyService
import com.tuya.smart.home.sdk.TuyaHomeSdk
import com.tuya.smart.optimus.sdk.TuyaOptimusSdk
import com.tuya.smart.wrapper.api.TuyaWrapper
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import timber.log.Timber

//import com.uuzuche.lib_zxing.activity.ZXingLibrary


*/
/**
 * Base Application
 *
 * @author qianqi <a href="mailto:developer@tuya.com"/>
 * @since 2021/1/6 11:50 AM
 */
/*val appModule = listOf(
    prefModule, fragmentModule, viewModelModule,
    networkModule, databaseModule, repositoryModule
)*//*

class BaseApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        TuyaHomeSdk.init(this)
        TuyaHomeSdk.setDebugMode(true)
        Timber.plant(Timber.DebugTree())
      //  ZXingLibrary.initDisplayOpinion(this)
        // SDK init
        Fresco.initialize(this)
        // SDK init
        // SDK init

    */
/*    startKoin {
            androidLogger()
            androidContext(this@BaseApplication)
            modules(appModule)
        }*//*

        // bizbundle init
        TuyaWrapper.init(this,
            { errorCode, urlBuilder ->
                Log.e(
                    "router not implement",
                    urlBuilder.target + urlBuilder.params.toString()
                )
            }
        ) { serviceName -> Log.e("service not implement", serviceName) }
        TuyaOptimusSdk.init(this)

        // register family service

        // register family service
        TuyaWrapper.registerService(
            AbsBizBundleFamilyService::class.java,
            BizBundleFamilyServiceImpl()
        )
//Intercept existing routes and jump to custom implementation pages with parameters
        //Intercept existing routes and jump to custom implementation pages with parameters
        val service = MicroContext.getServiceManager().findServiceByInterface<RedirectService>(
            RedirectService::class.java.name
        )
        service.registerUrlInterceptor { urlBuilder, interceptorCallback -> //Such as:
            //Intercept the event of clicking the panel right menu and jump to the custom page with the parameters of urlBuilder
            if (urlBuilder.target == "panelAction" && urlBuilder.params.getString("action") == "gotoPanelMore") {
                */
/*interceptorCallback.interceptor("interceptor")
                Log.e("interceptor", urlBuilder.params.toString())
                Log.e("extra_panel_dev_id", urlBuilder.params.getString("extra_panel_dev_id")?:"null")
                Log.e("extra_panel_name", urlBuilder.params.getString("extra_panel_name")?:"null")

                val urlBuilders = UrlBuilder(this, "panelMore")
                val bundle = Bundle()
                bundle.putString("extra_panel_dev_id", urlBuilder.params.getString("extra_panel_dev_id"))
                bundle.putString("extra_panel_name", urlBuilder.params.getString("extra_panel_name"))
                bundle.putLong("extra_panel_group_id", -1)

                Log.e("interceptor", bundle.toString())
                urlBuilders.putExtras(bundle)
                UrlRouter.execute(urlBuilders)*//*



                //Such as:
                //Intercept the event of clicking the panel right menu and jump to the custom page with the parameters of urlBuilder
                //例如：拦截点击面板右上角按钮事件，通过 urlBuilder 的参数跳转至自定义页面
//                if (urlBuilder.target.equals("panelAction") && urlBuilder.params.getString("action").equals("gotoPanelMore")) {
//                    interceptorCallback.interceptor("interceptor");
//                    Log.e("interceptor", urlBuilder.params.toString());
//                } else {
                interceptorCallback.onContinue(urlBuilder)
            } else {
                interceptorCallback.onContinue(urlBuilder)
            }
        }
    }
}*/
