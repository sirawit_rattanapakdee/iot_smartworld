package ai.apdigital.tuya

import ai.apdigital.tuya.base.BaseActivity
import ai.apdigital.tuya.base.Constants
import ai.apdigital.tuya.base.SmartWorldPreferences
import ai.apdigital.tuya.base.prefModule
import ai.apdigital.tuya.di.*
import ai.apdigital.tuya.networking.Resource
import ai.apdigital.tuya.networking.Status
import ai.apdigital.tuya.ui.MainActivity
import ai.apdigital.tuya.ui.control.security.SecurityActivity
import ai.apdigital.tuya.ui.viewmodel.TuyaViewModel
import ai.apdigital.tuya.utils.DialogCircleLoadingHelper
import ai.apdigital.tuya.utils.alerts.AlertUtil
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.lifecycle.Observer
import com.facebook.drawee.backends.pipeline.Fresco
import com.google.gson.Gson
import com.tuya.smart.android.user.api.ILoginCallback
import com.tuya.smart.android.user.bean.User
import com.tuya.smart.api.router.UrlBuilder
import com.tuya.smart.api.router.UrlRouter
import com.tuya.smart.home.sdk.TuyaHomeSdk
import com.tuya.smart.home.sdk.bean.HomeBean
import com.tuya.smart.home.sdk.callback.ITuyaGetHomeListCallback
import com.tuya.smart.home.sdk.callback.ITuyaHomeResultCallback
import com.tuya.smart.optimus.sdk.TuyaOptimusSdk
import com.tuya.smart.wrapper.api.TuyaWrapper
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import timber.log.Timber


val appModule = listOf(
    prefModule, fragmentModule, viewModelModule,
    networkModule, databaseModule, repositoryModule
)

class SplashScreenActivity : BaseActivity() {
    var preference: SmartWorldPreferences? = null
    var homeId: String? = null
    var homeName: String? = null
    var username: String? = null
    var password: String? = null
    override fun getLayoutView() = R.layout.activity_splash

    override fun bindView() {
        //changeThaiLanguage()
    }

    override fun setupInstance() {
        if (Timber.treeCount() == 0) if (BuildConfig.DEBUG) Timber.plant(Timber.DebugTree())
        Timber.e("init")
        TuyaHomeSdk.init(application)
        TuyaHomeSdk.setDebugMode(true)


        try {
            Fresco.initialize(this)
            TuyaOptimusSdk.init(this)
            startKoin {
                androidLogger(if (BuildConfig.DEBUG) Level.ERROR else Level.NONE)
                androidContext(application)
                modules(appModule)
            }
        } catch (e: Exception) {
            Timber.e("Error  ${e.localizedMessage}")
        }

        try {
            TuyaWrapper.init(application,
                { errorCode, urlBuilder ->
                    Log.e(
                        "router not implement",
                        urlBuilder.target + urlBuilder.params.toString()
                    )
                }
            ) { serviceName -> Log.e("service not implement", serviceName) }

        } catch (e: Exception) {
            Timber.e("Error Tuya Wrapper ${e.localizedMessage}")
            finish()
        }
    }

    private fun checkSameUser(
        username: String?,
        password: String?,
        homeId: String?,
        homeName: String?
    ): Boolean {
        if (preference?.getUserName() == username && preference?.getPassword() == password && preference?.getHomeName() == homeName && preference?.getHomeID() == homeId)
            return true
        return false
    }

    override fun onPause() {
        super.onPause()
        hideCircleProgressView()
    }

    private fun queryHome(homeName: String?) {
        TuyaHomeSdk.getHomeManagerInstance().queryHomeList(object : ITuyaGetHomeListCallback {
            override fun onSuccess(homeBeans: MutableList<HomeBean>?) {
                var home: HomeBean? = null
                if ((homeBeans?.size ?: 0) > 0) {
                    homeBeans?.forEach foundHome@{ homes ->
                        Timber.e("${homes.homeId}, ${homes.name}")
                        if (homeName.equals(homes.name)) {
                            home = homes
                            return@foundHome
                        }
                    }

                    // home = homeBeans?.get(1)

                    if (home != null) {
                        home?.let { homeSelected ->
                            Timber.e("Home not null ${Gson().toJson(homeSelected)}")
                            HomeModel.INSTANCE.setCurrentHome(
                                this@SplashScreenActivity,
                                homeSelected.homeId
                            )
                            openMain()
                        }
                    } else {
                        createHome(homeName)
                    }
                } else {
                    createHome(homeName)
                }
            }

            override fun onError(errorCode: String?, error: String?) {
                hideCircleProgressView()
                showToastErrorAlertNotices(
                    error ?: "", object : AlertUtil.OnSuccessListener {
                        override fun onSuccess() {
                            finish()
                        }
                    }
                )
            }
        })
    }

    fun createHome(homeName: String?) {
        TuyaHomeSdk.getHomeManagerInstance().createHome(
            homeName,
            0.0,
            0.0,
            null,
            mutableListOf(),
            object : ITuyaHomeResultCallback {
                override fun onSuccess(bean: HomeBean) {
                    TuyaHomeSdk.newHomeInstance(bean.homeId)
                    HomeModel.INSTANCE.setCurrentHome(
                        this@SplashScreenActivity,
                        bean.homeId
                    )
                    openMain()
                }

                override fun onError(errorCode: String, errorMsg: String) {
                    hideCircleProgressView()
                    showToastErrorAlertNotices(
                        errorMsg, object : AlertUtil.OnSuccessListener {
                            override fun onSuccess() {
                                finish()
                            }
                        }
                    )
                }
            })
    }

    private var dataPush: Bundle? = null
    private var isAlreadyOpenNotification = false
    fun openMain() {
        hideCircleProgressView()
        Timber.e("Open MainActivity")
        dataPush = intent.getBundleExtra(Constants.INTENT_NOTIFICATION_DATA)

        preference?.setHomeID(homeId)
        preference?.setHomeName(homeName)
        preference?.setPassword(password)
        preference?.setUserName(username)

        if (dataPush != null && !isAlreadyOpenNotification) {
            try {
/*                val dataA = dataPush?.getBundle("data")
                dataA?.let { data ->*/
                if (dataPush?.getString("categoryCode")?.contains("mal") == true) {
                    tuyaVM?.getHomeDetail()?.observe(this, homeRes)
                    tuyaVM?.getHomeDetail(HomeModel.INSTANCE.getCurrentHome(this))
                } else {
                    isAlreadyOpenNotification = true
                    openMainActivity()
                }
                //   }
            } catch (e: Exception) {
                isAlreadyOpenNotification = true
                openMainActivity()
            }
        } else {
            openMainActivity()
        }
    }

    val tuyaVM: TuyaViewModel? by viewModel()
    private val homeRes = Observer<Resource<HomeBean>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                /*val dataA = dataPush?.getBundle("data")
                dataA?.let { data ->*/
                if (dataPush?.getString("categoryCode")?.contains("mal") == true) {
                    hideCircleProgressView()
                    val intent =
                        Intent(this@SplashScreenActivity, SecurityActivity::class.java)
                    intent.putExtra(Constants.INTENT_DEV_ID, dataPush?.getString("devId"))
                    intent.putExtra(Constants.INTENT_NOTIFICATION_DATA, dataPush)
                    startActivity(intent)
                    finish()
                } else {
                    UrlRouter.execute(UrlBuilder(this, "messageCenter"))
                }
                // }
            }

            Status.ERROR -> {
                hideCircleProgressView()
                showToastErrorAlertNotices(
                    resource.message?.message ?: getString(R.string.msg_failure), null
                )
            }
            Status.LOADING -> {
                showCircleProgressView()
            }
        }
    }

    private fun openMainActivity() {
        val intent =
            Intent(this@SplashScreenActivity, MainActivity::class.java)
        //intent.putExtra(Constants.INTENT_DEV_ID, dataPush?.getString("devId"))
        intent.putExtra(Constants.INTENT_NOTIFICATION_DATA, dataPush)
        startActivity(intent)
        finish()

/*
        val intent = Intent(
            this@SplashScreenActivity,
            MainActivity::class.java
        )
        intent.putExtra(
            Constants.INTENT_NOTIFICATION_DATA,
            dataPush
        )
        startActivity(
            intent
        )
        finish()*/
    }

    override fun setupView() {

    }

    override fun initialize(savedInstanceState: Bundle?) {
        Timber.e("initialize - Splash")
        showCircleProgressView()
        // setLanguage(intent.getStringExtra(TuyaSmartWorldModule.EXTRA_LANGUAGE) ?: "en")
        // If login, then navigate to MainSampleList
        preference = SmartWorldPreferences(this)

        homeName =
            intent.getStringExtra(TuyaSmartWorldModule.EXTRA_HOME_NAME) ?: "Smart World"
        homeId =
            intent.getStringExtra(TuyaSmartWorldModule.EXTRA_HOME_ID) ?: "0847598786"
        username =
            intent.getStringExtra(TuyaSmartWorldModule.EXTRA_USERNAME) ?: "0847598786"
        password =
            intent.getStringExtra(TuyaSmartWorldModule.EXTRA_PASSWORD) ?: "0847598786"


        if (checkSameUser(
                username,
                password,
                homeId,
                homeName
            ) && TuyaHomeSdk.getUserInstance().isLogin
        ) {
            Timber.e("${TuyaHomeSdk.getUserInstance().isLogin} Login : True")
            if (HomeModel.INSTANCE.getCurrentHome(this) != 0L)
                openMain()
             else
                queryHome(homeName)
        } else {
            // preference?.setToken(null)
            Timber.e("Login : false")
            TuyaHomeSdk.getUserInstance().loginWithPhonePassword("66",
                username,
                password,
                object : ILoginCallback {
                    override fun onSuccess(user: User?) {
                        queryHome(homeName)
                    }

                    override fun onError(code: String?, error: String?) {
                        hideCircleProgressView()

                        //Toast.makeText(this@SplashScreenActivity, "code: " + code + "error:" + error, Toast.LENGTH_SHORT).show();
                        showToastErrorAlertNotices(
                            error ?: "", object : AlertUtil.OnSuccessListener {
                                override fun onSuccess() {
                                    finish()
                                }
                            }
                        )
                    }
                })
        }
    }

    private fun showCircleProgressView() {
        try {
            DialogCircleLoadingHelper.showProgress(this)
        } catch (ignore: Exception) {
        }
    }

    private fun hideCircleProgressView() {
        try {
            DialogCircleLoadingHelper.hideProgress()
        } catch (ignore: Exception) {
        }
    }
}
