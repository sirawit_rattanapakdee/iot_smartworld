package ai.apdigital.tuya

import ai.apdigital.tuya.base.Constants
import android.content.Intent
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import timber.log.Timber

class TuyaFcmListenerService : FirebaseMessagingService() {

    override fun onMessageReceived(message: RemoteMessage) {
        // notificationVM?.sendNotificationData(message)
        /*if(isSmartLockForeground) {*/
        /*Timber.e(Gson().toJson(message.data))
        Timber.e(Gson().toJson(message.notification))*/
        val intent =
            Intent(Constants.ACTION_NOTIFICATION)
        intent.putExtra(Constants.INTENT_NOTIFICATION_DATA, message.data["ts"])
        intent.putExtra(Constants.INTENT_DEV_ID, message.data["devId"])
        intent.putExtra(
            Constants.INTENT_NOTIFICATION_ALERT,
            Gson().toJson(message)
        )
        sendBroadcast(intent)
        /*}else{
            val intent =
                Intent(Constants.ACTION_NOTIFICATION)

            intent.putExtra(Constants.INTENT_NOTIFICATION_DATA, message.data["ts"])
            intent.putExtra(
                Constants.INTENT_NOTIFICATION_ALERT,
                Gson().toJson(message.notification)
            )
            sendBroadcast(intent)
        }*/


        /* Timber.e("FCM message received%s", message.data.toString())
         Timber.e("FCM message received%s", message.notification?.body.toString())
         message.data?.forEach {
             Timber.e(it.key.plus(" / ").plus(it.value))
         }*/
    }
}