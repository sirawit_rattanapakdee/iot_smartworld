package ai.apdigital.tuya

import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import com.facebook.react.bridge.*
import org.json.JSONArray
import org.json.JSONObject
import timber.log.Timber


class TuyaSmartWorldModule(private val reactContext: ReactApplicationContext) :
    ReactContextBaseJavaModule(reactContext) {

    companion object {
        lateinit var reactContextApp: ReactApplicationContext
        val EXTRA_USERNAME = "USERNAME"
        val EXTRA_PASSWORD = "PASSWORD"
        val EXTRA_HOME_ID = "HOME_ID"
        val EXTRA_HOME_NAME = "HOME_NAME"
        val EXTRA_LANGUAGE = "LANGUAGE"
        val EXTRA_DATA_NOTIFICATION = "DATA_NOTIFICATION"
        val EXTRA_IS_HAS_NOTIFICATION = "EXTRA_IS_HAS_NOTIFICATION"
        var RN_CALLBACK: Callback? = null

    }

    override fun getName(): String {
        return "Entrance"
    }

    override fun canOverrideExistingModule(): Boolean {
        return true
    }

    @ReactMethod
    fun openSmartHome(
        homeId: String,
        homeName: String,
        username: String,
        password: String,
        language: String,
        dataNotification: ReadableMap?,
        callback: Callback
    ) {
        if (Timber.treeCount() == 0) if (BuildConfig.DEBUG) Timber.plant(Timber.DebugTree())
        // Timber.e("init")
        // Timber.e("${dataNotification}")
        // if(dataNotification == null){
        //     Timber.e("Data Puish Null ")
        // }else{
        //     Timber.e("Data Puish Not   Null  ")
        // }
        val intent = Intent(reactContext, SplashScreenActivity::class.java).apply {
            addFlags(FLAG_ACTIVITY_NEW_TASK)
            putExtra(EXTRA_HOME_ID, homeId)
            putExtra(EXTRA_HOME_NAME, homeName)
            putExtra(EXTRA_USERNAME, username)
            putExtra(EXTRA_PASSWORD, password)
            putExtra(EXTRA_LANGUAGE, language)

            dataNotification?.let {
                it.toHashMap()?.forEach { d ->
                    Timber.e(d.key.plus(" / ").plus(d.value))
                }
                putExtra(EXTRA_DATA_NOTIFICATION, convertMapToJson(it).toString())
            }
        }
        RN_CALLBACK = callback
        reactContextApp = reactContext
        reactContext.startActivity(intent)
    }


    private fun convertMapToJson(readableMap: ReadableMap): JSONObject {
        val objectData = JSONObject()
        val iterator = readableMap.keySetIterator()
        while (iterator.hasNextKey()) {
            val key = iterator.nextKey()
            when (readableMap.getType(key)) {
                ReadableType.Null -> objectData.put(key, JSONObject.NULL)
                ReadableType.Boolean -> objectData.put(key, readableMap.getBoolean(key))
                ReadableType.Number -> objectData.put(key, readableMap.getDouble(key))
                ReadableType.String -> objectData.put(key, readableMap.getString(key))
                ReadableType.Map -> objectData.put(key, convertMapToJson(readableMap.getMap(key)))
                ReadableType.Array -> objectData.put(
                    key,
                    convertArrayToJson(readableMap.getArray(key))
                )
            }
        }
        return objectData
    }

    private fun convertArrayToJson(readableArray: ReadableArray): JSONArray? {
        val array = JSONArray()
        for (i in 0 until readableArray.size()) {
            when (readableArray.getType(i)) {
                ReadableType.Null -> {}
                ReadableType.Boolean -> array.put(readableArray.getBoolean(i))
                ReadableType.Number -> array.put(readableArray.getDouble(i))
                ReadableType.String -> array.put(readableArray.getString(i))
                ReadableType.Map -> array.put(convertMapToJson(readableArray.getMap(i)))
                ReadableType.Array -> array.put(convertArrayToJson(readableArray.getArray(i)))
            }
        }
        return array
    }
}
