package ai.apdigital.tuya.base

import ai.apdigital.tuya.R
import ai.apdigital.tuya.networking.ResponseDescription
import ai.apdigital.tuya.utils.DialogAlertHelper
import ai.apdigital.tuya.utils.DialogLoadingHelper
import ai.apdigital.tuya.utils.alerts.AlertTypeEnum
import ai.apdigital.tuya.utils.alerts.AlertUtil
import ai.apdigital.tuya.utils.alerts.ConfirmDialogFragment
import ai.apdigital.tuya.utils.exception.NotSetLayoutException
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.akexorcist.localizationactivity.ui.LocalizationActivity
import com.facebook.shimmer.ShimmerFrameLayout
import kotlinx.android.synthetic.main.toolbar_back_with_control_panel.*
import timber.log.Timber


abstract class BaseActivity : LocalizationActivity() {

    var toolBarListener: ToolBarListener? = null

    protected var shimmerViewContainer: ShimmerFrameLayout? = null
    protected var contentContainer: View? = null

    companion object {
        var haveUpdateMain = false
    }

    var isUpdate = false

    abstract fun getLayoutView(): Int

    abstract fun bindView() //findViewById

    abstract fun setupInstance()
    //checkPermission
    //getIntent
    //new object as Adapter

    abstract fun setupView()
    //set event as onclick

    val startForDeviceDetailResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                setResult(Activity.RESULT_OK, result.data)
                when {
                    intent?.getStringExtra("ACTION") == "REMOVE" -> {
                        finish()
                    }
                    intent?.getStringExtra("ACTION") == "RENAME" -> {
                        tvTitle?.text = intent.getStringExtra("NAME")
                        isUpdate = true
                    }
                }
            }
        }

    abstract fun initialize(savedInstanceState: Bundle?)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {
            val layoutResId = getLayoutView()
            if (getLayoutView() == 0) throw NotSetLayoutException()
        
            setContentView(layoutResId)
        
            bindView()
        
            setupInstance()
        
            setupView()
        
            initialize(savedInstanceState)
        } catch (e: Exception) {
        }
    }

    protected fun changeEnglishLanguage() {
        setLanguage("en", "US")
    }

    protected fun changeThaiLanguage() {
        setLanguage("th", "TH")
    }

    internal fun setupToolbar(/*toolbar: Toolbar,*/ customTitle: String,
                                                    showButtonBack: Boolean,
                                                    showRightMenu: Boolean
    ) {
        //setSupportActionBar(toolbar)
        //supportActionBar?.setDisplayHomeAsUpEnabled(false)
        if (!showButtonBack)
            boxBack?.visibility = View.INVISIBLE
        else {
            ivBack?.setOnClickListener { onBackMain() }
        }
        tvTitle?.text = customTitle

        tvSetting?.let {
            val manageRoomString = SpannableString(it.text)
            manageRoomString.setSpan(UnderlineSpan(), 0, manageRoomString.length, 0)
            it.text = manageRoomString
        }
    }

    fun showShimmerLoading() {
        contentContainer?.visibility = View.INVISIBLE
        shimmerViewContainer?.visibility = View.VISIBLE
        shimmerViewContainer?.startShimmer()
    }

    fun hideShimmerLoading() {
        shimmerViewContainer?.stopShimmer()
        contentContainer?.visibility = View.VISIBLE
        shimmerViewContainer?.visibility = View.GONE
    }


    internal fun setToolBarListener(toolBarListener: ToolBarListener) {
        this.toolBarListener = toolBarListener
    }

    internal fun setLoadingIndicator(active: Boolean) {
        try {
            /*if (active)
                APDialogHelper.showProgress(this)
            else
                APDialogHelper.hideProgress()*/
        } catch (ignore: Exception) {
        }
    }


    internal fun hideKeyboard() {
        if (currentFocus == null) View(this) else currentFocus?.let { hideKeyboard(it) }
    }

    private fun Context.hideKeyboard(view: View) {
        val inputMethodManager =
            getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    // add fragment to activity
    internal fun AppCompatActivity.addFragmentToActivity(
        fragment: Fragment,
        targetResId: Int,
        tag: String
    ) {
        //supportFragmentManager.beginTransaction().replace(targetResId, fragment, tag).addToBackStack(tag).commit()
        supportFragmentManager.beginTransaction().replace(targetResId, fragment, tag).commit()
    }

    // add fragment to activity
    internal fun AppCompatActivity.replaceFragmentToActivity(
        targetResId: Int, fragment: Fragment

    ) {
        supportFragmentManager.beginTransaction()
            .add(targetResId, fragment, fragment.javaClass.name).commit()
    }

/*override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
    super.onActivityResult(requestCode, resultCode, data)
    if (resultCode == Activity.RESULT_OK && data?.getStringExtra(ActionType.ACTION) == ActionType.BACK_TO_HOME) {

    }
}*/

    fun showAlertConfirmWithActionClick(
        title: String?, message: String?,
        successListener: AlertUtil.OnSuccessListener?, confirmColor: Int
    ) {
        supportFragmentManager.let {
            val notices = ConfirmDialogFragment(title, message, successListener, null, confirmColor)
            notices.isCancelable = false
            notices.show(
                it,
                "showAlertNotices"
            )
        }
    }

    fun showConfirmAndOK(
        title: String?,
        message: String,
        successListener: AlertUtil.OnSuccessListener?, confirmColor: Int
    ) {
        hideProgressView()
        AlertUtil.showAlertConfirmWithActionOKClick(
            title,
            supportFragmentManager,
            message,
            successListener, confirmColor
        )
    }

    fun showInfoAlertNoticesAndOK(
        message: ResponseDescription?,
        successListener: AlertUtil.OnSuccessListener?,
        buttonName: String
    ) {
        hideProgressView()
        AlertUtil.showAlertNoticesWithActionOKClick(
            AlertTypeEnum.NOTICES,
            supportFragmentManager,
            message,
            successListener, buttonName
        )
    }

    fun showInfoAlertNotices(
        message: ResponseDescription?,
        buttonName: String
    ) {
        hideProgressView()
        AlertUtil.showAlertNoticesWithActionOKClick(
            AlertTypeEnum.NOTICES,
            supportFragmentManager,
            message,
            null, buttonName
        )
    }

    fun showSuccessAlertNoticesAndOK(
        message: ResponseDescription?,
        successListener: AlertUtil.OnSuccessListener?,
        buttonName: String
    ) {
        hideProgressView()
        AlertUtil.showAlertNoticesWithActionOKClick(
            AlertTypeEnum.SUCCESS,
            supportFragmentManager,
            message,
            successListener, buttonName
        )
    }

    fun showSuccessAlertNotices(
        message: ResponseDescription?,
        buttonName: String
    ) {
        hideProgressView()
        AlertUtil.showAlertNoticesWithActionOKClick(
            AlertTypeEnum.SUCCESS,
            supportFragmentManager,
            message,
            null, buttonName
        )
    }

    fun showToastSuccessAlertNotices(
        message: String,
        successListener: AlertUtil.OnSuccessListener?
    ) {
        hideProgressView()
        DialogAlertHelper.showProgress(this, R.drawable.ic_alert_success, message, successListener)
    }

    fun showToastErrorAlertNotices(
        message: String,
        successListener: AlertUtil.OnSuccessListener?
    ) {
        hideProgressView()
        DialogAlertHelper.showProgress(this, R.drawable.ic_alert_error, message, successListener)
    }

    fun showToastAlertNotices(
        message: String, icon: Int,
        successListener: AlertUtil.OnSuccessListener?
    ) {
        hideProgressView()
        DialogAlertHelper.showProgress(this, icon, message, successListener)
    }

    fun showErrorAlertNotices(
        message: ResponseDescription?,
        buttonName: String
    ) {
        hideProgressView()
        hideShimmerLoading()
        AlertUtil.showAlertNoticesWithActionOKClick(
            AlertTypeEnum.ERROR,
            supportFragmentManager,
            message,
            null, buttonName
        )
    }

    fun showProgressView() {
        try {
            DialogLoadingHelper.showProgress(this)
        } catch (ignore: Exception) {
        }
    }

    fun hideProgressView() {
        try {
            DialogLoadingHelper.hideProgress()
        } catch (ignore: Exception) {
        }
    }

    fun onBackMain() {
        if (isUpdate) {
            setResult(Activity.RESULT_OK, Intent())
            finish()
        } else
            super.onBackPressed()
    }

    override fun onBackPressed() {
        onBackMain()
    }
}