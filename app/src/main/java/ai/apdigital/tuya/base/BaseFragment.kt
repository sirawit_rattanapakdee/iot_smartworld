package ai.apdigital.tuya.base

import ai.apdigital.tuya.R
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.facebook.shimmer.ShimmerFrameLayout
import ai.apdigital.tuya.networking.ResponseDescription
import ai.apdigital.tuya.utils.DialogAlertHelper
import ai.apdigital.tuya.utils.alerts.AlertUtil
import ai.apdigital.tuya.utils.DialogLoadingHelper
import ai.apdigital.tuya.utils.alerts.AlertTypeEnum
import ai.apdigital.tuya.utils.exception.NotSetLayoutException

abstract class BaseFragment : Fragment() {

    protected var shimmerViewContainer: ShimmerFrameLayout? = null
    protected var contentContainer: View? = null


    companion object {
        var currentContainerViewId: Int = 0
    }

    var toolBarListener: ToolBarListener? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val layoutResId = getLayoutView()
        if (getLayoutView() == 0) throw NotSetLayoutException()
        return inflater.inflate(layoutResId, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupInstance()
        setupView()
        if (savedInstanceState == null) {
            initialize()
        } else {
            onRestoreInstanceState(savedInstanceState)
        }
    }

    override fun onPause() {
        super.onPause()
        hideProgressView()
    }

    protected fun changeEnglishLanguage() {
        if (activity is BaseActivity)
            (activity as BaseActivity).setLanguage("en", "US")
    }

    protected fun changeThaiLanguage() {
        if (activity is BaseActivity)
            (activity as BaseActivity).setLanguage("th", "TH")
    }

    abstract fun onRestoreInstanceState(savedInstanceState: Bundle)

    abstract fun getLayoutView(): Int

    abstract fun setupInstance()

    abstract fun setupView()

    abstract fun initialize()


    internal fun hideKeyboard() {
        (activity as BaseActivity).hideKeyboard()
    }

    internal fun popBackStack() {
        (activity as AppCompatActivity).apply {
            onBackPressed()
        }
    }

    internal fun setupToolbar(/*toolbar: Toolbar,*/ customTitle: String,
                                                    showButtonBack: Boolean,
                                                    showRightMenu: Boolean
    ) {

    }

    fun showShimmerLoading() {
        contentContainer?.visibility = View.INVISIBLE
        shimmerViewContainer?.visibility = View.VISIBLE
        shimmerViewContainer?.startShimmer()
    }

    fun hideShimmerLoading() {
        shimmerViewContainer?.stopShimmer()
        contentContainer?.visibility = View.VISIBLE
        shimmerViewContainer?.visibility = View.GONE
    }

    fun showProgressView() {
        try {
            if (isAdded) {
                context?.let { DialogLoadingHelper.showProgress(it) }
            }
        } catch (ignore: Exception) {
        }
    }

    fun hideProgressView() {
        try {
            DialogLoadingHelper.hideProgress()
        } catch (ignore: Exception) {
        }
    }

    fun showEmptyStateView() {
        DialogLoadingHelper.hideProgress()
        //mEmptyView?.visibility = View.VISIBLE
    }

    fun showConfirmAndOK(
        title: String,
        message: String,
        successListener: AlertUtil.OnSuccessListener?, confirmColor: Int
    ) {
        hideProgressView()
        AlertUtil.showAlertConfirmWithActionOKClick(
            title,
            activity?.supportFragmentManager,
            message,
            successListener, confirmColor
        )
    }

    fun showConfirmAndOKAndFail(
        title: String,
        message: String,
        successListener: AlertUtil.OnSuccessListener?,
        cancelListener: AlertUtil.OnCancelListener?,
        confirmColor: Int
    ) {
        hideProgressView()
        AlertUtil.showAlertConfirmSFWithActionOKClick(
            title,
            activity?.supportFragmentManager,
            message,
            successListener, cancelListener, confirmColor
        )
    }

    fun showInfoAlertNoticesAndOK(
        message: ResponseDescription?,
        successListener: AlertUtil.OnSuccessListener?,
        buttonName: String
    ) {
        hideProgressView()
        AlertUtil.showAlertNoticesWithActionOKClick(
            AlertTypeEnum.NOTICES,
            activity?.supportFragmentManager,
            message,
            successListener, buttonName
        )
    }

    fun showInfoAlertNotices(
        message: ResponseDescription?,
        buttonName: String
    ) {
        hideProgressView()
        AlertUtil.showAlertNoticesWithActionOKClick(
            AlertTypeEnum.NOTICES,
            activity?.supportFragmentManager,
            message,
            null, buttonName
        )
    }


    fun showToastAlertNotices(
        message: String, icon: Int,
        successListener: AlertUtil.OnSuccessListener?
    ) {
        hideProgressView()
        activity?.let { DialogAlertHelper.showProgress(it, icon, message, successListener) }
    }

    fun showSuccessAlertNoticesAndOK(
        message: ResponseDescription?,
        successListener: AlertUtil.OnSuccessListener?,
        buttonName: String
    ) {
        hideProgressView()
        AlertUtil.showAlertNoticesWithActionOKClick(
            AlertTypeEnum.SUCCESS,
            activity?.supportFragmentManager,
            message,
            successListener, buttonName
        )
    }

    fun showSuccessAlertNotices(
        message: ResponseDescription?,
        buttonName: String
    ) {
        hideProgressView()
        AlertUtil.showAlertNoticesWithActionOKClick(
            AlertTypeEnum.SUCCESS,
            activity?.supportFragmentManager,
            message,
            null, buttonName
        )
    }

    fun showErrorAlertNoticesAndOK(
        message: ResponseDescription?,
        successListener: AlertUtil.OnSuccessListener?,
        buttonName: String
    ) {
        hideProgressView()
        AlertUtil.showAlertNoticesWithActionOKClick(
            AlertTypeEnum.ERROR,
            activity?.supportFragmentManager,
            message,
            successListener, buttonName
        )
    }

    fun showErrorAlertNotices(
        message: ResponseDescription?,
        buttonName: String
    ) {
        hideProgressView()
        hideShimmerLoading()
        AlertUtil.showAlertNoticesWithActionOKClick(
            AlertTypeEnum.ERROR,
            activity?.supportFragmentManager,
            message,
            null, buttonName
        )
    }

    @SuppressLint("MissingSuperCall")
    override fun onDestroy() {
        (activity as AppCompatActivity).apply {
            super.onDestroy()
        }
    }

    fun EditText.placeCursorToEnd() {
        this.setSelection(this.text.length)
    }

    open fun onBackPressed(): Boolean {

        return false
    }


    fun replaceFragmentBackStack(containerId: Int, fragment: Fragment) {
        val backStateName: String = fragment.javaClass.name
        val manager: FragmentManager = parentFragmentManager
        manager.beginTransaction()
            .replace(containerId, fragment, backStateName)
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
            .addToBackStack(backStateName)
            .commit()
    }

    fun replaceFragment(resId: Int, fragment: Fragment) {
        val backStateName: String = fragment.javaClass.name
        val manager: FragmentManager = parentFragmentManager
        manager.beginTransaction()
            .replace(resId, fragment, backStateName)
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
            .commit()
    }

    fun replaceFragmentTabBackStack(fragment: Fragment) {

        val backStateName: String = fragment.javaClass.name
        val manager: FragmentManager = parentFragmentManager
        val fragmentPopped: Boolean = manager.popBackStackImmediate(backStateName, 0)
        if (currentContainerViewId == 0)
            return
        if (!fragmentPopped && manager.findFragmentByTag(backStateName) == null) {
            manager.beginTransaction()
                .replace(currentContainerViewId, fragment, backStateName)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .addToBackStack(backStateName)
                .commit()
        } else {

        }
    }

    fun replaceFragmentInsideFragment(resId: Int, fragment: Fragment) {
        val backStateName: String = fragment.javaClass.name
        val manager: FragmentManager = childFragmentManager
        manager.beginTransaction()
            .replace(resId, fragment, backStateName)
            .commit()
    }

    fun popBackIntent(intent: Intent) {
        activity?.supportFragmentManager?.popBackStack()
    }

    fun Context.convertDpToPx(dp: Float): Float {
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            dp,
            this.resources.displayMetrics
        )
    }


    fun showToastSuccessAlertNotices(
        message: String,
        successListener: AlertUtil.OnSuccessListener?
    ) {
        hideProgressView()
        context?.let {
            DialogAlertHelper.showProgress(
                it,
                R.drawable.ic_alert_success,
                message,
                successListener
            )
        }
    }
}