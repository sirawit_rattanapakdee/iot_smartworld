package ai.apdigital.tuya.base

import android.os.Parcel
import androidx.lifecycle.ViewModel
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable

open class BaseViewModel() : ViewModel() {
    private val disposables: CompositeDisposable = CompositeDisposable()

    constructor(parcel: Parcel) : this() {
    }

    fun addToDisposable(disposable: Disposable) {
        disposables.add(disposable)
    }

    override fun onCleared() {
        disposables.clear()
        super.onCleared()
    }
}