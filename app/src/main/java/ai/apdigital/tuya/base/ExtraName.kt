package ai.apdigital.tuya.base

class ExtraName {
    companion object {
        const val DEVICE = "DEVICE"
        const val DEVICE_ALL = "DEVICE_ALL"
        const val COMMAND = "COMMAND"
        const val HOME = "HOME"
        const val SCENE = "SCENE"
        const val HOME_ID = "HOME_ID"
        const val ROOM = "ROOM"
        const val ROOM_ID = "ROOM_ID"
        const val ROOM_ALL = "ROOM_ALL"
        const val INTENT_P2P_TYPE="P2P_TYPE"
    }
}