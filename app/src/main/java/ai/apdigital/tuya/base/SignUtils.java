
package ai.apdigital.tuya.base;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 * RSA 加解密工具类
 *
 * @author wusongsong
 * @date 2021/2/25
 */
public class SignUtils {
    /**
     * 云开发平台签名
     *
     * @param clientId
     * @param secret
     * @param t
     * @return
     */
    public static String tokenSign(String clientId, String secret, Long t) {
        return generateHashWithHmac256(clientId + t.toString(), secret).toUpperCase();//new HmacUtils(HmacAlgorithms.HMAC_SHA_256, secret).hmacHex(clientId + t.toString()).toUpperCase();
        //HmacUtils.hmacSha256Hex(, ).toUpperCase();

    }

    private static String generateHashWithHmac256(String message, String key) {
        try {
            final String hashingAlgorithm = "HmacSHA256"; //or "HmacSHA1", "HmacSHA512"

            byte[] bytes = hmac(hashingAlgorithm, key.getBytes(), message.getBytes());

            return bytesToHex(bytes);

            //Log.i(TAG, "message digest: " + messageDigest);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static byte[] hmac(String algorithm, byte[] key, byte[] message) throws NoSuchAlgorithmException, InvalidKeyException {
        Mac mac = Mac.getInstance(algorithm);
        mac.init(new SecretKeySpec(key, algorithm));
        return mac.doFinal(message);
    }

    public static String bytesToHex(byte[] bytes) {
        final char[] hexArray = "0123456789abcdef".toCharArray();
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0, v; j < bytes.length; j++) {
            v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    /**
     * 云智造签名
     *
     * @param clientId
     * @param secret
     * @param t
     * @return
     */
    public static String bizSign(String clientId, String accessToken, String secret, Long t) {
        return generateHashWithHmac256(clientId + accessToken + t.toString(), secret).toUpperCase();
    }
}
