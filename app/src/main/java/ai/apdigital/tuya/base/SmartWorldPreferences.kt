package ai.apdigital.tuya.base

import ai.apdigital.tuya.model.TokenEntity
import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import timber.log.Timber


val prefModule = module {
    single { SmartWorldPreferences(androidContext()) }
}

class SmartWorldPreferences constructor(var context: Context?) {
    companion object {
        const val USERNAME = "USER_NAME"
        const val PASSWORD = "PASSWORD"
        const val HOME_ID = "HOME_ID"
        const val HOME_NAME = "HOME_NAME"
        const val TOKEN = "TOKEN"
    }

    private val preferences: SharedPreferences? =
        context?.getSharedPreferences("ai.apdigital.smartworld.iot", Context.MODE_PRIVATE)

    init {

    }

    fun setToken(token: TokenEntity?) {
        preferences?.edit()?.putString(TOKEN, Gson().toJson(token))?.apply()
    }

    fun getToken(): TokenEntity? {
        return Gson().fromJson(preferences?.getString(TOKEN, ""), TokenEntity::class.java)
    }


    fun setPasswordLock(key: String, value: String) {
        preferences?.edit()?.putString(key, value)?.apply()
    }

    fun getPasswordLock(key: String): String {
        return preferences?.getString(key, "") ?: ""
    }

    fun setUserName(userName: String?) {
        preferences?.edit()?.putString(USERNAME, userName)?.apply()
    }

    fun setPassword(password: String?) {
        preferences?.edit()?.putString(PASSWORD, password)?.apply()
    }

    fun setHomeID(homeID: String?) {
        preferences?.edit()?.putString(HOME_ID, homeID)?.apply()
    }

    fun setHomeName(homeName: String?) {
        preferences?.edit()?.putString(HOME_NAME, homeName)?.apply()
    }

    fun getUserName(): String? {
        return preferences?.getString(USERNAME, "")
    }

    fun getPassword(): String? {
        return preferences?.getString(PASSWORD, "")
    }

    fun getHomeID(): String? {
        return preferences?.getString(HOME_ID, "")
    }

    fun getHomeName(): String? {
        return preferences?.getString(HOME_NAME, "")
    }
}