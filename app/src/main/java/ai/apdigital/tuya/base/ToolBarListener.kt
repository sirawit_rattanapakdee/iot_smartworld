package ai.apdigital.tuya.base

interface ToolBarListener {
    fun onSearchClickListener()
    fun onBackClickListener()
}