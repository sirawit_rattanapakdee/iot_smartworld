package ai.apdigital.tuya.di

import ai.apdigital.tuya.BuildConfig
import ai.apdigital.tuya.base.SmartWorldPreferences
import ai.apdigital.tuya.networking.AuthInterceptor
import ai.apdigital.tuya.networking.ResponseHandler
import ai.apdigital.tuya.repository.TuyaDoorLockRepository
import ai.apdigital.tuya.repository.TuyaIRRepository
import android.content.Context

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.readystatesoftware.chuck.ChuckInterceptor
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val networkModule = module {
    factory { AuthInterceptor() }
    single { provideGSON() }
    factory { httpBuilder(get()) }
    factory { provideLoggingInterceptor() }
    factory { provideUserApi(get()) }
    factory { provideDoorLockApi(get()) }
    factory { provideRemoteApi(get()) }

    factory { ResponseHandler() }
}

fun provideGSON(): Gson {
    val builder = GsonBuilder()
    return builder.create()
}


fun httpBuilder(context: Context): OkHttpClient.Builder {
    val interceptor = HttpLoggingInterceptor()
    interceptor.level = if (BuildConfig.DEBUG) {
        HttpLoggingInterceptor.Level.BODY
    } else {
        HttpLoggingInterceptor.Level.NONE
    }
    val auth = SmartWorldPreferences(context)
    var accessToken = ""
    val language = ""
    auth.let { accessToken = it.getToken()?.access_token ?: "" }

    val timeStamp = System.currentTimeMillis()
    val message = "rsqhsxvaalfaqz2nualb"

    /*let hash = createSignature ()try ? HMAC(key: Array(secret.utf8), variant:.sha256)
        .authenticate(Array(message.utf8))
            .toHexString().uppercased()*/

    val httpClient = OkHttpClient.Builder()
    httpClient.connectTimeout(15, TimeUnit.SECONDS)
    httpClient.writeTimeout(15, TimeUnit.SECONDS)
    httpClient.readTimeout(15, TimeUnit.SECONDS)
    httpClient.addInterceptor { chain: Interceptor.Chain ->
        val original = chain.request()
        val request = original.newBuilder()
            .addHeader("client_id", "rsqhsxvaalfaqz2nualb")
            .addHeader("sign_method", "HMAC-SHA256")
            .method(original.method, original.body).build()
        chain.proceed(request)
    }
    val chuck = ChuckInterceptor(context)
    chuck.showNotification(true)
    httpClient.addInterceptor(chuck)
    httpClient.addInterceptor(interceptor)
    return httpClient
}

fun provideUserApi(httpClient: OkHttpClient.Builder): TuyaIRRepository.IRAuthApi {
    return Retrofit.Builder()
        .baseUrl("https://openapi.tuyaus.com/")
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
        .client(httpClient.build())
        .build().create(TuyaIRRepository.IRAuthApi::class.java)
}

fun provideDoorLockApi(httpClient: OkHttpClient.Builder): TuyaDoorLockRepository.DoorLockApi {
    return Retrofit.Builder()
        .baseUrl("https://openapi.tuyaus.com/")
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
        .client(httpClient.build())
        .build().create(TuyaDoorLockRepository.DoorLockApi::class.java)
}

fun provideRemoteApi(context: Context): TuyaIRRepository.IRApi {
    val interceptor = HttpLoggingInterceptor()
    interceptor.level = if (BuildConfig.DEBUG) {
        HttpLoggingInterceptor.Level.BODY
    } else {
        HttpLoggingInterceptor.Level.NONE
    }
    val auth = SmartWorldPreferences(context)
    var accessToken = ""
    val language = ""
    auth.let { accessToken = it.getToken()?.access_token ?: "" }

    val timeStamp = System.currentTimeMillis()
    val message = "rsqhsxvaalfaqz2nualb"

    /*let hash = createSignature ()try ? HMAC(key: Array(secret.utf8), variant:.sha256)
        .authenticate(Array(message.utf8))
            .toHexString().uppercased()*/

    val httpClient = OkHttpClient.Builder()
    httpClient.connectTimeout(15, TimeUnit.SECONDS)
    httpClient.writeTimeout(15, TimeUnit.SECONDS)
    httpClient.readTimeout(15, TimeUnit.SECONDS)
    httpClient.addInterceptor { chain: Interceptor.Chain ->
        val original = chain.request()
        val request = original.newBuilder()
            .addHeader("client_id", "rsqhsxvaalfaqz2nualb")
            .addHeader("sign_method", "HMAC-SHA256")

            .method(original.method, original.body).build()
        chain.proceed(request)
    }
    val chuck = ChuckInterceptor(context)
    chuck.showNotification(true)
    httpClient.addInterceptor(chuck)
    httpClient.addInterceptor(interceptor)
    return Retrofit.Builder()
        .baseUrl("https://openapi.tuyaus.com/")
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
        .client(httpClient.build())
        .build().create(TuyaIRRepository.IRApi::class.java)
}

/*

fun provideDeviceApi(httpClient: OkHttpClient): DeviceRepository.DeviceApi {
    return Retrofit.Builder()
        .baseUrl(BuildConfig.BASE_APP_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
        .client(httpClient)
        .build().create(DeviceRepository.DeviceApi::class.java)
}

fun provideFamilyApi(httpClient: OkHttpClient): FamilyRepository.FamilyApi {
    return Retrofit.Builder()
        .baseUrl(BuildConfig.BASE_APP_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
        .client(httpClient)
        .build().create(FamilyRepository.FamilyApi::class.java)
}

fun provideVerifyCodeApi(httpClient: OkHttpClient): VerifyCodeRepository.VerifyCodeApi {
    return Retrofit.Builder()
        .baseUrl(BuildConfig.BASE_APP_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
        .client(httpClient)
        .build().create(VerifyCodeRepository.VerifyCodeApi::class.java)
}

fun provideMessageApi(httpClient: OkHttpClient): MessageRepository.MessageApi {
    return Retrofit.Builder()
        .baseUrl(BuildConfig.BASE_APP_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
        .client(httpClient)
        .build().create(MessageRepository.MessageApi::class.java)
}

fun provideSchedulerApi(httpClient: OkHttpClient): SchedulerRepository.SchedulerApi {
    return Retrofit.Builder()
        .baseUrl(BuildConfig.BASE_APP_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
        .client(httpClient)
        .build().create(SchedulerRepository.SchedulerApi::class.java)
}

fun provideHealthApi(httpClient: OkHttpClient): HealthRepository.HealthApi {
    return Retrofit.Builder()
        .baseUrl(BuildConfig.BASE_APP_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
        .client(httpClient)
        .build().create(HealthRepository.HealthApi::class.java)
}

fun provideVideoCallApi(httpClient: OkHttpClient): VideoCallRepository.VideoCallApi {
    return Retrofit.Builder()
        .baseUrl(BuildConfig.BASE_APP_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
        .client(httpClient)
        .build().create(VideoCallRepository.VideoCallApi::class.java)
}*/

fun provideLoggingInterceptor(): HttpLoggingInterceptor {
    val logger = HttpLoggingInterceptor()
    logger.level =
        if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BASIC else HttpLoggingInterceptor.Level.NONE
    return logger
}


