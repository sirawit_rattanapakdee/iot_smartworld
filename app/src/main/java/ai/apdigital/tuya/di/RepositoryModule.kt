package ai.apdigital.tuya.di

import ai.apdigital.tuya.repository.TuyaDoorLockRepository
import ai.apdigital.tuya.repository.TuyaIRRepository
import org.koin.dsl.module


val repositoryModule = module {
    factory { TuyaIRRepository(get(), get(), get()) }
    factory { TuyaDoorLockRepository(get(), get()) }
    /*  factory { DeviceRepository(get(), get()) }
      factory { FamilyRepository(get(), get()) }
      factory { VerifyCodeRepository(get(), get()) }
      factory { MessageRepository(get(), get(), get()) }
      factory { SchedulerRepository(get(), get()) }
      factory { HealthRepository(get(), get()) }
      factory { VideoCallRepository(get(), get()) }*/
}
