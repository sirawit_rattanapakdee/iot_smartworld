package ai.apdigital.tuya.di


import ai.apdigital.tuya.ui.viewmodel.*
import org.koin.dsl.module

val viewModelModule = module {
    factory { TuyaViewModel(get(), get()) }
    factory { TuyaDoorLockViewModel(get(), get(),get(),get()) }
    factory { TuyaIRViewModel(get(), get(), get()) }
    factory { NotificationViewModel(get()) }


//    factory { DeviceViewModel(get(), get(), get()) }
//    factory { FamilyViewModel(get(), get(), get()) }
//    factory { VerifyCodeViewModel(get(), get(), get()) }
//    factory { InboxViewModel(get(), get(), get()) }
//    factory { MessageViewModel(get(), get()) }
//    factory { SchedulerViewModel(get(), get(), get()) }
//    factory { HealthViewModel(get(), get(), get()) }
//    factory { VideoCallViewModel(get(), get(), get()) }
}