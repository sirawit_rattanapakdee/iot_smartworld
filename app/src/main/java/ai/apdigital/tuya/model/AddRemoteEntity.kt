package ai.apdigital.tuya.model

data class AddRemoteEntity(
    var remote_id: String? = ""
)