package ai.apdigital.tuya.model

data class AirStatusEntity(
    var temp: String? = "25",
    var mode: String? = "0",
    var remote_id: String? = "",
    var wind: String? = "0",
    var power: String? = "0",

    )