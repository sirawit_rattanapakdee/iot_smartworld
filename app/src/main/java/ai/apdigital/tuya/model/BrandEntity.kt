package ai.apdigital.tuya.model

data class BrandEntity(
    var brand_id: String? = "",
    var brand_name: String? = ""
)
