package ai.apdigital.tuya.model

data class CategoryEntity(
    var category_id: String? = "",
    var category_name: String? = ""
)
