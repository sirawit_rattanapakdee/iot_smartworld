package ai.apdigital.tuya.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RemoteDeviceEntity(
    var area_id: String? = "",
    var brand_id: String? = "",
    var brand_name: String? = "",
    var category_id: String? = "",
    var iptv_type: Int = 0,
    var operator_id: String? = "",
    var remote_id: String? = "",
    var remote_index: String? = "",
    var remote_name: String? = "",
    var t: Long = 0
):Parcelable
