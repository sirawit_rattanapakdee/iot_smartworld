package ai.apdigital.tuya.model

data class TicketEntity(
    var expire_time: Int = 0,
    var ticket_id: String? = "",
    var ticket_key: String? = ""
)
