package ai.apdigital.tuya.model

data class TokenEntity(
    var uid: String? = "",
    var access_token: String? = "",
    var refresh_token: String? = "",
    var expire_time: Int? = 0,
    var expire_at: Long = 0L
)
