package ai.apdigital.tuya.model.temp

import com.google.gson.annotations.SerializedName

data class TemporaryPassword(
    val delivery_status: Int,
    val effective_time: Long,
    val invalid_time: Long,
    val name: String,
    @SerializedName("id")
    val password_id: Int,
    val phase: Int,
    val phone: String,
    val time_zone: String
)