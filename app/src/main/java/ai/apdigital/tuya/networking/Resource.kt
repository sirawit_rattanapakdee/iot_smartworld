package ai.apdigital.tuya.networking

data class Resource<out T>(val status: Status, val data: T?, val message: ResponseDescription?) {
    companion object {
        fun <T> success(data: T?): Resource<T> {
            return Resource(Status.SUCCESS, data, null)
        }

        fun <T> success(code: Int, msg: String, data: T?): Resource<T> {
            return Resource(Status.SUCCESS, data, ResponseDescription(message = msg, code = code))
        }

        fun <T> error(msg: ResponseDescription, data: T?): Resource<T> {
            return Resource(Status.ERROR, data, msg)
        }

        fun <T> error(code: Int?, msg: String?, data: T?): Resource<T> {
            return Resource(Status.ERROR, data, ResponseDescription(message = msg, code = code))
        }

        fun <T> loading(data: T?): Resource<T> {
            return Resource(Status.LOADING, data, null)
        }
    }
}
