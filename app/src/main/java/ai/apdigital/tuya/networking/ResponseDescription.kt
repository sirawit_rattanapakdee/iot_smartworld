package ai.apdigital.tuya.networking

import com.google.gson.annotations.SerializedName

data class ResponseDescription(
    @SerializedName("status") var code: Int? = 0,
    /*@SerializedName("isShow") var isShow: Boolean = false,*/
    @SerializedName("title") var title: String? = "",
    @SerializedName("message") var message: String? = "",
    @SerializedName("type") var type: String? = ""/*,
    @SerializedName("extensions") var extensions: JsonObject? = null*/
)
/*{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.4","title":"Not Found","status":404,"extensions":{"traceId":"|e031d71b-4a33e5cd2e643e7b."}}*/
