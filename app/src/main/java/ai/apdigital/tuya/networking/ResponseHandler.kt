package ai.apdigital.tuya.networking

import com.google.gson.Gson
import retrofit2.HttpException
import retrofit2.Response
import timber.log.Timber
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

open class ResponseHandler {

    fun <T : Any> handleResponse(response: Response<ResponseParser<T>>?): Resource<T> {
        response as Response<ResponseParser<T>>
        if (response.isSuccessful && response.code() == 200) {
            val content = response.body()
            val responseHandler: Response<T>? = Response.success(content?.data)
            content?.let {
                return if (it.success) {
                    Resource.success(
                        1,
                        content.message,
                        responseHandler?.body()
                    )

                } else {
                    Resource.error(
                        0,
                        content.message, null
                    )
                }
            }
        }
        val content = Gson().fromJson(
            response.errorBody()?.string(),
            ResponseStatus::class.java
        )
        return Resource.error(
            0,
            content.message, null
        )
    }

    fun <T : Any> handle(content: ResponseParser<T?>): Resource<T> {
        if (content.code == 1) {
            val responseHandler: Response<T>? = Response.success(content.data)
            return if (responseHandler?.isSuccessful == true) {
                Resource.success(
                    1,
                    content.message,
                    responseHandler.body()
                )
            } else {
                Resource.error(
                    0,
                    content.message, null
                )
            }
        } else {
            return Resource.error(
                0,
                content.message, null
            )
        }
    }

    fun <T : Any> handleSuccess(content: T?): Resource<T> {
        return Resource.success(
            1,
            "success",
            content
        )
    }


    fun <T : Any> handleError(code: String?, message: String?): Resource<T> {

        Timber.e("Code : $code, message : $message")
        return Resource.error(
            0,
            message,
            null
        )
    }

    /* fun <T : Any> handle(content: T): Resource<T> {
         content as ResponseStatus
         if (content.code == 0) {
             val responseHandler: Response<T>? = Response.success(content)
             return if (responseHandler?.isSuccessful == true) {
                 Resource.success(
                     content.code,
                     content.message,
                     responseHandler.body()
                 )
             } else {
                 Resource.error(
                     content.code,
                     content.message, null
                 )
             }
         } else {
             return Resource.error(
                 content.code,
                 content.message, null
             )
         }
     }
 */
    fun <T : Any> responseError(data: Response<T>): Resource<T> {
        Gson().fromJson(
            data.errorBody()?.string() ?: data.message(),
            ResponseDescription::class.java
        )?.let {
            return Resource.error(it, null)
        }
        return Resource.error(
            ResponseDescription(
                data.code(),
                data.errorBody()?.string() ?: data.message()
            ), null
        )
    }

    fun <T : Any> handleException(e: Throwable): Resource<T> {
        return when (e) {
            is ConnectException -> Resource.error(
                getErrorMessage(100, e.message ?: "ConnectException"),
                null
            )
            is UnknownHostException -> Resource.error(
                getErrorMessage(
                    101, e.message ?: "UnknownHostException"
                ), null
            )
            is HttpException -> Resource.error(
                getErrorMessage(
                    e.code(), e.message
                        ?: "HttpException"
                ), null
            )
            is SocketTimeoutException -> Resource.error(
                getErrorMessage(ErrorCodes.SocketTimeOut.code, "Request timeout"),
                null
            )
            else -> Resource.error(getErrorMessage(Int.MAX_VALUE, "Something wrong!"), null)
        }
    }


    private fun getErrorMessage(code: Int, message: String): ResponseDescription {
        return ResponseDescription(code = code, title = message)
    }

    enum class ErrorCodes(val code: Int) {
        SocketTimeOut(
            -1
        )
    }
}
