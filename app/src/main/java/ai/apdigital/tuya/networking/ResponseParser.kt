package ai.apdigital.tuya.networking

import com.google.gson.annotations.SerializedName


data class ResponseParser<T>(
    @SerializedName("success")
    var success: Boolean = false,
    @SerializedName("msg")
    var message: String = "",
    @SerializedName("code")
    var code: Int = 0,
    @SerializedName("result")
    var data: T? = null
)