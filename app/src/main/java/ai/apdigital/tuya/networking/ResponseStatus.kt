package ai.apdigital.tuya.networking

import com.google.gson.annotations.SerializedName


data class ResponseStatus(

        @SerializedName("success")
        var success: Boolean = false,
        @SerializedName("msg")
        var message: String = "",

        )