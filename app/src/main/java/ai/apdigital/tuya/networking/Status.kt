package ai.apdigital.tuya.networking

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}