package ai.apdigital.tuya.repository

import ai.apdigital.tuya.model.DynamicPasswordEntity
import ai.apdigital.tuya.model.TicketEntity
import ai.apdigital.tuya.model.temp.TemporaryPassword
import ai.apdigital.tuya.networking.Resource
import ai.apdigital.tuya.networking.ResponseHandler
import ai.apdigital.tuya.networking.ResponseParser
import ai.apdigital.tuya.repository.request.CreateTemporaryPasswordRequest
import ai.apdigital.tuya.repository.request.ZigbeeUnlock
import ai.apdigital.tuya.ui.control.smart_lock.model.TemporaryPasswordEntity
import com.tuya.smart.optimus.lock.api.bean.TempPassword
import io.reactivex.rxjava3.core.Observable
import retrofit2.Response
import retrofit2.http.*

class TuyaDoorLockRepository(
    private val tuyaAPI: DoorLockApi,
    private val responseHandler: ResponseHandler
) {

    interface DoorLockApi {
        @POST("v1.0/devices/{device_id}/door-lock/open-door")
        fun callOpenDoor(
            @Header("access_token") access_token: String,
            @Header("sign") sign: String,
            @Header("t") t: Long,
            @Path("device_id") device_id: String?,
            @Body data: ZigbeeUnlock
        ): Observable<Response<ResponseParser<Boolean>>>

        @POST("v1.0/devices/{device_id}/door-lock/password-ticket")
        fun callTicket(
            @Header("access_token") access_token: String,
            @Header("sign") sign: String,
            @Header("t") t: Long,
            @Path("device_id") device_id: String?
        ): Observable<Response<ResponseParser<TicketEntity>>>

        @GET("v1.0/devices/{device_id}/door-lock/dynamic-password")
        fun callDynamicDoorPassword(
            @Header("access_token") access_token: String,
            @Header("sign") sign: String,
            @Header("t") t: Long,
            @Path("device_id") device_id: String?
        ): Observable<Response<ResponseParser<DynamicPasswordEntity>>>

        @POST("v1.0/devices/{device_id}/door-lock/temp-password")
        fun callCreateTemporaryPassword(
            @Header("access_token") access_token: String,
            @Header("sign") sign: String,
            @Header("t") t: Long,
            @Path("device_id") device_id: String?, @Body data: CreateTemporaryPasswordRequest
        ): Observable<Response<ResponseParser<TemporaryPassword>>>


        @GET("/v1.0/devices/{device_id}/door-lock/temp-passwords?valid=true")
        fun callGetTemporaryPassword(
            @Header("access_token") access_token: String,
            @Header("sign") sign: String,
            @Header("t") t: Long,
            @Path("device_id") device_id: String?
        ): Observable<Response<ResponseParser<MutableList<TemporaryPassword>>>>


        @DELETE("v1.0/devices/{device_id}/door-lock/temp-passwords/{password_id}")
        fun callDeleteTemporaryPassword(
            @Header("access_token") access_token: String,
            @Header("sign") sign: String,
            @Header("t") t: Long,
            @Path("device_id") device_id: String?,
            @Path("password_id") password_id: Int?
        ): Observable<Response<ResponseParser<Boolean>>>
    }

    fun unlockDoor(
        access_token: String,
        sign: String, t: Long,
        device_id: String?, data: ZigbeeUnlock
    ): Observable<Resource<Boolean>> =
        tuyaAPI.callOpenDoor(access_token, sign, t, device_id, data).map {
            responseHandler.handleResponse(it)
        }

    fun getTicket(
        access_token: String,
        sign: String, t: Long,
        devId: String,
    ): Observable<Resource<TicketEntity>> =
        tuyaAPI.callTicket(access_token, sign, t, devId).map {
            responseHandler.handleResponse(it)
        }

    fun dynamicPassword(
        access_token: String,
        sign: String, t: Long,
        devId: String?
    ): Observable<Resource<DynamicPasswordEntity>> =
        tuyaAPI.callDynamicDoorPassword(access_token, sign, t, devId).map {
            responseHandler.handleResponse(it)
        }

    fun createTemporaryPassword(
        access_token: String,
        sign: String, t: Long,
        devId: String?, temporaryPasswordRequest: CreateTemporaryPasswordRequest
    ): Observable<Resource<TemporaryPassword>> =
        tuyaAPI.callCreateTemporaryPassword(access_token, sign, t, devId, temporaryPasswordRequest)
            .map {
                responseHandler.handleResponse(it)
            }


    fun getTemporaryPassword(
        access_token: String,
        sign: String, t: Long,
        devId: String?
    ): Observable<Resource<MutableList<TemporaryPassword>>> =
        tuyaAPI.callGetTemporaryPassword(access_token, sign, t, devId)
            .map {
                responseHandler.handleResponse(it)
            }


    fun deleteTemporaryPassword(
        access_token: String,
        sign: String, t: Long,
        devId: String?, password_id: Int?
    ): Observable<Resource<Boolean>> =
        tuyaAPI.callDeleteTemporaryPassword(access_token, sign, t, devId, password_id)
            .map {
                responseHandler.handleResponse(it)
            }
}


/*

*** Note: To get hash for auth ***

let timestamp = String(Date().millisecondsSince1970)
let message = clientId + timestamp
let hash = try? HMAC(key: Array(secret.utf8), variant: .sha256)
.authenticate(Array(message.utf8))
.toHexString().uppercased()

>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

1. Get Access Token
METHOD: GET "https://openapi.tuyaus.com/v1.0/token"
headers: [
"client_id": clientId,
"sign": hash,
"t": timestamp,
"sign_method": "HMAC-SHA256"
]

2. Get IR Brands
METHOD: GET "https://openapi.tuyaus.com/v1.0/infrareds/{infraredId}/categories/{categoryValue}/brands"
headers: [
"client_id": clientId,
"sign": hash,
"t": timestamp,
"sign_method": "HMAC-SHA256",
"access_token": accessToken
]

3. Get Added IR Remotes
METHOD: GET "https://openapi.tuyaus.com/v1.0/infrareds/{infraredId}/remotes"
headers: [
"client_id": clientId,
"sign": hash,
"t": timestamp,
"sign_method": "HMAC-SHA256",
"access_token": accessToken
]

4. Get Remote Indexes (ไว้ใช้ดึง Remote ทั้งหมด ใช้ในหน้า Matching หารีโมตที่ทำงานได้จริง ทั้ง Air และ TV)
METHOD: GET "https://openapi.tuyaus.com/v1.0/infrareds/{infraredId}/categories/{category}/brands/{brandId}"
headers: [
"client_id": clientId,
"sign": hash,
"t": timestamp,
"sign_method": "HMAC-SHA256",
"access_token": accessToken
]

5. Test AirConditioner Commands (ไว้ยิงเทสปุ่มกด Air แต่ละปุ่ม)
METHOD: POST "https://openapi.tuyaus.com/v1.0/infrareds/{infraredId}/air-conditioners/testing/command"
headers: [
"client_id": clientId,
"sign": hash,
"t": timestamp,
"sign_method": "HMAC-SHA256",
"access_token": accessToken
]
params_Body: [
"infrared_id": infraredId,
"remote_index": remoteIndex,
"category_id": "5",
"code": code,
"value": value
]

6. Add IR Remote
METHOD: POST "https://openapi.tuyaus.com/v1.0/infrareds/{infraredId}/normal/add-remote"
headers: [
"client_id": clientId,
"sign": hash,
"t": timestamp,
"sign_method": "HMAC-SHA256",
"access_token": accessToken
]
params_Body: [
"category_id": category == .airConditioner ? "5" : "2",
"brand_id": brandId,
"brand_name": brandName,
"remote_index": remoteIndex,
"remote_name": remoteName
]

7. Get Air Status to show Current Status
METHOD: GET "https://openapi.tuyaus.com/v1.0/infrareds/{infraredId}/remotes/{remoteId}/ac/status"
headers: [
"client_id": clientId,
"sign": hash,
"t": timestamp,
"sign_method": "HMAC-SHA256",
"access_token": accessToken
]

8. Control Air With Multiple Commands
METHOD: POST "https://openapi.tuyaus.com/v1.0/infrareds/{infraredId}/air-conditioners/{remoteId}/scenes/command"
headers: [
"client_id": clientId,
"sign": hash,
"t": timestamp,
"sign_method": "HMAC-SHA256",
"access_token": accessToken
]
params_Body: [
"power": codePower,
"mode": codeMode,
"temp": codeTemp,
"wind": codeWind
]

9. Test / Check TV Commands
METHOD: POST "https://openapi.tuyaus.com/v1.0/infrareds/{infraredId}/testing/command"
headers: [
"client_id": clientId,
"sign": hash,
"t": timestamp,
"sign_method": "HMAC-SHA256",
"access_token": accessToken
]
params_Body: [
"remote_index": remoteIndex,
"category_id": "2",
"key": code,
]

10. Control TV With Command
METHOD: POST "https://openapi.tuyaus.com/v1.0/infrareds/{infraredId}/remotes/{remoteId}/command"
headers: [
"client_id": clientId,
"sign": hash,
"t": timestamp,
"sign_method": "HMAC-SHA256",
"access_token": accessToken
]
params_Body: [
"key": code
]*/
