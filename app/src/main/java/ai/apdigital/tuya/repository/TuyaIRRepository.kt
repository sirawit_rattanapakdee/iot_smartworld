package ai.apdigital.tuya.repository

import ai.apdigital.tuya.model.*
import ai.apdigital.tuya.networking.Resource
import ai.apdigital.tuya.networking.ResponseHandler
import ai.apdigital.tuya.networking.ResponseParser
import ai.apdigital.tuya.repository.request.*
import io.reactivex.rxjava3.core.Observable
import retrofit2.Response
import retrofit2.http.*

class TuyaIRRepository(
    private val tuyaAuthAPI: IRAuthApi,
    private val tuyaAPI: IRApi,
    private val responseHandler: ResponseHandler
) {


    interface IRAuthApi {
        @GET("v1.0/token/{refresh_token}")
        fun callRefreshAccessToken(
            @Path("refresh_token") refresh_token: String,
            @Header("sign") sign: String,
            @Header("t") t: Long,
        ): Observable<Response<ResponseParser<TokenEntity>>>?

        @GET("v1.0/token?grant_type=1")
        fun callGetAccessToken(
            @Header("access_token") access_token: String,
            @Header("sign") sign: String,
            @Header("t") t: Long,
        ): Observable<Response<ResponseParser<TokenEntity>>>?
    }

    interface IRApi {
        @GET("v1.0/infrareds/{infrared_id}/categories")
        fun callCategories(
            @Header("access_token") access_token: String,
            @Header("sign") sign: String,
            @Header("t") t: Long,
            @Path("infrared_id") infraredId: String
        ): Observable<Response<ResponseParser<MutableList<CategoryEntity>>>>?

        @GET("v1.0/infrareds/{infrared_id}/remotes")
        fun callRemotes(
            @Header("access_token") access_token: String,
            @Header("sign") sign: String,
            @Header("t") t: Long,
            @Path("infrared_id") infraredId: String
        ): Observable<Response<ResponseParser<MutableList<RemoteDeviceEntity>>>>?

        @GET("v1.0/infrareds/{infrared_id}/categories/{categoryValue}/brands")
        fun callBrands(
            @Header("access_token") access_token: String,
            @Header("sign") sign: String,
            @Header("t") t: Long,
            @Path("infrared_id") infraredId: String,
            @Path("categoryValue") categoryValue: String
        ): Observable<Response<ResponseParser<MutableList<BrandEntity>>>>?

        @GET("v1.0/infrareds/{infrared_id}/categories/{category_id}/brands/{brand_id}")
        fun callGetRemoteIndexes(
            @Header("access_token") access_token: String,
            @Header("sign") sign: String,
            @Header("t") t: Long,
            @Path("infrared_id") infraredId: String,
            @Path("category_id") category_id: String,
            @Path("brand_id") brand_id: String,
        ): Observable<Response<ResponseParser<MutableList<RemoteIndexEntity>>>>?

        @POST("v1.0/infrareds/{infrared_id}/air-conditioners/testing/command")
        fun callAirConditionersTesting(
            @Header("access_token") access_token: String,
            @Header("sign") sign: String,
            @Header("t") t: Long,
            @Path("infrared_id") infraredId: String,
            @Body data: AirTestRequest
        ): Observable<Response<ResponseParser<TokenEntity>>>?

        @POST("v1.0/infrareds/{infrared_id}/normal/add-remote")
        fun callAddRemote(
            @Header("access_token") access_token: String,
            @Header("sign") sign: String,
            @Header("t") t: Long,
            @Path("infrared_id") infraredId: String,
            @Body data: AddRemoteRequest
        ): Observable<Response<ResponseParser<AddRemoteEntity>>>?

        @GET("v1.0/infrareds/{infrared_id}/remotes/{remote_id}/ac/status")
        fun callAirStatus(
            @Header("access_token") access_token: String,
            @Header("sign") sign: String,
            @Header("t") t: Long,
            @Path("infrared_id") infraredId: String,
            @Path("remote_id") remoteId: String,
        ): Observable<Response<ResponseParser<AirStatusEntity>>>?

        @POST("v1.0/infrareds/{infrared_id}/air-conditioners/{remote_id}/scenes/command")
        fun callAirConditionersCommand(
            @Header("access_token") access_token: String,
            @Header("sign") sign: String,
            @Header("t") t: Long,
            @Path("infrared_id") infraredId: String,
            @Path("remote_id") remoteId: String,
            @Body data: AirSendCommandRequest
        ): Observable<Response<ResponseParser<Boolean>>>?

        @POST("v1.0/infrareds/{infrared_id}/air-conditioners/{remote_id}/command")
        fun callAirConditionerCommand(
            @Header("access_token") access_token: String,
            @Header("sign") sign: String,
            @Header("t") t: Long,
            @Path("infrared_id") infraredId: String,
            @Path("remote_id") remoteId: String,
            @Body data: AirCommandRequest
        ): Observable<Response<ResponseParser<Boolean>>>?

        @POST("v1.0/infrareds/{infrared_id}/testing/command")
        fun callRemoteTesting(
            @Header("access_token") access_token: String,
            @Header("sign") sign: String,
            @Header("t") t: Long,
            @Path("infrared_id") infraredId: String,
            @Body data: TVTestRequest
        ): Observable<Response<ResponseParser<TokenEntity>>>?

        @POST("v1.0/infrareds/{infrared_id}/remotes/{remote_id}/command")
        fun callRemoteCommand(
            @Header("access_token") access_token: String,
            @Header("sign") sign: String,
            @Header("t") t: Long,
            @Path("infrared_id") infraredId: String,
            @Path("remote_id") remoteId: String,
            @Body data: TVSendCommandRequest
        ): Observable<Response<ResponseParser<Boolean>>>?

        @DELETE("v1.0/infrareds/{infrared_id}/remotes/{remote_id}")
        fun callDeleteRemote(
            @Header("access_token") access_token: String,
            @Header("sign") sign: String,
            @Header("t") t: Long,
            @Path("infrared_id") infraredId: String?,
            @Path("remote_id") remoteId: String?
        ): Observable<Response<ResponseParser<Boolean>>>?

        @FormUrlEncoded
        @PUT("v1.0/infrareds/{infrared_id}")
        fun callRenameRemote(
            @Header("access_token") access_token: String,
            @Header("sign") sign: String,
            @Header("t") t: Long,
            @Path("infrared_id") infraredId: String?,
            @Field("remote_id") remote_id: String?,
            @Field("remote_name") remote_name: String
        ): Observable<Response<ResponseParser<Boolean>>>?

        @PUT("v1.0/devices/{device_id}/functions/{function_code}")
        fun callRenameSwitch(
            @Header("access_token") access_token: String,
            @Header("sign") sign: String,
            @Header("t") t: Long,
            @Path("device_id") device_id: String?,
            @Path("function_code") function_code: String?,
            @Body name: RenameSwitchRequest
        ): Observable<Response<ResponseParser<Boolean>>>?
    }

    fun getToken(
        access_token: String,
        sign: String, t: Long
    ): Observable<Resource<TokenEntity>>? =
        tuyaAuthAPI.callGetAccessToken(access_token, sign, t)?.map {
            responseHandler.handleResponse(it)
        }

    fun getRefreshToken(
        refresh_token: String,
        sign: String, t: Long
    ): Observable<Resource<TokenEntity>>? =
        tuyaAuthAPI.callRefreshAccessToken(refresh_token, sign, t)?.map {
            responseHandler.handleResponse(it)
        }

    fun getRemote(
        access_token: String,
        sign: String, t: Long, infrared_id: String
    ): Observable<Resource<MutableList<RemoteDeviceEntity>>>? =
        tuyaAPI.callRemotes(access_token, sign, t, infrared_id)?.map {
            responseHandler.handleResponse(it)
        }

    fun getCategories(
        access_token: String,
        sign: String, t: Long,
        infrared_id: String
    ): Observable<Resource<MutableList<CategoryEntity>>>? =
        tuyaAPI.callCategories(access_token, sign, t, infrared_id)?.map {
            responseHandler.handleResponse(it)
        }

    fun getBrands(
        access_token: String,
        sign: String, t: Long, infrared_id: String, categoryId: String
    ): Observable<Resource<MutableList<BrandEntity>>>? =
        tuyaAPI.callBrands(access_token, sign, t, infrared_id, categoryId)?.map {
            responseHandler.handleResponse(it)
        }

    fun getRemoteIndexes(
        access_token: String,
        sign: String, t: Long,
        devId: String,
        categoryId: String,
        brandId: String
    ): Observable<Resource<MutableList<RemoteIndexEntity>>>? =
        tuyaAPI.callGetRemoteIndexes(access_token, sign, t, devId, categoryId, brandId)?.map {
            responseHandler.handleResponse(it)
        }

    fun testSendAirCommand(
        access_token: String,
        sign: String, t: Long,
        devId: String,
        data: AirTestRequest
    ): Observable<Resource<TokenEntity>>? =
        tuyaAPI.callAirConditionersTesting(access_token, sign, t, devId, data)?.map {
            responseHandler.handleResponse(it)
        }


    fun testSendRemoteCommand(
        access_token: String,
        sign: String, t: Long,
        devId: String,
        data: TVTestRequest
    ): Observable<Resource<TokenEntity>>? =
        tuyaAPI.callRemoteTesting(access_token, sign, t, devId, data)?.map {
            responseHandler.handleResponse(it)
        }

    fun getAirStatus(
        access_token: String,
        sign: String, t: Long,
        devId: String,
        remoteId: String
    ): Observable<Resource<AirStatusEntity>>? =
        tuyaAPI.callAirStatus(access_token, sign, t, devId, remoteId)?.map {
            responseHandler.handleResponse(it)
        }

    fun sendRemoteCommand(
        access_token: String,
        sign: String, t: Long,
        devId: String,
        remoteId: String,
        data: TVSendCommandRequest
    ): Observable<Resource<Boolean>>? =
        tuyaAPI.callRemoteCommand(access_token, sign, t, devId, remoteId, data)?.map {
            responseHandler.handleResponse(it)
        }

    fun sendAirCommand(
        access_token: String,
        sign: String, t: Long,
        devId: String,
        remoteId: String,
        data: AirSendCommandRequest
    ): Observable<Resource<Boolean>>? =
        tuyaAPI.callAirConditionersCommand(access_token, sign, t, devId, remoteId, data)?.map {
            responseHandler.handleResponse(it)
        }

    fun sendAirCCommand(
        access_token: String,
        sign: String, t: Long,
        devId: String,
        remoteId: String,
        airCommand: AirCommandRequest
    ): Observable<Resource<Boolean>>? =
        tuyaAPI.callAirConditionerCommand(access_token, sign, t, devId, remoteId, airCommand)?.map {
            responseHandler.handleResponse(it)
        }

    fun addDevice(
        access_token: String,
        sign: String, t: Long,
        devId: String,
        data: AddRemoteRequest
    ): Observable<Resource<AddRemoteEntity>>? =
        tuyaAPI.callAddRemote(access_token, sign, t, devId, data)?.map {
            responseHandler.handleResponse(it)
        }


    fun renameRemote(
        access_token: String,
        sign: String, t: Long,
        devId: String?,
        remoteId: String?,
        name: String
    ): Observable<Resource<Boolean>>? =
        tuyaAPI.callRenameRemote(access_token, sign, t, devId, remoteId, name)?.map {
            responseHandler.handleResponse(it)
        }

    fun deleteRemote(
        access_token: String,
        sign: String, t: Long,
        devId: String?,
        remoteId: String?
    ): Observable<Resource<Boolean>>? =
        tuyaAPI.callDeleteRemote(access_token, sign, t, devId, remoteId)?.map {
            responseHandler.handleResponse(it)
        }

    fun updateNameSwitch(
        access_token: String,
        sign: String, t: Long,
        devId: String?,
        code: String?,
        name: String?
    ): Observable<Resource<Boolean>>? =
        tuyaAPI.callRenameSwitch(access_token, sign, t, devId, code, RenameSwitchRequest(name))
            ?.map {
                responseHandler.handleResponse(it)
            }
}


/*

*** Note: To get hash for auth ***

let timestamp = String(Date().millisecondsSince1970)
let message = clientId + timestamp
let hash = try? HMAC(key: Array(secret.utf8), variant: .sha256)
.authenticate(Array(message.utf8))
.toHexString().uppercased()

>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

1. Get Access Token
METHOD: GET "https://openapi.tuyaus.com/v1.0/token"
headers: [
"client_id": clientId,
"sign": hash,
"t": timestamp,
"sign_method": "HMAC-SHA256"
]

2. Get IR Brands
METHOD: GET "https://openapi.tuyaus.com/v1.0/infrareds/{infraredId}/categories/{categoryValue}/brands"
headers: [
"client_id": clientId,
"sign": hash,
"t": timestamp,
"sign_method": "HMAC-SHA256",
"access_token": accessToken
]

3. Get Added IR Remotes
METHOD: GET "https://openapi.tuyaus.com/v1.0/infrareds/{infraredId}/remotes"
headers: [
"client_id": clientId,
"sign": hash,
"t": timestamp,
"sign_method": "HMAC-SHA256",
"access_token": accessToken
]

4. Get Remote Indexes (ไว้ใช้ดึง Remote ทั้งหมด ใช้ในหน้า Matching หารีโมตที่ทำงานได้จริง ทั้ง Air และ TV)
METHOD: GET "https://openapi.tuyaus.com/v1.0/infrareds/{infraredId}/categories/{category}/brands/{brandId}"
headers: [
"client_id": clientId,
"sign": hash,
"t": timestamp,
"sign_method": "HMAC-SHA256",
"access_token": accessToken
]

5. Test AirConditioner Commands (ไว้ยิงเทสปุ่มกด Air แต่ละปุ่ม)
METHOD: POST "https://openapi.tuyaus.com/v1.0/infrareds/{infraredId}/air-conditioners/testing/command"
headers: [
"client_id": clientId,
"sign": hash,
"t": timestamp,
"sign_method": "HMAC-SHA256",
"access_token": accessToken
]
params_Body: [
"infrared_id": infraredId,
"remote_index": remoteIndex,
"category_id": "5",
"code": code,
"value": value
]

6. Add IR Remote
METHOD: POST "https://openapi.tuyaus.com/v1.0/infrareds/{infraredId}/normal/add-remote"
headers: [
"client_id": clientId,
"sign": hash,
"t": timestamp,
"sign_method": "HMAC-SHA256",
"access_token": accessToken
]
params_Body: [
"category_id": category == .airConditioner ? "5" : "2",
"brand_id": brandId,
"brand_name": brandName,
"remote_index": remoteIndex,
"remote_name": remoteName
]

7. Get Air Status to show Current Status
METHOD: GET "https://openapi.tuyaus.com/v1.0/infrareds/{infraredId}/remotes/{remoteId}/ac/status"
headers: [
"client_id": clientId,
"sign": hash,
"t": timestamp,
"sign_method": "HMAC-SHA256",
"access_token": accessToken
]

8. Control Air With Multiple Commands
METHOD: POST "https://openapi.tuyaus.com/v1.0/infrareds/{infraredId}/air-conditioners/{remoteId}/scenes/command"
headers: [
"client_id": clientId,
"sign": hash,
"t": timestamp,
"sign_method": "HMAC-SHA256",
"access_token": accessToken
]
params_Body: [
"power": codePower,
"mode": codeMode,
"temp": codeTemp,
"wind": codeWind
]

9. Test / Check TV Commands
METHOD: POST "https://openapi.tuyaus.com/v1.0/infrareds/{infraredId}/testing/command"
headers: [
"client_id": clientId,
"sign": hash,
"t": timestamp,
"sign_method": "HMAC-SHA256",
"access_token": accessToken
]
params_Body: [
"remote_index": remoteIndex,
"category_id": "2",
"key": code,
]

10. Control TV With Command
METHOD: POST "https://openapi.tuyaus.com/v1.0/infrareds/{infraredId}/remotes/{remoteId}/command"
headers: [
"client_id": clientId,
"sign": hash,
"t": timestamp,
"sign_method": "HMAC-SHA256",
"access_token": accessToken
]
params_Body: [
"key": code
]*/
