package ai.apdigital.tuya.repository.request

data class AddRemoteRequest(
    var category_id: String? = "", //category == .airConditioner ? "5" : "2",
    var brand_id: String? = "",
    var brand_name: String? = "",
    var remote_index: String? = "",
    var remote_name: String? = ""
)