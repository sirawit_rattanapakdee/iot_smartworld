package ai.apdigital.tuya.repository.request

open class AirCommandRequest(
    var code: String? = "",
    var value: Int = 0
)