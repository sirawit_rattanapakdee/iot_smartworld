package ai.apdigital.tuya.repository.request

data class AirSendCommandRequest(
    var power: Int =0, //category == .airConditioner ? "5" : "2",
    var mode: Int = 0,
    var temp: Int = 0,
    var wind: Int = 0
)