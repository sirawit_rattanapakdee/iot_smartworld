package ai.apdigital.tuya.repository.request

data class AirTestRequest(
    var infrared_id: String? = "",
    var remote_index: String? = "",
    var category_id: String? = "",
) : AirCommandRequest()