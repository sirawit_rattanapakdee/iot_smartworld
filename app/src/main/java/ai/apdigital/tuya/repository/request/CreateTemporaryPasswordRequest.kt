package ai.apdigital.tuya.repository.request

data class CreateTemporaryPasswordRequest(
    var delivery_status: Int?=0,
    var effective_time: Long?=0,
    var invalid_time: Long?=0,
    var name: String? = null,
    var password: String? = null,
    var password_type: String? = null,
    var phone: String? = null,
    var ticket_id: String? = null,
    var type: Int? = 0,
    var time_zone: String? = null,
    var device_id: String? = null
)
