package ai.apdigital.tuya.repository.request

data class RenameSwitchRequest(
    var name: String? = null)
