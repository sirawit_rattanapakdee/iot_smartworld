package ai.apdigital.tuya.repository.request

data class TVSendCommandRequest(
    var key: String? = ""
)