package ai.apdigital.tuya.repository.request

data class TVTestRequest(
    var remote_index: String? = "",
    var category_id: String? = "",
    var key: String? = ""
)