package ai.apdigital.tuya.repository.request

data class ZigbeeUnlock(
    var password: String? = "",
    var password_type: String? = "",
    var ticket_id: String? = ""
)