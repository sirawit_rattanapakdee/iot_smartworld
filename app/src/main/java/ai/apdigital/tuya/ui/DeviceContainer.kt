package ai.apdigital.tuya.ui

import com.tuya.smart.sdk.bean.DeviceBean

data class DeviceContainer(
    var device: DeviceBean? = null,
    var switchCount: Int = 0,
    var position: Int = 0,
    var deviceType: String = "NORMAL",
    var deviceTypeDP: String = "",
    var switchSchema: MutableList<DeviceSwitchValue>? = mutableListOf()
)
