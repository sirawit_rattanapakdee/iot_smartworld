package ai.apdigital.tuya.ui

import com.tuya.smart.android.device.bean.SchemaBean

class DeviceSwitchValue(
    var switchEnable: Boolean = false,
    var switchPosition:Int = 0,
    var switchSchema: SchemaBean? = null
)
