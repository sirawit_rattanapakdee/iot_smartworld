package ai.apdigital.tuya.ui

import ai.apdigital.tuya.HomeModel
import ai.apdigital.tuya.R
import ai.apdigital.tuya.TuyaSmartWorldModule
import ai.apdigital.tuya.base.BaseActivity
import ai.apdigital.tuya.base.Constants
import ai.apdigital.tuya.networking.Resource
import ai.apdigital.tuya.networking.Status
import ai.apdigital.tuya.ui.control.DeviceDetailActivity
import ai.apdigital.tuya.ui.control.camera.CameraUtils
import ai.apdigital.tuya.ui.control.ir.IRActivity
import ai.apdigital.tuya.ui.control.ir.IRControlAirConditionActivity
import ai.apdigital.tuya.ui.control.ir.IRTVActivity
import ai.apdigital.tuya.ui.control.ir.RemoteType
import ai.apdigital.tuya.ui.control.panal.ControlPanelActivity
import ai.apdigital.tuya.ui.control.security.SecurityActivity
import ai.apdigital.tuya.ui.control.smart_lock.SmartLockActivity
import ai.apdigital.tuya.ui.device.DeviceSwitchAdapter
import ai.apdigital.tuya.ui.room.RoomActivity
import ai.apdigital.tuya.ui.scene.SceneActivity
import ai.apdigital.tuya.ui.scene.SceneAdapter
import ai.apdigital.tuya.ui.viewmodel.TuyaViewModel
import ai.apdigital.tuya.utils.*
import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.widget.PopupMenu
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.alibaba.fastjson.JSONObject
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import com.tuya.smart.activator.config.api.ITuyaDeviceActiveListener
import com.tuya.smart.activator.config.api.TuyaDeviceActivatorManager
import com.tuya.smart.api.MicroContext
import com.tuya.smart.api.router.UrlBuilder
import com.tuya.smart.api.router.UrlRouter
import com.tuya.smart.home.sdk.TuyaHomeSdk
import com.tuya.smart.home.sdk.bean.HomeBean
import com.tuya.smart.home.sdk.bean.RoomBean
import com.tuya.smart.home.sdk.bean.scene.SceneBean
import com.tuya.smart.panelcaller.api.AbsPanelCallerService
import com.tuya.smart.sdk.api.IDevListener
import com.tuya.smart.sdk.api.IResultCallback
import com.tuya.smart.sdk.api.ITuyaDevice
import com.tuya.smart.sdk.bean.DeviceBean
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.content
import kotlinx.android.synthetic.main.device_mgt_item_security_dps.*
import kotlinx.android.synthetic.main.toolbar_back_with_menu.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber


class MainActivity : BaseActivity() {
    private val tuyaVM: TuyaViewModel? by viewModel()
    private var sceneAdapter: SceneAdapter? = null
    private var deviceAdapter: DeviceSwitchAdapter? = null
    private var sceneList: MutableList<SceneBean>? = null
    private var homeBean: HomeBean? = null
    private var currentRoomSelected: RoomBean? = null
    private var deviceInstance: MutableList<ITuyaDevice>? = null
    private var roomAdapter: RoomDeviceAdapter? = null
    var homeId: Long = 0
    override fun getLayoutView() = R.layout.activity_main
    override fun bindView() {

        setupToolbar(
            getString(R.string.smart_home),
            showButtonBack = true,
            showRightMenu = false
        )
    }

    override fun setupInstance() {
        //val language = intent.getStringExtra(TuyaSmartWorldModule.EXTRA_LANGUAGE)
    }

    override fun setupView() {
        homeId = HomeModel.INSTANCE.getCurrentHome(this)
        TuyaHomeSdk.newHomeInstance(homeId)
        shimmerViewContainer = shimmer_container
        contentContainer = content

        val menu = PopupMenu(this, ivInfo)
        menu.gravity = Gravity.END
        menu.inflate(R.menu.popup)
        menu.setOnMenuItemClickListener { item ->
            item?.let {
                if (item.itemId == R.id.menu_manage_room) {
                    EventUtils.sendEvent(
                        EventUtils.TYPE_EVENT,
                        EventHomeUtils.TUYA_HOME_MANAGE_ROOM,
                        ""
                    )
                    val intent = Intent(this@MainActivity, RoomActivity::class.java)
                    startForRoomManagementResult.launch(intent)
                } else {
                    EventUtils.sendEvent(
                        EventUtils.TYPE_EVENT,
                        EventHomeUtils.TUYA_HOME_SORT_DEVICE,
                        ""
                    )
                    val intent = Intent(this@MainActivity, ReorderDeviceMainActivity::class.java)
                    startForSortDeviceManagementResult.launch(intent)
                }
            }
            true
        }
        ivInfo?.setOnClickListener { menu.show() }

        /*
        val manageRoomString = SpannableString(tvHomeManagement?.text)
        manageRoomString.setSpan(UnderlineSpan(), 0, manageRoomString.length, 0)

        tvHomeManagement?.text = manageRoomString

        tvHomeManagement?.setOnClickListener {
*//*            EventUtils.sendEvent(EventUtils.TYPE_EVENT, EventHomeUtils.TUYA_HOME_MANAGE_ROOM, "")
            val intent = Intent(this, RoomActivity::class.java)
            startForRoomManagementResult.launch(intent)*//*

            EventUtils.sendEvent(EventUtils.TYPE_EVENT, EventHomeUtils.TUYA_HOME_SORT_DEVICE, "")
            val intent = Intent(this, ReorderDeviceMainActivity::class.java)
            startForSortDeviceManagementResult.launch(intent)
        }*/

        //listV.isNestedScrollingEnabled = false

        ivAdd?.setOnClickListener {
            openAddDevice()
        }
        ivMicrophone?.setOnClickListener {
            changeEnglishLanguage()
        }
        ivNotice?.setOnClickListener {
            EventUtils.sendEvent(EventUtils.TYPE_EVENT, EventHomeUtils.TUYA_HOME_NOTI_CENTER, "")
            UrlRouter.execute(UrlBuilder(this, "messageCenter"))
        }

        tvAll?.setOnClickListener {
            EventUtils.sendEvent(EventUtils.TYPE_EVENT, EventHomeUtils.TUYA_HOME_SHOW_ALL_SCENE, "")
            val intent = Intent(this, SceneActivity::class.java)
            startForSceneManagementResult.launch(intent)
        }
        swipeContainer?.setOnRefreshListener {
            getScene(homeId)
            getHomeDetail(homeId)
        }

        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Timber.e("Fetching FCM registration token failed ${task.exception}")
                return@OnCompleteListener
            }

            // Get new FCM registration token
            val token = task.result

            try {
                TuyaHomeSdk.getPushInstance()
                    .registerDevice(token, "fcm", object : IResultCallback {
                        override fun onError(code: String?, error: String?) {
                            Timber.e("Error FCM")
                        }

                        override fun onSuccess() {
                            Timber.e("Success FCM")
                        }
                    })
                // some code
            } catch (e: Exception) {
                // handler
            }
        })
    }

    override fun initialize(savedInstanceState: Bundle?) {

        EventUtils.sendEvent(EventUtils.TYPE_PAGE, PageUtils.TUYA_HOME, "")
        tuyaVM?.getHomeDetail()?.observe(this, homeRes)
        tuyaVM?.getSceneList()?.observe(this, sceneRes)
        tuyaVM?.setEnableScene()?.observe(this, sceneEnableRes)
        recyclerList?.let {
            sceneAdapter =
                SceneAdapter(
                    this, 0
                )
            val layoutManager = GridLayoutManager(this, 2)
            layoutManager.orientation = LinearLayoutManager.VERTICAL
            it.layoutManager = layoutManager
            it.adapter = sceneAdapter
            it.setHasFixedSize(false)
            it.isNestedScrollingEnabled = false

            sceneAdapter?.setOnItemClickListener(object : SceneAdapter.OnItemClickListener {

                override fun onRecyclerItemClick(
                    view: View,
                    position: Int,
                    entity: SceneBean?
                ) {
                    entity?.let { scene ->
                        if (scene.conditions == null) {
                            EventUtils.sendEvent(
                                EventUtils.TYPE_EVENT,
                                EventHomeUtils.TUYA_HOME_SCENE,
                                scene.name
                            )
                            tuyaVM?.tapToRubScene(entity)
                        }
                    }
                }

                override fun onDeleteItemClick(view: View, position: Int, entity: SceneBean?) {
                }

                override fun onEnableItemClick(
                    view: View,
                    position: Int,
                    entity: SceneBean?,
                    isChecked: Boolean
                ) {
                }
            })
        }

        recyclerListDevice?.let {
            deviceAdapter =
                DeviceSwitchAdapter(
                    this
                )
            val layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
            //layoutManager.orientation = LinearLayoutManager.VERTICAL
            it.layoutManager = layoutManager
            it.adapter = deviceAdapter
            it.setHasFixedSize(false)
            it.isNestedScrollingEnabled = false

            deviceAdapter?.setOnItemClickListener(object : DeviceSwitchAdapter.OnItemClickListener {

                override fun onRecyclerItemClick(
                    view: View,
                    position: Int,
                    entity: DeviceContainer?
                ) {
                     if (entity?.device?.isOnline == false)
                         return
                     Timber.e("Code  ${entity?.device?.categoryCode}")
                     EventUtils.sendEvent(
                         EventUtils.TYPE_EVENT,
                         EventHomeUtils.TUYA_HOME_DEVICE_PANEL,
                         entity?.device?.deviceCategory
                     )
                     when {
                         CameraUtils.ipcProcess(this@MainActivity, entity?.device) -> {
                             return
                         }
                         entity?.device?.deviceCategory?.endsWith("ms") == true -> {
                             val intent =
                                 Intent(this@MainActivity, SmartLockActivity::class.java)
                             intent.putExtra(Constants.INTENT_DEV_ID, entity.device?.devId)
                             startForResult.launch(intent)
                         }
                         entity?.device?.deviceCategory?.endsWith("wnykq") == true -> {
                             val intent = Intent(this@MainActivity, IRActivity::class.java)
                             intent.putExtra(Constants.INTENT_DEV_ID, entity.device?.devId)
                             startIRForResult.launch(intent)
                         }
                         entity?.device?.categoryCode == "inf_qt" -> {
                             var isAirConditioner = false
                             TuyaHomeSdk.getDataInstance().getSchema(entity.device?.devId)
                                 ?.let temp@{ map ->
                                     map.values.forEach { bean ->
                                         if (bean.code == "temperature" || bean.code == "temp") {
                                             isAirConditioner = true
                                             return@temp
                                         }
                                     }
                                 }
                             val intent = if (isAirConditioner) Intent(
                                 this@MainActivity,
                                 IRControlAirConditionActivity::class.java
                             )
                             else Intent(this@MainActivity, IRTVActivity::class.java)

                             intent.putExtra(Constants.INTENT_DEV_ID, entity.device?.parentDevId)
                             intent.putExtra(Constants.INTENT_REMOTE_ID, entity.device?.devId)
                             intent.putExtra(Constants.INTENT_REMOTE_TYPE, RemoteType.CONTROL)
                             startIRForResult.launch(intent)
                         }
                         entity?.device?.categoryCode?.contains("wf_mal") == true -> {
                             val intent =
                                 Intent(this@MainActivity, SecurityActivity::class.java)
                             intent.putExtra(Constants.INTENT_DEV_ID, entity.device?.devId)
                             startForResult.launch(intent)
                         }
                         else -> {
                             val intent =
                                 Intent(this@MainActivity, ControlPanelActivity::class.java)
                             intent.putExtra(Constants.INTENT_DEV_ID, entity?.device?.devId)
                             startForResult.launch(intent)
                         }
                     }
                    /*val service = MicroContext.getServiceManager()
                        .findServiceByInterface<AbsPanelCallerService>(
                            AbsPanelCallerService::class.java.name
                        )
                    service.goPanelWithCheckAndTip(this@MainActivity, entity?.device?.devId)*/
                }

                override fun onRecyclerSwitchClick(
                    view: View?,
                    position: Int,
                    deviceSwitch: DeviceSwitchValue?,
                    entity: DeviceContainer?
                ) {

                    if (deviceSwitch?.switchSchema?.mode?.contains("w") == true) {
                        val map = HashMap<String, Any>()
                        val isChecked = !deviceSwitch.switchEnable
                        map[deviceSwitch.switchSchema?.id ?: ""] = isChecked
                        JSONObject.toJSONString(map)?.let { param ->
                            Timber.e(param)
                            val switchStr =
                                if (isChecked) getString(R.string.device_on) else getString(R.string.device_off)

                            var event =
                                EventHomeUtils.TUYA_HOME_QUICK_TOGGLE_SW.plus(deviceSwitch.switchPosition + 1)
                            if (entity?.switchSchema?.size == 1)
                                event = EventHomeUtils.TUYA_HOME_QUICK_TOGGLE_SW

                            EventUtils.sendEvent(
                                EventUtils.TYPE_EVENT,
                                event,
                                switchStr
                            )
                            deviceInstance?.get(position)
                                ?.publishDps(param, object : IResultCallback {
                                    override fun onError(code: String?, error: String?) {
                                        Toast.makeText(
                                            this@MainActivity,
                                            "Failed to switch ".plus(switchStr),
                                            Toast.LENGTH_SHORT
                                        )
                                            .show()
                                    }

                                    override fun onSuccess() {

                                    }
                                })
                        }

                    } else {
                        Toast.makeText(
                            this@MainActivity,
                            "Not allow.",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                }

                override fun onRecyclerSOSModeChanged(
                    view: View?,
                    position: Int,
                    dp: String,
                    value: String
                ) {
                    val map = HashMap<String, Any>()
                    map[dp] = value
                    JSONObject.toJSONString(map)?.let { param ->
                        deviceInstance?.get(position)
                            ?.publishDps(param, object : IResultCallback {
                                override fun onError(code: String?, error: String?) {

                                }

                                override fun onSuccess() {

                                }
                            })
                    }
                }

                override fun onAlarmSwitchClick(
                    view: View?,
                    position: Int,
                    dp: String,
                    value: Boolean
                ) {
                    val map = HashMap<String, Any>()
                    map[dp] = value
                    JSONObject.toJSONString(map)?.let { param ->
                        deviceInstance?.get(position)
                            ?.publishDps(param, object : IResultCallback {
                                override fun onError(code: String?, error: String?) {

                                }

                                override fun onSuccess() {

                                }
                            })
                    }
                }

                override fun onRecyclerInfo(view: View, position: Int, entity: DeviceContainer?) {
                    /*val urlBuilders = UrlBuilder(this@MainActivity, "panelMore")
                    val bundle = Bundle()
                    bundle.putString("extra_panel_dev_id", entity?.device?.devId)
                    bundle.putString("extra_panel_name", entity?.device?.name)
                    bundle.putLong("extra_panel_group_id", -1)
                    urlBuilders.putExtras(bundle)
                    UrlRouter.execute(urlBuilders)*/
                    EventUtils.sendEvent(
                        EventUtils.TYPE_EVENT,
                        EventHomeUtils.TUYA_HOME_DEVICE_DETAIL,
                        entity?.device?.deviceCategory
                    )
                    val intent = Intent(this@MainActivity, DeviceDetailActivity::class.java)
                    intent.putExtra(Constants.INTENT_DEV_ID, entity?.device?.devId)
                    startForDeviceDetailMainResult.launch(intent)
                    /*val urlBuilder = UrlBuilder(this@PanelMoreActivity, "panelMore")
                    val devId: String = edt.getText().toString().trim()
                    val deviceBean = TuyaHomeSdk.getDataInstance().getDeviceBean(devId)
                    val bundle = Bundle()
                    bundle.putString("extra_panel_dev_id", devId)
                    bundle.putString("extra_panel_name", deviceBean!!.getName())
                    bundle.putLong("extra_panel_group_id", groupId)
                    urlBuilder.putExtras(bundle)
                    UrlRouter.execute(urlBuilder)*/
                }
            })
        }
        getScene(homeId)
        getHomeDetail(homeId)
        roomAdapter = RoomDeviceAdapter(
            this, listOf(), getString(R.string.all_devices)
        )
        row_item_spinner?.adapter = roomAdapter
        addSceneBox?.setOnClickListener {
            EventUtils.sendEvent(EventUtils.TYPE_EVENT, EventSceneUtils.TUYA_SHOW_ALL_CREATE, "")
            val intent = Intent(this, SceneActivity::class.java)
            /*intent.putExtra(ExtraName.SCENE, Gson().toJson(sceneList))
            intent.putExtra(ExtraName.HOME_ID, homeId)*/
            startForSceneManagementResult.launch(intent)
        }
        addDeviceBox?.setOnClickListener {
            /*     val intent = Intent(this@MainActivity, RoomFormActivity::class.java)
                 intent.putExtra(ExtraName.COMMAND, CommandType.UPDATE)
                 intent.putExtra(ExtraName.HOME, Gson().toJson(homeBean))
                 intent.putExtra(ExtraName.ROOM, Gson().toJson(currentRoomSelected))
                 startActivity(intent)*/
            openAddDevice()
        }

        /*deviceSwitchVM?.deviceSwitchChanged()?.observe(this, { updateDevice ->
            val newList: MutableList<DeviceBean> = mutableListOf()
            homeBean?.deviceList?.filterTo(
                newList,
                { originalDevice -> originalDevice.devId == updateDevice.devId })

            *//*homeBean?.deviceList?.forEach {originalDevice->
                if(originalDevice.devId == updateDevice.devId) {
                    originalDevice = updateDevice
                }
            }*//*
        })*/

        val dataPush = intent.getBundleExtra(Constants.INTENT_NOTIFICATION_DATA)
        if (dataPush != null) {
            UrlRouter.execute(UrlBuilder(this, "messageCenter"))
            // Timber.e("dataPush != null")
        }/*else{
            Timber.e("dataPush == null")
        }*/
    }


    private val sceneEnableRes = Observer<Resource<SceneBean>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                resource.data?.let {
                    showToastSuccessAlertNotices(
                        getString(R.string.run).plus(" ").plus(
                            getString(R.string.msg_successfully)
                        ), null
                    )
                }
            }

            Status.ERROR -> {
                showToastErrorAlertNotices(
                    resource.message?.message ?: getString(R.string.msg_failure), null
                )
            }
            Status.LOADING -> {
                // showProgressView()
            }
        }
    }

    private fun prepareRoom(rooms: MutableList<RoomBean>) {
        roomAdapter = RoomDeviceAdapter(
            this, rooms, getString(R.string.all_devices)
        )

        row_item_spinner?.adapter = roomAdapter

        /*val data = mutableListOf<String>()
        data.add("All devices" )
        rooms.forEach { data.add(it.name )}
        val adapter = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, data)*/
        row_item_spinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View, position: Int, id: Long
            ) {

                if (position == 0) {
                    EventUtils.sendEvent(
                        EventUtils.TYPE_EVENT,
                        EventHomeUtils.TUYA_HOME_SELECT_ROOM,
                        "All"
                    )
                    enableDeviceList(homeBean?.deviceList?.isNotEmpty() == true)

                    deviceAdapter?.updateData(
                        convertSchemaDevice(homeBean?.deviceList)
                    )
                    registerDeviceCallback(homeBean?.deviceList)
                } else {
                    currentRoomSelected = rooms[position - 1]
                    EventUtils.sendEvent(
                        EventUtils.TYPE_EVENT,
                        EventHomeUtils.TUYA_HOME_SELECT_ROOM,
                        currentRoomSelected?.name
                    )
                    enableDeviceList(currentRoomSelected?.deviceList?.isNotEmpty() == true)

                    deviceAdapter?.updateData(
                        convertSchemaDevice(currentRoomSelected?.deviceList)
                    )
                    registerDeviceCallback(currentRoomSelected?.deviceList)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }
    }

    private val startForSortDeviceManagementResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                getHomeUpdate()
            }
        }

    private val startForRoomManagementResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                getHomeUpdate()
            }
        }

    private val startForSceneManagementResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                getScene(homeId)
            }
        }


    val startForDeviceDetailMainResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                Timber.e("startForDeviceDetailResult : getHomeUpdate")
                getHomeUpdate()
            }
        }

    val startForResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                Timber.e("startForResult : getHomeUpdate")
                getHomeUpdate()
            }
        }


    val startIRForResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                Timber.e("startIRForResult : getHomeDetail")
                getHomeDetail(homeId)
            }
        }

    private fun registerDeviceCallback(deviceList: MutableList<DeviceBean>?) {
        GlobalScope.launch {
            deviceInstance?.forEach {
                it.unRegisterDevListener()
            }
            deviceInstance = mutableListOf()
            deviceList?.forEach { entity ->
                val deviceIns = TuyaHomeSdk.newDeviceInstance(entity.devId)
                deviceIns?.registerDevListener(deviceListener)
                deviceInstance?.add(deviceIns)
            }
        }
    }

    private fun convertSchemaDevice(deviceList: MutableList<DeviceBean>?): MutableList<DeviceContainer> {
        val deviceContainer: MutableList<DeviceContainer> = mutableListOf()
        deviceList?.sortBy { it.homeDisplayOrder }
        deviceList?.forEach { entity ->
            deviceContainer.add(SchemaConverter.convertSchemaDevice(entity.devId ?: ""))
        }
        return deviceContainer
    }

    private fun enableDeviceList(isEnable: Boolean) {
        recyclerListDevice?.visibility = if (isEnable) View.VISIBLE else View.GONE
        contentAddDevice?.visibility = if (isEnable) View.GONE else View.VISIBLE
    }


    private fun enableSceneList(isEnable: Boolean) {
        recyclerList?.visibility = if (isEnable) View.VISIBLE else View.GONE
        contentAddScene?.visibility = if (isEnable) View.GONE else View.VISIBLE
    }

    private fun getHomeDetail(homeId: Long) {
        deviceAdapter?.clear()
        tuyaVM?.getHomeDetail(homeId)
    }

    override fun onResume() {
        super.onResume()
        if (haveUpdateMain) {
            haveUpdateMain = false
            getHomeDetail(homeId)
        }
        startEventReceiver()
    }

    override fun onPause() {
        super.onPause()
        stopEventReceiver()
    }

    private fun stopEventReceiver() {
        try {
            unregisterReceiver(mLocationReceiver)
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        }
    }

    private fun startEventReceiver() {
        try {
            val filter = IntentFilter()
            filter.addAction(Constants.ACTION_NOTIFICATION)
            registerReceiver(mLocationReceiver, filter)
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        }
    }

    private val mLocationReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            //entity?.device?.categoryCode?.contains("wf_mal") == true -> {
            val notification = Gson().fromJson(
                intent.getStringExtra(Constants.INTENT_NOTIFICATION_ALERT) ?: "",
                RemoteMessage::class.java
            )
            if (notification != null) {
                if (notification.data["categoryCode"]?.contains("mal") == true) {
                    if (!SecurityActivity.isSmartLockForeground) {
                        val intentSecurity =
                            Intent(this@MainActivity, SecurityActivity::class.java)
                        intentSecurity.putExtra(
                            Constants.INTENT_DEV_ID,
                            intent.getStringExtra(Constants.INTENT_DEV_ID)
                        )
                        intentSecurity.putExtra(
                            Constants.INTENT_NOTIFICATION_ALERT,
                            intent.getStringExtra(Constants.INTENT_NOTIFICATION_ALERT)
                        )

                        startForResult.launch(intentSecurity)
                    }
                }
            }
        }
    }


    private fun getHomeUpdate() {
        //deviceAdapter?.updateData(mutableListOf())

        // homeShowShimmerLoading()
        TuyaHomeSdk.getDataInstance().getHomeBean(homeId)?.let { data ->
            homeBean = data
            if (data.deviceList?.isNotEmpty() == true) {
                prepareRoom(homeBean?.rooms ?: mutableListOf())
            } else {
                enableDeviceList(false)
            }
            //homeHideShimmerLoading()
        }
    }

    private fun getScene(homeId: Long) {
        tuyaVM?.getScene(homeId)
    }

    private fun openAddDevice() {
        EventUtils.sendEvent(EventUtils.TYPE_EVENT, EventHomeUtils.TUYA_HOME_ADD_DEVICE, "")
        TuyaDeviceActivatorManager.startDeviceActiveAction(this, homeId)
        TuyaDeviceActivatorManager.setListener(
            object : ITuyaDeviceActiveListener {
                override fun onDevicesAdd(list: List<String?>?) {
                    Timber.e("onDevicesAdd")
                    getHomeDetail(homeId)
                }

                override fun onRoomDataUpdate() {
                    Timber.e("onRoomDataUpdate")
                }

                override fun onOpenDevicePanel(s: String?) {
                    Timber.e("onOpenDevicePanel")
                }

                override fun onExitConfigBiz() {
                    Timber.e("onExitConfigBiz")
                }
            })
    }


    private val otaCheckRes = Observer<Resource<Boolean>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                hideProgressView()

            }

            Status.ERROR -> {
                hideProgressView()
                showToastErrorAlertNotices(
                    resource.message?.message ?: getString(R.string.msg_failure), null
                )
            }
            Status.LOADING -> {
                showProgressView()
            }
        }
    }

    private val sceneRes = Observer<Resource<List<SceneBean>>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                swipeContainer.isRefreshing = false
                resource.data?.let { data ->
                    if (data.isNotEmpty()) {
                        lineScene?.visibility = View.GONE
                        sceneList = data.toMutableList()
                        val scenes: MutableList<SceneBean> = mutableListOf()

                        sceneList?.forEach {
                            if (it.isStickyOnTop && it.conditions == null)
                                scenes.add(it)
                        }
                        sceneAdapter?.updateData(scenes)
                        enableSceneList(scenes.size > 0)
/*
                        scenes.let {
                            val sce = if (scenes.size > 4) {
                                scenes.take(4).toMutableList()
                            } else {
                                scenes.toMutableList()
                            }
                            sceneAdapter?.updateData(sce)
                        }*/
                    } else {
                        lineScene?.visibility = View.VISIBLE
                        enableSceneList(false)
                        Timber.e("Scene Success null data")
                    }
                }
                hideShimmerLoading()
            }

            Status.ERROR -> {
                hideShimmerLoading()
                swipeContainer.isRefreshing = false
                showToastErrorAlertNotices(
                    resource.message?.message ?: getString(R.string.msg_failure), null
                )
            }
            Status.LOADING -> {
                showShimmerLoading()
            }
        }
    }

    private val homeRes = Observer<Resource<HomeBean>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                swipeContainer.isRefreshing = false
                haveUpdateMain = false
                resource.data?.let { data ->
                    homeBean = data
                    if (data.deviceList?.isNotEmpty() == true) {
                        prepareRoom(homeBean?.rooms ?: mutableListOf())
                    } else {
                        enableDeviceList(false)
                    }
                }
                homeHideShimmerLoading()
            }

            Status.ERROR -> {
                homeHideShimmerLoading()
                swipeContainer.isRefreshing = false
                haveUpdateMain = false
                showToastErrorAlertNotices(
                    resource.message?.message ?: getString(R.string.msg_failure), null
                )
            }
            Status.LOADING -> {
                homeShowShimmerLoading()
            }
        }
    }

    private fun homeShowShimmerLoading() {
        content_device?.visibility = View.GONE
        shimmer_container_device?.visibility = View.VISIBLE
        shimmer_container_device?.startShimmer()
    }

    private fun homeHideShimmerLoading() {
        shimmer_container_device?.visibility = View.GONE
        shimmer_container_device?.stopShimmer()
        content_device?.visibility = View.VISIBLE
    }

    private var deviceListener: IDevListener? = object : IDevListener {
        override fun onDpUpdate(devId: String?, dpStr: String?) {
            //override fun onDpUpdate(devId: String?, dpStr: MutableMap<String, Any>?) {
            val dps: Map<*, *>? = Gson().fromJson(dpStr, Map::class.java)
            Timber.e("onDpUpdate $devId $dpStr $dps")
            var position = -1
            deviceAdapter?.data?.forEachIndexed { index, data ->
                Timber.e("${data.device?.devId} == $devId => $index")
                if (data.device?.devId == devId) {
                    position = index
                    return@forEachIndexed
                }
            }

            if (position < 0)
                return
            recyclerListDevice?.findViewHolderForAdapterPosition(
                position
            )?.let { holder ->
                var isUpdated = false
                dps?.forEach { code ->
                    val deviceContainer = deviceAdapter?.data?.get(position)
                    deviceContainer?.let { deviceContent ->
                        if (deviceContent.switchCount > 0) {
                            if (deviceContent.deviceTypeDP == code.key)
                                isUpdated = true
                            deviceContent.switchSchema?.forEachIndexed { index, bean ->
                                if (code.value is Boolean) {
                                    when (index) {
                                        0 -> {
                                            isUpdated = true
                                            if (code.key == bean.switchSchema?.id) {
                                                deviceAdapter?.data?.get(position)?.switchSchema?.get(
                                                    0
                                                )?.switchEnable =
                                                    code.value as Boolean
                                            }
                                        }
                                        1 -> {
                                            isUpdated = true
                                            if (code.key == bean.switchSchema?.id) {
                                                deviceAdapter?.data?.get(position)?.switchSchema?.get(
                                                    1
                                                )?.switchEnable =
                                                    code.value as Boolean
                                            }
                                        }
                                        2 -> {
                                            isUpdated = true
                                            if (code.key == bean.switchSchema?.id) {
                                                deviceAdapter?.data?.get(position)?.switchSchema?.get(
                                                    2
                                                )?.switchEnable =
                                                    code.value as Boolean
                                            }
                                        }
                                    }
                                }
                            }
                            if (isUpdated)
                                deviceAdapter?.updateSwitchView(holder, deviceContent)

                        } else {
                            when (deviceContent.deviceType) {
                                "PIR", "presence_state", "pir_state" -> {
                                    deviceAdapter?.updateViewPIR(holder, deviceContent)
                                }
                                "DOOR_CONTACT" -> {
                                    deviceAdapter?.updateViewDoorContact(holder, deviceContent)
                                }
                                "ALARM_SWITCH" -> {
                                    deviceAdapter?.updateViewAlarmSwitch(holder, deviceContent)
                                }
                                "SOS" -> {
                                    deviceAdapter?.updateViewSOSSwitch(holder, deviceContent)
                                }
                                else -> {
                                    deviceAdapter?.updateViewNormal(holder, deviceContent)
                                }
                            }

                        }
                    }
                }
            }
        }

        override fun onRemoved(devId: String?) {
            Timber.e("onRemoved $devId $devId")
            getHomeDetail(homeId)
        }

        override fun onStatusChanged(devId: String?, online: Boolean) {
            //Timber.e("onStatusChanged {$devId} = $online ")
            deviceAdapter?.data?.find { it.device?.devId == devId }.let { deviceContainer ->
                recyclerListDevice?.findViewHolderForLayoutPosition(
                    deviceContainer?.position ?: 0
                )?.let {
                    when (deviceContainer?.deviceType) {
                        "PIR", "presence_state", "pir_state" -> {
                            deviceAdapter?.updateViewPIR(it, deviceContainer)
                        }
                        "DOOR_CONTACT" -> {
                            deviceAdapter?.updateViewDoorContact(it, deviceContainer)
                        }
                        "SWITCH" -> {
                            deviceAdapter?.updateSwitchView(
                                it, deviceContainer
                            )
                        }
                        "SOS" -> {
                            deviceAdapter?.updateViewSOSSwitch(
                                it, deviceContainer
                            )
                        }
                        else -> {
                            deviceAdapter?.updateViewNormal(it, deviceContainer)
                        }
                    }
                }
            }

        }

        override fun onNetworkStatusChanged(devId: String?, status: Boolean) {
            Timber.e("onNetworkStatusChanged {$devId")
        }


        override fun onDevInfoUpdate(devId: String?) {
            Timber.e("onDevInfoUpdate : $devId")
        }

    }

    override fun onDestroy() {
        /*service?.onDestroy()
        service = null*/
        deviceListener = null
        deviceInstance?.forEach {
            it.unRegisterDevListener()
        }
        deviceInstance = null
        deviceAdapter?.mListener = null
        deviceAdapter = null
        roomAdapter = null
        sceneAdapter = null
        sceneList = null
        homeBean = null
        currentRoomSelected = null
        contentContainer = null
        shimmerViewContainer = null
        TuyaHomeSdk.getHomeManagerInstance().onDestroy()
        TuyaHomeSdk.closeService()
        TuyaHomeSdk.onDestroy()
        TuyaSmartWorldModule.RN_CALLBACK?.invoke()
        TuyaSmartWorldModule.RN_CALLBACK = null
        super.onDestroy()

    }
}