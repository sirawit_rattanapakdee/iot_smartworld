package ai.apdigital.tuya.ui

import ai.apdigital.tuya.HomeModel
import ai.apdigital.tuya.R
import ai.apdigital.tuya.base.BaseActivity
import ai.apdigital.tuya.networking.Resource
import ai.apdigital.tuya.networking.Status
import ai.apdigital.tuya.ui.room.ReorderDeviceAdapter
import ai.apdigital.tuya.ui.viewmodel.TuyaViewModel
import ai.apdigital.tuya.utils.EventUtils
import ai.apdigital.tuya.utils.PageUtils
import ai.apdigital.tuya.utils.alerts.AlertUtil
import ai.apdigital.tuya.utils.drag.OnStartDragListener
import ai.apdigital.tuya.utils.drag.ReorderHelperCallback
import android.app.Activity
import android.app.TimePickerDialog
import android.content.Intent
import android.os.Bundle
import android.view.View

import androidx.lifecycle.Observer
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.tuya.smart.home.sdk.TuyaHomeSdk
import com.tuya.smart.home.sdk.bean.DeviceAndGroupInHomeBean
import com.tuya.smart.interior.enums.BizParentTypeEnum
import com.tuya.smart.sdk.bean.DeviceBean
import kotlinx.android.synthetic.main.activity_room_create.*
import kotlinx.android.synthetic.main.toolbar_back.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber


class ReorderDeviceMainActivity : BaseActivity(), OnStartDragListener {
    private val tuyaVM: TuyaViewModel? by viewModel()
    private var deviceAdapter: ReorderDeviceAdapter? = null
    private var homeId: Long = 0L
    private var mItemTouchHelper: ItemTouchHelper? = null
    private var sortDeviceChanged: Boolean = false
    private var deviceHomeBeanSort: MutableList<DeviceAndGroupInHomeBean>? = null

    override fun getLayoutView() = R.layout.activity_device_sort_main
    override fun bindView() {
        setupToolbar(
            getString(R.string.title_sort_devices),
            showButtonBack = true,
            showRightMenu = false
        )
    }

    override fun setupInstance() {
        homeId = HomeModel.INSTANCE.getCurrentHome(this)
    }

    override fun setupView() {
        setUpViewAdapter()
    }

    private fun setUpViewAdapter() {
        recyclerListDevice?.let { it ->
            deviceAdapter =
                ReorderDeviceAdapter(
                    this, 1, this
                ) {
                }
            val layoutManager = LinearLayoutManager(this)
            layoutManager.orientation = LinearLayoutManager.VERTICAL
            it.layoutManager = layoutManager
            it.adapter = deviceAdapter
            it.setHasFixedSize(true)

            deviceAdapter?.setOnItemClickListener(object :
                ReorderDeviceAdapter.OnItemClickListener {

                override fun onRecyclerItemClick(
                    view: View,
                    position: Int,
                    entity: DeviceBean?
                ) {

                }

                override fun onRecyclerItemLongClick(
                    view: View,
                    position: Int,
                    entity: DeviceBean?
                ) {
                }

                override fun onSorted(data: MutableList<DeviceBean>?) {
                    deviceHomeBeanSort = mutableListOf()
                    data?.forEach { dev ->
                        val devMove = DeviceAndGroupInHomeBean()
                        devMove.bizId = dev.devId
                        devMove.bizType = BizParentTypeEnum.DEVICE.getType()
                        deviceHomeBeanSort?.add(devMove)
                    }
                    sortDeviceChanged = true
                }
            })
            val callback: ItemTouchHelper.Callback = ReorderHelperCallback(deviceAdapter!!)
            mItemTouchHelper = ItemTouchHelper(callback)
            mItemTouchHelper?.attachToRecyclerView(it)
        }
        val devices = TuyaHomeSdk.newHomeInstance(homeId).homeBean.deviceList
        devices.sortBy { it.homeDisplayOrder }
        deviceAdapter?.updateData(devices)
    }

    override fun initialize(savedInstanceState: Bundle?) {
        EventUtils.sendEvent(EventUtils.TYPE_PAGE, PageUtils.TUYA_SORT_DEVICE, "")
        tuyaVM?.homeSortDeviceList()
            ?.observe(
                this@ReorderDeviceMainActivity,
                updateSortDevice
            )
        tvSave?.visibility = View.VISIBLE
        tvSave?.setOnClickListener {
            tuyaVM?.sortDeviceInHome(homeId, deviceHomeBeanSort)
        }
    }

    private val updateSortDevice = Observer<Resource<Long>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                sortDeviceChanged = false
                isUpdate = true
                showToastSuccessAlertNotices(
                    getString(R.string.msg_successfully),
                    object : AlertUtil.OnSuccessListener {
                        override fun onSuccess() {
                            onBackMain()
                        }
                    })
            }

            Status.ERROR -> {
                showToastErrorAlertNotices(
                    resource.message?.message ?: getString(R.string.msg_failure), null
                )
            }
            Status.LOADING -> showProgressView()
        }
    }

    override fun onStartDrag(viewHolder: RecyclerView.ViewHolder?) {
        viewHolder?.let {
            mItemTouchHelper?.startDrag(it)
        }
    }

    override fun onBackPressed() {
        if (sortDeviceChanged) {
            showAlertConfirmWithActionClick(
                getString(R.string.alert_leave_not_save),
                null,
                object : AlertUtil.OnSuccessListener {
                    override fun onSuccess() {
                        sortDeviceChanged = false
                        onBackMain()
                    }
                }, R.color.red
            )
        } else {
            onBackMain()
        }
    }


    override fun onDestroy() {
        deviceHomeBeanSort = null
        deviceAdapter = null
        super.onDestroy()
    }
}