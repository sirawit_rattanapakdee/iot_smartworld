package ai.apdigital.tuya.ui

import ai.apdigital.tuya.R
import android.content.Context
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.tuya.smart.activator.ui.kit.utils.getString
import com.tuya.smart.home.sdk.bean.RoomBean

class RoomDeviceAdapter(
    context: Context, roomList: List<RoomBean>,val allDevice:String
) : ArrayAdapter<RoomBean>(context, 0, roomList), View.OnTouchListener  {
    val layoutInflater: LayoutInflater = LayoutInflater.from(context)
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View =
            convertView ?: layoutInflater.inflate(R.layout.custom_spinner_default, parent, false)
        getItem(position)?.let { country ->
            val tvTitle: TextView? = view.findViewById(R.id.tvTitle)
            if (position == 0) {
                tvTitle?.text = allDevice
            } else {
                tvTitle?.text = country.name
            }
        }
        return view
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = layoutInflater.inflate(R.layout.custom_spinner_item, parent, false)
        val tvTitle: TextView? = view.findViewById(R.id.tvTitle)
        if (position == 0) {
            tvTitle?.text = allDevice
        } else {
            tvTitle?.text = getItem(position)?.name
        }
        return view
    }

    override fun getItem(position: Int): RoomBean? {
        if (position == 0) {
            return null
        }
        return super.getItem(position - 1)
    }

    override fun getCount() = super.getCount() + 1

    /*override fun isEnabled(position: Int) = position != 0*/
    private fun setItemForCountry(view: View, room: RoomBean?) {
        val tvTitle: TextView? = view.findViewById(R.id.tvTitle)
        tvTitle?.text = room?.name
    }


    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        when (event?.action) {
            MotionEvent.ACTION_DOWN -> {
                v?.alpha = 0.5f
            }
            MotionEvent.ACTION_UP
            -> {
                v?.alpha = 1f
            }
            MotionEvent.ACTION_CANCEL -> {
                v?.alpha = 1f
            }
        }
        return false
    }
}
