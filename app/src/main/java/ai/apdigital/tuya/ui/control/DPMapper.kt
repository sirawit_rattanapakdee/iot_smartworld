package ai.apdigital.tuya.ui.control

import ai.apdigital.tuya.ui.DPName
import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import timber.log.Timber
import java.io.IOException
import kotlin.collections.HashMap

object DPMapper {
     fun getDPFromAsset(context: Context, fileName: String): HashMap<String, String>? {
        val dpName = HashMap<String, String>()
        try {
            val jsonString = context.assets.open(fileName).bufferedReader().use { it.readText() }

            val dataDP: List<DPName>? =
                Gson().fromJson(jsonString, object : TypeToken<List<DPName>>() {}.type)

            dataDP?.forEach { dp ->
                dpName[dp.dp] = dp.name
            }

        } catch (ioException: IOException) {
            ioException.printStackTrace()
            return HashMap()
        }
        return dpName
    }
}
