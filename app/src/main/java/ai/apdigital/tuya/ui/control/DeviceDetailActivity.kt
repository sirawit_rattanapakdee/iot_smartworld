package ai.apdigital.tuya.ui.control

import ai.apdigital.tuya.R
import ai.apdigital.tuya.base.BaseActivity
import ai.apdigital.tuya.base.Constants
import ai.apdigital.tuya.utils.EventUtils
import ai.apdigital.tuya.utils.PageUtils
import ai.apdigital.tuya.utils.alerts.AlertUtil
import ai.apdigital.tuya.utils.alerts.EditRoomDialogFragment
import android.content.Intent
import android.os.Bundle
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.view.View
import android.widget.TextView
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.tuya.smart.android.device.bean.UpgradeInfoBean
import com.tuya.smart.home.sdk.TuyaHomeSdk
import com.tuya.smart.sdk.api.*
import com.tuya.smart.sdk.bean.OTAErrorMessageBean
import kotlinx.android.synthetic.main.activity_device_detail.*
import kotlinx.android.synthetic.main.toolbar_back.*
import timber.log.Timber


class DeviceDetailActivity : BaseActivity() {
    var dialog: BottomSheetDialog? = null
    override fun getLayoutView() = R.layout.activity_device_detail

    override fun bindView() {

    }

    override fun setupInstance() {
    }

    override fun setupView() {
        setupToolbar(
            "",
            showButtonBack = true,
            showRightMenu = true
        )

    }

    var mDevice: ITuyaDevice? = null
    override fun initialize(savedInstanceState: Bundle?) {
        EventUtils.sendEvent(EventUtils.TYPE_PAGE, PageUtils.TUYA_PANEL_GANERAL_DEVICE_DETAIL, "")
        val deviceId = intent.getStringExtra(Constants.INTENT_DEV_ID)
        Timber.e("Device ID $deviceId")
        mDevice = TuyaHomeSdk.newDeviceInstance(deviceId)
        val deviceBean = TuyaHomeSdk.getDataInstance().getDeviceBean(deviceId)
        val roomBean = TuyaHomeSdk.getDataInstance().getDeviceRoomBean(deviceId)

        tvRemove?.let {
            val manageRoomString = SpannableString(it.text)
            manageRoomString.setSpan(UnderlineSpan(), 0, manageRoomString.length, 0)
            it.text = manageRoomString
        }
        tvDeviceName?.text = deviceBean?.name
        tvTitle?.visibility = View.GONE
        ivEdit?.setOnClickListener {
            supportFragmentManager.let {
                val notices = EditRoomDialogFragment(
                    getString(R.string.device_add),
                    tvDeviceName.text.toString(),
                    object : EditRoomDialogFragment.OnEditNameListener {
                        override fun onSaveClick(name: String) {
                            showProgressView()
                            mDevice?.renameDevice(name, object : IResultCallback {
                                override fun onError(code: String?, error: String?) {
                                    showToastErrorAlertNotices(error ?: "", null)
                                }

                                override fun onSuccess() {
                                    showToastSuccessAlertNotices(getString(R.string.msg_successfully),
                                        object : AlertUtil.OnSuccessListener {
                                            override fun onSuccess() {
                                                val intent = Intent()
                                                intent.putExtra("ACTION", "RENAME")
                                                intent.putExtra("NAME", name)
                                                setResult(RESULT_OK, intent)
                                                finish()
                                            }
                                        })
                                }
                            })

                        }
                    })
                notices.isCancelable = false
                notices.show(
                    it,
                    "showAlertNotices"
                )
            }
        }
        ivDevice?.let { iv ->
            Glide
                .with(this@DeviceDetailActivity)
                .load(deviceBean?.iconUrl)
                .into(iv)
        }
        roomBean?.let {
            tvRoom?.text = it.name
        }
        btnResetFactory.visibility = View.GONE
        TuyaHomeSdk.getDataInstance().getSchema(deviceId)?.entries?.sortedBy { it.value.id }
            ?.associate { it.toPair() }?.let { map ->
                for (bean in map.values) {
                    if (bean?.code == "factory_reset") {
                        val value = deviceBean?.dps?.get(bean.id)
                        if (value == 1)
                            btnDeviceUpdate.visibility = View.VISIBLE
                        else
                            btnDeviceUpdate.visibility = View.GONE
                    }
                }
            }

        btnDeviceUpdate.setOnClickListener {
            /*showAlertConfirmWithActionClick(
                "ต้องการลบห้องใช่หรือไม่",
                "ห้อง ".plus(tvRoomName?.text.toString()),
                object : AlertUtil.OnSuccessListener {
                    override fun onSuccess() {
                        mDevice.resetFactory(object : IResultCallback {
                            override fun onSuccess() {
                                finish()
                            }

                            override fun onError(code: String?, error: String?) {

                            }
                        })
                    }
                }, R.color.red
            )*/
        }

        btnDeviceInfo?.setOnClickListener {
            val intent = Intent(this@DeviceDetailActivity, DeviceInfoActivity::class.java)
            intent.putExtra(Constants.INTENT_DEV_ID, deviceId)
            startActivity(intent)
        }

        btnResetFactory?.setOnClickListener {
            showAlertConfirmWithActionClick(
                "ต้องการตั้งค่าจากโรงงานหรือไม่",
                "",
                object : AlertUtil.OnSuccessListener {
                    override fun onSuccess() {
                        mDevice?.resetFactory(object : IResultCallback {
                            override fun onSuccess() {
                                val intent = Intent()
                                intent.putExtra("ACTION", "RESET_FACTORY")
                                setResult(RESULT_OK, intent)
                                finish()
                            }

                            override fun onError(code: String?, error: String?) {

                            }

                        })
                    }
                }, R.color.red
            )
        }

        btnRemove?.setOnClickListener {
            /*showAlertConfirmWithActionClick(
                "ต้องการลบอุปกรณ์ใช่หรือไม่",
                "อุปกรณ์ ".plus(tvDeviceName?.text.toString()),
                object : AlertUtil.OnSuccessListener {
                    override fun onSuccess() {
                        mDevice?.removeDevice(object : IResultCallback {
                            override fun onSuccess() {
                                val intent = Intent()
                                intent.putExtra("ACTION", "REMOVE")
                                setResult(RESULT_OK, intent)
                                finish()
                            }

                            override fun onError(code: String?, error: String?) {

                            }

                        })
                    }
                }, R.color.red
            )*/


            val dialogView: View? =
                layoutInflater.inflate(R.layout.alert_disconnect, null)
            dialogView?.let { view ->
                dialog = BottomSheetDialog(this)
                dialog?.setContentView(view)
                val btnCancel = view.findViewById<TextView?>(R.id.tvCancel)
                val tvDisConnect = view.findViewById<TextView?>(R.id.tvDisConnect)
                val tvDisConnectWithWipe = view.findViewById<TextView?>(R.id.tvDisConnectWithWipe)
                btnCancel?.setOnClickListener { if (dialog?.isShowing == true) dialog?.dismiss() }

                tvDisConnect?.setOnClickListener {
                    showAlertConfirmWithActionClick(
                        "Are you sure to Disconnect",
                        "Device ".plus(tvDeviceName?.text.toString()),
                        object : AlertUtil.OnSuccessListener {
                            override fun onSuccess() {
                                mDevice?.removeDevice(object : IResultCallback {
                                    override fun onSuccess() {
                                        val intent = Intent()
                                        intent.putExtra("ACTION", "REMOVE")
                                        setResult(RESULT_OK, intent)
                                        finish()
                                    }

                                    override fun onError(code: String?, error: String?) {

                                    }

                                })
                            }
                        }, R.color.red
                    )
                }

                tvDisConnectWithWipe?.setOnClickListener {
                    showAlertConfirmWithActionClick(
                        "Are you sure to Disconnect and wipe data",
                        "Device ".plus(tvDeviceName?.text.toString()),
                        object : AlertUtil.OnSuccessListener {
                            override fun onSuccess() {
                                mDevice?.resetFactory(object : IResultCallback {
                                    override fun onSuccess() {
                                        val intent = Intent()
                                        intent.putExtra("ACTION", "REMOVE")
                                        setResult(RESULT_OK, intent)
                                        finish()
                                    }

                                    override fun onError(code: String?, error: String?) {

                                    }

                                })
                            }
                        }, R.color.red
                    )
                }
            }

            dialog?.show()
        }

    }

    private var iTuyaOta: ITuyaOta? = null
    private val mDevId = "your_devId"

    fun startOTA() {
        initOta()
        initListener()
        checkOTAInfo()
    }

    //Initialize the firmware information
    private fun initOta() {
        iTuyaOta = TuyaHomeSdk.newOTAInstance(mDevId)
    }

    private fun initListener() {
        iTuyaOta?.setOtaListener(object : IOtaListener {

            override fun onSuccess(otaType: Int) {
                iTuyaOta?.onDestroy()
            }

            override fun onFailure(otaType: Int, code: String?, error: String?) {
                iTuyaOta?.onDestroy()
            }

            override fun onFailureWithText(
                otaType: Int,
                code: String?,
                messageBean: OTAErrorMessageBean?
            ) {
                iTuyaOta?.onDestroy()
            }

            override fun onProgress(otaType: Int, progress: Int) {

            }

            override fun onTimeout(otaType: Int) {
                iTuyaOta?.onDestroy()
            }


            override fun onStatusChanged(otaStatus: Int, otaType: Int) {

            }

        });
    }

    private fun checkOTAInfo() {
        iTuyaOta?.getOtaInfo(object : IGetOtaInfoCallback {

            override fun onSuccess(upgradeInfoBeans: MutableList<UpgradeInfoBean>?) {
                iTuyaOta?.startOta()
            }

            override fun onFailure(code: String?, error: String?) {

            }
        });
    }

    private fun hasHardwareUpdate(list: List<UpgradeInfoBean>?): Boolean {
        if (null == list || list.isEmpty()) return false
        for (upgradeInfoBean in list) {
            if (upgradeInfoBean.upgradeStatus == 1) return true
        }
        return false
    }

    override fun onDestroy() {
        super.onDestroy()
        mDevice?.onDestroy()
    }
}