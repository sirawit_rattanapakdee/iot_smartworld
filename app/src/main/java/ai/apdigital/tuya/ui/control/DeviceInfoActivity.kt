package ai.apdigital.tuya.ui.control

import ai.apdigital.tuya.R
import ai.apdigital.tuya.base.BaseActivity
import ai.apdigital.tuya.base.Constants
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import com.tuya.smart.home.sdk.TuyaHomeSdk
import com.tuya.smart.sdk.api.ITuyaDevice
import com.tuya.smart.sdk.api.WifiSignalListener
import kotlinx.android.synthetic.main.activity_device_information.*


class DeviceInfoActivity : BaseActivity() {
    override fun getLayoutView() = R.layout.activity_device_information

    override fun bindView() {

    }

    override fun setupInstance() {
    }

    override fun setupView() {
        setupToolbar(
            getString(R.string.device_info),
            showButtonBack = true,
            showRightMenu = true
        )

    }

    private fun setClipboard(context: Context?, text: String?) {
        val clipboard = context?.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText("Copied Text", text)
        clipboard.setPrimaryClip(clip)
        showToastSuccessAlertNotices(getString(R.string.copied_to_the_phone_clipboard), null)
    }

    var mDevice: ITuyaDevice? = null
    override fun initialize(savedInstanceState: Bundle?) {

        val deviceId = intent.getStringExtra(Constants.INTENT_DEV_ID)
        val deviceBean = TuyaHomeSdk.getDataInstance().getDeviceBean(deviceId)
        mDevice = TuyaHomeSdk.newDeviceInstance(deviceBean?.getDevId())
        deviceBean?.let {
            tvMac?.text = getString(R.string.ty_mac_addr).plus(" : ").plus(it.mac)
            tvIP.text = getString(R.string.ty_ip_addr).plus(" : ").plus(it.ip)
            tvVirtualDevice.text = getString(R.string.virtual_dev_id).plus(" : ").plus(it.devId)
            tvTimezone.text = getString(R.string.user_time_zone).plus(" : ").plus(it.timezoneId)
        }

        mDevice?.requestWifiSignal(object : WifiSignalListener {
            override fun onSignalValueFind(signal: String) {
                tvSignalStrength.text =
                    getString(R.string.wifi_signal_strength).plus(" : ").plus(signal.plus("dBM"))
            }

            override fun onError(errorCode: String, errorMsg: String) {

            }
        })

        tvCopy?.setOnClickListener {
            setClipboard(this, deviceBean?.devId)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mDevice?.onDestroy()
    }
}