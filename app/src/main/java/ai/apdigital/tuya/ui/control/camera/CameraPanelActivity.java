package ai.apdigital.tuya.ui.control.camera;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;


import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import androidx.core.widget.ImageViewCompat;

import com.alibaba.fastjson.JSONObject;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.tuya.smart.android.camera.sdk.TuyaIPCSdk;
import com.tuya.smart.android.camera.sdk.api.ICameraConfigInfo;
import com.tuya.smart.android.camera.sdk.api.ITuyaIPCCore;
import com.tuya.smart.android.camera.sdk.api.ITuyaIPCDoorbell;
import com.tuya.smart.android.device.bean.SchemaBean;
import com.tuya.smart.camera.camerasdk.typlayer.callback.AbsP2pCameraListener;
import com.tuya.smart.camera.camerasdk.typlayer.callback.OnRenderDirectionCallback;
import com.tuya.smart.camera.camerasdk.typlayer.callback.OperationDelegateCallBack;
import com.tuya.smart.camera.ipccamerasdk.p2p.ICameraP2P;
import com.tuya.smart.camera.middleware.p2p.ITuyaSmartCameraP2P;
import com.tuya.smart.camera.middleware.widget.AbsVideoViewCallback;
import com.tuya.smart.camera.middleware.widget.TuyaCameraView;
import com.tuya.smart.home.sdk.TuyaHomeSdk;
import com.tuya.smart.sdk.api.IResultCallback;
import com.tuya.smart.sdk.api.ITuyaDevice;
import com.tuya.smart.sdk.bean.DeviceBean;
import com.tuya.smart.utils.ToastUtil;
import com.tuyasmart.camera.devicecontrol.ITuyaCameraDevice;
import com.tuyasmart.camera.devicecontrol.TuyaCameraDeviceControlSDK;
import com.tuyasmart.camera.devicecontrol.bean.DpBasicNightvision;

import java.io.File;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ai.apdigital.tuya.R;
import ai.apdigital.tuya.base.BaseActivity;
import ai.apdigital.tuya.ui.control.DeviceDetailActivity;
import ai.apdigital.tuya.base.Constants;
import ai.apdigital.tuya.ui.control.camera.playback.CameraPlaybackActivity;
import ai.apdigital.tuya.ui.control.camera.utils.DPConstants;
import ai.apdigital.tuya.ui.control.camera.utils.MessageUtil;
import ai.apdigital.tuya.ui.control.panal.ControlPanelActivity;
import ai.apdigital.tuya.utils.EventSenderUtil;
import timber.log.Timber;

import static ai.apdigital.tuya.base.Constants.ARG1_OPERATE_FAIL;
import static ai.apdigital.tuya.base.Constants.ARG1_OPERATE_SUCCESS;
import static ai.apdigital.tuya.base.Constants.MSG_GET_VIDEO_CLARITY;
import static ai.apdigital.tuya.base.Constants.MSG_SCREENSHOT;
import static androidx.core.content.FileProvider.getUriForFile;

/**
 * @author chenbj
 */
public class CameraPanelActivity extends BaseActivity implements View.OnClickListener, View.OnTouchListener {

    private static final String TAG = "CameraPanelActivity";
    private TuyaCameraView mVideoView;
    private LinearLayoutCompat btnSpeaker, boxBattery, btnQuality, btnNightMode, btnFullScreen, boxOfflineCamera, boxOfflineControl, boxTalk, boxPlayback, boxSnapPhoto, menuEdit;
    private ConstraintLayout layoutControlPanel, boxPTZ;
    private ColorStateList colorStateBlack, colorStateGray, colorStateYellow, colorStateGrayPTZ;

    private ProgressBar progressbar;
    private AppCompatImageView ivMute, ivBattery, ivMore, ivNightMode, ivQuality, btnSlideUp, btnSlideDown, btnSlideRight, btnSlideLeft, ivTalk, btnTalk, btnSnapPhoto, ivSnapPhoto, btnMore, btnPlayback;

    private TextView tvPercent;//recordTxt, tvPercent, replayTxt, cloudStorageTxt, messageCenterTxt;

    private static final int ASPECT_RATIO_WIDTH = 9;
    private static final int ASPECT_RATIO_HEIGHT = 16;
    private boolean isSpeaking = false;
    private boolean isRecording = false;
    private boolean isPlay = false;
    private int previewMute = ICameraP2P.MUTE;
    private int videoClarity = ICameraP2P.HD;

    private int p2pType;
    private String devId, deviceName;
    private ITuyaSmartCameraP2P mCameraP2P;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Constants.MSG_CONNECT:
                    handleConnect(msg);
                    break;
                case Constants.MSG_SET_CLARITY:
                    handleClarity(msg);
                    break;
                case Constants.MSG_MUTE:
                    handleMute(msg);
                    break;
                case MSG_SCREENSHOT:
                    handleSnapshot(msg);
                    break;
                case Constants.MSG_VIDEO_RECORD_BEGIN:
                    showToastSuccessAlertNotices(getString(R.string.text_video_start_record), null);
                    break;
                case Constants.MSG_VIDEO_RECORD_FAIL:
                    showToastErrorAlertNotices(getString(R.string.text_video_save_fail), null);
                    break;
                case Constants.MSG_VIDEO_RECORD_OVER:
                    handleVideoRecordOver(msg);
                    break;
                case Constants.MSG_TALK_BACK_BEGIN:
                    handleStartTalk(msg);
                    break;
                case Constants.MSG_TALK_BACK_OVER:
                    handleStopTalk(msg);
                    break;
                case MSG_GET_VIDEO_CLARITY:
                    handleGetVideoClarity(msg);
                    break;
                default:
                    break;
            }
            super.handleMessage(msg);
        }
    };

    private void handleStopTalk(Message msg) {
        if (msg.arg1 == ARG1_OPERATE_SUCCESS) {
            //    ToastUtil.shortToast(CameraPanelActivity.this, getString(R.string.operation_suc));
            ImageViewCompat.setImageTintList(ivTalk, colorStateBlack);
        } else {
            showToastErrorAlertNotices(getString(R.string.text_video_save_fail), null);
        }
    }

    private void handleStartTalk(Message msg) {
        if (msg.arg1 == ARG1_OPERATE_SUCCESS) {
            ImageViewCompat.setImageTintList(ivTalk, colorStateYellow);
//            ToastUtil.shortToast(CameraPanelActivity.this, getString(R.string.operation_suc));
        } else {
            showToastErrorAlertNotices(getString(R.string.text_video_save_fail), null);
        }
    }

    private void handleVideoRecordOver(Message msg) {
        if (msg.arg1 == ARG1_OPERATE_SUCCESS) {
            showToastSuccessAlertNotices(getString(R.string.text_video_save_success), null);
        } else {
            showToastErrorAlertNotices(getString(R.string.text_video_save_fail), null);
        }
    }

    private void handleSnapshot(Message msg) {
        if (msg.arg1 == ARG1_OPERATE_SUCCESS) {
            showToastSuccessAlertNotices(getString(R.string.text_screen_shot_success), null);
        } else {
            showToastErrorAlertNotices(getString(R.string.text_screen_shot_fail), null);
        }
    }

    private void handleMute(Message msg) {
        if (msg.arg1 == ARG1_OPERATE_SUCCESS) {
            ImageViewCompat.setImageTintList(ivMute, previewMute == ICameraP2P.MUTE ? colorStateGray : colorStateBlack);
        } else {
            showToastErrorAlertNotices(getString(R.string.fail), null);
        }
    }


    private void handleClarity(Message msg) {
        if (msg.arg1 == ARG1_OPERATE_SUCCESS) {
            ImageViewCompat.setImageTintList(ivQuality, videoClarity == ICameraP2P.HD ? colorStateBlack : colorStateGray);
        } else {
            showToastErrorAlertNotices(getString(R.string.fail), null);
        }
    }

    private void handleConnect(Message msg) {
        progressbar.setVisibility(View.GONE);
        if (msg.arg1 == ARG1_OPERATE_SUCCESS) {
            boxOfflineCamera.setVisibility(View.GONE);
            boxOfflineControl.setVisibility(View.GONE);
            btnSnapPhoto.setEnabled(true);
            boxSnapPhoto.setAlpha(1f);
            //boxPlayback.setAlpha(1f);
            ImageViewCompat.setImageTintList(ivSnapPhoto, colorStateBlack);
            preview();
        } else {
            btnSnapPhoto.setEnabled(false);
            boxSnapPhoto.setAlpha(0.3f);
            //boxPlayback.setAlpha(0.5f);
            boxOfflineCamera.setVisibility(View.VISIBLE);
            boxOfflineControl.setVisibility(View.VISIBLE);
            //ToastUtil.shortToast(CameraPanelActivity.this, getString(R.string.connect_failed));
        }
    }

    private void handleGetVideoClarity(Message msg) {
        if (msg.arg1 == Constants.ARG1_OPERATE_SUCCESS) {
            if (videoClarity == ICameraP2P.HD) {
                ImageViewCompat.setImageTintList(ivQuality, colorStateBlack);
            } else if (videoClarity == ICameraP2P.STANDEND) {
                ImageViewCompat.setImageTintList(ivQuality, colorStateGray);
            }//ToastUtil.shortToast(CameraPanelActivity.this, getString(R.string.get_current_clarity) + info);
        } /*else {
            //ToastUtil.shortToast(CameraPanelActivity.this, getString(R.string.operation_failed));
        }*/
    }


    public void videoSizeFitWidthChange() {
        WindowManager windowManager = (WindowManager) this.getSystemService(WINDOW_SERVICE);
        int width = windowManager.getDefaultDisplay().getWidth();
        int height = width * ASPECT_RATIO_WIDTH / ASPECT_RATIO_HEIGHT;
        LinearLayoutCompat.LayoutParams layoutParams = new LinearLayoutCompat.LayoutParams(width, height);
        findViewById(R.id.camera_video_view_Rl).setLayoutParams(layoutParams);
    }


    public void videoSizeFitHeightChange() {
        //WindowManager windowManager = (WindowManager) this.getSystemService(WINDOW_SERVICE);
        int height = LinearLayoutCompat.LayoutParams.MATCH_PARENT;//windowManager.getDefaultDisplay().getHeight();
        int width = LinearLayoutCompat.LayoutParams.MATCH_PARENT;//height * ASPECT_RATIO_HEIGHT | ASPECT_RATIO_WIDTH;
        LinearLayoutCompat.LayoutParams layoutParams = new LinearLayoutCompat.LayoutParams(width, height);
        findViewById(R.id.camera_video_view_Rl).setLayoutParams(layoutParams);
    }

    private void showNotSupportToast() {
        ToastUtil.shortToast(CameraPanelActivity.this, getString(R.string.not_support_device));
    }

    private void preview() {
        if (querySupportByDPID(DPConstants.PTZ_CONTROL)) {
            boxPTZ.setAlpha(1f);
            btnSlideUp.setEnabled(true);
            btnSlideDown.setEnabled(true);
            btnSlideLeft.setEnabled(true);
            btnSlideRight.setEnabled(true);
            if (mVideoView == null)
                return;
            mVideoView.setOnRenderDirectionCallback(new OnRenderDirectionCallback() {

                @Override
                public void onLeft() {
                    EventSenderUtil.sendEvent(EventSenderUtil.EVENT, EventSenderUtil.TUYA_PANEL_CCTV_CONTROL, "left");
                    publishDps(DPConstants.PTZ_CONTROL, DPConstants.PTZ_LEFT);
                }

                @Override
                public void onRight() {
                    EventSenderUtil.sendEvent(EventSenderUtil.EVENT, EventSenderUtil.TUYA_PANEL_CCTV_CONTROL, "right");
                    publishDps(DPConstants.PTZ_CONTROL, DPConstants.PTZ_RIGHT);
                }

                @Override
                public void onUp() {
                    EventSenderUtil.sendEvent(EventSenderUtil.EVENT, EventSenderUtil.TUYA_PANEL_CCTV_CONTROL, "up");
                    publishDps(DPConstants.PTZ_CONTROL, DPConstants.PTZ_UP);
                }

                @Override
                public void onDown() {
                    EventSenderUtil.sendEvent(EventSenderUtil.EVENT, EventSenderUtil.TUYA_PANEL_CCTV_CONTROL, "down");
                    publishDps(DPConstants.PTZ_CONTROL, DPConstants.PTZ_DOWN);
                }

                @Override
                public void onCancel() {
                    publishDps(DPConstants.PTZ_STOP, true);
                }
            });
        } else {
            boxPTZ.setAlpha(0.5f);
        }

        ITuyaIPCCore cameraInstance = TuyaIPCSdk.getCameraInstance();
        if (cameraInstance != null) {
            ICameraConfigInfo cameraConfig = cameraInstance.getCameraConfig(devId);
            if (cameraConfig != null) {
                if (cameraConfig.isSupportChangeTalkBackMode()) {
                    btnTalk.setEnabled(true);
                    boxTalk.setAlpha(1f);
                    ImageViewCompat.setImageTintList(ivTalk, colorStateBlack);
                }
                btnSpeaker.setEnabled(cameraConfig.isSupportSpeaker());
                ImageViewCompat.setImageTintList(ivQuality, cameraConfig.getDefaultDefinition() == ICameraP2P.HD ? colorStateBlack : colorStateGray);
            }
        }

        if (mCameraP2P == null)
            return;
        mCameraP2P.startPreview(videoClarity, new OperationDelegateCallBack() {
            @Override
            public void onSuccess(int sessionId, int requestId, String data) {
                Log.d(TAG, "start preview onSuccess");
                isPlay = true;
            }

            @Override
            public void onFailure(int sessionId, int requestId, int errCode) {
                Log.d(TAG, "start preview onFailure, errCode: " + errCode);
                isPlay = false;
            }
        });
    }


    private void recordClick() {
        if (!isRecording) {
            String picPath = getExternalFilesDir(null).getPath() + "/" + devId;
            File file = new File(picPath);
            if (!file.exists()) {
                file.mkdirs();
            }
            String fileName = System.currentTimeMillis() + ".mp4";
            mCameraP2P.startRecordLocalMp4(picPath, fileName, CameraPanelActivity.this, new OperationDelegateCallBack() {
                @Override
                public void onSuccess(int sessionId, int requestId, String data) {
                    isRecording = true;
                    mHandler.sendEmptyMessage(Constants.MSG_VIDEO_RECORD_BEGIN);
                    //returns the recorded thumbnail path （.jpg）
                    Log.i(TAG, "record :" + data);
                }

                @Override
                public void onFailure(int sessionId, int requestId, int errCode) {
                    mHandler.sendEmptyMessage(Constants.MSG_VIDEO_RECORD_FAIL);
                }
            });
            recordStatue(true);
        } else {
            mCameraP2P.stopRecordLocalMp4(new OperationDelegateCallBack() {
                @Override
                public void onSuccess(int sessionId, int requestId, String data) {
                    isRecording = false;
                    mHandler.sendMessage(MessageUtil.getMessage(Constants.MSG_VIDEO_RECORD_OVER, ARG1_OPERATE_SUCCESS));
                }

                @Override
                public void onFailure(int sessionId, int requestId, int errCode) {
                    isRecording = false;
                    mHandler.sendMessage(MessageUtil.getMessage(Constants.MSG_VIDEO_RECORD_OVER, ARG1_OPERATE_FAIL));
                }
            });
            recordStatue(false);
        }
    }

    private Uri getCacheImagePath(String fileName) {
        File path = new File(getExternalCacheDir(), "camera");
        if (!path.exists()) path.mkdirs();
        File image = new File(path, fileName);
        return getUriForFile(this, getPackageName() + ".provider", image);
    }


    /*private fun getFilePath():

    File {
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
                ?:context ?.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)
                ?:Environment.getRootDirectory()
    }*/


    private void snapShotClick() {

        String picPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getAbsolutePath();
        Timber.e("picPath :: %s", picPath);
        File file = new File(picPath, "Smartworld");
        if (!file.exists())
            file.mkdirs();

        mCameraP2P.snapshot(picPath + "/Smartworld/", CameraPanelActivity.this, new OperationDelegateCallBack() {
            @Override
            public void onSuccess(int sessionId, int requestId, String data) {
                mHandler.sendMessage(MessageUtil.getMessage(MSG_SCREENSHOT, ARG1_OPERATE_SUCCESS));
            }

            @Override
            public void onFailure(int sessionId, int requestId, int errCode) {
                mHandler.sendMessage(MessageUtil.getMessage(MSG_SCREENSHOT, ARG1_OPERATE_FAIL));
            }
        });
    }

    private void muteClick() {
        int mute;
        mute = previewMute == ICameraP2P.MUTE ? ICameraP2P.UNMUTE : ICameraP2P.MUTE;
        mCameraP2P.setMute(mute, new OperationDelegateCallBack() {
            @Override
            public void onSuccess(int sessionId, int requestId, String data) {
                previewMute = Integer.valueOf(data);
                mHandler.sendMessage(MessageUtil.getMessage(Constants.MSG_MUTE, ARG1_OPERATE_SUCCESS));
            }

            @Override
            public void onFailure(int sessionId, int requestId, int errCode) {
                mHandler.sendMessage(MessageUtil.getMessage(Constants.MSG_MUTE, ARG1_OPERATE_FAIL));
            }
        });
    }

    private void speakClick() {
        if (isSpeaking) {
            mCameraP2P.stopAudioTalk(new OperationDelegateCallBack() {
                @Override
                public void onSuccess(int sessionId, int requestId, String data) {
                    isSpeaking = false;
                    mHandler.sendMessage(MessageUtil.getMessage(Constants.MSG_TALK_BACK_OVER, ARG1_OPERATE_SUCCESS));
                }

                @Override
                public void onFailure(int sessionId, int requestId, int errCode) {
                    isSpeaking = false;
                    mHandler.sendMessage(MessageUtil.getMessage(Constants.MSG_TALK_BACK_OVER, ARG1_OPERATE_FAIL));

                }
            });
        } else {
            if (Constants.hasRecordPermission()) {
                mCameraP2P.startAudioTalk(new OperationDelegateCallBack() {
                    @Override
                    public void onSuccess(int sessionId, int requestId, String data) {
                        isSpeaking = true;
                        mHandler.sendMessage(MessageUtil.getMessage(Constants.MSG_TALK_BACK_BEGIN, ARG1_OPERATE_SUCCESS));
                    }

                    @Override
                    public void onFailure(int sessionId, int requestId, int errCode) {
                        isSpeaking = false;
                        mHandler.sendMessage(MessageUtil.getMessage(Constants.MSG_TALK_BACK_BEGIN, ARG1_OPERATE_FAIL));
                    }
                });
            } else {
                Constants.requestPermission(CameraPanelActivity.this, Manifest.permission.RECORD_AUDIO, Constants.EXTERNAL_AUDIO_REQ_CODE, "open_recording");
            }
        }
    }

    private void setVideoClarity() {
        mCameraP2P.setVideoClarity(videoClarity == ICameraP2P.HD ? ICameraP2P.STANDEND : ICameraP2P.HD, new OperationDelegateCallBack() {
            @Override
            public void onSuccess(int sessionId, int requestId, String data) {
                videoClarity = Integer.valueOf(data);
                mHandler.sendMessage(MessageUtil.getMessage(Constants.MSG_SET_CLARITY, ARG1_OPERATE_SUCCESS));
            }

            @Override
            public void onFailure(int sessionId, int requestId, int errCode) {
                mHandler.sendMessage(MessageUtil.getMessage(Constants.MSG_SET_CLARITY, ARG1_OPERATE_FAIL));
            }
        });
    }

    private void recordStatue(boolean isRecording) {
        ivTalk.setEnabled(!isRecording);
        ivSnapPhoto.setEnabled(!isRecording);
        /*replayTxt.setEnabled(!isRecording);
        recordTxt.setEnabled(true);
        recordTxt.setSelected(isRecording);*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        progressbar.setVisibility(View.VISIBLE);
        handler.postDelayed(resumeRun, 1000L);
    }

    Handler handler = new Handler();
    private Runnable resumeRun = new Runnable() {
        @Override
        public void run() {

            mVideoView.onResume();
            //must register again,or can't callback
            if (null != mCameraP2P) {
                //AudioUtils.getModel(CameraPanelActivity.this);
                mCameraP2P.registerP2PCameraListener(p2pCameraListener);
                mCameraP2P.generateCameraView(mVideoView.createdView());
                if (mCameraP2P.isConnecting()) {
                    progressbar.setVisibility(View.GONE);
                    boxOfflineCamera.setVisibility(View.GONE);
                    boxOfflineControl.setVisibility(View.GONE);
                    btnSnapPhoto.setEnabled(true);
                    boxSnapPhoto.setAlpha(1f);
                    //boxPlayback.setAlpha(1f);
                    ImageViewCompat.setImageTintList(ivSnapPhoto, colorStateBlack);
                    preview();/*
                mCameraP2P.startPreview(new OperationDelegateCallBack() {
                    @Override
                    public void onSuccess(int sessionId, int requestId, String data) {
                        isPlay = true;
                    }

                    @Override
                    public void onFailure(int sessionId, int requestId, int errCode) {
                        Log.d(TAG, "start preview onFailure, errCode: " + errCode);
                    }
                });*/
                } else {

                    ITuyaIPCCore cameraInstance = TuyaIPCSdk.getCameraInstance();
                    if (cameraInstance != null && cameraInstance.isLowPowerDevice(devId)) {
                        ITuyaIPCDoorbell doorbell = TuyaIPCSdk.getDoorbell();
                        if (doorbell != null) {
                            doorbell.wirelessWake(devId);
                        }
                    }
                    mCameraP2P.connect(devId, new OperationDelegateCallBack() {
                        @Override
                        public void onSuccess(int i, int i1, String s) {
                            Log.d(TAG, "start connect, onSuccess");
                            mHandler.sendMessage(MessageUtil.getMessage(Constants.MSG_CONNECT, ARG1_OPERATE_SUCCESS));
                        }

                        @Override
                        public void onFailure(int i, int i1, int i2) {
                            Log.d(TAG, "start connect, onFailure");
                            mHandler.sendMessage(MessageUtil.getMessage(Constants.MSG_CONNECT, ARG1_OPERATE_FAIL));
                        }
                    });
                } /*else {
                boxOfflineCamera.setVisibility(View.GONE);
                boxOfflineControl.setVisibility(View.GONE);
                btnSnapPhoto.setEnabled(true);
                boxSnapPhoto.setAlpha(1f);
                //boxPlayback.setAlpha(1f);
                ImageViewCompat.setImageTintList(ivSnapPhoto, colorStateBlack);
                preview();
            }*/
            }
        }
    };
    private AbsP2pCameraListener p2pCameraListener = new AbsP2pCameraListener() {
        @Override
        public void onReceiveSpeakerEchoData(ByteBuffer pcm, int sampleRate) {
            if (null != mCameraP2P) {
                int length = pcm.capacity();
                Log.d(TAG, "receiveSpeakerEchoData pcmlength " + length + " sampleRate " + sampleRate);
                byte[] pcmData = new byte[length];
                pcm.get(pcmData, 0, length);
                mCameraP2P.sendAudioTalkData(pcmData, length);
            }
        }
    };

    @Override
    protected void onPause() {
        super.onPause();
        handler.removeCallbacks(resumeRun);
        mVideoView.onPause();
        if (null != mCameraP2P) {
            if (isSpeaking) {
                mCameraP2P.stopAudioTalk(null);
            }
            if (isPlay) {
                mCameraP2P.stopPreview(new OperationDelegateCallBack() {
                    @Override
                    public void onSuccess(int sessionId, int requestId, String data) {

                    }

                    @Override
                    public void onFailure(int sessionId, int requestId, int errCode) {

                    }
                });
                isPlay = false;
            }
            mCameraP2P.removeOnP2PCameraListener();
            mCameraP2P.disconnect(new OperationDelegateCallBack() {
                @Override
                public void onSuccess(int i, int i1, String s) {

                }

                @Override
                public void onFailure(int i, int i1, int i2) {

                }
            });
        }
       // AudioUtils.changeToNomal(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (iTuyaDevice != null)
            iTuyaDevice.onDestroy();
        if (null != mHandler) {
            mHandler.removeCallbacksAndMessages(null);
        }
        if (null != mCameraP2P) {
            mCameraP2P.destroyP2P();
        }

        mCameraP2P = null;
        mVideoView = null;
    }


    private boolean querySupportByDPID(String dpId) {
        DeviceBean deviceBean = TuyaHomeSdk.getDataInstance().getDeviceBean(devId);
        if (deviceBean != null) {
            Map<String, Object> dps = deviceBean.getDps();
            return dps != null && dps.get(dpId) != null;
        }
        return false;
    }

    private ITuyaDevice iTuyaDevice;

    private void publishDps(String dpId, Object value) {
        if (iTuyaDevice == null) {
            iTuyaDevice = TuyaHomeSdk.newDeviceInstance(devId);
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(dpId, value);
        String dps = jsonObject.toString();
        iTuyaDevice.publishDps(dps, new IResultCallback() {
            @Override
            public void onError(String code, String error) {
                Log.e(TAG, "publishDps err " + dps);
            }

            @Override
            public void onSuccess() {
                Log.i(TAG, "publishDps suc " + dps);
            }
        });
    }

    private void unBindDevice() {
        TuyaHomeSdk.newDeviceInstance(devId).removeDevice(new IResultCallback() {
            @Override
            public void onError(String s, String s1) {
                ToastUtil.shortToast(CameraPanelActivity.this, s1);
            }

            @Override
            public void onSuccess() {
                mHandler.removeCallbacksAndMessages(null);
                CameraPanelActivity.this.finish();
            }
        });
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        int id = v.getId();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (id == R.id.btnSlideUp) {
                    publishDps(DPConstants.PTZ_CONTROL, DPConstants.PTZ_UP);
                    ImageViewCompat.setImageTintList(btnSlideUp, colorStateYellow);
                    EventSenderUtil.sendEvent(EventSenderUtil.EVENT, EventSenderUtil.TUYA_PANEL_CCTV_CONTROL, "up");
                } else if (id == R.id.btnSlideDown) {
                    publishDps(DPConstants.PTZ_CONTROL, DPConstants.PTZ_DOWN);
                    EventSenderUtil.sendEvent(EventSenderUtil.EVENT, EventSenderUtil.TUYA_PANEL_CCTV_CONTROL, "down");
                    ImageViewCompat.setImageTintList(btnSlideDown, colorStateYellow);
                } else if (id == R.id.btnSlideLeft) {
                    ImageViewCompat.setImageTintList(btnSlideLeft, colorStateYellow);
                    EventSenderUtil.sendEvent(EventSenderUtil.EVENT, EventSenderUtil.TUYA_PANEL_CCTV_CONTROL, "left");
                    publishDps(DPConstants.PTZ_CONTROL, DPConstants.PTZ_RIGHT);
                } else if (id == R.id.btnSlideRight) {
                    ImageViewCompat.setImageTintList(btnSlideRight, colorStateYellow);
                    EventSenderUtil.sendEvent(EventSenderUtil.EVENT, EventSenderUtil.TUYA_PANEL_CCTV_CONTROL, "right");
                    publishDps(DPConstants.PTZ_CONTROL, DPConstants.PTZ_LEFT);
                }
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                publishDps(DPConstants.PTZ_STOP, true);
                if (id == R.id.btnSlideUp) {
                    ImageViewCompat.setImageTintList(btnSlideUp, colorStateGrayPTZ);
                } else if (id == R.id.btnSlideDown) {
                    ImageViewCompat.setImageTintList(btnSlideDown, colorStateGrayPTZ);
                } else if (id == R.id.btnSlideLeft) {
                    ImageViewCompat.setImageTintList(btnSlideLeft, colorStateGrayPTZ);
                } else if (id == R.id.btnSlideRight) {
                    ImageViewCompat.setImageTintList(btnSlideRight, colorStateGrayPTZ);
                }
                break;
        }
        return true;
    }


    @Override
    public int getLayoutView() {
        return R.layout.activity_camera_panel;
    }

    @Override
    public void bindView() {
        initView();

    }

    @Override
    public void setupInstance() {

    }

    @Override
    public void setupView() {

    }

    @Override
    public void initialize(@org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        Timber.e("initialize1 ");
        EventSenderUtil.sendEvent(EventSenderUtil.PAGE, EventSenderUtil.TUYA_PANEL_CCTV_PAGE, "");
        Timber.e("initialize2");
        initData();
        Timber.e("initialize3");
        initListener();
        Timber.e("initialize4");
        findViewById(R.id.boxBack).setOnClickListener(v -> finish());
        ((AppCompatTextView) findViewById(R.id.tvTitle)).setText(deviceName);
        // EventUtils.sendEvent(EventUtils.TYPE_PAGE, PageUtils.TUYA_PANEL_CCTV, "")
    }


    private void initView() {
        //circlePTZ = findViewById(R.id.circlePTZ);
    
        ivBattery = findViewById(R.id.ivBattery);
        tvPercent = findViewById(R.id.tvPercent);
        boxBattery = findViewById(R.id.boxBattery);
        boxPTZ = findViewById(R.id.boxPTZ);
        boxOfflineCamera = findViewById(R.id.boxOfflineCamera);
        layoutControlPanel = findViewById(R.id.layoutControlPanel);
        boxOfflineControl = findViewById(R.id.boxOfflineControl);
        mVideoView = findViewById(R.id.camera_video_view);
        btnSlideUp = findViewById(R.id.btnSlideUp);
        btnSlideDown = findViewById(R.id.btnSlideDown);
        btnSlideRight = findViewById(R.id.btnSlideRight);
        btnSlideLeft = findViewById(R.id.btnSlideLeft);
        ivMute = findViewById(R.id.ivMute);
        ivQuality = findViewById(R.id.ivQuality);
        btnTalk = findViewById(R.id.btnTalk);
        ivTalk = findViewById(R.id.ivTalk);
        boxTalk = findViewById(R.id.boxTalk);
        ivSnapPhoto = findViewById(R.id.ivSnapPhoto);
        btnSnapPhoto = findViewById(R.id.btnSnapPhoto);
        btnPlayback = findViewById(R.id.btnPlayback);
        boxSnapPhoto = findViewById(R.id.boxSnapPhoto);
        boxPlayback = findViewById(R.id.boxPlayback);
        btnMore = findViewById(R.id.btnMore);
        btnSpeaker = findViewById(R.id.btnSpeaker);
        btnQuality = findViewById(R.id.btnQuality);
        menuEdit = findViewById(R.id.menuEdit);
        btnFullScreen = findViewById(R.id.btnFullScreen);
        btnNightMode = findViewById(R.id.btnNightMode);
        progressbar = findViewById(R.id.progressbar);
        //recordTxt = findViewById(R.id.record_Txt);
       // replayTxt = findViewById(R.id.replay_Txt);

        ivNightMode = findViewById(R.id.ivNightMode);
        //findViewById(R.id.get_clarity_Txt).setOnClickListener(this);

        
        //cloudStorageTxt = findViewById(R.id.cloud_Txt);
        //messageCenterTxt = findViewById(R.id.message_center_Txt);
        colorStateGray = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.gray_camera));
        
        colorStateBlack = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.black));
        
        colorStateYellow = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.yellow));
        
        colorStateGrayPTZ = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.gray_ptz));


        
        ivMute.setSelected(true);
        videoSizeFitWidthChange();
        
        boxPTZ.setEnabled(false);
        boxPTZ.setAlpha(0.5f);

        btnTalk.setEnabled(false);
        boxTalk.setAlpha(0.3f);
        //ImageViewCompat.setImageTintList(ivTalk, colorStateBlack);


        btnSnapPhoto.setEnabled(false);
        boxSnapPhoto.setAlpha(0.3f);
        //ImageViewCompat.setImageTintList(ivSnapPhoto, colorStateBlack);
        //boxPlayback.setAlpha(0.3f);

        btnSlideUp.setEnabled(false);
        btnSlideDown.setEnabled(false);
        btnSlideLeft.setEnabled(false);
        btnSlideRight.setEnabled(false);
        
    }

    private void initData() {
        /*SpannableString allSceneString = new SpannableString(tvSetting.getText());
        allSceneString.setSpan(new UnderlineSpan(), 0, allSceneString.length(), 0);
        tvSetting.setText(allSceneString);*/
        devId = getIntent().getStringExtra(Constants.INTENT_DEV_ID);
        deviceName = getIntent().getStringExtra(Constants.INTENT_DEVICE_NAME);
        ITuyaIPCCore cameraInstance = TuyaIPCSdk.getCameraInstance();
        if (cameraInstance != null) {
            mCameraP2P = cameraInstance.createCameraP2P(devId);
            p2pType = cameraInstance.getP2PType(devId);
        }
        mVideoView.setViewCallback(new AbsVideoViewCallback() {
            @Override
            public void onCreated(Object o) {
                super.onCreated(o);
                if (null != mCameraP2P) {
                    mCameraP2P.generateCameraView(o);
                }
            }
        });
        boolean haveBattery = false;
        Map<String, SchemaBean> data = TuyaHomeSdk.getDataInstance().getSchema(devId);
        for (SchemaBean bean : data.values()) {
            DeviceBean deviceBean = TuyaHomeSdk.getDataInstance().getDeviceBean(devId);
            if (bean.code.toLowerCase().equalsIgnoreCase("battery_percentage") || bean.code.toLowerCase().equalsIgnoreCase("battery")) {
                haveBattery = true;
                boxBattery.setVisibility(View.VISIBLE);
                int value = (int) deviceBean.getDps().get(bean.id);
                ivBattery.setVisibility(View.VISIBLE);
                ivBattery.setImageResource(convertPercentToDrawable(value));
                tvPercent.setText(String.valueOf(value));
            }
        }
        if (!haveBattery)
            boxBattery.setVisibility(View.GONE);


        mVideoView.createVideoView(p2pType);
        if (null == mCameraP2P) {
            showNotSupportToast();
        } else {

         /*   mCameraP2P.getVideoClarity(new OperationDelegateCallBack() {
                @Override
                public void onSuccess(int i, int i1, String s) {
                    videoClarity = Integer.parseInt(s);
                    Timber.e("currVideoClarity %s", videoClarity);
                    mHandler.sendMessage(MessageUtil.getMessage(MSG_GET_VIDEO_CLARITY, ARG1_OPERATE_SUCCESS));
                }

                @Override
                public void onFailure(int i, int i1, int i2) {
                    Timber.e("currVideoClarity Error");
                    mHandler.sendMessage(MessageUtil.getMessage(MSG_GET_VIDEO_CLARITY, ARG1_OPERATE_FAIL));
                }
            });*/

            ImageViewCompat.setImageTintList(ivMute, mCameraP2P.getMute() == ICameraP2P.MUTE ? colorStateBlack : colorStateGray);
            ImageViewCompat.setImageTintList(ivTalk, mCameraP2P.isTalking() ? colorStateYellow : colorStateBlack);
        }

        mTuyaCameraDevice = TuyaCameraDeviceControlSDK.getCameraDeviceInstance(devId);

        if (mTuyaCameraDevice.isSupportCameraDps(DpBasicNightvision.ID)) {
            String enableNight = mTuyaCameraDevice.queryStringCurrentCameraDps(DpBasicNightvision.ID);
            //ImageViewCompat.setImageTintList(ivNightMode, enableNight.equalsIgnoreCase("2") ? colorStateBlack : colorStateGray);
            int nightMode = Integer.parseInt(enableNight);
            updateNightView(nightMode);
            btnNightMode.setEnabled(true);
        } else {
            btnNightMode.setEnabled(false);
            ImageViewCompat.setImageTintList(ivNightMode, colorStateGray);
        }

    }

    private int convertPercentToDrawable(int percent) {
        if (percent <= 0) {
            return R.drawable.ic_battery_0;
        } else if (percent < 21) {
            return R.drawable.ic_battery_20;
        } else if (percent < 41) {
            return R.drawable.ic_battery_40;
        } else if (percent < 61) {
            return R.drawable.ic_battery_60;
        } else if (percent <= 81) {
            return R.drawable.ic_battery_80;
        } else {
            return R.drawable.ic_battery_100;
        }
    }

    ITuyaCameraDevice mTuyaCameraDevice = null;

    private void initListener() {
        if (mCameraP2P == null) {
            return;
        }

        btnQuality.setOnClickListener(this);
        btnSpeaker.setOnClickListener(this);
        btnTalk.setOnClickListener(this);
        //recordTxt.setOnClickListener(this);
        btnSnapPhoto.setOnClickListener(this);
        btnPlayback.setOnClickListener(this);
        menuEdit.setOnClickListener(this);
        //replayTxt.setOnClickListener(this);
        btnFullScreen.setOnClickListener(this);
        btnNightMode.setOnClickListener(this);
        //cloudStorageTxt.setOnClickListener(this);
        /*findViewById(R.id.btnSetting).setOnClickListener(this);*/
        //messageCenterTxt.setOnClickListener(this);
        btnSlideUp.setOnTouchListener(this);
        btnSlideDown.setOnTouchListener(this);
        btnSlideLeft.setOnTouchListener(this);
        btnSlideRight.setOnTouchListener(this);
        btnMore.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btnSpeaker) {
            muteClick();
        } else if (id == R.id.btnQuality) {
            setVideoClarity();
        } else if (id == R.id.btnTalk) {
            EventSenderUtil.sendEvent(EventSenderUtil.EVENT, EventSenderUtil.TUYA_PANEL_CCTV_PRESS_TO_SPEAK, "");
            speakClick();
        }/* else if (id == R.id.record_Txt) {
            recordClick();
        }*/ else if (id == R.id.btnPlayback) {
            EventSenderUtil.sendEvent(EventSenderUtil.EVENT, EventSenderUtil.TUYA_PANEL_CCTV_PLAYBACK, "");
            Intent intent = new Intent(this, CameraPlaybackActivity.class);
            intent.putExtra(Constants.INTENT_DEV_ID, devId);
            intent.putExtra(Constants.INTENT_P2P_TYPE, p2pType);
            launchPlaybackActivity.launch(intent);
        } else if (id == R.id.btnSnapPhoto) {
            EventSenderUtil.sendEvent(EventSenderUtil.EVENT, EventSenderUtil.TUYA_PANEL_CCTV_SCREENSHOT, "");
            Dexter.withContext(this).withPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE).withListener(new MultiplePermissionsListener() {
                @Override
                public void onPermissionsChecked(MultiplePermissionsReport multiplePermissionsReport) {
                    snapShotClick();
                }

                @Override
                public void onPermissionRationaleShouldBeShown(List<PermissionRequest> list, PermissionToken permissionToken) {

                }
            }).check();
        } else if (id == R.id.menuEdit) {
            EventSenderUtil.sendEvent(EventSenderUtil.EVENT, EventSenderUtil.TUYA_PANEL_CCTV_DEVICE_DETAIL, "");
            Intent intent = new Intent(this, DeviceDetailActivity.class);
            intent.putExtra(Constants.INTENT_DEV_ID, devId);
            launchSomeActivity.launch(intent);
        } else if (id == R.id.btnMore) {
            EventSenderUtil.sendEvent(EventSenderUtil.EVENT, EventSenderUtil.TUYA_PANEL_CCTV_MORE, "");
            ArrayList<String> listIgnore = new ArrayList<>();
            listIgnore.add("ptz_control");
            listIgnore.add("ptz_stop");
            listIgnore.add("basic_nightvision");
            listIgnore.add("sd_storge");
            listIgnore.add("sd_status");
            listIgnore.add("sd_format");
            listIgnore.add("sd_format_state");
            listIgnore.add("record_switch");
            listIgnore.add("record_mode");
            listIgnore.add(DpBasicNightvision.ID);
            Intent intent = new Intent(this, ControlPanelActivity.class);
            intent.putExtra(Constants.INTENT_DEV_ID, devId);
            intent.putExtra(Constants.INTENT_DEV_CAMERA, true);
            intent.putStringArrayListExtra(Constants.INTENT_IGNORE_DPS, listIgnore);
            launchMoreActivity.launch(intent);
        } else if (id == R.id.btnFullScreen) {
            setRequestedOrientation(isPortrait ? ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE : ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            // opposite the value of isPortrait
            isPortrait = !isPortrait;
        } else if (id == R.id.btnNightMode) {
            String enableNight = mTuyaCameraDevice.queryStringCurrentCameraDps(DpBasicNightvision.ID);
            //ImageViewCompat.setImageTintList(ivNightMode, enableNight.equalsIgnoreCase("2") ? colorStateBlack : colorStateGray);
            int nightMode = Integer.parseInt(enableNight);

            if (nightMode < 2)
                nightMode++;
            else
                nightMode = 0;
            updateNightView(nightMode);
        }
    }

    boolean isUpdate = false;
    // Create lanucher variable inside onAttach or onCreate or global
    ActivityResultLauncher<Intent> launchSomeActivity = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {

                if (result.getResultCode() == Activity.RESULT_OK) {
                    setResult(Activity.RESULT_OK, result.getData());
                    Intent intent = result.getData();
                    if (intent.getStringExtra("ACTION").equals("REMOVE")) {
                        setResult(RESULT_OK, intent);
                        finish();
                    } else if (intent.getStringExtra("ACTION").equals("RENAME")) {
                        isUpdate = true;
                        ((AppCompatTextView) findViewById(R.id.tvTitle)).setText(intent.getStringExtra("NAME"));
                    }
                }
            });

    ActivityResultLauncher<Intent> launchPlaybackActivity = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
            });

    ActivityResultLauncher<Intent> launchMoreActivity = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    setResult(Activity.RESULT_OK, result.getData());
                    Intent intent = result.getData();
                    if (intent.getStringExtra("ACTION").equals("REMOVE")) {
                        setResult(RESULT_OK, intent);
                        finish();
                    } else if (intent.getStringExtra("ACTION").equals("RENAME")) {
                        isUpdate = true;
                        ((AppCompatTextView) findViewById(R.id.tvTitle)).setText(intent.getStringExtra("NAME"));
                    }
                }
            });

    private void updateNightView(int nightMode) {
        if (nightMode == 0) {
            publishDps(DpBasicNightvision.ID, "0");
            ivNightMode.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.camera_ic_moon_auto));
        } else if (nightMode == 1) {
            ivNightMode.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.camera_ic_moon));
            ImageViewCompat.setImageTintList(ivNightMode, colorStateGray);
            publishDps(DpBasicNightvision.ID, "1");
        } else {
            ivNightMode.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.camera_ic_moon));
            ImageViewCompat.setImageTintList(ivNightMode, colorStateBlack);
            publishDps(DpBasicNightvision.ID, "2");
        }
    }


    private boolean isPortrait = true;

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            isPortrait = false;
            setLandscapeLayout();
            videoSizeFitHeightChange();
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            isPortrait = true;
            setPortraitLayout();
            videoSizeFitWidthChange();
        }
    }

    @Override
    public void onBackPressed() {
        if (!isPortrait) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else onBackMain();
    }

    private void setPortraitLayout() {
        showSystemUI();
        findViewById(R.id.toolbar).setVisibility(View.VISIBLE);
        layoutControlPanel.setVisibility(View.VISIBLE);
    }

    private void setLandscapeLayout() {
        hideSystemUI();
        findViewById(R.id.toolbar).setVisibility(View.GONE);
        layoutControlPanel.setVisibility(View.GONE);
    }


    private void hideSystemUI() {
        Window window = getWindow();
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.setDecorFitsSystemWindows(false);
            window.getInsetsController().setSystemBarsAppearance(
                    WindowInsetsController.APPEARANCE_LIGHT_STATUS_BARS,
                    WindowInsetsController.APPEARANCE_LIGHT_STATUS_BARS);
        } else {*/
        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        /*}*/
    }

    private void showSystemUI() {
        Window window = getWindow();
       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.setDecorFitsSystemWindows(true);
            window.getInsetsController().show(WindowInsets.Type.navigationBars());
            window.getInsetsController().show(WindowInsets.Type.statusBars());
            window.getInsetsController().setSystemBarsAppearance(
                    WindowInsetsController.APPEARANCE_LIGHT_STATUS_BARS | WindowInsetsController.APPEARANCE_LIGHT_NAVIGATION_BARS,
                    WindowInsetsController.APPEARANCE_LIGHT_STATUS_BARS
            );
        }*/

        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR | View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR);
    }
}
