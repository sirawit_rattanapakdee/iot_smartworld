package ai.apdigital.tuya.ui.control.camera;

import android.app.Application;
import android.content.Context;
import android.content.Intent;

import com.tuya.smart.android.camera.sdk.TuyaIPCSdk;
import com.tuya.smart.android.camera.sdk.api.ITuyaIPCCore;
import com.tuya.smart.sdk.bean.DeviceBean;

import ai.apdigital.tuya.base.Constants;

public final class CameraUtils {

    private CameraUtils() {
    }

    public static void init(Application application) {
        
    }

    public static boolean ipcProcess(Context context, DeviceBean device) {
        ITuyaIPCCore cameraInstance = TuyaIPCSdk.getCameraInstance();
        if (cameraInstance != null) {
            if (cameraInstance.isIPCDevice(device.getDevId())) {
                Intent intent = new Intent(context, CameraPanelActivity.class);
                intent.putExtra(Constants.INTENT_DEV_ID, device.devId);
                intent.putExtra(Constants.INTENT_DEVICE_NAME, device.name);
                context.startActivity(intent);
                return true;
            }
        }
        return false;
    }
}
