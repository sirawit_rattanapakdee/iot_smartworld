package ai.apdigital.tuya.ui.control.camera.playback;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.core.content.ContextCompat;
import androidx.core.widget.ImageViewCompat;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.niwattep.materialslidedatepicker.SlideDatePickerDialogCallback;
import com.tuya.smart.android.camera.sdk.TuyaIPCSdk;
import com.tuya.smart.android.camera.sdk.api.ITuyaIPCCore;
import com.tuya.smart.android.camera.timeline.OnBarMoveListener;
import com.tuya.smart.android.camera.timeline.OnSelectedTimeListener;
import com.tuya.smart.android.camera.timeline.TimeBean;
import com.tuya.smart.android.camera.timeline.TuyaTimelineView;
import com.tuya.smart.android.common.utils.L;
import com.tuya.smart.camera.camerasdk.typlayer.callback.AbsP2pCameraListener;
import com.tuya.smart.camera.camerasdk.typlayer.callback.OperationDelegateCallBack;
import com.tuya.smart.camera.ipccamerasdk.bean.MonthDays;
import com.tuya.smart.camera.ipccamerasdk.p2p.ICameraP2P;
import com.tuya.smart.camera.middleware.p2p.ITuyaSmartCameraP2P;
import com.tuya.smart.camera.middleware.widget.AbsVideoViewCallback;
import com.tuya.smart.camera.middleware.widget.TuyaCameraView;

import com.tuya.smart.utils.ToastUtil;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.lang.reflect.Array;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ai.apdigital.tuya.R;
import ai.apdigital.tuya.base.BaseActivity;
import ai.apdigital.tuya.base.Constants;
import ai.apdigital.tuya.ui.control.camera.bean.RecordInfoBean;
import ai.apdigital.tuya.ui.control.camera.bean.TimePieceBean;
import ai.apdigital.tuya.ui.control.camera.utils.MessageUtil;
import ai.apdigital.tuya.utils.EventSenderUtil;
import ai.apdigital.tuya.utils.alerts.CalendarDialogFragment;
import timber.log.Timber;

import static ai.apdigital.tuya.base.Constants.ARG1_OPERATE_FAIL;
import static ai.apdigital.tuya.base.Constants.ARG1_OPERATE_SUCCESS;
import static ai.apdigital.tuya.base.Constants.INTENT_DEV_ID;
import static ai.apdigital.tuya.base.Constants.INTENT_P2P_TYPE;
import static ai.apdigital.tuya.base.Constants.MSG_DATA_DATE;
import static ai.apdigital.tuya.base.Constants.MSG_DATA_DATE_BY_DAY_FAIL;
import static ai.apdigital.tuya.base.Constants.MSG_DATA_DATE_BY_DAY_SUCC;
import static ai.apdigital.tuya.base.Constants.MSG_MUTE;
import static ai.apdigital.tuya.base.Constants.MSG_SCREENSHOT;


/**
 * @author chenbj
 */
public class CameraPlaybackActivity extends BaseActivity implements View.OnClickListener, SlideDatePickerDialogCallback {

    private static final String TAG = "CameraPlaybackActivity";
    private TuyaCameraView mVideoView;
    private AppCompatImageView ivMute, ivPlay, ivRecord, btnRecord;
    private TuyaTimelineView timelineView;
    private AppCompatTextView tvRecord;
    private ProgressBar progressbar;
    private ITuyaSmartCameraP2P mCameraP2P;
    private static final int ASPECT_RATIO_WIDTH = 9;
    private static final int ASPECT_RATIO_HEIGHT = 16;
    private String devId;

    private boolean isPlayback = false;

    protected Map<String, List<String>> mBackDataMonthCache;
    protected Map<String, List<TimePieceBean>> mBackDataDayCache;
    private int mPlaybackMute = ICameraP2P.MUTE;
    private int p2pType;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Constants.MSG_VIDEO_RECORD_BEGIN:
                    ToastUtil.shortToast(CameraPlaybackActivity.this, getString(R.string.text_video_start_record));
                    break;
                case Constants.MSG_VIDEO_RECORD_FAIL:
                    showToastErrorAlertNotices(getString(R.string.fail), null);
                    break;
                case Constants.MSG_VIDEO_RECORD_OVER:
                    handleVideoRecordOver(msg);
                    break;
                case MSG_SCREENSHOT:
                    handleSnapshot(msg);
                    break;
                case MSG_MUTE:
                    handleMute(msg);
                    break;
                case MSG_DATA_DATE:
                    handleDataDate(msg);

                    break;
                case MSG_DATA_DATE_BY_DAY_SUCC:
                case MSG_DATA_DATE_BY_DAY_FAIL:
                    handleDataDay(msg);
                    break;
            }
            super.handleMessage(msg);
        }
    };


    private void handleVideoRecordOver(Message msg) {
        if (msg.arg1 == ARG1_OPERATE_SUCCESS) {
            showToastSuccessAlertNotices(getString(R.string.text_video_save_success), null);
        } else {
            showToastErrorAlertNotices(getString(R.string.text_video_save_fail), null);
        }
    }

    private void handleSnapshot(Message msg) {
        if (msg.arg1 == ARG1_OPERATE_SUCCESS) {
            showToastSuccessAlertNotices(getString(R.string.text_screen_shot_success), null);
        } else {
            showToastErrorAlertNotices(getString(R.string.text_screen_shot_fail), null);
        }
    }


    private void handleDataDay(Message msg) {
        if (msg.arg1 == ARG1_OPERATE_SUCCESS) {
            //Timepieces with data for the query day
            List<TimePieceBean> timePieceBeans = mBackDataDayCache.get(mCameraP2P.getDayKey());
            if (timePieceBeans != null) {
                List<TimeBean> timelineData = new ArrayList<>();
                for (TimePieceBean bean : timePieceBeans) {
                    TimeBean b = new TimeBean();
                    b.setStartTime(bean.getStartTime());
                    b.setEndTime(bean.getEndTime());
                    timelineData.add(b);
                }
                timelineView.setCurrentTimeConfig(timePieceBeans.get(0).getEndTime() * 1000L);
                timelineView.setRecordDataExistTimeClipsList(timelineData);
                playback(timelineData.get(0).getStartTime(), timelineData.get(0).getEndTime(), timelineData.get(0).getStartTime());
            } else {
                showErrorNoData();
            }
        } else {

        }
    }

    private void handleDataDate(Message msg) {
        if (msg.arg1 == ARG1_OPERATE_SUCCESS) {
            List<String> days = mBackDataMonthCache.get(mCameraP2P.getMonthKey());

            try {
                if (days.size() == 0) {
                    showErrorNoData();
                    showLoading(false);
                    return;
                }
                final String inputStr = dateSelect;
                if (!TextUtils.isEmpty(inputStr) && inputStr.contains("-")) {
                    String[] substring = inputStr.split("-");
                    int year = Integer.parseInt(substring[0]);
                    int mouth = Integer.parseInt(substring[1]);
                    int day = Integer.parseInt(substring[2]);
                    mCameraP2P.queryRecordTimeSliceByDay(year, mouth, day, new OperationDelegateCallBack() {
                        @Override
                        public void onSuccess(int sessionId, int requestId, String data) {
                            L.e(TAG, inputStr + " --- " + data);
                            parsePlaybackData(data);
                        }

                        @Override
                        public void onFailure(int sessionId, int requestId, int errCode) {
                            mHandler.sendEmptyMessage(MSG_DATA_DATE_BY_DAY_FAIL);
                        }
                    });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {

        }
    }

    private void parsePlaybackData(Object obj) {
        RecordInfoBean recordInfoBean = JSONObject.parseObject(obj.toString(), RecordInfoBean.class);
        if (recordInfoBean.getCount() != 0) {
            mVideoView.onResume();
            List<TimePieceBean> timePieceBeanList = recordInfoBean.getItems();
            if (timePieceBeanList != null && timePieceBeanList.size() != 0) {
                mBackDataDayCache.put(mCameraP2P.getDayKey(), timePieceBeanList);
            }
            mHandler.sendMessage(MessageUtil.getMessage(MSG_DATA_DATE_BY_DAY_SUCC, ARG1_OPERATE_SUCCESS));
        } else {
            mCameraP2P.stopPlayBack(null);
            mCameraP2P.stopPreview(null);
            mVideoView.onPause();
            showLoading(false);
            showErrorNoData();
            mHandler.sendMessage(MessageUtil.getMessage(MSG_DATA_DATE_BY_DAY_FAIL, ARG1_OPERATE_FAIL));
        }
    }

    private void handleMute(Message msg) {
        if (msg.arg1 == ARG1_OPERATE_SUCCESS) {
            ImageViewCompat.setImageTintList(ivMute, (mPlaybackMute == ICameraP2P.MUTE) ? colorStateBlack : colorStateGray);
        } else {
            showToastErrorAlertNotices(getString(R.string.fail), null);
        }
    }

    AppCompatTextView tvTitle;

    public void videoSizeFitWidthChange() {
        WindowManager windowManager = (WindowManager) this.getSystemService(WINDOW_SERVICE);
        int width = windowManager.getDefaultDisplay().getWidth();
        int height = width * ASPECT_RATIO_WIDTH / ASPECT_RATIO_HEIGHT;
        LinearLayoutCompat.LayoutParams layoutParams = new LinearLayoutCompat.LayoutParams(width, height);
        findViewById(R.id.camera_video_view_Rl).setLayoutParams(layoutParams);
    }

    private void initView() {
        progressbar = findViewById(R.id.progressbar);
        timelineView = findViewById(R.id.timeline);
        ivMute = findViewById(R.id.ivMute);
        ivPlay = findViewById(R.id.ivPlay);
        ivRecord = findViewById(R.id.ivRecord);
        btnRecord = findViewById(R.id.btnRecord);
        tvRecord = findViewById(R.id.tvRecord);

        mVideoView = findViewById(R.id.camera_video_view);
        videoSizeFitWidthChange();
        timelineView.setOnBarMoveListener(new OnBarMoveListener() {
            @Override
            public void onBarMove(long l, long l1, long l2) {

            }

            @Override
            public void onBarMoveFinish(long startTime, long endTime, long currentTime) {

                showLoading(true);
                timelineView.setCanQueryData();
                timelineView.setQueryNewVideoData(false);
                if (startTime != -1 && endTime != -1) {
                    playback((int) startTime, (int) endTime, (int) currentTime);
                }
            }

            @Override
            public void onBarActionDown() {

            }
        });
        timelineView.setOnSelectedTimeListener(new OnSelectedTimeListener() {
            @Override
            public void onDragging(long selectStartTime, long selectEndTime) {

            }
        });
    }

    private void showLoading(Boolean isLoad) {
        runOnUiThread(() -> progressbar.setVisibility(isLoad ? View.VISIBLE : View.GONE));
    }

    private void initData() {
        mBackDataMonthCache = new HashMap<>();
        mBackDataDayCache = new HashMap<>();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date(System.currentTimeMillis());
        dateSelect = simpleDateFormat.format(date);
        tvTitle.setText(dateSelect);

        ITuyaIPCCore cameraInstance = TuyaIPCSdk.getCameraInstance();
        mCameraP2P = cameraInstance.createCameraP2P(devId);
        p2pType = cameraInstance.getP2PType(devId);
        //mCameraP2P = TuyaSmartCameraP2PFactory.createCameraP2P(p2pType, devId);
        mVideoView.setViewCallback(new AbsVideoViewCallback() {
            @Override
            public void onCreated(Object o) {
                super.onCreated(o);
                if (mCameraP2P != null) {
                    mCameraP2P.generateCameraView(mVideoView.createdView());
                }
            }
        });
        mVideoView.createVideoView(p2pType);
        if (!mCameraP2P.isConnecting()) {
            mCameraP2P.connect(devId, new OperationDelegateCallBack() {
                @Override
                public void onSuccess(int i, int i1, String s) {
                    queryDayByMonthClick(dateSelect);
                }

                @Override
                public void onFailure(int i, int i1, int i2) {

                }
            });
        }
        ImageViewCompat.setImageTintList(ivMute, colorStateBlack);

    }

    private ColorStateList colorStateBlack, colorStateGray, colorStateYellow, colorStateWhite;

    private void initListener() {
        //ivMute.setOnClickListener(this);
        findViewById(R.id.btnSnapPhoto).setOnClickListener(this);
        findViewById(R.id.btnRecord).setOnClickListener(this);
        findViewById(R.id.btnMute).setOnClickListener(this);
        findViewById(R.id.btnPlay).setOnClickListener(this);
        findViewById(R.id.menuEdit).setOnClickListener(this);
    }

    private void playback(int startTime, int endTime, int playTime) {
        showLoading(true);

        mCameraP2P.startPlayBack(startTime,
                endTime,
                playTime, new OperationDelegateCallBack() {
                    @Override
                    public void onSuccess(int sessionId, int requestId, String data) {
                        showLoading(false);
                        isPlayback = true;
                    }

                    @Override
                    public void onFailure(int sessionId, int requestId, int errCode) {
                        isPlayback = false;
                    }
                }, new OperationDelegateCallBack() {
                    @Override
                    public void onSuccess(int sessionId, int requestId, String data) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                showLoading(false);
                            }
                        });
                        isPlayback = false;
                    }

                    @Override
                    public void onFailure(int sessionId, int requestId, int errCode) {
                        isPlayback = false;
                    }
                });
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btnPlay) {
            EventSenderUtil.sendEvent(EventSenderUtil.EVENT, EventSenderUtil.TUYA_PANEL_CCTV_PLAYBACK_PAUSE, "");
            if (isPlayback)
                pauseClick();
            else
                resumeClick();
        } else if (id == R.id.btnMute) {
            EventSenderUtil.sendEvent(EventSenderUtil.EVENT, EventSenderUtil.TUYA_PANEL_CCTV_PLAYBACK_SOUND, "");
            muteClick();
        } else if (id == R.id.btnSnapPhoto) {
            EventSenderUtil.sendEvent(EventSenderUtil.EVENT, EventSenderUtil.TUYA_PANEL_CCTV_PLAYBACK_SCREENSHOT, "");
            Dexter.withContext(this).withPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE).withListener(new MultiplePermissionsListener() {
                @Override
                public void onPermissionsChecked(MultiplePermissionsReport multiplePermissionsReport) {
                    snapShotClick();
                }

                @Override
                public void onPermissionRationaleShouldBeShown(List<PermissionRequest> list, PermissionToken permissionToken) {

                }
            }).check();

        } else if (id == R.id.btnRecord) {
            EventSenderUtil.sendEvent(EventSenderUtil.EVENT, EventSenderUtil.TUYA_PANEL_CCTV_PLAYBACK_RECORD, "");
            Dexter.withContext(this).withPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE).withListener(new MultiplePermissionsListener() {
                @Override
                public void onPermissionsChecked(MultiplePermissionsReport multiplePermissionsReport) {
                    recordClick();
                }

                @Override
                public void onPermissionRationaleShouldBeShown(List<PermissionRequest> list, PermissionToken permissionToken) {

                }
            }).check();
        } else if (id == R.id.menuEdit) {
           /* Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) - 1);
            new SlideDatePickerDialog.Builder()
                    .setStartDate(
                            calendar
                    )
                    //.setStartDate(Calendar.getInstance().add(Calendar.YEAR, 10))
                    .setEndDate(Calendar.getInstance())
                    .setPreselectedDate(Calendar.getInstance())
                    //.setYearModifier(543)
                    .setLocale(getCurrentLanguage())
                    .setThemeColor(ContextCompat.getColor(this, R.color.yellow))
                    .setHeaderTextColor(R.color.yellow)
                    .setHeaderDateFormat("EEE dd MMMM")
                    .setShowYear(true)
                    .setCancelText(getString(R.string.action_cancel))
                    .setConfirmText(getString(R.string.action_ok))
                    .build()
                    .show(getSupportFragmentManager(), "TAG");*/
            EventSenderUtil.sendEvent(EventSenderUtil.EVENT, EventSenderUtil.TUYA_PANEL_CCTV_PLAYBACK_DATE, "");
            //onCreateDialog().show();


            calendarAlert = new CalendarDialogFragment(new CalendarDialogFragment.OnCalendarClickListener() {
                @Override
                public void onMonthChanged(String calendar) {
                    queryDayByMonth(calendar);
                }

                @Override
                public void onDateSelected(@NonNull String calendar) {
                    calendarAlert = null;
                    showLoading(true);
                    //SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    dateSelect = calendar;
                    tvTitle.setText(dateSelect);
                    findViewById(R.id.tvNoVideoToday).setVisibility(View.GONE);
                    queryDayByMonthClick(dateSelect);
                }

                @Override
                public void onCancelClick() {
                    calendarAlert = null;
                }
            });
            if (calendarAlert != null) {
                calendarAlert.show(getSupportFragmentManager(),
                        "showAlertNotices");
                queryDayByMonth(dateSelect);
            }
        } else if (id == R.id.ivBack) {
            finish();
        }
    }

    CalendarDialogFragment calendarAlert = null;

    public Dialog onCreateDialog() {
        final Calendar c = Calendar.getInstance();
        //c.add(Calendar.YEAR, -1);

        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        if (dateSelect != null && !dateSelect.equals("") && !dateSelect.isEmpty()) {
            String[] dates = dateSelect.split("-");
            if (dates.length == 3) {
                day = Integer.parseInt(dates[2]);
                month = Integer.parseInt(dates[1]) - 1;
                year = Integer.parseInt(dates[0]);
            }
        }
        return new DatePickerDialog(this, (view, year1, month1, dayOfMonth) -> {
            showLoading(true);
            findViewById(R.id.tvNoVideoToday).setVisibility(View.GONE);
            //SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
            int monthStr = month1 + 1;
            String mStr = String.valueOf(monthStr);
            if (monthStr < 10)
                mStr = "0" + monthStr;
            String dStr = String.valueOf(dayOfMonth);
            if (dayOfMonth < 10)
                dStr = "0" + monthStr;
            dateSelect = year1 + "-" + mStr + "-" + dStr;//simpleDateFormat.format(calendar.getTime());
            tvTitle.setText(dateSelect);
            queryDayByMonthClick(dateSelect);
        }, year, month, day);
    }

    Boolean isRecording = false;

    private void recordClick() {
        /*ImageViewCompat.setImageTintList(btnRecord, isRecording  ? colorStateYellow : colorStateWhite);
        ImageViewCompat.setImageTintList(ivRecord, isRecording  ? colorStateWhite : colorStateBlack);*/

        if (!isRecording) {
            String picPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getAbsolutePath();
            Timber.e("picPath :: %s", picPath);
            File file = new File(picPath, "Smartworld");
            if (!file.exists())
                file.mkdirs();
            String fileName = System.currentTimeMillis() + ".mp4";
            mCameraP2P.startRecordLocalMp4(picPath + "/Smartworld/", fileName, this, new OperationDelegateCallBack() {
                @Override
                public void onSuccess(int sessionId, int requestId, String data) {
                    isRecording = true;
                    tvRecord.setText(getString(R.string.action_stop_recording));
                    ivRecord.setImageResource(R.drawable.ic_stop);
                    mHandler.sendEmptyMessage(Constants.MSG_VIDEO_RECORD_BEGIN);
                }

                @Override
                public void onFailure(int sessionId, int requestId, int errCode) {
                    mHandler.sendEmptyMessage(Constants.MSG_VIDEO_RECORD_FAIL);
                }
            });
        } else {
            mCameraP2P.stopRecordLocalMp4(new OperationDelegateCallBack() {
                @Override
                public void onSuccess(int sessionId, int requestId, String data) {
                    isRecording = false;
                    tvRecord.setText(getString(R.string.action_start_recording));
                    ivRecord.setImageResource(R.drawable.camera_video_record);
                    mHandler.sendMessage(MessageUtil.getMessage(Constants.MSG_VIDEO_RECORD_OVER, ARG1_OPERATE_SUCCESS));
                }

                @Override
                public void onFailure(int sessionId, int requestId, int errCode) {
                    isRecording = false;
                    mHandler.sendMessage(MessageUtil.getMessage(Constants.MSG_VIDEO_RECORD_OVER, ARG1_OPERATE_FAIL));
                }
            });
            // recordStatue(false);
        }
    }


    private void snapShotClick() {
        String picPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getAbsolutePath();
        Timber.e("picPath :: %s", picPath);
        File file = new File(picPath, "Smartworld");
        if (!file.exists())
            file.mkdirs();

        mCameraP2P.snapshot(picPath + "/Smartworld/", CameraPlaybackActivity.this, new OperationDelegateCallBack() {
            @Override
            public void onSuccess(int sessionId, int requestId, String data) {
                mHandler.sendMessage(MessageUtil.getMessage(MSG_SCREENSHOT, ARG1_OPERATE_SUCCESS));
            }

            @Override
            public void onFailure(int sessionId, int requestId, int errCode) {
                mHandler.sendMessage(MessageUtil.getMessage(MSG_SCREENSHOT, ARG1_OPERATE_FAIL));
            }
        });
    }


    private void resumeClick() {
        mCameraP2P.resumePlayBack(new OperationDelegateCallBack() {
            @Override
            public void onSuccess(int sessionId, int requestId, String data) {
                isPlayback = true;
                ivPlay.setImageResource(R.drawable.camera_ic_pause);
            }

            @Override
            public void onFailure(int sessionId, int requestId, int errCode) {

            }
        });
    }

    private void pauseClick() {
        mCameraP2P.pausePlayBack(new OperationDelegateCallBack() {
            @Override
            public void onSuccess(int sessionId, int requestId, String data) {
                isPlayback = false;
                ivPlay.setImageResource(R.drawable.camera_ic_play);
            }

            @Override
            public void onFailure(int sessionId, int requestId, int errCode) {

            }
        });
    }

    String dateSelect;

    private void queryDayByMonthClick(String inputStr) {
        if (mCameraP2P != null && !mCameraP2P.isConnecting()) {
            showLoading(false);
            showToastErrorAlertNotices(getString(R.string.fail), null);
            return;
        }

        if (TextUtils.isEmpty(inputStr)) {
            return;
        }
        if (inputStr.contains("-")) {
            String[] substring = inputStr.split("-");
            if (substring.length > 2) {
                try {
                    int year = Integer.parseInt(substring[0]);
                    int mouth = Integer.parseInt(substring[1]);
                    int day = Integer.parseInt(substring[2]);
                    Timber.e("DATE : %s", inputStr);
                    mCameraP2P.queryRecordDaysByMonth(year, mouth, new OperationDelegateCallBack() {
                        @Override
                        public void onSuccess(int sessionId, int requestId, String data) {
                            MonthDays monthDays = JSONObject.parseObject(data, MonthDays.class);
                            mBackDataMonthCache.put(mCameraP2P.getMonthKey(), monthDays.getDataDays());
                            L.e(TAG, "MonthDays --- " + data);

                            mHandler.sendMessage(MessageUtil.getMessage(MSG_DATA_DATE, ARG1_OPERATE_SUCCESS));
                        }

                        @Override
                        public void onFailure(int sessionId, int requestId, int errCode) {
                            mHandler.sendMessage(MessageUtil.getMessage(MSG_DATA_DATE, ARG1_OPERATE_FAIL));
                        }
                    });
                } catch (Exception e) {
                    showToastErrorAlertNotices(getString(R.string.fail), null);
                }
            }
        }
    }


    private void queryDayByMonth(String inputStr) {
        if (mCameraP2P != null && !mCameraP2P.isConnecting()) {
            showLoading(false);
            showToastErrorAlertNotices(getString(R.string.fail), null);
            return;
        }

        if (TextUtils.isEmpty(inputStr)) {
            return;
        }
        if (inputStr.contains("-")) {
            String[] substring = inputStr.split("-");
            if (substring.length > 1) {
                try {
                    int year = Integer.parseInt(substring[0]);
                    int mouth = Integer.parseInt(substring[1]);
                    Timber.e("DATE : %s", inputStr);
                    mCameraP2P.queryRecordDaysByMonth(year, mouth, new OperationDelegateCallBack() {
                        @RequiresApi(api = Build.VERSION_CODES.O)
                        @Override
                        public void onSuccess(int sessionId, int requestId, String data) {
                            MonthDays monthDays = JSONObject.parseObject(data, MonthDays.class);
                            mBackDataMonthCache.put(mCameraP2P.getMonthKey(), monthDays.getDataDays());
                            ArrayList<String> days = new ArrayList();
                            if (calendarAlert != null)
                                for (String day :
                                        monthDays.getDataDays()) {
                                    days.add(year + "-" + mouth + "-" + day);
                                }
                            runOnUiThread(() -> calendarAlert.updateView(days));
                            //mHandler.sendMessage(MessageUtil.getMessage(MSG_DATA_DATE, ARG1_OPERATE_SUCCESS));
                        }

                        @Override
                        public void onFailure(int sessionId, int requestId, int errCode) {
//                            mHandler.sendMessage(MessageUtil.getMessage(MSG_DATA_DATE, ARG1_OPERATE_FAIL));
                        }
                    });
                } catch (Exception e) {
                    showToastErrorAlertNotices(getString(R.string.fail), null);
                }
            }
        }
    }

    private void muteClick() {
        int mute;
        mute = mPlaybackMute == ICameraP2P.MUTE ? ICameraP2P.UNMUTE : ICameraP2P.MUTE;
        mCameraP2P.setMute(ICameraP2P.PLAYMODE.PLAYBACK, mute, new OperationDelegateCallBack() {
            @Override
            public void onSuccess(int sessionId, int requestId, String data) {
                mPlaybackMute = Integer.valueOf(data);
                mHandler.sendMessage(MessageUtil.getMessage(MSG_MUTE, ARG1_OPERATE_SUCCESS));
            }

            @Override
            public void onFailure(int sessionId, int requestId, int errCode) {
                mHandler.sendMessage(MessageUtil.getMessage(MSG_MUTE, ARG1_OPERATE_FAIL));
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (null != mHandler) {
            mHandler.removeCallbacksAndMessages(null);
        }
        if (null != mCameraP2P) {
            mCameraP2P.destroyP2P();
        }
        mCameraP2P = null;
        mVideoView = null;
        timelineView = null;
        mBackDataMonthCache = null;
        mBackDataDayCache = null;
    }

    @Override
    protected void onResume() {
        super.onResume();

        initData();
        initListener();
        mVideoView.onResume();
        if (null != mCameraP2P) {
            //AudioUtils.getModel(this);
            mCameraP2P.registerP2PCameraListener(p2pCameraListener);
            mCameraP2P.generateCameraView(mVideoView.createdView());
        }
    }

    private AbsP2pCameraListener p2pCameraListener = new AbsP2pCameraListener() {
        @Override
        public void onReceiveFrameYUVData(int i, ByteBuffer byteBuffer, ByteBuffer byteBuffer1, ByteBuffer byteBuffer2, int i1, int i2, int i3, int i4, long l, long l1, long l2, Object o) {
            super.onReceiveFrameYUVData(i, byteBuffer, byteBuffer1, byteBuffer2, i1, i2, i3, i4, l, l1, l2, o);
            timelineView.setCurrentTimeInMillisecond(l * 1000L);
        }
    };

    @Override
    protected void onPause() {
        super.onPause();
        mVideoView.onPause();
        if (isPlayback) {
            mCameraP2P.stopPlayBack(null);
        }
        if (null != mCameraP2P) {
            mCameraP2P.removeOnP2PCameraListener();
            if (isFinishing()) {
                mCameraP2P.disconnect(new OperationDelegateCallBack() {
                    @Override
                    public void onSuccess(int i, int i1, String s) {

                    }

                    @Override
                    public void onFailure(int i, int i1, int i2) {

                    }
                });
                mCameraP2P.destroyP2P();
            }
        }
      //  AudioUtils.changeToNomal(this);
    }

    private void showErrorNoData() {
        runOnUiThread(() -> {
            /*findViewById(R.id.frameNoData).setVisibility(View.VISIBLE);*/
            findViewById(R.id.tvNoVideoToday).setVisibility(View.VISIBLE);
            mVideoView.onPause();
        });
    }

    @Override
    public int getLayoutView() {
        return R.layout.activity_camera_playback;
    }

    @Override
    public void bindView() {

        findViewById(R.id.ivBack).setOnClickListener(this);
        findViewById(R.id.boxBack).setVisibility(View.VISIBLE);

        tvTitle = findViewById(R.id.tvTitle);
        AppCompatImageView ivEdit = findViewById(R.id.ivEdit);
        ivEdit.setImageResource(R.drawable.ic_date);
        AppCompatTextView tvSetting = findViewById(R.id.tvSetting);
        tvSetting.setText(getString(R.string.ipc_panel_button_calendar));
    }

    @Override
    public void setupInstance() {
        colorStateGray = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.gray_camera));
        colorStateBlack = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.black));
        colorStateYellow = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.yellow));
        colorStateWhite = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.white));
    }

    @Override
    public void setupView() {
    }

    @Override
    public void initialize(@org.jetbrains.annotations.Nullable Bundle savedInstanceState) {

        EventSenderUtil.sendEvent(EventSenderUtil.PAGE, EventSenderUtil.TUYA_PANEL_CCTV_PAGE_PLAYBACK, "");
        p2pType = getIntent().getIntExtra(INTENT_P2P_TYPE, 1);
        devId = getIntent().getStringExtra(INTENT_DEV_ID);
        initView();
    }

    @Override
    public void onPositiveClick(int i, int i1, int i2, @NotNull Calendar calendar) {
        showLoading(true);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateSelect = simpleDateFormat.format(calendar.getTime());
        tvTitle.setText(dateSelect);
        findViewById(R.id.tvNoVideoToday).setVisibility(View.GONE);
        queryDayByMonthClick(dateSelect);
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
/*
    private void stopClick() {
        mCameraP2P.stopPlayBack(new OperationDelegateCallBack() {
            @Override
            public void onSuccess(int sessionId, int requestId, String data) {

            }

            @Override
            public void onFailure(int sessionId, int requestId, int errCode) {

            }
        });
        isPlayback = false;
    }
*/
}