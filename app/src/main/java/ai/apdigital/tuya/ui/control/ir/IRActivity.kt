package ai.apdigital.tuya.ui.control.ir

import ai.apdigital.tuya.R
import ai.apdigital.tuya.base.BaseActivity
import ai.apdigital.tuya.model.RemoteDeviceEntity
import ai.apdigital.tuya.networking.Resource
import ai.apdigital.tuya.networking.Status
import ai.apdigital.tuya.model.TokenEntity
import ai.apdigital.tuya.ui.control.DeviceDetailActivity
import ai.apdigital.tuya.base.Constants
import ai.apdigital.tuya.ui.control.ir.adapter.IRDeviceListAdapter

import ai.apdigital.tuya.ui.viewmodel.TuyaIRViewModel
import ai.apdigital.tuya.utils.EventIRAirUtils
import ai.apdigital.tuya.utils.EventIRTVUtils
import ai.apdigital.tuya.utils.EventUtils
import ai.apdigital.tuya.utils.PageUtils
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.tuya.smart.home.sdk.TuyaHomeSdk
import kotlinx.android.synthetic.main.activity_list_with_button.*
import kotlinx.android.synthetic.main.toolbar_back.*
import kotlinx.android.synthetic.main.toolbar_back.tvTitle
import kotlinx.android.synthetic.main.toolbar_back_with_control_panel.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class IRActivity : BaseActivity() {
    var deviceId: String = ""
    private val tuyaIRVM: TuyaIRViewModel? by viewModel()
    private var deviceListAdapter: IRDeviceListAdapter? = null
    override fun getLayoutView() = R.layout.activity_list_with_button

    override fun bindView() {

    }

    override fun setupInstance() {
        setupToolbar(
            getString(R.string.title_smart_lock),
            showButtonBack = true,
            showRightMenu = true
        )
    }


    override fun setupView() {
        shimmerViewContainer = shimmer_container
        contentContainer = content
        btnButtom?.text = getString(R.string.action_add_remote)

        menuEdit?.setOnClickListener {
            EventUtils.sendEvent(EventUtils.TYPE_EVENT, EventIRTVUtils.TUYA_PANEL_IR_DETAIL, "")
            val intent = Intent(this@IRActivity, DeviceDetailActivity::class.java)
            intent.putExtra(Constants.INTENT_DEV_ID, deviceId)
            startForDeviceDetailResult.launch(intent)
        }

        recyclerList?.let {
            deviceListAdapter =
                IRDeviceListAdapter(
                    this
                )
            val layoutManager = LinearLayoutManager(this)
            layoutManager.orientation = LinearLayoutManager.VERTICAL
            it.layoutManager = layoutManager
            it.adapter = deviceListAdapter
            it.setHasFixedSize(true)

            deviceListAdapter?.setOnItemClickListener(object :
                IRDeviceListAdapter.OnItemClickListener {

                override fun onRecyclerItemClick(
                    view: View,
                    position: Int,
                    entity: RemoteDeviceEntity?
                ) {

                    val intent = if (entity?.category_id == "5") {
                        Intent(this@IRActivity, IRControlAirConditionActivity::class.java)
                    } else {
                        Intent(this@IRActivity, IRTVActivity::class.java)
                    }

                    intent.putExtra(Constants.INTENT_DEV_ID, deviceId)
                    intent.putExtra(Constants.INTENT_REMOTE_TYPE, RemoteType.CONTROL)
                    intent.putExtra(Constants.INTENT_REMOTE_ID, entity?.remote_id)
                    startControlForResult.launch(intent)
                }
            })
        }

        swipeContainer?.isRefreshing = false
        swipeContainer?.setOnRefreshListener {
            tuyaIRVM?.callRemotes(deviceId)
        }
    }

    override fun initialize(savedInstanceState: Bundle?) {
        EventUtils.sendEvent(EventUtils.TYPE_PAGE, PageUtils.TUYA_PANEL_IR_P, "")
        tuyaIRVM?.getTokenListener()?.observe(this, tokenResp)
        tuyaIRVM?.getRemotesListener()?.observe(this, remoteResp)

        tuyaIRVM?.getToken()

        deviceId = intent.getStringExtra(Constants.INTENT_DEV_ID) ?: ""
        val deviceBean = TuyaHomeSdk.getDataInstance().getDeviceBean(deviceId)
        tvTitle?.text = deviceBean?.name

        btnButtom?.setOnClickListener {
            EventUtils.sendEvent(EventUtils.TYPE_EVENT, EventIRTVUtils.TUYA_PANEL_IR_ADD_REMOTTE, "")
            val intent = Intent(this@IRActivity, IRCategoryActivity::class.java)
            intent.putExtra(Constants.INTENT_DEV_ID, deviceId)
            intent.putExtra(Constants.INTENT_REMOTE_TYPE, RemoteType.ADD)
            startForCreateResult.launch(intent)
        }
    }

    val startForCreateResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                tuyaIRVM?.getToken()
                isUpdate = true
            }
        }

    val startControlForResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                tuyaIRVM?.getToken()
                isUpdate = true
            }
        }


    private val tokenResp = Observer<Resource<TokenEntity>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                swipeContainer?.isRefreshing = false
                hideProgressView()
                tuyaIRVM?.callRemotes(deviceId)
            }

            Status.ERROR -> {
                swipeContainer?.isRefreshing = false
                hideProgressView()
                showToastErrorAlertNotices(
                    resource.message?.message ?: getString(R.string.msg_failure), null
                )
            }
            Status.LOADING -> {
                showProgressView()
            }
        }
    }

    private val remoteResp = Observer<Resource<MutableList<RemoteDeviceEntity>>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                swipeContainer?.isRefreshing = false
                hideShimmerLoading()
                deviceListAdapter?.updateData(resource.data)
                if (resource.data?.size ?: 0 > 0)
                    tvNotFoundData?.visibility = View.GONE
                else
                    tvNotFoundData?.visibility = View.VISIBLE
            }

            Status.ERROR -> {
                swipeContainer?.isRefreshing = false
                hideShimmerLoading()
                showToastErrorAlertNotices(
                    resource.message?.message ?: getString(R.string.msg_failure), null
                )
            }
            Status.LOADING -> {
                showShimmerLoading()
            }
        }
    }

}