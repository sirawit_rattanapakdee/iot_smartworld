package ai.apdigital.tuya.ui.control.ir

import ai.apdigital.tuya.R
import ai.apdigital.tuya.base.BaseActivity
import ai.apdigital.tuya.base.Constants
import ai.apdigital.tuya.model.BrandEntity
import ai.apdigital.tuya.model.TokenEntity
import ai.apdigital.tuya.networking.Resource
import ai.apdigital.tuya.networking.Status
import ai.apdigital.tuya.ui.control.ir.adapter.IRBrandAdapter
import ai.apdigital.tuya.ui.viewmodel.TuyaIRViewModel
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import android.widget.TextView.OnEditorActionListener
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_list_with_search.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*


class IRBrandActivity : BaseActivity() {
    private val tuyaIRVM: TuyaIRViewModel? by viewModel()
    var deviceId: String = ""
    var categoryId: String = ""
    var brands: MutableList<BrandEntity>? = mutableListOf()
    private var brandListAdapter: IRBrandAdapter? = null
    override fun getLayoutView() = R.layout.activity_list_with_search

    override fun bindView() {

    }

    override fun setupInstance() {
        setupToolbar(
            getString(R.string.title_select_brand),
            showButtonBack = true,
            showRightMenu = false
        )
    }

    override fun setupView() {
        shimmerViewContainer = shimmer_container
        contentContainer = content
        recyclerList?.let {
            brandListAdapter =
                IRBrandAdapter(
                    this
                )
            val layoutManager = LinearLayoutManager(this)
            layoutManager.orientation = LinearLayoutManager.VERTICAL
            it.layoutManager = layoutManager
            it.adapter = brandListAdapter
            it.setHasFixedSize(true)

            brandListAdapter?.setOnItemClickListener(object :
                IRBrandAdapter.OnItemClickListener {

                override fun onRecyclerItemClick(
                    view: View,
                    position: Int,
                    entity: BrandEntity?
                ) {

                    if (categoryId == "5") {
                        val intent =
                            Intent(this@IRBrandActivity, IRControlAirConditionActivity::class.java)
                        intent.putExtra(Constants.INTENT_DEV_ID, deviceId)
                        intent.putExtra(Constants.INTENT_REMOTE_TYPE, RemoteType.ADD)
                        intent.putExtra(Constants.INTENT_BRAND_ID, entity?.brand_id)
                        intent.putExtra(Constants.INTENT_BRAND_NAME, entity?.brand_name)
                        intent.putExtra(Constants.INTENT_CATE_ID, categoryId)
                        startForDeviceDetailResult.launch(intent)
                    } else {
                        val intent =
                            Intent(this@IRBrandActivity, IRTVActivity::class.java)
                        intent.putExtra(Constants.INTENT_DEV_ID, deviceId)
                        intent.putExtra(Constants.INTENT_REMOTE_TYPE, RemoteType.ADD)
                        intent.putExtra(Constants.INTENT_BRAND_ID, entity?.brand_id)
                        intent.putExtra(Constants.INTENT_BRAND_NAME, entity?.brand_name)
                        intent.putExtra(Constants.INTENT_CATE_ID, categoryId)
                        startForDeviceDetailResult.launch(intent)
                    }
                }
            })
        }
        edtSearch?.hint = "Brand name"
        edtSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                filterData(s.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })
        edtSearch?.setOnEditorActionListener(OnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                filterData(edtSearch?.text.toString())
                return@OnEditorActionListener true
            }
            false
        })
    }

    fun filterData(code: String) {
        val data: MutableList<BrandEntity> = mutableListOf()
        brands?.forEach { it ->
            if (it.brand_name?.contains(code) == true || it.brand_name?.toUpperCase(Locale.ROOT)
                    ?.contains(code.toUpperCase(Locale.ROOT)) == true
            ) {
                data.add(it)
            }
        }
        brandListAdapter?.updateData(data)
    }

    override fun initialize(savedInstanceState: Bundle?) {
        deviceId = intent.getStringExtra(Constants.INTENT_DEV_ID) ?: ""
        categoryId = intent.getStringExtra(Constants.INTENT_CATE_ID) ?: ""

        //Timber.e("call API $deviceId $categoryId")

        tuyaIRVM?.getTokenListener()?.observe(this, tokenResp)
        tuyaIRVM?.getToken()

        tuyaIRVM?.getBrandsListener()?.observe(this, brandResp)

        //Handler().postDelayed({ tuyaIRVM?.callCategories(deviceId) }, 5000)
    }


    private val tokenResp = Observer<Resource<TokenEntity>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                hideProgressView()
                tuyaIRVM?.callBrands(deviceId, categoryId)
            }

            Status.ERROR -> {
                hideProgressView()
                showToastErrorAlertNotices(
                    resource.message?.message ?: getString(R.string.msg_failure), null
                )
            }
            Status.LOADING -> {
                showProgressView()
            }
        }
    }

    private val brandResp = Observer<Resource<MutableList<BrandEntity>>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                swipeContainer?.isRefreshing = false
                hideShimmerLoading()
                brands = resource.data
                brandListAdapter?.updateData(resource.data)
                if (resource.data?.size ?: 0 > 0)
                    tvNotFoundData?.visibility = View.GONE
                else
                    tvNotFoundData?.visibility = View.VISIBLE
            }

            Status.ERROR -> {
                swipeContainer?.isRefreshing = false
                hideShimmerLoading()
                showToastErrorAlertNotices(
                    resource.message?.message ?: getString(R.string.msg_failure), null
                )
            }
            Status.LOADING -> {
                showShimmerLoading()
            }
        }
    }
}