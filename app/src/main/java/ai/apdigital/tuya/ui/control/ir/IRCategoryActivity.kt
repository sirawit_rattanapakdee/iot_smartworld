package ai.apdigital.tuya.ui.control.ir

import ai.apdigital.tuya.R
import ai.apdigital.tuya.base.BaseActivity
import ai.apdigital.tuya.model.CategoryEntity
import ai.apdigital.tuya.networking.Resource
import ai.apdigital.tuya.networking.Status
import ai.apdigital.tuya.base.Constants
import ai.apdigital.tuya.ui.control.ir.adapter.IRCategoryAdapter

import ai.apdigital.tuya.ui.viewmodel.TuyaIRViewModel
import ai.apdigital.tuya.utils.EventIRTVUtils
import ai.apdigital.tuya.utils.EventUtils
import ai.apdigital.tuya.utils.PageUtils
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_list.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class IRCategoryActivity : BaseActivity() {
    var deviceId: String = ""
    private val tuyaIRVM: TuyaIRViewModel? by viewModel()
    private var categoryListAdapter: IRCategoryAdapter? = null
    override fun getLayoutView() = R.layout.activity_list

    override fun bindView() {
        shimmerViewContainer = shimmer_container
        contentContainer = content
    }

    override fun setupInstance() {

    }

    override fun setupView() {
        setupToolbar(
            getString(R.string.title_select_device_type),
            showButtonBack = true,
            showRightMenu = false
        )
    }

    override fun initialize(savedInstanceState: Bundle?) {
        EventUtils.sendEvent(EventUtils.TYPE_PAGE, PageUtils.TUYA_PANEL_IR_ADD_REMOTE, "")
        deviceId = intent.getStringExtra(Constants.INTENT_DEV_ID) ?: ""
        recyclerList?.let {
            categoryListAdapter =
                IRCategoryAdapter(
                    this
                )
            val layoutManager = LinearLayoutManager(this)
            layoutManager.orientation = LinearLayoutManager.VERTICAL
            it.layoutManager = layoutManager
            it.adapter = categoryListAdapter
            it.setHasFixedSize(true)

            categoryListAdapter?.setOnItemClickListener(object :
                IRCategoryAdapter.OnItemClickListener {

                override fun onRecyclerItemClick(
                    view: View,
                    position: Int,
                    entity: CategoryEntity?
                ) {
                    EventUtils.sendEvent(
                        EventUtils.TYPE_EVENT,
                        EventIRTVUtils.TUYA_PANEL_IR_ADD_REMOTTE,
                        if (entity?.category_id == "5") "air" else "tv"
                    )
                    val intent = Intent(this@IRCategoryActivity, IRBrandActivity::class.java)
                    intent.putExtra(Constants.INTENT_DEV_ID, deviceId)
                    intent.putExtra(Constants.INTENT_CATE_ID, entity?.category_id)
                    startForDeviceDetailResult.launch(intent)
                }
            })
        }

        swipeContainer?.isRefreshing = false
        swipeContainer?.setOnRefreshListener {
            //tuyaIRVM?.callCategories(deviceId)
        }
        val categoryEntity: MutableList<CategoryEntity> = mutableListOf()
        categoryEntity.add(
            CategoryEntity(
                category_id = "5",
                category_name = getString(R.string.air_con)
            )
        )
        categoryEntity.add(
            CategoryEntity(
                category_id = "2",
                category_name = getString(R.string.tv)
            )
        )
        categoryListAdapter?.updateData(categoryEntity)
        /*tuyaIRVM?.getCategoriesListener()?.observe(this, categoryResp)
        tuyaIRVM?.callCategories(deviceId)
*/
    }


    private val categoryResp = Observer<Resource<MutableList<CategoryEntity>>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                swipeContainer?.isRefreshing = false
                hideShimmerLoading()
                categoryListAdapter?.updateData(resource.data)
            }

            Status.ERROR -> {
                swipeContainer?.isRefreshing = false
                hideShimmerLoading()
                showToastErrorAlertNotices(
                    resource.message?.message ?: getString(R.string.msg_failure), null
                )
            }
            Status.LOADING -> {
                showShimmerLoading()
            }
        }
    }

}