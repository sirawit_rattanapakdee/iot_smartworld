package ai.apdigital.tuya.ui.control.ir

import ai.apdigital.tuya.R
import ai.apdigital.tuya.base.BaseActivity
import ai.apdigital.tuya.base.Constants
import ai.apdigital.tuya.model.*
import ai.apdigital.tuya.networking.Resource
import ai.apdigital.tuya.networking.Status
import ai.apdigital.tuya.repository.request.AirSendCommandRequest
import ai.apdigital.tuya.ui.viewmodel.TuyaIRViewModel
import ai.apdigital.tuya.utils.EventIRAirUtils
import ai.apdigital.tuya.utils.EventIRTVUtils
import ai.apdigital.tuya.utils.EventUtils
import ai.apdigital.tuya.utils.PageUtils
import ai.apdigital.tuya.utils.alerts.AlertUtil
import ai.apdigital.tuya.utils.alerts.EditRoomDialogFragment
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.Observer
import com.tuya.smart.home.sdk.TuyaHomeSdk
import kotlinx.android.synthetic.main.activity_ir_air.*
import kotlinx.android.synthetic.main.layout_ir_index.*
import kotlinx.android.synthetic.main.toolbar_back_with_control_panel.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*


class IRControlAirConditionActivity : BaseActivity() {
    var deviceId: String = ""
    var remoteId: String = ""
    var cateId: String = ""
    var brandId: String = ""
    var brandName: String = ""
    var airStatus: AirStatusEntity? = AirStatusEntity()
    private val tuyaIRVM: TuyaIRViewModel? by viewModel()
    override fun getLayoutView() = R.layout.activity_ir_air

    override fun bindView() {

    }

    override fun setupInstance() {

        remoteCommand = intent.getSerializableExtra(Constants.INTENT_REMOTE_TYPE) as RemoteType
        deviceId = intent.getStringExtra(Constants.INTENT_DEV_ID) ?: ""
        setupToolbar(
            if (remoteCommand == RemoteType.ADD) {
                getString(R.string.title_air_conditioner_add)
            } else {
                getString(R.string.title_air_conditioner)
            },
            showButtonBack = true,
            showRightMenu = true
        )
    }

    override fun setupView() {
        menuEdit?.setOnClickListener {
            EventUtils.sendEvent(
                EventUtils.TYPE_EVENT,
                EventIRAirUtils.TUYA_PANEL_IR_AIR_DEVICE_DETAIL,
                ""
            )

            val intent = Intent(this@IRControlAirConditionActivity, IRDetailActivity::class.java)
            intent.putExtra(Constants.INTENT_DEV_ID, deviceId)
            intent.putExtra(Constants.INTENT_REMOTE_ID, remoteId)
            startForDeviceDetailResult.launch(intent)
        }

        ivSub?.setOnClickListener {
            val temp = airStatus?.temp?.toInt() ?: 0
            if (temp <= 18)
                return@setOnClickListener
            airStatus?.temp = (temp - 1).toString()
            publishDps("temp", (temp - 1), (temp - 1).toString())
        }

        ivAdd?.setOnClickListener {
            val temp = airStatus?.temp?.toInt() ?: 0
            if (temp >= 30)
                return@setOnClickListener
            airStatus?.temp = (temp + 1).toString()
            publishDps("temp", (temp + 1), (temp + 1).toString())
        }

        btnMode?.setOnClickListener {
            var mode = airStatus?.mode?.toInt() ?: 0
            mode++
            if (mode >= 5)
                mode = 0
            airStatus?.mode = mode.toString()
            val track = when (mode) {
                0 -> {
                    getString(R.string.air_cold)
                }
                1 -> {
                    getString(R.string.air_heat)
                }
                2 -> {
                    getString(R.string.air_auto)
                }
                3 -> {
                    getString(R.string.air_fan)
                }
                else -> {
                    getString(R.string.air_dry)
                }
            }
            publishDps("mode", mode, track)

        }

        btnSpeed?.setOnClickListener {
            var wind = airStatus?.wind?.toInt() ?: 0
            wind++
            if (wind >= 4)
                wind = 0
            airStatus?.wind = wind.toString()

            val speed = when (wind) {
                0 -> {
                    getString(R.string.air_auto)
                }
                1 -> {
                    getString(R.string.air_low)
                }
                2 -> {
                    getString(R.string.air_medium)
                }
                else -> {
                    getString(R.string.air_high)
                }
            }
            publishDps("speed", wind, speed)
        }

        btnPower?.setOnClickListener {

            if (boxDisable?.visibility == View.VISIBLE)
                boxDisable?.visibility = View.GONE
            btnCanControl?.isEnabled = true
            val airCommand = AirSendCommandRequest()
            airCommand.mode = airStatus?.mode?.toInt() ?: 0
            airCommand.power = if (airStatus?.power == "1") 0 else 1
            airCommand.temp = airStatus?.temp?.toInt() ?: 0
            airCommand.wind = airStatus?.wind?.toInt() ?: 0
            airStatus?.power = airCommand.power.toString()
            publishDps("power", airCommand.power, if (airCommand.power == 1) "on" else "off")
        }
        nestedScrollView.post { nestedScrollView.fullScroll(View.FOCUS_DOWN) }
        //nestedScrollView.smoothScrollTo(boxHide)
    }

    fun NestedScrollView.smoothScrollTo(view: View) {
        var distance = view.top
        var viewParent = view.parent
        //traverses 10 times
        for (i in 0..9) {
            if ((viewParent as View) === this) break
            distance += (viewParent as View).top
            viewParent = viewParent.getParent()
        }
        smoothScrollTo(0, distance)
    }

    fun publishDps(key: String, value: Int, track: String) {
        if (remoteCommand == RemoteType.CONTROL) {
            EventUtils.sendEvent(
                EventUtils.TYPE_EVENT,
                EventIRAirUtils.TUYA_PANEL_IR_AIR.plus("_").plus(key),
                track.toLowerCase()
            )
            tuyaIRVM?.callAirCCommand(deviceId, remoteId, key, value)
        } else {
            EventUtils.sendEvent(
                EventUtils.TYPE_EVENT,
                EventIRAirUtils.TUYA_PANEL_IR_MATCH_AIR.plus("_").plus(key),
                track.toLowerCase()
            )
            tuyaIRVM?.callTestAirCommand(
                deviceId,
                cateId,
                key, value, remoteIndex?.get(positionIndex)?.remote_index
            )
        }
    }

    var positionIndex: Int = 0
    var remoteCommand: RemoteType? = RemoteType.CONTROL

    override fun initialize(savedInstanceState: Bundle?) {

        tuyaIRVM?.getAirStatusListener()?.observe(this, airResp)
        tuyaIRVM?.getAirCommandListener()?.observe(this, commandResp)
        tuyaIRVM?.getRemotedIndexListener()?.observe(this, remoteIndexResp)
        tuyaIRVM?.getTokenListener()?.observe(this, tokenResp)
        tuyaIRVM?.getToken()

        if (remoteCommand == RemoteType.ADD) {
            EventUtils.sendEvent(
                EventUtils.TYPE_PAGE, PageUtils.TUYA_PANEL_IR_MATCH.plus("air"),
                ""
            )
            cateId = intent.getStringExtra(Constants.INTENT_CATE_ID) ?: ""
            brandId = intent.getStringExtra(Constants.INTENT_BRAND_ID) ?: ""
            brandName = intent.getStringExtra(Constants.INTENT_BRAND_NAME) ?: ""
            boxTest?.visibility = View.VISIBLE
            tuyaIRVM?.addRemoteListener()?.observe(this, addResp)
            btnCanControl?.isEnabled = false
            btnNext?.setOnClickListener {
                EventUtils.sendEvent(
                    EventUtils.TYPE_EVENT,
                    EventIRAirUtils.TUYA_PANEL_IR_MATCH_AIR_NEXT,
                    ""
                )
                btnCanControl?.isEnabled = false
                boxDisable?.visibility = View.VISIBLE
                if (remoteIndex?.size ?: 0 > 0) {
                    if (positionIndex >= ((remoteIndex?.size ?: 0) - 1))
                        positionIndex = 0
                    else
                        positionIndex++
                    setUpIndexCheck()
                }
            }
            btnCanControl?.setOnClickListener {
                EventUtils.sendEvent(
                    EventUtils.TYPE_EVENT,
                    EventIRAirUtils.TUYA_PANEL_IR_MATCH_AIR_CAN_CONTROL,
                    ""
                )
                showAlertConfirmWithActionClick(
                    getString(R.string.alert_confirm_can_control),
                    brandName,
                    object : AlertUtil.OnSuccessListener {
                        override fun onSuccess() {
                            confirmAddRemote()
                        }
                    },
                    R.color.black
                )
            }
        } else {
            EventUtils.sendEvent(
                EventUtils.TYPE_PAGE,
                PageUtils.TUYA_PANEL_IR.plus("air"),
                ""
            )
            boxTest?.visibility = View.GONE
            remoteId = intent.getStringExtra(Constants.INTENT_REMOTE_ID) ?: ""
            val deviceBean =
                TuyaHomeSdk.getDataInstance().getDeviceBean(remoteId)
            deviceBean?.let {
                tvTitle?.text = it.name
            }
        }
    }

    fun confirmAddRemote() {
        supportFragmentManager.let {
            val notices = EditRoomDialogFragment(
                getString(R.string.alert_remote_name),
                brandName,
                object : EditRoomDialogFragment.OnEditNameListener {
                    override fun onSaveClick(name: String) {
                        tuyaIRVM?.callAddRemote(
                            deviceId,
                            cateId,
                            brandId,
                            brandName,
                            remoteIndex?.get(positionIndex)?.remote_index,
                            name
                        )
                    }
                })
            notices.isCancelable = false
            notices.show(
                it,
                "showAlertNotices"
            )
        }
    }

    private val tokenResp = Observer<Resource<TokenEntity>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                hideProgressView()
                if (remoteCommand == RemoteType.ADD) {
                    tuyaIRVM?.callRemotedIndex(deviceId, cateId, brandId)
                } else {
                    tuyaIRVM?.callAirStatus(deviceId, remoteId)
                }
            }

            Status.ERROR -> {
                hideProgressView()
                showToastErrorAlertNotices(
                    resource.message?.message ?: getString(R.string.msg_failure), null
                )
            }
            Status.LOADING -> {
                showProgressView()
            }
        }
    }

    private val airResp = Observer<Resource<AirStatusEntity>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                hideProgressView()
                resource.data?.let {
                    enablePower(it.power)
                    setTemp(it.temp)
                    setMode(it.mode)
                    setWind(it.wind)
                    airStatus = it
                }
            }

            Status.ERROR -> {
                hideProgressView()
                showToastErrorAlertNotices(
                    resource.message?.message ?: getString(R.string.msg_failure), null
                )
            }
            Status.LOADING -> {
                showProgressView()
            }
        }
    }

    private fun setUpIndexCheck() {
        btnNext?.text =
            getString(R.string.action_next_one).plus(" ").plus((positionIndex + 1).toString())
                .plus("/").plus(remoteIndex?.size)
                .plus(")")
    }

    var remoteIndex: MutableList<RemoteIndexEntity>? = null
    private val remoteIndexResp = Observer<Resource<MutableList<RemoteIndexEntity>>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                hideProgressView()
                remoteIndex = resource.data
                if (remoteIndex?.size ?: 0 > 0) {
                    btnNext?.isEnabled = true
                    btnPower?.isEnabled = true
                    btnCanControl?.isEnabled = true
                    setUpIndexCheck()
                } else {
                    btnNext?.isEnabled = false
                    btnCanControl?.isEnabled = true
                    btnPower?.isEnabled = false
                }
            }

            Status.ERROR -> {
                //swipeContainer?.isRefreshing = false
                hideProgressView()
                showToastErrorAlertNotices(
                    resource.message?.message ?: getString(R.string.msg_failure), null
                )
            }
            Status.LOADING -> {
                showProgressView()
            }
        }
    }

    private val addResp = Observer<Resource<AddRemoteEntity>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                hideProgressView()
                resource.data?.let {
                    showToastSuccessAlertNotices(
                        getString(R.string.msg_successfully),
                        object : AlertUtil.OnSuccessListener {
                            override fun onSuccess() {
                                val intent = Intent()
                                intent.putExtra("ACTION", "CREATE")
                                intent.putExtra("REMOTE_ID", it.remote_id)
                                setResult(RESULT_OK, intent)
                                finish()
                            }
                        })
                }
            }

            Status.ERROR -> {
                showToastErrorAlertNotices(
                    resource.message?.message ?: getString(R.string.msg_failure), null
                )
            }
            Status.LOADING -> {
                showProgressView()
            }
        }
    }

    private val commandResp = Observer<Resource<Boolean>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                // hideProgressView()
                resource.data?.let {
                    if (it) {
                        setTemp(airStatus?.temp)
                        setMode(airStatus?.mode)
                        setWind(airStatus?.wind)
                        enablePower(airStatus?.power)
                    }
                }
            }

            Status.ERROR -> {
                //swipeContainer?.isRefreshing = false
                //hideProgressView()
                showToastErrorAlertNotices(
                    resource.message?.message ?: getString(R.string.msg_failure), null
                )
            }
            Status.LOADING -> {
                //showProgressView()
            }
        }
    }

    private fun enablePower(enable: String?) {
        if (enable == "1") {
            boxDisable?.visibility = View.GONE
            btnPower?.setImageResource(R.drawable.air_button_power_active)
        } else {
            boxDisable?.visibility = View.VISIBLE
            btnPower?.setImageResource(R.drawable.air_button_power)
        }
    }

    private fun setTemp(temp: String?) {
        tvTemp.text = temp ?: ""
    }

    private fun setMode(mode: String?) {
        when (mode) {
            "0" -> {
                btnMode?.setImageResource(R.drawable.air_button_mode_cold)
                tvMode?.text = getString(R.string.air_cold)
            }
            "1" -> {
                btnMode?.setImageResource(R.drawable.air_button_mode_heat)
                tvMode?.text = getString(R.string.air_heat)
            }
            "2" -> {
                btnMode?.setImageResource(R.drawable.air_button_mode_auto)
                tvMode?.text = getString(R.string.air_auto)
            }
            "3" -> {
                btnMode?.setImageResource(R.drawable.air_button_mode_fan)
                tvMode?.text = getString(R.string.air_fan)
            }
            else -> {
                btnMode?.setImageResource(R.drawable.air_button_mode_dry)
                tvMode?.text = getString(R.string.air_dry)
            }
        }
    }

    private fun setWind(wind: String?) {
        when (wind) {
            "0" -> {
                btnSpeed?.setImageResource(R.drawable.air_button_fan_auto)
                tvSpeed?.text = getString(R.string.air_auto)
            }
            "1" -> {
                btnSpeed?.setImageResource(R.drawable.air_button_fan_low)
                tvSpeed?.text = getString(R.string.air_low)
            }
            "2" -> {
                btnSpeed?.setImageResource(R.drawable.air_button_fan_medium)
                tvSpeed?.text = getString(R.string.air_medium)
            }
            else -> {
                btnSpeed?.setImageResource(R.drawable.air_button_fan_high)
                tvSpeed?.text = getString(R.string.air_high)
            }
        }
    }
}