package ai.apdigital.tuya.ui.control.ir

import ai.apdigital.tuya.R
import ai.apdigital.tuya.base.BaseActivity
import ai.apdigital.tuya.networking.Resource
import ai.apdigital.tuya.networking.Status
import ai.apdigital.tuya.base.Constants
import ai.apdigital.tuya.ui.viewmodel.TuyaIRViewModel
import ai.apdigital.tuya.utils.alerts.AlertUtil
import ai.apdigital.tuya.utils.alerts.EditRoomDialogFragment
import android.content.Intent
import android.os.Bundle
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.view.View
import androidx.lifecycle.Observer
import com.tuya.smart.home.sdk.TuyaHomeSdk
import com.tuya.smart.sdk.bean.DeviceBean

import kotlinx.android.synthetic.main.activity_ir_device_detail.*
import kotlinx.android.synthetic.main.activity_ir_device_detail.ivEdit
import kotlinx.android.synthetic.main.toolbar_back.*
import kotlinx.android.synthetic.main.toolbar_back.tvTitle
import kotlinx.android.synthetic.main.toolbar_back_with_control_panel.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*

class IRDetailActivity : BaseActivity() {

    private val tuyaIRVM: TuyaIRViewModel? by viewModel()

    override fun getLayoutView() = R.layout.activity_ir_device_detail

    override fun bindView() {

    }

    override fun setupInstance() {
    }

    override fun setupView() {
        setupToolbar(
            "",
            showButtonBack = true,
            showRightMenu = true
        )
    }

    var deviceId: String? = ""
    var remoteId: String? = ""
    var deviceBean: DeviceBean? = null

    override fun initialize(savedInstanceState: Bundle?) {
        deviceId = intent.getStringExtra(Constants.INTENT_DEV_ID)
        remoteId = intent.getStringExtra(Constants.INTENT_REMOTE_ID)

        tuyaIRVM?.renameRemoteListener()?.observe(this, renameResp)
        tuyaIRVM?.removeRemoteListener()?.observe(this, deleteResp)

        tvRemove?.let {
            val manageRoomString = SpannableString(it.text)
            manageRoomString.setSpan(UnderlineSpan(), 0, manageRoomString.length, 0)
            it.text = manageRoomString
        }

        deviceBean =
            TuyaHomeSdk.getDataInstance().getDeviceBean(remoteId)

        tvDeviceName?.text = deviceBean?.name
        tvTitle?.visibility = View.GONE
        //tvBand?.text = deviceBean?.brand_name
        ivEdit?.setOnClickListener {
            supportFragmentManager.let {
                val notices = EditRoomDialogFragment(
                    getString(R.string.edit),
                    tvDeviceName.text.toString(),
                    object : EditRoomDialogFragment.OnEditNameListener {
                        override fun onSaveClick(name: String) {
                            deviceBean?.name = name
                            tuyaIRVM?.renameDevice(deviceId, remoteId ?: "", name)
                        }
                    })
                notices.isCancelable = false
                notices.show(
                    it,
                    "showAlertNotices"
                )
            }
        }
        ivDevice?.let { iv ->
            var isAirConditioner = false
            TuyaHomeSdk.getDataInstance().getSchema(remoteId)
                ?.let temp@{ map ->
                    map.values.forEach { bean ->
                        if (bean.code?.toLowerCase(Locale.ROOT) == "temperature" || bean.code?.toLowerCase(
                                Locale.ROOT
                            ) == "temp"
                        ) {
                            isAirConditioner = true
                            return@temp
                        }
                    }
                }

            if (isAirConditioner)
                iv.setImageResource(R.drawable.ic_air)
            else
                iv.setImageResource(R.drawable.ic_tv)
        }

        btnRemove?.setOnClickListener {
            showAlertConfirmWithActionClick(
                getString(R.string.action_do_you_want_to_delete),
                getString(R.string.ty_smart_scene_device).plus("  ").plus(tvDeviceName?.text.toString()),
                object : AlertUtil.OnSuccessListener {
                    override fun onSuccess() {
                        tuyaIRVM?.removeDevice(deviceId, remoteId ?: "")
                    }
                }, R.color.red
            )
        }
    }

    private val renameResp = Observer<Resource<Boolean>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                hideProgressView()
                resource.data?.let {
                    if (it) {
                        showToastSuccessAlertNotices(
                            getString(R.string.msg_successfully),
                            object : AlertUtil.OnSuccessListener {
                                override fun onSuccess() {
                                    val intent = Intent()
                                    intent.putExtra("ACTION", "RENAME")
                                    intent.putExtra("NAME", deviceBean?.name)
                                    setResult(RESULT_OK, intent)
                                    finish()
                                }
                            })
                    } else {
                        showToastErrorAlertNotices(
                            getString(R.string.msg_failure),
                            object : AlertUtil.OnSuccessListener {
                                override fun onSuccess() {

                                }
                            })
                    }
                }
            }

            Status.ERROR -> {
                showToastErrorAlertNotices(
                    getString(R.string.msg_failure),
                    object : AlertUtil.OnSuccessListener {
                        override fun onSuccess() {

                        }
                    })
            }
            Status.LOADING -> {
                showProgressView()
            }
        }
    }


    private val deleteResp = Observer<Resource<Boolean>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                hideProgressView()
                resource.data?.let {
                    if (it) {
                        showToastSuccessAlertNotices(
                            getString(R.string.msg_successfully),
                            object : AlertUtil.OnSuccessListener {
                                override fun onSuccess() {
                                    val intent = Intent()
                                    intent.putExtra("ACTION", "REMOVE")
                                    setResult(RESULT_OK, intent)
                                    finish()
                                }
                            })
                    } else {
                        showToastErrorAlertNotices(
                            getString(R.string.msg_failure),
                            object : AlertUtil.OnSuccessListener {
                                override fun onSuccess() {

                                }
                            })
                    }
                }
            }

            Status.ERROR -> {
                showToastErrorAlertNotices(
                    getString(R.string.msg_failure),
                    object : AlertUtil.OnSuccessListener {
                        override fun onSuccess() {

                        }
                    })
            }
            Status.LOADING -> {
                showProgressView()
            }
        }
    }


}