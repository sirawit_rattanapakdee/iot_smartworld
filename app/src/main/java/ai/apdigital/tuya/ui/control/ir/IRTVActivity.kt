package ai.apdigital.tuya.ui.control.ir

import ai.apdigital.tuya.R
import ai.apdigital.tuya.base.BaseActivity
import ai.apdigital.tuya.model.AddRemoteEntity
import ai.apdigital.tuya.model.RemoteIndexEntity
import ai.apdigital.tuya.model.TokenEntity
import ai.apdigital.tuya.networking.Resource
import ai.apdigital.tuya.networking.Status
import ai.apdigital.tuya.base.Constants
import ai.apdigital.tuya.ui.viewmodel.TuyaIRViewModel
import ai.apdigital.tuya.utils.EventIRAirUtils
import ai.apdigital.tuya.utils.EventIRTVUtils
import ai.apdigital.tuya.utils.EventUtils
import ai.apdigital.tuya.utils.PageUtils
import ai.apdigital.tuya.utils.alerts.AlertUtil
import ai.apdigital.tuya.utils.alerts.EditRoomDialogFragment
import android.content.Intent
import android.content.res.ColorStateList
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.widget.ImageView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.core.content.ContextCompat
import androidx.core.widget.ImageViewCompat
import androidx.lifecycle.Observer
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.tuya.smart.home.sdk.TuyaHomeSdk
import kotlinx.android.synthetic.main.activity_door_lock_temporary_password_create.*
import kotlinx.android.synthetic.main.activity_ir_tv.*
import kotlinx.android.synthetic.main.layout_ir_index.*
import kotlinx.android.synthetic.main.toolbar_back_with_control_panel.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*


class IRTVActivity : BaseActivity(), View.OnTouchListener {
    var deviceId: String = ""
    var cateId: String = ""
    var brandId: String = ""
    var remoteId: String = ""
    var dialogNumber: BottomSheetDialog? = null
    private val tuyaIRVM: TuyaIRViewModel? by viewModel()
    private var colorStateBlack: ColorStateList? = null
    private var colorStateGray: ColorStateList? = null
    private var colorStateYellow: ColorStateList? = null
    private var colorStateGrayPTZ: ColorStateList? = null
    var event_type_name = ""

    override fun getLayoutView() = R.layout.activity_ir_tv

    override fun bindView() {
        colorStateGray = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.gray_camera))
        colorStateBlack = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.black))
        colorStateYellow = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.yellow))
        colorStateGrayPTZ = ColorStateList.valueOf(ContextCompat.getColor(this, R.color.gray_ptz))
    }

    override fun setupInstance() {
        setupToolbar(
            getString(R.string.title_tv),
            showButtonBack = true,
            showRightMenu = true
        )
    }

    override fun setupView() {
        menuEdit?.setOnClickListener {
            EventUtils.sendEvent(
                EventUtils.TYPE_EVENT,
                EventIRTVUtils.TUYA_PANEL_IR_TV_DEVICE_DETAIL,
                ""
            )
            val intent = Intent(this@IRTVActivity, IRDetailActivity::class.java)
            intent.putExtra(Constants.INTENT_DEV_ID, deviceId)
            intent.putExtra(Constants.INTENT_REMOTE_ID, remoteId)
            startForDeviceDetailResult.launch(intent)
        }

        btnSlideUp?.setOnTouchListener(this)
        btnSlideDown?.setOnTouchListener(this)
        btnSlideRight?.setOnTouchListener(this)
        btnSlideLeft?.setOnTouchListener(this)

        btnBack?.setOnClickListener { publishDps("Back") }
        btnIncrease?.setOnClickListener { publishDps("Volume+", "vol", "up") }
        btnDecrease?.setOnClickListener { publishDps("Volume-", "vol", "down") }
        btnIncreaseChannel?.setOnClickListener { publishDps("Channel+", "ch", "up") }
        btnDecreaseChannel?.setOnClickListener { publishDps("Channel-", "ch", "down") }
        btnPower?.setOnClickListener { publishDps("Power") }
        btnMenu?.setOnClickListener { publishDps("Menu") }
        btnOK?.setOnClickListener { publishDps("OK") }
        btnHome?.setOnClickListener { publishDps("Home") }
        btnMute?.setOnClickListener { publishDps("Mute") }
        btnInput?.setOnClickListener {
            publishDps("Input")
        }
        btnNumber?.setOnClickListener {
            dialogNumber?.show()
        }
        val dialogView: View? =
            layoutInflater.inflate(R.layout.layout_number_bottom_sheet, null)
        dialogView?.let { view ->
            dialogNumber = BottomSheetDialog(this)
            dialogNumber?.setContentView(view)
            val btn1 = view.findViewById<LinearLayoutCompat?>(R.id.btn1)
            val btn2 = view.findViewById<LinearLayoutCompat?>(R.id.btn2)
            val btn3 = view.findViewById<LinearLayoutCompat?>(R.id.btn3)
            val btn4 = view.findViewById<LinearLayoutCompat?>(R.id.btn4)
            val btn5 = view.findViewById<LinearLayoutCompat?>(R.id.btn5)
            val btn6 = view.findViewById<LinearLayoutCompat?>(R.id.btn6)
            val btn7 = view.findViewById<LinearLayoutCompat?>(R.id.btn7)
            val btn8 = view.findViewById<LinearLayoutCompat?>(R.id.btn8)
            val btn9 = view.findViewById<LinearLayoutCompat?>(R.id.btn9)
            val btn0 = view.findViewById<LinearLayoutCompat?>(R.id.btn0)
            val btnDot = view.findViewById<LinearLayoutCompat?>(R.id.btnDot)
            val btnShift = view.findViewById<LinearLayoutCompat?>(R.id.btnShift)

            btn1?.setOnClickListener { publishDpsNumber("1") }
            btn2?.setOnClickListener { publishDpsNumber("2") }
            btn3?.setOnClickListener { publishDpsNumber("3") }
            btn4?.setOnClickListener { publishDpsNumber("4") }
            btn5?.setOnClickListener { publishDpsNumber("5") }
            btn6?.setOnClickListener { publishDpsNumber("6") }
            btn7?.setOnClickListener { publishDpsNumber("7") }
            btn8?.setOnClickListener { publishDpsNumber("8") }
            btn9?.setOnClickListener { publishDpsNumber("9") }
            btn0?.setOnClickListener { publishDpsNumber("0") }
            btnDot?.setOnClickListener { publishDpsNumber("*") }
            btnShift?.setOnClickListener {
                publishDpsNumber("-/–")
            }
        }
        //nestedScrollView?.post { nestedScrollView?.fullScroll(View.FOCUS_DOWN) }
    }

    private val tokenResp = Observer<Resource<TokenEntity>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                hideProgressView()
                if (remoteCommand == RemoteType.ADD) {
                    tuyaIRVM?.callRemotedIndex(deviceId, cateId, brandId)
                }
            }

            Status.ERROR -> {
                hideProgressView()
                showToastErrorAlertNotices(
                    resource.message?.message ?: getString(R.string.msg_failure), null
                )
            }
            Status.LOADING -> {
                showProgressView()
            }
        }
    }

    var positionIndex: Int = 0
    var remoteCommand: RemoteType? = RemoteType.CONTROL
    var brandName: String? = ""

    override fun initialize(savedInstanceState: Bundle?) {

        tuyaIRVM?.getRemoteCommandListener()?.observe(this, airResp)
        tuyaIRVM?.getRemotedIndexListener()?.observe(this, remoteIndexResp)
        tuyaIRVM?.getTokenListener()?.observe(this, tokenResp)
        tuyaIRVM?.getToken()

        deviceId = intent.getStringExtra(Constants.INTENT_DEV_ID) ?: ""
        remoteCommand = intent.getSerializableExtra(Constants.INTENT_REMOTE_TYPE) as RemoteType
        if (remoteCommand == RemoteType.ADD) {
            EventUtils.sendEvent(EventUtils.TYPE_PAGE, PageUtils.TUYA_PANEL_IR_MATCH.plus("tv"), "")
            cateId = intent.getStringExtra(Constants.INTENT_CATE_ID) ?: ""
            brandId = intent.getStringExtra(Constants.INTENT_BRAND_ID) ?: ""
            brandName = intent.getStringExtra(Constants.INTENT_BRAND_NAME) ?: ""
            boxTest?.visibility = View.VISIBLE
            tuyaIRVM?.addRemoteListener()?.observe(this, addResp)
            btnNext?.setOnClickListener {
                EventUtils.sendEvent(
                    EventUtils.TYPE_EVENT,
                    EventIRTVUtils.TUYA_PANEL_IR_MATCH_TV_NEXT,
                    ""
                )
                if (remoteIndex?.size ?: 0 > 0) {
                    if (positionIndex >= ((remoteIndex?.size ?: 0) - 1))
                        positionIndex = 0
                    else
                        positionIndex++
                    setUpIndexCheck()
                }
            }

            btnCanControl?.setOnClickListener {
                EventUtils.sendEvent(
                    EventUtils.TYPE_EVENT,
                    EventIRTVUtils.TUYA_PANEL_IR_MATCH_TV_CAN_CONTROL,
                    ""
                )
                showAlertConfirmWithActionClick(
                    getString(R.string.alert_confirm_can_control),
                    brandName,
                    object : AlertUtil.OnSuccessListener {
                        override fun onSuccess() {
                            confirmAddRemote()
                        }
                    },
                    R.color.black
                )
            }
            tvTitle?.text = getString(R.string.title_tv_add)

        } else {
            EventUtils.sendEvent(EventUtils.TYPE_PAGE, PageUtils.TUYA_PANEL_IR.plus("tv"), "")
            boxTest?.visibility = View.GONE
            remoteId = intent.getStringExtra(Constants.INTENT_REMOTE_ID) ?: ""
            val deviceBean =
                TuyaHomeSdk.getDataInstance().getDeviceBean(remoteId)
            deviceBean?.let {
                tvTitle?.text = it.name
            }
        }
    }

    fun confirmAddRemote() {
        supportFragmentManager.let {
            val notices = EditRoomDialogFragment(
                getString(R.string.alert_remote_name),
                brandName,
                object : EditRoomDialogFragment.OnEditNameListener {
                    override fun onSaveClick(name: String) {
                        tuyaIRVM?.callAddRemote(
                            deviceId,
                            cateId,
                            brandId,
                            brandName,
                            remoteIndex?.get(positionIndex)?.remote_index,
                            name
                        )
                    }
                })
            notices.isCancelable = false
            notices.show(
                it,
                "showAlertNotices"
            )
        }
    }

    private fun setUpIndexCheck() {
        btnNext?.text =
            getString(R.string.action_next_one).plus(" ").plus((positionIndex + 1).toString())
                .plus("/").plus(remoteIndex?.size)
                .plus(")")
    }

    var remoteIndex: MutableList<RemoteIndexEntity>? = null
    private val remoteIndexResp = Observer<Resource<MutableList<RemoteIndexEntity>>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                hideProgressView()
                remoteIndex = resource.data
                if (remoteIndex?.size ?: 0 > 0) {
                    btnNext?.isEnabled = true
                    setUpIndexCheck()
                } else
                    btnNext?.isEnabled = false
            }

            Status.ERROR -> {
                //swipeContainer?.isRefreshing = false
                hideProgressView()
                showToastErrorAlertNotices(
                    resource.message?.message ?: getString(R.string.msg_failure), null
                )
            }
            Status.LOADING -> {
                showProgressView()
            }
        }
    }

    private val airResp = Observer<Resource<Boolean>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                resource.data?.let {

                }
            }

            Status.ERROR -> {
                showToastErrorAlertNotices(
                    resource.message?.message ?: getString(R.string.msg_failure), null
                )
            }
            Status.LOADING -> {
            }
        }
    }

    private val addResp = Observer<Resource<AddRemoteEntity>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                hideProgressView()
                resource.data?.let {
                    showToastSuccessAlertNotices(
                        getString(R.string.msg_successfully),
                        object : AlertUtil.OnSuccessListener {
                            override fun onSuccess() {
                                val intent = Intent()
                                intent.putExtra("ACTION", "CREATE")
                                intent.putExtra("REMOTE_ID", it.remote_id)
                                setResult(RESULT_OK, intent)
                                finish()
                            }
                        })
                }
            }

            Status.ERROR -> {
                showToastErrorAlertNotices(
                    resource.message?.message ?: getString(R.string.msg_failure), null
                )
            }
            Status.LOADING -> {
                showProgressView()
            }
        }
    }


    var lastTouchId: View? = null
    override fun onTouch(v: View, event: MotionEvent): Boolean {

        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                lastTouchId = v
                lastTouchId?.let {
                    ImageViewCompat.setImageTintList(it as ImageView, colorStateYellow)
                }
            }
            MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> {
                lastTouchId?.let {
                    ImageViewCompat.setImageTintList(it as ImageView, colorStateGrayPTZ)
                }
                when (v.id) {
                    R.id.btnSlideUp -> {
                        publishDps("Up", "control", "up")
                    }
                    R.id.btnSlideDown -> {
                        publishDps("Down", "control", "down")
                    }
                    R.id.btnSlideLeft -> {
                        publishDps("Left", "control", "left")
                    }
                    R.id.btnSlideRight -> {
                        publishDps("Right", "control", "right")
                    }
                }
            }
        }
        return true
    }

    fun publishDps(command: String) {
        if (remoteCommand == RemoteType.CONTROL) {
            EventUtils.sendEvent(
                EventUtils.TYPE_EVENT,
                EventIRTVUtils.TUYA_PANEL_IR_TV.plus("_")
                    .plus(command.toLowerCase()),
                ""
            )
            tuyaIRVM?.callRemoteCommand(deviceId, remoteId, command)
        } else {
            EventUtils.sendEvent(
                EventUtils.TYPE_EVENT,
                EventIRTVUtils.TUYA_PANEL_IR_MATCH_TV.plus("_")
                    .plus(command.toLowerCase()),
                ""
            )
            tuyaIRVM?.callTestRemoteCommand(
                deviceId,
                cateId,
                command,
                remoteIndex?.get(positionIndex)?.remote_index
            )
        }
    }

    fun publishDpsNumber(command: String) {
        if (remoteCommand == RemoteType.CONTROL) {
            EventUtils.sendEvent(
                EventUtils.TYPE_EVENT,
                EventIRTVUtils.TUYA_PANEL_IR_TV.plus("_number"), ""
            )
            tuyaIRVM?.callRemoteCommand(deviceId, remoteId, command)
        } else {
            EventUtils.sendEvent(
                EventUtils.TYPE_EVENT,
                EventIRTVUtils.TUYA_PANEL_IR_MATCH_TV.plus("_number"), ""
            )
            tuyaIRVM?.callTestRemoteCommand(
                deviceId,
                cateId,
                command,
                remoteIndex?.get(positionIndex)?.remote_index
            )
        }
    }

    fun publishDps(command: String, key: String, track: String) {
        if (remoteCommand == RemoteType.CONTROL) {
            EventUtils.sendEvent(
                EventUtils.TYPE_EVENT,
                EventIRTVUtils.TUYA_PANEL_IR_TV.plus("_").plus(key),
                track.toLowerCase()
            )
            tuyaIRVM?.callRemoteCommand(deviceId, remoteId, command)
        } else {
            EventUtils.sendEvent(
                EventUtils.TYPE_EVENT,
                EventIRTVUtils.TUYA_PANEL_IR_MATCH_TV.plus("_").plus(key),
                track.toLowerCase()
            )
            tuyaIRVM?.callTestRemoteCommand(
                deviceId,
                cateId,
                command,
                remoteIndex?.get(positionIndex)?.remote_index
            )
        }
    }
}