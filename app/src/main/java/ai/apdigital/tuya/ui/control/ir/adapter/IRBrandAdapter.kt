package ai.apdigital.tuya.ui.control.ir.adapter

import ai.apdigital.tuya.R
import ai.apdigital.tuya.model.BrandEntity
import ai.apdigital.tuya.model.CategoryEntity
import ai.apdigital.tuya.model.RemoteDeviceEntity
import android.content.Context
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.tuya.smart.home.sdk.bean.RoomBean


class IRBrandAdapter(val context: Context) :
    RecyclerView.Adapter<IRBrandAdapter.AdapterViewHolder>() {

    interface OnItemClickListener {
        fun onRecyclerItemClick(view: View, position: Int, entity: BrandEntity?)
    }

    private var data: List<BrandEntity> = mutableListOf()
    private var mListener: OnItemClickListener? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val inflatedView = layoutInflater.inflate(
            R.layout.item_ir_brand,
            parent,
            false
        )
        return AdapterViewHolder(inflatedView, this)
    }


    fun setOnItemClickListener(onEntryClickListener: OnItemClickListener) {
        mListener = onEntryClickListener
    }

    override fun getItemCount(): Int = data.size
    override fun onBindViewHolder(
        viewHolder: AdapterViewHolder,
        position: Int
    ) {

        viewHolder.holdItem = data[position]
        viewHolder.holdItem?.let { entity ->
            viewHolder.tvBrandName?.text = entity.brand_name
            viewHolder.tvBrandDesc?.text = entity.brand_name
        }
    }

    fun updateData(items: List<BrandEntity>?) {
        this.data = mutableListOf()
        items?.let { it ->
            this.data = it.toMutableList()
        }
        notifyDataSetChanged()
    }

    inner class AdapterViewHolder(
        itemView: View,
        val adapterHome: IRBrandAdapter
    ) :
        RecyclerView.ViewHolder(itemView),
        View.OnClickListener, View.OnTouchListener {

        var holdItem: BrandEntity? = null
        private var container: ConstraintLayout? = itemView.findViewById(R.id.container)

        internal var tvBrandName: AppCompatTextView? = itemView.findViewById(R.id.tvBrandName)
        internal var tvBrandDesc: AppCompatTextView? = itemView.findViewById(R.id.tvBrandDesc)
        /*internal var ivCover: AppCompatImageView? = itemView.findViewById(R.id.ivCover)
        internal var ivEdit: AppCompatImageView? = itemView.findViewById(R.id.ivEdit)*/

        init {
            container?.setOnClickListener(this)
            container?.setOnTouchListener(this)
        }

        override fun onClick(v: View?) {
            mListener?.onRecyclerItemClick(itemView, layoutPosition, holdItem)
        }

        override fun onTouch(v: View?, event: MotionEvent?): Boolean {
            when (event?.action) {
                MotionEvent.ACTION_DOWN -> {
                    v?.alpha = 0.5f
                }
                MotionEvent.ACTION_UP
                -> {
                    v?.alpha = 1f
                }
                MotionEvent.ACTION_CANCEL -> {
                    v?.alpha = 1f
                }
            }
            return false
        }
    }
}