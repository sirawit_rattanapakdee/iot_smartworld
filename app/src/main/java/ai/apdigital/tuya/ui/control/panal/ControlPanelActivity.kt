package ai.apdigital.tuya.ui.control.panal

import ai.apdigital.tuya.R
import ai.apdigital.tuya.base.BaseActivity
import ai.apdigital.tuya.ui.DeviceContainer
import ai.apdigital.tuya.ui.control.DPMapper
import ai.apdigital.tuya.ui.control.DeviceDetailActivity
import ai.apdigital.tuya.base.Constants
import ai.apdigital.tuya.ui.control.panal.dpItem.*
import ai.apdigital.tuya.utils.EventUtils
import ai.apdigital.tuya.utils.PageUtils
import ai.apdigital.tuya.utils.SchemaConverter
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import com.google.gson.Gson
import com.tuya.smart.android.device.bean.*
import com.tuya.smart.android.device.enums.DataTypeEnum
import com.tuya.smart.home.sdk.TuyaHomeSdk
import com.tuya.smart.home.sdk.utils.SchemaMapper
import com.tuya.smart.sdk.api.IDevListener
import com.tuya.smart.sdk.api.ITuyaDevice
import kotlinx.android.synthetic.main.device_mgt_activity_control.*
import kotlinx.android.synthetic.main.toolbar_back_with_control_panel.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.*
import kotlin.collections.HashMap

class ControlPanelActivity : BaseActivity() {
    private var dpPoints: HashMap<String, String>? = null
    private var deviceContainer: DeviceContainer? = null
    private var ignoreDPs: MutableList<String>? = mutableListOf()
    private var dpsWidget: MutableList<WidgetPack>? = mutableListOf()
    private var deviceId: String? = null
    var mDevice: ITuyaDevice? = null
    override fun getLayoutView() = R.layout.device_mgt_activity_control

    override fun bindView() {
    }

    override fun setupInstance() {
    }

    override fun setupView() {
        setupToolbar(
            getString(R.string.smart_home),
            showButtonBack = true,
            showRightMenu = true
        )
    }

    val startForDeviceDetailControlResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                setResult(Activity.RESULT_OK, result.data)
                when {
                    intent?.getStringExtra("ACTION") == "REMOVE" -> {
                        finish()
                    }
                    intent?.getStringExtra("ACTION") == "RENAME" -> {
                        tvTitle?.text = intent.getStringExtra("NAME")
                        isUpdate = true
                    }
                }
            }
        }

    private val devListener = object : IDevListener {
        override fun onDpUpdate(devId: String?, dpStr: String?) {
            val dps: Map<*, *>? = Gson().fromJson(dpStr, Map::class.java)
            dps?.forEach { dp ->
                //
                /*if(dp.c)*/

                dpsWidget?.forEach widget@{ content ->
                    val dpKey = dp.key as String
                    if (content.dp.contains(dpKey)) {
                        when (content.viewName) {
                            "DpBooleanItem" -> {
                                (content.widget as DpBooleanItem).updateValue(dp)
                            }
                            "DpStatusItem" -> {
                                // (content.widget as DpStatusItem).updateValue(dp)
                            }
                            "DpBooleanStartItem" -> {
                                (content.widget as DpBooleanStartItem).updateValue(dp)
                            }
                            "DpEnumItem" -> {
                                (content.widget as DpEnumItem).updateValue(dp)
                            }
                            "DpSceneItem" -> {
                                (content.widget as DpSceneItem).updateValue(dp)
                            }
                            "DpBoolStatusItem" -> {
                                (content.widget as DpBoolWithIconStatusItem).updateValue(
                                    dp
                                )
                            }
                            "DpColorItem" -> {
                                (content.widget as DpColorItem).updateValue(dp)
                            }
                            "DpValueTemperatureItem" -> {
                                (content.widget as DpValueTemperatureItem).updateValue(
                                    dp
                                )
                            }
                            "DpValueCountdownItem" -> {
                                //(content.widget as DpValueCountdownItem).updateValue(dp)
                            }
                            "DpValueSlideItem" -> {
                                (content.widget as DpValueSlideItem).updateValue(dp)
                            }
                            "DpValueItem" -> {
                                (content.widget as DpValueIntItem).updateValue(dp)
                            }
                            "DpFaultItem" -> {
                                //(content.widget as DpFaultItem).updateValue(dp)
                            }
                            "DpValueStringItem" -> {
                                (content.widget as DpValueStringItem).updateValue(dp)
                            }
                            "DpDoorContactStatusItem" -> {
                                (content.widget as DpDoorContactStatusItem).updateValue(
                                    dp
                                )
                            }
                            "DpBooleanStringItem" -> {
                                (content.widget as DpBooleanStringItem).updateValue(dp)
                            }
                            "DpBooleanStartAndStopItem" -> {
                                (content.widget as DpBooleanStartAndStopItem).updateValue(
                                    dp
                                )
                            }
                            "DpStringStatusItem" -> {
                                (content.widget as DpStringStatusItem).updateValue(dp)
                            }
                            "DpMemoryCardStatusItem" -> {
                                (content.widget as DpMemoryCardStatusItem).updateValue(dp)
                            }
                            else -> {
                            }
                        }
                        return@widget
                    }
                }
            }
        }

        override fun onRemoved(devId: String?) {
        }

        override fun onStatusChanged(devId: String?, online: Boolean) {

        }

        override fun onNetworkStatusChanged(devId: String?, status: Boolean) {

        }

        override fun onDevInfoUpdate(devId: String?) {

        }
    }

    override fun initialize(savedInstanceState: Bundle?) {


        this.dpPoints = DPMapper.getDPFromAsset(this@ControlPanelActivity, "dp.json")

        ignoreDPs = intent.getStringArrayListExtra(Constants.INTENT_IGNORE_DPS) ?: mutableListOf()
        deviceId = intent.getStringExtra(Constants.INTENT_DEV_ID)
        val isCamera = intent.getBooleanExtra(Constants.INTENT_DEV_CAMERA, false)

        deviceContainer = SchemaConverter.convertSchemaDevice(deviceId ?: "")
        EventUtils.sendEvent(
            EventUtils.TYPE_PAGE,
            PageUtils.TUYA_PANEL_GENERAL_DEVICE,
            deviceContainer?.device?.deviceCategory
        )
        menuEdit?.setOnClickListener {
            val intent = Intent(this@ControlPanelActivity, DeviceDetailActivity::class.java)
            intent.putExtra(Constants.INTENT_DEV_ID, deviceId)
            startForDeviceDetailControlResult.launch(intent)
        }

        if (deviceContainer?.switchCount ?: 0 > 1) {
            replaceFragmentToActivity(
                R.id.mainPanel,
                MultiplePanelFragment.newInstance(deviceContainer)
            )
        } else {
            replaceFragmentToActivity(
                R.id.mainPanel,
                SinglePanelFragment.newInstance(deviceContainer)
            )
        }

        deviceId?.let { it ->

            val deviceBean = TuyaHomeSdk.getDataInstance().getDeviceBean(it)
            GlobalScope.launch {
                mDevice = TuyaHomeSdk.newDeviceInstance(it)
                mDevice?.registerDevListener(devListener)
            }
            tvTitle?.text = deviceBean?.name
            var switchCount = 0

            var dpStart = 0
            var dpStop = 0
            var dps: MutableMap<Int, SchemaBean> = mutableMapOf()
            TuyaHomeSdk.getDataInstance().getSchema(it)?.forEach {
                if (ignoreDPs?.contains(it.value?.code) == false)
                    dps[it.key.toInt()] = it.value

                if (it.value?.code?.toLowerCase(Locale.ROOT)?.equals("start") == true)
                    dpStart = it.key.toInt()

                if (it.value?.code?.toLowerCase(Locale.ROOT)?.equals("stop") == true)
                    dpStop = it.key.toInt()
            }

            if (isCamera) {
                val vItem =
                    DpMemoryCardStatusItem(
                        this,
                        supportFragmentManager = supportFragmentManager,
                        deviceBean = deviceBean,
                        device = mDevice
                    )
                llDp?.addView(vItem)
                dpsWidget?.add(
                    WidgetPack(
                        arrayListOf("109", "110", "111", "117", "150", "151"),
                        vItem,
                        "DpMemoryCardStatusItem"
                    )
                )
            }

            dps = dps.toSortedMap()
            if (dpStop != 0 && dpStart != 0) {
                val vItem =
                    DpBooleanStartAndStopItem(
                        this,
                        dpStart = dpStart,
                        dpStop = dpStop,
                        schemaBeanStart = dps[dpStart],
                        device = mDevice,
                        devId = deviceId
                    )
                llDp?.addView(vItem)
                dpsWidget?.add(
                    WidgetPack(
                        arrayListOf(dpStart.toString()),
                        vItem,
                        "DpBooleanStartAndStopItem"
                    )
                )
                dps.remove(dpStop)
                dps.remove(dpStart)
            }

            dps.values.forEach { bean ->
                val value = deviceBean?.dps?.get(bean.id)
                Timber.e("DP ${bean.id}, Code : ${bean.code}, Value :$value, ${bean.getSchemaType()}")
                if (bean.type == DataTypeEnum.OBJ.type) {
                    //if (bean.mode.contains("w")) {
                    when (bean.getSchemaType()) {
                        BoolSchemaBean.type -> {
                            when {
                                bean.code?.contains("doorcontact") == true -> {
                                    val vItem =
                                        DpDoorContactStatusItem(
                                            this,
                                            schemaBean = bean,
                                            value = value as Boolean,
                                            device = mDevice,
                                            name = (dpPoints?.get(bean.code)
                                                ?: convertDPToName(bean.code))
                                        )
                                    llDp?.addView(vItem)
                                    dpsWidget?.add(
                                        WidgetPack(
                                            arrayListOf(bean.id),
                                            vItem,
                                            "DpDoorContactStatusItem"
                                        )
                                    )
                                }
                                bean.code?.startsWith("PIR") == true -> {
                                    val vItem = DpBoolWithIconStatusItem(
                                        this,
                                        schemaBean = bean,
                                        value = value as Boolean,
                                        device = mDevice,
                                        name = (dpPoints?.get(bean.code)
                                            ?: convertDPToName(bean.code))
                                    )
                                    dpsWidget?.add(
                                        WidgetPack(
                                            arrayListOf(bean.id),
                                            vItem,
                                            "DpBoolStatusItem"
                                        )
                                    )
                                    llDp?.addView(vItem)
                                }
                                else -> {
                                    when {
                                        switchCount >= deviceContainer?.switchCount ?: 0 -> {
                                            Timber.e("$switchCount : DpBooleanItem : ${deviceContainer?.switchCount}")
                                            when {
                                                bean.mode.contains("w") -> {
                                                    val vItem =
                                                        DpBooleanItem(
                                                            this,
                                                            schemaBean = bean,
                                                            value = value as Boolean,
                                                            device = mDevice,
                                                            name = (dpPoints?.get(bean.code)
                                                                ?: convertDPToName(bean.code))
                                                        )
                                                    llDp?.addView(vItem)
                                                    dpsWidget?.add(
                                                        WidgetPack(
                                                            arrayListOf(bean.id),
                                                            vItem,
                                                            "DpBooleanItem"
                                                        )
                                                    )
                                                    switchCount++
                                                }
                                                else -> {
                                                    val vItem = DpBooleanStringItem(
                                                        this,
                                                        schemaBean = bean,
                                                        value = (if (value as Boolean) "ON" else "OFF"),
                                                        device = mDevice,
                                                        name = (dpPoints?.get(bean.code)
                                                            ?: convertDPToName(bean.code))
                                                    )
                                                    dpsWidget?.add(
                                                        WidgetPack(
                                                            arrayListOf(bean.id),
                                                            vItem,
                                                            "DpBooleanStringItem"
                                                        )
                                                    )
                                                    llDp?.addView(vItem)
                                                }
                                            }
                                        }
                                        bean.code?.startsWith("start") == true || bean.code?.startsWith(
                                            "stop"
                                        ) == true -> {
                                            if (bean.mode.contains("w")) {
                                                val vItem =
                                                    DpBooleanStartItem(
                                                        this,
                                                        schemaBean = bean,
                                                        value = value as Boolean,
                                                        device = mDevice,
                                                        name = dpPoints?.get(bean.code)
                                                            ?: convertDPToName(bean.code)
                                                    )
                                                llDp?.addView(vItem)
                                                dpsWidget?.add(
                                                    WidgetPack(
                                                        arrayListOf(bean.id),
                                                        vItem,
                                                        "DpBooleanStartItem"
                                                    )
                                                )
                                            } else {
                                                val vItem = DpValueIntItem(
                                                    this,
                                                    schemaBean = bean,
                                                    value = value as Int,
                                                    device = mDevice,
                                                    name = (dpPoints?.get(bean.code)
                                                        ?: convertDPToName(bean.code))
                                                )
                                                dpsWidget?.add(
                                                    WidgetPack(
                                                        arrayListOf(bean.id),
                                                        vItem,
                                                        "DpValueItem"
                                                    )
                                                )
                                                llDp?.addView(vItem)
                                            }
                                        }
                                        bean.code?.startsWith("switch") == false && bean.code
                                            ?.startsWith("led_switch") == false -> {
                                            if (bean.mode.contains("w")) {
                                                val vItem =
                                                    DpBooleanItem(
                                                        this,
                                                        schemaBean = bean,
                                                        value = value as Boolean,
                                                        device = mDevice,
                                                        name = dpPoints?.get(bean.code)
                                                            ?: convertDPToName(bean.code)
                                                    )
                                                llDp?.addView(vItem)
                                                dpsWidget?.add(
                                                    WidgetPack(
                                                        arrayListOf(bean.id),
                                                        vItem,
                                                        "DpBooleanItem"
                                                    )
                                                )
                                            } else {
                                                val vItem = DpValueIntItem(
                                                    this,
                                                    schemaBean = bean,
                                                    value = value as Int,
                                                    device = mDevice,
                                                    name = (dpPoints?.get(bean.code)
                                                        ?: convertDPToName(bean.code))
                                                )
                                                dpsWidget?.add(
                                                    WidgetPack(
                                                        arrayListOf(bean.id),
                                                        vItem,
                                                        "DpValueItem"
                                                    )
                                                )
                                                llDp?.addView(vItem)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        EnumSchemaBean.type -> {
                            val vItem: View?

                            if (bean.mode.contains("w")) {
                                when {
                                    bean.code?.startsWith("direction_control") == true -> {
                                        vItem = DpRobotControlItem(
                                            this,
                                            schemaBean = bean,
                                            value = value.toString(),
                                            device = mDevice,
                                            name = (dpPoints?.get(bean.code)
                                                ?: convertDPToName(bean.code))
                                        )
                                        dpsWidget?.add(
                                            WidgetPack(
                                                arrayListOf(bean.id),
                                                vItem,
                                                "DpRobotControlItem"
                                            )
                                        )
                                    }
                                    bean.code?.startsWith("control") == true -> {
                                        vItem = DpCurtainControlItem(
                                            this,
                                            schemaBean = bean,
                                            value = value.toString(),
                                            device = mDevice,
                                            name = (dpPoints?.get(bean.code)
                                                ?: convertDPToName(bean.code))
                                        )
                                        dpsWidget?.add(
                                            WidgetPack(
                                                arrayListOf(bean.id),
                                                vItem,
                                                "DpRobotControlItem"
                                            )
                                        )
                                    }
                                    else -> {
                                        vItem = DpEnumItem(
                                            this,
                                            schemaBean = bean,
                                            value = value.toString(),
                                            device = mDevice,
                                            name = (dpPoints?.get(bean.code)
                                                ?: convertDPToName(bean.code))
                                        )
                                        dpsWidget?.add(
                                            WidgetPack(
                                                arrayListOf(bean.id),
                                                vItem,
                                                "DpEnumItem"
                                            )
                                        )
                                    }
                                }
                            } else {
                                when {
                                    bean.code?.startsWith("presence_state") == true -> {
                                        vItem = DpStringStatusItem(
                                            this,
                                            schemaBean = bean,
                                            value = value as String,
                                            device = mDevice,
                                            name = (dpPoints?.get(bean.code)
                                                ?: convertDPToName(bean.code))
                                        )
                                        dpsWidget?.add(
                                            WidgetPack(
                                                arrayListOf(bean.id),
                                                vItem,
                                                "DpStringStatusItem"
                                            )
                                        )
                                    }
                                    bean.code?.startsWith("pir_state") == true -> {
                                        vItem = DpBoolWithIconStatusItem(
                                            this,
                                            schemaBean = bean,
                                            value = !(value as String).equals(
                                                "none",
                                                ignoreCase = true
                                            ),
                                            device = mDevice,
                                            name = (dpPoints?.get(bean.code)
                                                ?: convertDPToName(bean.code))
                                        )
                                        dpsWidget?.add(
                                            WidgetPack(
                                                arrayListOf(bean.id),
                                                vItem,
                                                "DpBoolStatusItem"
                                            )
                                        )
                                        llDp?.addView(vItem)
                                    }
                                    else -> {
                                        vItem = DpValueStringItem(
                                            this,
                                            schemaBean = bean,
                                            value = value,
                                            device = mDevice,
                                            name = (dpPoints?.get(bean.code)
                                                ?: convertDPToName(bean.code))
                                        )
                                        dpsWidget?.add(
                                            WidgetPack(
                                                arrayListOf(bean.id),
                                                vItem,
                                                "DpValueStringItem"
                                            )
                                        )
                                    }
                                }
                            }


                            if (vItem != null)
                                llDp.addView(vItem)
                        }
                        StringSchemaBean.type -> {
                            val vItem: View?
                            when {
                                bean.code?.startsWith("colour_data") == true -> {
                                    vItem = DpColorItem(
                                        this,
                                        schemaBean = bean,
                                        valueData = value, tuyaDevice = mDevice,
                                        name = dpPoints?.get(bean.code)
                                            ?: convertDPToName(bean.code)
                                    )

                                    dpsWidget?.add(
                                        WidgetPack(
                                            arrayListOf(bean.id),
                                            vItem,
                                            "DpColorItem"
                                        )
                                    )
                                }
                                bean.code?.startsWith("scene_data") == true -> {
                                    vItem = DpSceneItem(
                                        this,
                                        schemaBean = bean,
                                        value = value.toString(),
                                        device = mDevice,
                                        name = (dpPoints?.get(bean.code)
                                            ?: convertDPToName(bean.code))
                                    )
                                    dpsWidget?.add(
                                        WidgetPack(
                                            arrayListOf(bean.id),
                                            vItem,
                                            "DpSceneItem"
                                        )
                                    )
                                }
                                else -> {
                                    vItem = null
                                    //if (bean.mode.contains("w")) {
                                    /*vItem = DpValueStringItem(
                                        this,
                                        schemaBean = bean,
                                        value = value,
                                        device = mDevice,
                                        name = (dpPoints?.get(bean.code)
                                            ?: convertDPToName(bean.code))
                                    )
                                    dpsWidget?.add(
                                        WidgetPack(
                                            bean.id,
                                            vItem,
                                            "DpValueStringItem"
                                        )
                                    )*/
                                }
                            }
                            if (vItem != null)
                                llDp.addView(vItem)

                        }
                        ValueSchemaBean.type -> {
                            if ("battery_percentage" != bean.code && "battary" != bean.code && "battery" != bean.code && "residual_electricity" != bean.code && "Battery" != bean.code) {
                                val vItem: View?
                                when {
                                    bean.code?.startsWith("temperature") == true -> {
                                        vItem = DpValueTemperatureItem(
                                            this,
                                            schemaBean = bean,
                                            value = value as Int,
                                            device = mDevice,
                                            name = dpPoints?.get(bean.code)
                                                ?: convertDPToName(bean.code)
                                        )
                                        dpsWidget?.add(
                                            WidgetPack(
                                                arrayListOf(bean.id),
                                                vItem,
                                                "DpValueTemperatureItem"
                                            )
                                        )

                                    }
                                    bean.code.startsWith("countdown") -> {
                                        vItem = DpValueCountdownItem(
                                            this,
                                            schemaBean = bean,
                                            value = value as Int,
                                            device = mDevice,
                                            name = (dpPoints?.get(bean.code)
                                                ?: convertDPToName(bean.code))
                                        )
                                        dpsWidget?.add(
                                            WidgetPack(
                                                arrayListOf(bean.id),
                                                vItem,
                                                "DpValueCountdownItem"
                                            )
                                        )
                                    }
                                    else -> {
                                        val valueSchemaBean =
                                            SchemaMapper.toValueSchema(bean.property)
                                        //Timber.e("valueSchemaBean.max : ${valueSchemaBean.max}")


                                        if (bean.mode.contains(
                                                "w"
                                            )
                                        ) {
                                            if (valueSchemaBean.max in 51..1000) {
                                                vItem = DpValueSlideItem(
                                                    this,
                                                    schemaBean = bean,
                                                    value = value as Int,
                                                    device = mDevice,
                                                    name = (dpPoints?.get(bean.code)
                                                        ?: convertDPToName(bean.code))
                                                )
                                                dpsWidget?.add(
                                                    WidgetPack(
                                                        arrayListOf(bean.id),
                                                        vItem,
                                                        "DpValueSlideItem"
                                                    )
                                                )
                                            } else {
                                                vItem = DpValueIntItem(
                                                    this,
                                                    schemaBean = bean,
                                                    value = value as Int,
                                                    device = mDevice,
                                                    name = (dpPoints?.get(bean.code)
                                                        ?: convertDPToName(bean.code))
                                                )
                                                dpsWidget?.add(
                                                    WidgetPack(
                                                        arrayListOf(bean.id),
                                                        vItem,
                                                        "DpValueIntItem"
                                                    )
                                                )
                                            }

                                        } else {
                                            vItem = DpValueStringItem(
                                                this,
                                                schemaBean = bean,
                                                value = value,
                                                device = mDevice,
                                                name = (dpPoints?.get(bean.code)
                                                    ?: convertDPToName(bean.code))
                                            )
                                            dpsWidget?.add(
                                                WidgetPack(
                                                    arrayListOf(bean.id),
                                                    vItem,
                                                    "DpValueStringItem"
                                                )
                                            )
                                        }
                                    }
                                }


                                if (vItem != null)
                                    llDp.addView(vItem)
                            }
                        }/*
                        BitmapSchemaBean.type -> {
                            val vItem =
                                DpFaultItem(
                                    this,
                                    schemaBean = bean,
                                    value = value.toString(),
                                    name = (dpPoints?.get(bean.code)
                                        ?: convertDPToName(bean.code))
                                )
                            llDp?.addView(vItem)
                            dpsWidget?.add(WidgetPack(bean.id, vItem, "DpFaultItem"))
                        }*/
                        else -> {

                        }
                    }
                    /*} else {
                        val vItem = DpValueItem(
                            this,
                            schemaBean = bean,
                            value = value as Int,
                            device = mDevice,
                            name = (dpPoints?.get(bean.code)
                                ?: convertDPToName(bean.code))
                        )
                        dpsWidget?.add(
                            WidgetPack(
                                bean.id,
                                vItem,
                                "DpValueItem"
                            )
                        )
                        llDp?.addView(vItem)
                    }*/
                } else if (bean.type == DataTypeEnum.RAW.type) {
                    Timber.e("Other ::::::: ")
                    when {
                        bean.code?.startsWith("colour_data") == true -> {
                            Timber.e("Other ::::::: colour_data")
                            val vItem = DpColorItem(
                                this,
                                schemaBean = bean,
                                valueData = value, tuyaDevice = mDevice,
                                name = dpPoints?.get(bean.code)
                                    ?: convertDPToName(bean.code)
                            )
                            dpsWidget?.add(
                                WidgetPack(
                                    arrayListOf(bean.id),
                                    vItem,
                                    "DpStringColorItem"
                                )
                            )
                            llDp?.addView(vItem)
                        }
                        else -> {
                            /*  val vItem = DpValueStringItem(
                                  this,
                                  schemaBean = bean,
                                  value = value,
                                  device = mDevice,
                                  name = (dpPoints?.get(bean.code)
                                      ?: convertDPToName(bean.code))
                              )
                              dpsWidget?.add(
                                  WidgetPack(
                                      bean.id,
                                      vItem,
                                      "DpValueStringItem"
                                  )
                              )
                              llDp?.addView(vItem)*/
                        }
                    }
                }

            }

        }
    }

    private fun convertDPToName(value: String): String {
        var key = value
        return if (key.contains("_")) {
            val dataSplit = key.split("_")
            key = ""
            dataSplit.forEach {
                key += it.capitalize().plus(" ")
            }
            key
        } else {
            key.capitalize()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mDevice?.registerDevListener(devListener)
        mDevice?.onDestroy()
        dpPoints = null
    }
}