package ai.apdigital.tuya.ui.control.panal

import ai.apdigital.tuya.R
import ai.apdigital.tuya.base.BaseFragment
import ai.apdigital.tuya.model.CategoryEntity
import ai.apdigital.tuya.model.TokenEntity
import ai.apdigital.tuya.networking.Resource
import ai.apdigital.tuya.networking.Status
import ai.apdigital.tuya.ui.DeviceContainer
import ai.apdigital.tuya.ui.DeviceSwitchValue
import ai.apdigital.tuya.ui.viewmodel.TuyaIRViewModel
import ai.apdigital.tuya.utils.EventGeneralDeviceUtils
import ai.apdigital.tuya.utils.EventHomeUtils
import ai.apdigital.tuya.utils.EventUtils
import ai.apdigital.tuya.utils.SchemaConverter
import ai.apdigital.tuya.utils.alerts.EditRoomDialogFragment
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.lifecycle.Observer
import com.alibaba.fastjson.JSONObject
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.tuya.smart.home.sdk.TuyaHomeSdk
import com.tuya.smart.sdk.api.IDevListener
import com.tuya.smart.sdk.api.IResultCallback
import com.tuya.smart.sdk.api.ITuyaDevice
import kotlinx.android.synthetic.main.device_mgt_item_multiple_dps.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class MultiplePanelFragment : BaseFragment() {
    private var deviceContainer: DeviceContainer? = null
    private var deviceInstance: ITuyaDevice? = null
    private val tuyaIRVM: TuyaIRViewModel? by viewModel()

    companion object {
        fun newInstance(deviceContainer: DeviceContainer?) =
            MultiplePanelFragment()
                .apply {
                    this.deviceContainer = deviceContainer
                    arguments = Bundle().apply {
                    }
                }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {

    }

    override fun getLayoutView() = R.layout.device_mgt_item_multiple_dps

    override fun setupInstance() {

    }

    override fun setupView() {
        tuyaIRVM?.getTokenListener()?.observe(this, tokenResp)
        tuyaIRVM?.switchUpdateListener()?.observe(this, renameSuccess)
    }

    fun updateName(name: String) {
        tvDeviceName?.text = name
    }

    override fun initialize() {
        val entity = deviceContainer?.device
        ivDevice?.let { iv ->
            context?.let {
                Glide
                    .with(it)
                    .load(entity?.iconUrl)
                    .into(iv)
            }
        }

        val roomBean = TuyaHomeSdk.getDataInstance().getDeviceRoomBean(entity?.devId ?: "")
        roomBean?.let {
            tvRoom?.text = it.name
        }

        GlobalScope.launch {
            deviceInstance = TuyaHomeSdk.newDeviceInstance(entity?.devId)
            deviceInstance?.registerDevListener(object : IDevListener {
                override fun onDpUpdate(devId: String?, dpStr: String?) {
                    val dps: Map<*, *>? = Gson().fromJson(dpStr, Map::class.java)
                    var isUpdated = false
                    dps?.forEach { code ->
                        deviceContainer?.switchSchema?.forEachIndexed { index, bean ->
                            if (code.value is Boolean) {
                                when (index) {
                                    0 -> {
                                        isUpdated = true
                                        if (code.key == bean.switchSchema?.id) {
                                            deviceContainer?.switchSchema?.get(
                                                0
                                            )?.switchEnable =
                                                code.value as Boolean
                                        }
                                    }
                                    1 -> {
                                        isUpdated = true
                                        if (code.key == bean.switchSchema?.id) {
                                            deviceContainer?.switchSchema?.get(
                                                1
                                            )?.switchEnable =
                                                code.value as Boolean
                                        }
                                    }
                                    2 -> {
                                        isUpdated = true
                                        if (code.key == bean.switchSchema?.id) {
                                            deviceContainer?.switchSchema?.get(
                                                2
                                            )?.switchEnable =
                                                code.value as Boolean
                                        }
                                    }
                                    3 -> {
                                        isUpdated = true
                                        if (code.key == bean.switchSchema?.id) {
                                            deviceContainer?.switchSchema?.get(
                                                3
                                            )?.switchEnable =
                                                code.value as Boolean
                                        }
                                    }

                                    4 -> {
                                        isUpdated = true
                                        if (code.key == bean.switchSchema?.id) {
                                            deviceContainer?.switchSchema?.get(
                                                4
                                            )?.switchEnable =
                                                code.value as Boolean
                                        }
                                    }
                                    5 -> {
                                        isUpdated = true
                                        if (code.key == bean.switchSchema?.id) {
                                            deviceContainer?.switchSchema?.get(
                                                5
                                            )?.switchEnable =
                                                code.value as Boolean
                                        }
                                    }
                                }
                            }
                        }
                        if (isUpdated)
                            drawSwitchMenu()
                    }
                }

                override fun onRemoved(devId: String?) {

                }

                override fun onStatusChanged(devId: String?, online: Boolean) {

                    /*recyclerListDevice?.findViewHolderForLayoutPosition(
                    deviceContainer?.position ?: 0
                )?.let {
                    if (deviceContainer != null) {
                      deviceAdapter?.updateView(
                           it, deviceContainer
                       )

                }*/
                }

                override fun onNetworkStatusChanged(devId: String?, status: Boolean) {

                }

                override fun onDevInfoUpdate(devId: String?) {

                }
            })
        }

//https://developer.tuya.com/en/docs/cloud/d33378b16a?id=Ka7kk2sso8177
        tvDeviceName?.text = entity?.name

        drawSwitchMenu()
        ivSwitch1?.setOnClickListener { v ->
            sendDataSwitch(v.tag.toString().toInt())
        }
        ivSwitch2?.setOnClickListener { v ->
            sendDataSwitch(v.tag.toString().toInt())
        }
        ivSwitch3?.setOnClickListener { v ->
            sendDataSwitch(v.tag.toString().toInt())
        }
        ivSwitch4?.setOnClickListener { v ->
            sendDataSwitch(v.tag.toString().toInt())
        }
        ivSwitch5?.setOnClickListener { v ->
            sendDataSwitch(v.tag.toString().toInt())
        }
        ivSwitch6?.setOnClickListener { v ->
            sendDataSwitch(v.tag.toString().toInt())
        }

        tvSwitch1?.setOnLongClickListener { v ->
            touchToRename(v)
            true
        }
        tvSwitch2?.setOnLongClickListener { v ->
            touchToRename(v)
            true
        }
        tvSwitch3?.setOnLongClickListener { v ->
            touchToRename(v)
            true
        }
        tvSwitch4?.setOnLongClickListener { v ->
            touchToRename(v)
            true
        }
        tvSwitch5?.setOnLongClickListener { v ->
            touchToRename(v)
            true
        }
        tvSwitch6?.setOnLongClickListener { v ->
            touchToRename(v)
            true
        }

        ivSwitch1?.setOnLongClickListener { v ->
            touchToRename(v)
            true
        }
        ivSwitch2?.setOnLongClickListener { v ->
            touchToRename(v)
            true
        }
        ivSwitch3?.setOnLongClickListener { v ->
            touchToRename(v)
            true
        }
        ivSwitch4?.setOnLongClickListener { v ->
            touchToRename(v)
            true
        }
        ivSwitch5?.setOnLongClickListener { v ->
            touchToRename(v)
            true
        }
        ivSwitch6?.setOnLongClickListener { v ->
            touchToRename(v)
            true
        }

        btnSwitchAllOn?.setOnClickListener {
            EventUtils.sendEvent(
                EventUtils.TYPE_EVENT,
                EventGeneralDeviceUtils.TUYA_PANEL_GANERAL_DEVICE_ALLON,
                ""
            )
            sendAllDPPoint(getAllSwitchDP(true))
        }

        btnSwitchAllOff?.setOnClickListener {
            EventUtils.sendEvent(
                EventUtils.TYPE_EVENT,
                EventGeneralDeviceUtils.TUYA_PANEL_GANERAL_DEVICE_ALLOFF,
                ""
            )
            sendAllDPPoint(getAllSwitchDP(false))
        }
    }


    fun touchToRename(v: View) {
        alertRename(v.tag.toString().toInt())
    }

    private val renameSuccess = Observer<Resource<Boolean>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                EventUtils.sendEvent(
                    EventUtils.TYPE_EVENT,
                    EventGeneralDeviceUtils.TUYA_PANEL_GANERAL_DEVICE_EDIT_NAME,
                    ""
                )
                resource.message?.message = getString(R.string.success)
                showSuccessAlertNotices(
                    resource.message, getString(R.string.action_ok)
                )
            }

            Status.ERROR -> {
                showErrorAlertNotices(
                    resource.message, getString(R.string.action_close)
                )
            }
            Status.LOADING -> {
                showProgressView()
            }
        }
    }


    private val tokenResp = Observer<Resource<TokenEntity>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                //hideProgressView()
                tuyaIRVM?.updateSwitchName(
                    deviceContainer?.device?.devId,
                    mCode,
                    mName
                )
            }

            Status.ERROR -> {
                hideProgressView()
                /*showToastErrorAlertNotices(
                    resource.message?.message ?: getString(R.string.msg_failure), null
                )*/
            }
            Status.LOADING -> {
                showProgressView()
            }
        }
    }

    var mName: String? = ""
    var mTag: Int = 0
    var mCode: String? = ""
    fun alertRename(tag: Int) {
        mTag = tag
        val schema = deviceContainer?.switchSchema?.get(tag)

        val name = deviceContainer?.device?.dpName?.get(schema?.switchSchema?.id)
            ?: convertDPToName(schema?.switchSchema?.code ?: "")

        activity?.supportFragmentManager.let {
            val notices = EditRoomDialogFragment(
                getString(R.string.alert_switch_name),
                name,
                object : EditRoomDialogFragment.OnEditNameListener {
                    override fun onSaveClick(name: String) {
                        mCode = schema?.switchSchema?.code
                        mName = name
                        tuyaIRVM?.getToken()
                    }
                })
            notices.isCancelable = false
            if (it != null) {
                notices.show(
                    it,
                    "showAlertNotices"
                )
            }
        }
    }

    private fun sendDataSwitch(tag: Int) {

        val schema = deviceContainer?.switchSchema?.get(tag)
        sendDPPoint(
            schema
        )
        EventUtils.sendEvent(
            EventUtils.TYPE_EVENT,
            EventGeneralDeviceUtils.TUYA_PANEL_GANERAL_DEVICE_SW.plus(tag+1),
            if (schema?.switchEnable == true) getString(R.string.device_on) else getString(R.string.device_off)
        )
    }

    private fun getAllSwitchDP(isAllOn: Boolean): HashMap<String, Any> {
        val map = HashMap<String, Any>()
        deviceContainer?.switchSchema?.forEach { schema ->
            map[schema.switchSchema?.id ?: ""] = isAllOn
        }
        return map
    }

    private fun drawSwitchMenu() {

        boxSwitch3?.visibility = View.GONE
        boxSwitch2?.visibility = View.GONE
        boxSwitch1?.visibility = View.GONE
        boxSwitch6?.visibility = View.INVISIBLE
        boxSwitch5?.visibility = View.INVISIBLE
        boxSwitch4?.visibility = View.INVISIBLE
        var isEnableSwitch = false
        deviceContainer?.let { content ->
            if ((content.switchSchema?.size ?: 0) > 3) {
                Timber.e("TAG : boxSwitchRow2")
                boxSwitchRow2?.visibility = View.VISIBLE
            }

            content.switchSchema?.forEachIndexed { index, bean ->
                val value = bean.switchEnable

                if (!isEnableSwitch)
                    isEnableSwitch = value == true
                val tag = (index + 1) - 1

                val name = deviceContainer?.device?.dpName?.get(bean.switchSchema?.id)
                    ?: convertDPToName(bean.switchSchema?.code ?: "")
                when (index) {
                    0 -> {
                        ivSwitch1?.tag = tag
                        tvSwitch1?.tag = tag
                        switchEnable(
                            value, ivSwitch1,
                            boxSwitch1
                        )
                        updateName(name, tvSwitch1)
                    }
                    1 -> {
                        ivSwitch2?.tag = tag
                        tvSwitch2?.tag = tag
                        switchEnable(
                            value, ivSwitch2,
                            boxSwitch2
                        )
                        updateName(name, tvSwitch2)
                    }
                    2 -> {
                        ivSwitch3?.tag = tag
                        tvSwitch3?.tag = tag
                        switchEnable(
                            value, ivSwitch3,
                            boxSwitch3
                        )
                        updateName(name, tvSwitch3)
                    }

                    3 -> {
                        ivSwitch4?.tag = tag
                        tvSwitch4?.tag = tag
                        switchEnable(
                            value, ivSwitch4,
                            boxSwitch4
                        )
                        updateName(bean.switchSchema?.name, tvSwitch4)
                    }

                    4 -> {
                        ivSwitch5?.tag = tag
                        tvSwitch5?.tag = tag
                        switchEnable(
                            value, ivSwitch5,
                            boxSwitch5
                        )
                        updateName(name, tvSwitch5)
                    }

                    5 -> {
                        ivSwitch6?.tag = tag
                        tvSwitch6?.tag = tag
                        switchEnable(
                            value, ivSwitch6,
                            boxSwitch6
                        )
                        updateName(name, tvSwitch6)
                    }
                }
            }
        }
    }

    fun switchEnable(
        enable: Boolean,
        switchIcon: AppCompatImageView?,
        box: LinearLayoutCompat?
    ) {
        box?.visibility = View.VISIBLE
        switchIcon?.setImageResource(
            if (enable) {
                R.drawable.btn_power_on_panel
            } else {
                R.drawable.btn_power_off_panel
            }
        )
    }

    fun updateName(name: String?, viewText: AppCompatTextView?) {
        viewText?.text = name
    }

    fun sendDPPoint(deviceSwitch: DeviceSwitchValue?) {
        if (deviceSwitch?.switchSchema?.mode?.contains("w") == true) {
            val map = HashMap<String, Any>()
            val isChecked = !deviceSwitch.switchEnable
            map[deviceSwitch.switchSchema?.id ?: ""] = isChecked
            JSONObject.toJSONString(map)?.let { param ->
                sendAllDPPoint(map)
            }
        }
    }

    fun sendAllDPPoint(map: HashMap<String, Any>) {
        JSONObject.toJSONString(map)?.let { param ->
            Timber.e(param)
            deviceInstance
                ?.publishDps(param, object : IResultCallback {
                    override fun onError(code: String?, error: String?) {

                    }

                    override fun onSuccess() {
                    }
                })
        }
    }

    private fun convertDPToName(value: String): String {
        var key = value
        return if (key.contains("_")) {
            val dataSplit = key.split("_")
            key = ""
            dataSplit.forEach {
                key += it.capitalize().plus(" ")
            }
            key
        } else {
            key.capitalize()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        deviceContainer = null
        deviceInstance?.onDestroy()
        deviceInstance = null
    }
}