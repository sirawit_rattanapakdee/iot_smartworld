package ai.apdigital.tuya.ui.control.panal

import ai.apdigital.tuya.R
import ai.apdigital.tuya.base.BaseFragment
import ai.apdigital.tuya.ui.DeviceContainer
import ai.apdigital.tuya.ui.DeviceSwitchValue
import ai.apdigital.tuya.utils.EventGeneralDeviceUtils
import ai.apdigital.tuya.utils.EventHomeUtils
import ai.apdigital.tuya.utils.EventUtils
import ai.apdigital.tuya.utils.SchemaConverter

import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.alibaba.fastjson.JSONObject
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.tuya.smart.home.sdk.TuyaHomeSdk
import com.tuya.smart.sdk.api.IDevListener
import com.tuya.smart.sdk.api.IResultCallback
import com.tuya.smart.sdk.api.ITuyaDevice
import kotlinx.android.synthetic.main.device_mgt_item_single_dps.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import timber.log.Timber

class SinglePanelFragment : BaseFragment() {
    private var deviceContainer: DeviceContainer? = null
    private var deviceInstance: ITuyaDevice? = null

    companion object {
        fun newInstance(deviceContainer: DeviceContainer?) =
            SinglePanelFragment()
                .apply {
                    this.deviceContainer = deviceContainer
                    arguments = Bundle().apply {
                    }
                }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {

    }

    override fun getLayoutView() = R.layout.device_mgt_item_single_dps

    override fun setupInstance() {

    }

    override fun setupView() {

    }

    override fun initialize() {
        val entity = deviceContainer?.device
        ivDevice?.let { iv ->
            context?.let {
                Glide
                    .with(it)
                    .load(entity?.iconUrl)
                    .into(iv)
            }
        }

        val roomBean =
            TuyaHomeSdk.getDataInstance().getDeviceRoomBean(deviceContainer?.device?.devId ?: "")

        roomBean?.let {
            tvRoom?.text = it.name
        }
        GlobalScope.launch {
        deviceInstance = TuyaHomeSdk.newDeviceInstance(entity?.devId)
        deviceInstance?.registerDevListener(object : IDevListener {
            override fun onDpUpdate(devId: String?, dpStr: String?) {
                val dps: Map<*, *>? = Gson().fromJson(dpStr, Map::class.java)
                var isUpdated = false
                dps?.forEach { code ->
                    deviceContainer?.switchSchema?.forEachIndexed { index, bean ->
                        if (code.value is Boolean) {
                            //Timber.e("onDpUpdate ${code.key} == ${bean.switchSchema?.code}")
                            when (index) {
                                0 -> {
                                    isUpdated = true
                                    if (code.key == bean.switchSchema?.id) {
                                        deviceContainer?.switchSchema?.get(
                                            0
                                        )?.switchEnable =
                                            code.value as Boolean
                                    }
                                }
                                1 -> {
                                    isUpdated = true
                                    if (code.key == bean.switchSchema?.id) {
                                        deviceContainer?.switchSchema?.get(
                                            1
                                        )?.switchEnable =
                                            code.value as Boolean
                                    }
                                }
                                2 -> {
                                    isUpdated = true
                                    if (code.key == bean.switchSchema?.id) {
                                        deviceContainer?.switchSchema?.get(
                                            2
                                        )?.switchEnable =
                                            code.value as Boolean
                                    }
                                }
                            }
                        }
                    }
                    if (isUpdated)
                        updateSwitchButton()
                }
            }

            override fun onRemoved(devId: String?) {

            }

            override fun onStatusChanged(devId: String?, online: Boolean) {

                /*recyclerListDevice?.findViewHolderForLayoutPosition(
                    deviceContainer?.position ?: 0
                )?.let {
                    if (deviceContainer != null) {
                      deviceAdapter?.updateView(
                           it, deviceContainer
                       )

                }*/
            }

            override fun onNetworkStatusChanged(devId: String?, status: Boolean) {

            }

            override fun onDevInfoUpdate(devId: String?) {

            }
        })
        }

        tvDeviceName?.text = entity?.name

        val battery = SchemaConverter.checkBattery(deviceContainer?.device?.devId ?: "")
        if (battery != 1000) {
            ivBattery?.setImageResource(SchemaConverter.convertPercentToDrawable(battery))
            ivBattery?.visibility = View.VISIBLE
            tvPercent?.text = battery.toString().plus("%")
        } else {
            ivBattery?.visibility = View.GONE
            tvPercent?.visibility = View.GONE
        }

        if (deviceContainer?.switchSchema?.size ?: 0 > 0) {
            updateSwitchButton()
            ivSwitch?.visibility = View.VISIBLE
            ivSwitch?.isEnabled = true
            ivSwitch?.isClickable = true
            ivSwitch?.setOnClickListener {
                sendDPPoint(deviceContainer?.switchSchema?.get(0), deviceContainer)
            }
        } else {
            ivSwitch?.visibility = View.INVISIBLE
            ivSwitch?.isEnabled = false
            ivSwitch?.isClickable = false
        }

        /* rooms.forEach findRoom@{ room ->
             room?.deviceList?.forEach { device ->
                 if (device.devId == entity?.devId) {
                     viewHolder.tvRoom?.text = room.name
                     return@findRoom
                 }
             }
         }*/
    }

    fun updateName(name:String){
        tvDeviceName?.text = name
    }

    fun updateSwitchButton() {
        ivSwitch?.setImageResource(if (deviceContainer?.switchSchema?.get(0)?.switchEnable == true) R.drawable.btn_power_on_panel else R.drawable.btn_power_off_panel)
    }

    fun sendDPPoint(deviceSwitch: DeviceSwitchValue?, entity: DeviceContainer?) {
        if (deviceSwitch?.switchSchema?.mode?.contains("w") == true) {
            val map = HashMap<String, Any>()
            val isChecked = !deviceSwitch.switchEnable
            map[deviceSwitch.switchSchema?.id ?: ""] = isChecked
            JSONObject.toJSONString(map)?.let { param ->
                Timber.e(param)
                val switchStr =
                    if (isChecked) getString(R.string.device_on) else getString(R.string.device_off)

                EventUtils.sendEvent(
                    EventUtils.TYPE_EVENT,
                    EventGeneralDeviceUtils.TUYA_PANEL_GANERAL_DEVICE_SW,
                    switchStr
                )

                deviceInstance
                    ?.publishDps(param, object : IResultCallback {
                        override fun onError(code: String?, error: String?) {
                            Toast.makeText(
                                context,
                                "Failed to switch ".plus(switchStr),
                                Toast.LENGTH_SHORT
                            )
                                .show()
                        }

                        override fun onSuccess() {
                            /* Toast.makeText(
                                 context,
                                 switchStr,
                                 Toast.LENGTH_SHORT
                             ).show()*/
                        }
                    })
            }
        }


    }

    override fun onDestroy() {
        super.onDestroy()
        deviceContainer = null
        deviceInstance?.onDestroy()
        deviceInstance = null
    }
}