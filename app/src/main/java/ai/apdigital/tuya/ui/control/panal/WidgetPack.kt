package ai.apdigital.tuya.ui.control.panal

import android.widget.FrameLayout

data class WidgetPack(
    var dp: MutableList<String> = mutableListOf(),
    var widget: FrameLayout? = null,
    var viewName:String =""
)