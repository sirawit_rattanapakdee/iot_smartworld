/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2021 Tuya Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ai.apdigital.tuya.ui.control.panal.dpItem

import ai.apdigital.tuya.R
import ai.apdigital.tuya.utils.EventGeneralDeviceUtils
import ai.apdigital.tuya.utils.EventUtils
import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.widget.FrameLayout
import android.widget.Switch
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.AppCompatToggleButton
import com.alibaba.fastjson.JSONObject
import com.google.gson.Gson
import com.tuya.smart.android.device.bean.SchemaBean
import com.tuya.smart.sdk.api.IDevListener
import com.tuya.smart.sdk.api.IResultCallback
import com.tuya.smart.sdk.api.ITuyaDevice
//import kotlinx.android.synthetic.main.device_mgt_item_dp_boolean.view.*
import timber.log.Timber

/**
 * Data point(DP) Boolean type item
 *
 * @author qianqi <a href="mailto:developer@tuya.com"/>
 * @since 2021/1/21 3:06 PM
 */
class DpBooleanItem @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    schemaBean: SchemaBean,
    value: Boolean,
    device: ITuyaDevice?, name: String?
) : FrameLayout(context, attrs, defStyle) {
    var tvDpName: AppCompatTextView? = null
    var switchEnable: AppCompatToggleButton? = null
    fun updateValue(dp: Map.Entry<*, *>) {
        switchEnable?.isChecked = dp.value as Boolean
    }

    init {
        val view = inflate(context, R.layout.device_mgt_item_dp_boolean, this)
        tvDpName = view.findViewById(R.id.tvDpName) as AppCompatTextView
        switchEnable = view.findViewById(R.id.switchEnable) as AppCompatToggleButton
        tvDpName?.text = name//schemaBean.name
        switchEnable?.isChecked = value

        if (schemaBean.mode.contains("w")) {
            // Data can be issued by the cloud.
            switchEnable?.setOnCheckedChangeListener { buttonView, isChecked ->
                if (buttonView.isPressed) {
                    val map = HashMap<String, Any>()
                    map[schemaBean.id] = isChecked
                    JSONObject.toJSONString(map)?.let {
                        EventUtils.sendEvent(
                            EventUtils.TYPE_EVENT,
                            EventGeneralDeviceUtils.TUYA_PANEL_GANERAL_DEVICE_DP.plus(schemaBean.code),
                            isChecked.toString()
                        )
                        device?.publishDps(it, object : IResultCallback {
                            override fun onSuccess() {
                                switchEnable?.isChecked = isChecked
                            }

                            override fun onError(code: String?, error: String?) {
                            }
                        })
                    }
                }
            }
            /*device?.registerDevListener(object : IDevListener {
                override fun onDpUpdate(devId: String?, dpStr: String?) {
                    val dps: Map<*, *>? = Gson().fromJson(dpStr, Map::class.java)
                    dps?.forEach {
                        Timber.e("${it.key} - ${it.value}")
                        if (schemaBean.id == it.key) {
                            switchEnable?.isChecked = it.value as Boolean
                        }
                    }
                }

                override fun onRemoved(devId: String?) {
                }

                override fun onStatusChanged(devId: String?, online: Boolean) {

                }

                override fun onNetworkStatusChanged(devId: String?, status: Boolean) {

                }

                override fun onDevInfoUpdate(devId: String?) {

                }
            })*/
        }
    }
}