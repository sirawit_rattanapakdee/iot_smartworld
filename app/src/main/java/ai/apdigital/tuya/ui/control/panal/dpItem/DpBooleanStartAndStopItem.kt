/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2021 Tuya Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ai.apdigital.tuya.ui.control.panal.dpItem

import ai.apdigital.tuya.R
import ai.apdigital.tuya.utils.EventGeneralDeviceUtils
import ai.apdigital.tuya.utils.EventUtils
import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.AppCompatToggleButton
import com.alibaba.fastjson.JSONObject
import com.tuya.smart.android.device.bean.SchemaBean
import com.tuya.smart.home.sdk.TuyaHomeSdk
import com.tuya.smart.sdk.api.IResultCallback
import com.tuya.smart.sdk.api.ITuyaDevice

import timber.log.Timber

/**
 * Data point(DP) Boolean type item
 *
 * @author qianqi <a href="mailto:developer@tuya.com"/>
 * @since 2021/1/21 3:06 PM
 */
class DpBooleanStartAndStopItem @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    dpStart: Int,
    dpStop: Int,
    schemaBeanStart: SchemaBean?,
    defStyle: Int = 0, devId: String?,
    device: ITuyaDevice?
) : FrameLayout(context, attrs, defStyle) {

    var tvDpName: AppCompatTextView? = null
    var switchEnable: AppCompatImageView? = null

    var valueStart: Boolean? = false
    var devId: String? = null
    var mSchemaBeanStart: SchemaBean? = null
    fun updateValue(dp: Map.Entry<Any?, Any?>) {
        getValueDevice(mSchemaBeanStart?.id, devId)
    }

    init {
        val view = inflate(context, R.layout.device_mgt_item_dp_boolean_start, this)
        tvDpName = view.findViewById(R.id.tvDpName) as AppCompatTextView
        switchEnable = view.findViewById(R.id.switchEnable) as AppCompatImageView
        getValueDevice(schemaBeanStart?.id, devId)
        this.devId = devId
        this.mSchemaBeanStart = schemaBeanStart
        switchEnable?.setOnClickListener {
            val map = HashMap<String, Any>()
            valueStart = valueStart == false
            map[dpStart.toString()] = valueStart == true
            map[dpStop.toString()] = valueStart == false
            JSONObject.toJSONString(map)?.let {
                Timber.e("valueStart $map")
                EventUtils.sendEvent(
                    EventUtils.TYPE_EVENT,
                    EventGeneralDeviceUtils.TUYA_PANEL_GANERAL_DEVICE_DP.plus(schemaBeanStart?.code),
                    valueStart.toString()
                )
                device?.publishDps(it, object : IResultCallback {
                    override fun onSuccess() {
                        setUpView(valueStart)
                    }

                    override fun onError(code: String?, error: String?) {
                    }
                })
            }
        }

    }

    private fun getValueDevice(schemaBeanStart: String?, devId: String?) {
        val deviceBean = TuyaHomeSdk.getDataInstance().getDeviceBean(devId)
        valueStart = deviceBean?.dps?.get(schemaBeanStart) as Boolean
        Timber.e("valueStart $valueStart")
        setUpView(valueStart)
    }

    fun setUpView(active: Boolean?) {
        if (active == true) {
            tvDpName?.text = context.getString(R.string.start)
            switchEnable?.setImageResource(R.drawable.control_button_start)
        } else {
            tvDpName?.text = context.getString(R.string.stop)
            switchEnable?.setImageResource(R.drawable.control_button_stop)
        }
    }
}