/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2021 Tuya Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ai.apdigital.tuya.ui.control.panal.dpItem

import ai.apdigital.tuya.R
import ai.apdigital.tuya.utils.EventGeneralDeviceUtils
import ai.apdigital.tuya.utils.EventUtils
import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import com.alibaba.fastjson.JSONObject
import com.tuya.smart.android.device.bean.SchemaBean
import com.tuya.smart.sdk.api.IResultCallback
import com.tuya.smart.sdk.api.ITuyaDevice
import timber.log.Timber
import kotlin.collections.HashMap

/**
 * Data point(DP) Boolean type item
 *
 * @author qianqi <a href="mailto:developer@tuya.com"/>
 * @since 2021/1/21 3:06 PM
 */
class DpBooleanStartItem @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    schemaBean: SchemaBean,
    value: Boolean,
    device: ITuyaDevice?, name: String?
) : FrameLayout(context, attrs, defStyle) {

    var tvDpName: AppCompatTextView? = null
    var switchEnable: AppCompatImageView? = null

    var mSchemaBean: SchemaBean? = null
    var valueReal = false
    fun updateValue(dp: Map.Entry<Any?, Any?>) {
        Timber.e("${dp.key} : ${dp.value}")
        //getValueDevice(dp.key,)
        if (dp.value is Boolean)
            setUpView(mSchemaBean?.code ?: "", dp.value as Boolean)
    }

    init {
        val view = inflate(context, R.layout.device_mgt_item_dp_boolean_start, this)
        tvDpName = view.findViewById(R.id.tvDpName) as AppCompatTextView
        switchEnable = view.findViewById(R.id.switchEnable) as AppCompatImageView
        mSchemaBean = schemaBean
        tvDpName?.text = name//schemaBean.name
        valueReal = value
        setUpView(mSchemaBean?.code ?: "", valueReal)
        //getValueDevice(schemaBean.code)

        if (schemaBean.mode.contains("w")) {
            switchEnable?.setOnClickListener {
                val map = HashMap<String, Any>()
                valueReal = !valueReal
                map[schemaBean.id] = valueReal
                JSONObject.toJSONString(map)?.let {
                    EventUtils.sendEvent(
                        EventUtils.TYPE_EVENT,
                        EventGeneralDeviceUtils.TUYA_PANEL_GANERAL_DEVICE_DP.plus(schemaBean?.code),
                        valueReal.toString()
                    )
                    device?.publishDps(it, object : IResultCallback {
                        override fun onSuccess() {
                            //setUpView(valueReal, schemaBean.code)
                            //switchEnable?.isChecked = isChecked
                        }

                        override fun onError(code: String?, error: String?) {
                        }
                    })
                }
            }
        }
    }

    private fun getValueDevice(dp: String, devId: String?) {
        /*TuyaHomeSdk.getDataInstance().getSchema(devId)?.let found@ { map ->
            var deviceBean = TuyaHomeSdk.getDataInstance().getDeviceBean(devId)

            for (bean in map.values) {
                Timber.e("")
                if(bean.id.equals(dp)){
                    deviceBean?.dps?.get(bean.id)
                    return@found
                }
                //if (bean.type == DataTypeEnum.OBJ.type) {
                    //iceBean?.dps?.get(bean.id)
                //}
            }
        }*/
        /*
            TuyaHomeSdk.getDataInstance().getSchema(devId)?.forEach {
                if (it.value?.code?.toLowerCase(Locale.ROOT)?.equals("start") == true)
                    dpStart = it.key.toInt()

                if (it.value?.code?.toLowerCase(Locale.ROOT)?.equals("stop") == true)
                    dpStop = it.key.toInt()
            }*/
    }


    fun setUpView(type: String, value: Boolean) {
        if (type == "start") {
            switchEnable?.setImageResource(
                if (value)
                    R.drawable.control_button_start
                else R.drawable.control_button_start_active
            )
        } else {
            switchEnable?.setImageResource(
                if (value)
                    R.drawable.control_button_stop
                else R.drawable.control_button_stop_active
            )
        }
    }
}