/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2021 Tuya Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ai.apdigital.tuya.ui.control.panal.dpItem

import ai.apdigital.tuya.R
import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import com.tuya.smart.android.device.bean.SchemaBean
import com.tuya.smart.home.sdk.utils.SchemaMapper
import com.tuya.smart.sdk.api.ITuyaDevice

import kotlinx.android.synthetic.main.device_mgt_item_dp_status.view.*
import timber.log.Timber
import java.util.*


/**
 * Data point(DP) Integer type item
 *
 * @author qianqi <a href="mailto:developer@tuya.com"/>
 * @since 2021/1/21 3:06 PM
 */
class DpBooleanStringItem @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    schemaBean: SchemaBean,
    value: String?,
    device: ITuyaDevice?, name: String?
) : FrameLayout(context, attrs, defStyle) {

    var tvDpName: AppCompatTextView? = null
    var edtValue: AppCompatTextView? = null


    var mSchemaBean: SchemaBean? = null
    fun updateValue(dp: Map.Entry<*, *>) {
        edtValue?.text = dp.value.toString()
    }

    init {
        val view = inflate(context, R.layout.device_mgt_item_dp_status, this)
        tvDpName = view.findViewById(R.id.tvDpName) as AppCompatTextView
        edtValue = view.findViewById(R.id.edtValue) as AppCompatTextView
        mSchemaBean = schemaBean
        //val slDp = findViewById<AppCompatSeekBar>(R.id.slDp)

        edtValue?.text = value

        tvDpName?.text = name//"${schemaBean.name}(${valueSchemaBean.unit})"
        /*if (valueSchemaBean.max > 1000) {
            edtValue?.isEnabled = true
            edtValue?.isClickable = true
            edtValue.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    curValue = v.text.toString().toDouble()
                    changeValue(calValue(scale, curValue, valueSchemaBean), device, schemaBean)
                }
                false
            })
        }*/
        /*slDp.value = curValue.toFloat()
         slDp.stepSize = (valueSchemaBean.step.toDouble() / scale).toFloat()
         slDp.valueFrom = valueSchemaBean.min.toFloat()
         slDp.valueTo = valueSchemaBean.max.toFloat()*/

    }
}