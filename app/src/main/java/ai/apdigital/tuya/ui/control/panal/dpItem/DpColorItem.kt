/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2021 Tuya Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ai.apdigital.tuya.ui.control.panal.dpItem

import ai.apdigital.tuya.R
import ai.apdigital.tuya.utils.EventGeneralDeviceUtils
import ai.apdigital.tuya.utils.EventUtils
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.util.AttributeSet
import android.view.View
import android.widget.EditText
import android.widget.FrameLayout
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.alibaba.fastjson.JSONObject
import com.skydoves.colorpickerview.ColorPickerDialog
import com.skydoves.colorpickerview.listeners.ColorEnvelopeListener
import com.tuya.smart.android.device.bean.SchemaBean
import com.tuya.smart.centralcontrol.TuyaLightDevice
import com.tuya.smart.sdk.api.IResultCallback
import com.tuya.smart.sdk.api.ITuyaDevice
import okhttp3.internal.toHexString
import timber.log.Timber


/**
 * Data point(DP) Char Type type item
 *
 * @author qianqi <a href="mailto:developer@tuya.com"/>
 * @since 2021/1/21 3:06 PM
 */
class DpColorItem @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    schemaBean: SchemaBean,
    valueData: Any?,
    tuyaDevice: ITuyaDevice?,
    name: String
) : FrameLayout(context, attrs, defStyle) {
    var tuyaLightDevice: TuyaLightDevice? = null

    fun updateValue(dp: Map.Entry<Any?, Any?>) {
        // updateStatus(dp.value as Boolean)
    }

    var tvDpName: AppCompatTextView? = null
    var content: ConstraintLayout? = null
    var color: View? = null

    init {
        val view = inflate(context, R.layout.device_mgt_item_dp_color, this)

        tvDpName = view.findViewById(R.id.tvDpName)
        content = view.findViewById(R.id.content)
        color = view.findViewById(R.id.color)
        tvDpName?.text = name
        valueData?.let {
            if (valueData is String) {
                if (valueData.length in 5..13) {
                    val values = valueData.chunked(4)
                    if (values.size >= 3) {
                        color?.visibility = View.VISIBLE
                        /* Timber.e(
                    "$value COLOR : ${
                        "#".plus(values[0].toLong(radix = 16)).plus(values[1].toLong(radix = 16))
                            .plus(values[2].toLong(radix = 16))
                    }"
                )*/

                        val hsv = FloatArray(3)
                        hsv[0] = values[0].toLong(radix = 16).toFloat()
                        hsv[1] = values[1].toLong(radix = 16).toFloat() / 10
                        hsv[2] = values[2].toLong(radix = 16).toFloat() / 10

                        color?.backgroundTintList = ColorStateList.valueOf(Color.HSVToColor(hsv))

                    }
                } else {
                    val values = valueData.chunked(6)
                    values.let {
                        color?.visibility = View.VISIBLE
                        color?.backgroundTintList =
                            ColorStateList.valueOf(Color.parseColor(it[0]))
                    }
                }
            }
        }
        content?.setOnClickListener {
            if (schemaBean.mode.contains("w")) {
                ColorPickerDialog.Builder(context)
                    .setTitle("ColorPicker Dialog")
                    .setPreferenceName("MyColorPickerDialog")
                    .setPositiveButton(context.getString(R.string.confirm),
                        ColorEnvelopeListener { envelope, fromUser ->
                            val map = HashMap<String, Any>()

                            val hsv = FloatArray(3)

                            Timber.e("${envelope.hexCode} ${envelope.argb[0]}:${envelope.argb[1]}:${envelope.argb[2]}:${envelope.argb[3]}")
                            Color.RGBToHSV(
                                envelope.argb[1],
                                envelope.argb[2],
                                envelope.argb[3],
                                hsv
                            )
                            color?.visibility = View.VISIBLE
                            color?.backgroundTintList =
                                ColorStateList.valueOf(envelope.color)
                            Timber.e("${hsv[0].toInt()} _ ${(hsv[1] * 100).toInt()} _ ${(hsv[2] * 100).toInt()}")
                            if (valueData is String) {
                                if (valueData.length in 5..13) {
                                    val a = (hsv[0].toInt()).toHexString().padStart(4, '0')
                                    val b =
                                        ((hsv[1] * 100) * 10).toInt().toHexString().padStart(4, '0')
                                    val c =
                                        ((hsv[2] * 100) * 10).toInt().toHexString().padStart(4, '0')
                                    map[schemaBean.id] = a.plus(b).plus(c)
                                } else {
                                    val a = (hsv[0].toInt()).toHexString().padStart(4, '0')
                                    val b = ((hsv[1] * 100)).toInt()
                                    val c = ((hsv[2] * 100)).toInt()
                                    map[schemaBean.id] = envelope.hexCode.plus(a.plus(b).plus(c))
                                }
                            }
                            /* if (device.deviceCategory == "dj") {

                            tuyaLightDevice?.colorHSV(
                                hsv[0].toInt(),
                                (hsv[1] * 100).toInt(),
                                (hsv[2] * 100).toInt(),
                                object : IResultCallback {
                                    override fun onError(code: String, error: String) {

                                    }

                                    override fun onSuccess() {}
                                })
                        } else {*/

                            EventUtils.sendEvent(
                                EventUtils.TYPE_EVENT,
                                EventGeneralDeviceUtils.TUYA_PANEL_GANERAL_DEVICE_SM_BULB_COLOR,
                                ""
                            )
                            JSONObject.toJSONString(map)?.let {
                                tuyaDevice?.publishDps(it, object : IResultCallback {
                                    override fun onSuccess() {
                                    }

                                    override fun onError(code: String?, error: String?) {

                                    }

                                })
                            }
                            //  }
                        })
                    .setNegativeButton(
                        context.getString(R.string.cancel)
                    ) { dialogInterface, i -> dialogInterface.dismiss() }
                    .attachAlphaSlideBar(true) // the default value is true.
                    .attachBrightnessSlideBar(true) // the default value is true.
                    .setBottomSpace(12) // set a bottom space between the last slidebar and buttons.
                    .show()


                /*etDp.setOnEditorActionListener { v, actionId, event ->
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    val map = HashMap<String, Any>()
                    map[schemaBean.id] = etDp.text.toString()
                    JSONObject.toJSONString(map)?.let {
                        device.publishDps(it, object : IResultCallback {
                            override fun onSuccess() {
                            }

                            override fun onError(code: String?, error: String?) {

                            }

                        })
                    }
                    return@setOnEditorActionListener true
                }
                return@setOnEditorActionListener false
            }*/
            }
        }
    }
}