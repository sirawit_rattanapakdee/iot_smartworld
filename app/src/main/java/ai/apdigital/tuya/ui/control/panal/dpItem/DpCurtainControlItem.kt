/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2021 Tuya Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ai.apdigital.tuya.ui.control.panal.dpItem

import ai.apdigital.tuya.R
import ai.apdigital.tuya.utils.EventGeneralDeviceUtils
import ai.apdigital.tuya.utils.EventUtils
import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatTextView
import com.alibaba.fastjson.JSONObject
import com.tuya.smart.android.device.bean.SchemaBean
import com.tuya.smart.sdk.api.IResultCallback
import com.tuya.smart.sdk.api.ITuyaDevice


/**
 * Data point(DP) Integer type item
 *
 * @author qianqi <a href="mailto:developer@tuya.com"/>
 * @since 2021/1/21 3:06 PM
 */
class DpCurtainControlItem @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    schemaBean: SchemaBean,
    value: Any?,
    device: ITuyaDevice?, name: String?
) : FrameLayout(context, attrs, defStyle) {
    var btnOpen: AppCompatButton? = null
    var btnClose: AppCompatButton? = null
    var btnStop: AppCompatButton? = null
    var tvDpName: AppCompatTextView? = null

    init {
        val view = inflate(context, R.layout.device_mgt_item_dp_curtain, this)
        btnOpen = view.findViewById(R.id.btnOpen)
        btnClose = view.findViewById(R.id.btnClose)
        btnStop = view.findViewById(R.id.btnStop)
        tvDpName = view.findViewById(R.id.tvDpName)
        //val slDp = findViewById<AppCompatSeekBar>(R.id.slDp)

        //val valueSchemaBean = SchemaMapper.toValueSchema(schemaBean.property)
        btnOpen?.setOnClickListener { publishDps("open", device, schemaBean) }
        btnClose?.setOnClickListener { publishDps("close", device, schemaBean) }
        btnStop?.setOnClickListener { publishDps("stop", device, schemaBean) }
        tvDpName?.text = name//"${schemaBean.name}(${valueSchemaBean.unit})"
        /*if (valueSchemaBean.max > 1000) {
            edtValue?.isEnabled = true
            edtValue?.isClickable = true
            edtValue.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    curValue = v.text.toString().toDouble()
                    changeValue(calValue(scale, curValue, valueSchemaBean), device, schemaBean)
                }
                false
            })
        }*/
        /*slDp.value = curValue.toFloat()
         slDp.stepSize = (valueSchemaBean.step.toDouble() / scale).toFloat()
         slDp.valueFrom = valueSchemaBean.min.toFloat()
         slDp.valueTo = valueSchemaBean.max.toFloat()*/
    }

    private fun publishDps(
        sValue: String,
        device: ITuyaDevice?, schemaBean: SchemaBean
    ) {
        val map = HashMap<String, Any>()
        map[schemaBean.id] = sValue
        EventUtils.sendEvent(
            EventUtils.TYPE_EVENT,
            EventGeneralDeviceUtils.TUYA_PANEL_GANERAL_DEVICE_DP.plus(schemaBean.code),
            sValue.toString()
        )
        JSONObject.toJSONString(map)?.let {
            device?.publishDps(it, object : IResultCallback {
                override fun onSuccess() {
                }

                override fun onError(code: String?, error: String?) {

                }

            })
        }
    }

}