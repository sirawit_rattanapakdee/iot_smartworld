/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2021 Tuya Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ai.apdigital.tuya.ui.control.panal.dpItem

/*import com.bigkoo.pickerview.MyOptionsPickerView*/
import ai.apdigital.tuya.R
import ai.apdigital.tuya.utils.EventGeneralDeviceUtils
import ai.apdigital.tuya.utils.EventUtils
import android.content.Context
import android.content.DialogInterface
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.alibaba.fastjson.JSONObject
import com.google.gson.Gson
import com.tuya.smart.android.device.bean.SchemaBean
import com.tuya.smart.home.sdk.utils.SchemaMapper
import com.tuya.smart.sdk.api.IDevListener
import com.tuya.smart.sdk.api.IResultCallback
import com.tuya.smart.sdk.api.ITuyaDevice
import timber.log.Timber


/**
 * Data point(DP) Enum type item
 *
 * @author qianqi <a href="mailto:developer@tuya.com"/>
 * @since 2021/1/21 3:06 PM
 */
class DpEnumItem @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    schemaBean: SchemaBean,
    value: String,
    device: ITuyaDevice?, name: String?
) : FrameLayout(context, attrs, defStyle) {
    var selectPosition = 0
    var items: List<String> = mutableListOf()
    fun updateValue(dp: Map.Entry<*, *>) {
        getValue(dp.value as String)
    }

    var tvDpName: AppCompatTextView? = null
    var tvEnumName: AppCompatTextView? = null
    var content: ConstraintLayout? = null
    var ivArrowRight: AppCompatImageView? = null

    init {

        val view = inflate(context, R.layout.device_mgt_item_dp_enum, this)

        tvDpName = view.findViewById(R.id.tvDpName)
        tvEnumName = view.findViewById(R.id.tvEnumName)
        content = view.findViewById(R.id.content)

        val mBuilder: AlertDialog.Builder = AlertDialog.Builder(context)
        mBuilder.setTitle("Choose an item")

        val enumSchemaBean = SchemaMapper.toEnumSchema(schemaBean.property)

        items = enumSchemaBean.range.filter {
            it != "music" && it != "real-time" && it != "debug"
        }.toList()

        tvDpName?.text = name
        getValue(value)

        /*var key = value
        if (key.contains("_")) {
            val dataSplit = key.split("_")
            key = ""
            dataSplit.forEach {
                Timber.e(it.capitalize())
                key += it.capitalize().plus(" ")

            }
        } else {
            key = key.capitalize()
        }

        tvEnumName?.text = key*/

        if (schemaBean.mode.contains("w")) {
            content?.setOnClickListener {
                if (items.isNotEmpty()) {
                    val reName: MutableList<String> = mutableListOf()
                    items.forEach { name ->
                        var key = name
                        if (key.contains("_")) {
                            val dataSplit = key.split("_")
                            key = ""
                            dataSplit.forEach {
                                Timber.e(it.capitalize())
                                key += it.capitalize().plus(" ")

                            }
                        } else {
                            key = key.capitalize()
                        }
                        reName.add(key)
                    }

                    mBuilder.setSingleChoiceItems(
                        reName.toTypedArray(), selectPosition
                    ) { dialogInterface, i ->
                        selectPosition = i
                        val map = HashMap<String, Any>()
                        map[schemaBean.id] = items[selectPosition]
                        EventUtils.sendEvent(
                            EventUtils.TYPE_EVENT,
                            EventGeneralDeviceUtils.TUYA_PANEL_GANERAL_DEVICE_DP.plus(schemaBean.code),
                            schemaBean.id
                        )
                        JSONObject.toJSONString(map)?.let {
                            device?.publishDps(it, object : IResultCallback {
                                override fun onSuccess() {
                                    tvEnumName?.text = items[selectPosition].capitalize()
                                }

                                override fun onError(code: String?, error: String?) {
                                    //   Log.e("DpEnumItem", "$code --> $error")
                                }

                            })
                        }

                        dialogInterface.dismiss()
                    }
                    val mDialog: AlertDialog = mBuilder.create()
                    mDialog.show()
                }
            }
            val itemsString = ArrayList<String>()

            items.forEachIndexed { index, element ->
                itemsString.add(element)
                if (value == element)
                    selectPosition = index
            }

            /* singlePicker.setPicker(itemsString)
            //singlePicker.setTitle(truncate(name ?: "", 10)
            singlePicker.setTitle("")
            singlePicker.setCyclic(false)

            singlePicker.setSelectOptions(selectPosition)
            singlePicker.setOnoptionsSelectListener { options1, option2, options3 -> //  singleTVOptions.setText("Single Picker " + items.get(options1));
                val map = HashMap<String, Any>()
                map[schemaBean.id] = items[options1]
                JSONObject.toJSONString(map)?.let {
                    device.publishDps(it, object : IResultCallback {
                        override fun onSuccess() {
                            tvEnumName?.text = items[options1]
                            //btnDp.text = items[options1]
                        }

                        override fun onError(code: String?, error: String?) {
                            //   Log.e("DpEnumItem", "$code --> $error")
                        }

                    })
                }
            }*/
            // Data can be issued by the cloud.
            /*val listPopupWindow = ListPopupWindow(context, null, R.attr.listPopupWindowStyle)
            listPopupWindow.anchorView = btnDp

            val enumSchemaBean = SchemaMapper.toEnumSchema(schemaBean.property)
            val items = enumSchemaBean.range.toList()
            val adapter = ArrayAdapter(context, R.layout.device_mgt_item_dp_enum_popup_item, items)
            listPopupWindow.setAdapter(adapter)
            listPopupWindow.setOnItemClickListener { parent, view, position, id ->

                val map = HashMap<String, Any>()
                map[schemaBean.id] = items[position]
                JSONObject.toJSONString(map)?.let {
                    device.publishDps(it, object : IResultCallback {
                        override fun onSuccess() {
                            btnDp.text = items[position]
                        }

                        override fun onError(code: String?, error: String?) {
                            Log.e("DpEnumItem", "$code --> $error")
                        }

                    })
                }

                listPopupWindow.dismiss()
            }

            btnDp.setOnClickListener {
                listPopupWindow.show()
            }*/
        } else {
            ivArrowRight?.visibility = View.INVISIBLE
        }
    }

    fun truncate(str: String, len: Int): String {
        return if (str.length > len) {
            str.substring(0, len) + "..."
        } else {
            str
        }
    }

    fun getValue(value: String) {
        items.forEachIndexed { index, element ->
            if (value == element) {
                selectPosition = index
                return@forEachIndexed
            }
        }

        var key = value
        if (key.contains("_")) {
            val dataSplit = key.split("_")
            key = ""
            dataSplit.forEach {
                //Timber.e(it.capitalize())
                key += " ".plus(it.capitalize())

            }
        } else {
            key = key.capitalize()
        }

        tvEnumName?.text = key
    }
}