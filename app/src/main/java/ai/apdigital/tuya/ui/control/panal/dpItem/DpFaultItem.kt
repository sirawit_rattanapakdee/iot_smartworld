/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2021 Tuya Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ai.apdigital.tuya.ui.control.panal.dpItem

import ai.apdigital.tuya.R
import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import com.tuya.smart.android.device.bean.SchemaBean

/**
 * Data point(DP) Fault type item
 *
 * @author qianqi <a href="mailto:developer@tuya.com"/>
 * @since 2021/1/21 3:06 PM
 */
class DpFaultItem @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    schemaBean: SchemaBean,
    value: String, name: String?
) : FrameLayout(context, attrs, defStyle) {

    var tvDpName: AppCompatTextView? = null
    var tvFault: AppCompatTextView? = null

    init {
        val view = inflate(context, R.layout.device_mgt_item_dp_fault, this)
        tvDpName = view.findViewById(R.id.tvDpName)
        tvDpName?.text = name

        tvFault = findViewById(R.id.tvFault)
        tvFault?.text = value

    }
}