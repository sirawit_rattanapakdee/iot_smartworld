/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2021 Tuya Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ai.apdigital.tuya.ui.control.panal.dpItem

import ai.apdigital.tuya.R
import ai.apdigital.tuya.utils.EventGeneralDeviceUtils
import ai.apdigital.tuya.utils.EventUtils
import ai.apdigital.tuya.utils.alerts.AlertUtil
import ai.apdigital.tuya.utils.alerts.ConfirmDialogFragment
import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.AppCompatToggleButton
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.FragmentManager
import com.alibaba.fastjson.JSONObject
import com.tuya.smart.home.sdk.TuyaHomeSdk
import com.tuya.smart.home.sdk.utils.SchemaMapper
import com.tuya.smart.sdk.api.IResultCallback
import com.tuya.smart.sdk.api.ITuyaDevice
import com.tuya.smart.sdk.bean.DeviceBean
import timber.log.Timber
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


/**
 * Data point(DP) Integer type item
 *
 * @author qianqi <a href="mailto:developer@tuya.com"/>
 * @since 2021/1/21 3:06 PM
 */
class DpMemoryCardStatusItem @JvmOverloads constructor(
    context: Context,
    supportFragmentManager: FragmentManager,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    deviceBean: DeviceBean?,
    device: ITuyaDevice?
) : FrameLayout(context, attrs, defStyle) {
    var switchEnableRecord: AppCompatToggleButton? = null
    var tvEnumRecordingMode: AppCompatTextView? = null
    var edtValueUsedCapacity: AppCompatTextView? = null
    var tvDpFreeCapacity: AppCompatTextView? = null
    var edtValueFreeCapacity: AppCompatTextView? = null
    var edtValueTotalCapacity: AppCompatTextView? = null
    var edtValueMemoryCardStatus: AppCompatTextView? = null
    var tvEnumFormatCard: AppCompatTextView? = null
    var memoryDetail: LinearLayoutCompat? = null
    var boxSDCard: LinearLayoutCompat? = null
    var contentFormat: ConstraintLayout? = null
    var contentRecord: ConstraintLayout? = null


    val cameraDPS = arrayListOf("109", "110", "111", "117")
    private val recordArr = arrayListOf("Event Record", "Continuous Record")
    var selectRecordPosition = 0
    var recordList: List<String> = mutableListOf()

    fun updateValue(dp: Map.Entry<*, *>) {
        Timber.e("${dp.key} : ${dp.value}")
        if (dp.key == "151") {
            val recordType = (dp.value as String).toInt()
            if (recordType in 1..2)
                tvEnumRecordingMode?.text =
                    recordArr[recordType - 1]
        } else if (dp.key == "150") {
            switchEnableRecord?.isChecked = dp.value as Boolean
        } else if (dp.key == "110") {
            val status = (dp.value as Double).toInt()
            edtValueMemoryCardStatus?.text = getValue(status)
            if (status != 4)
                tvEnumFormatCard?.text = ""
            if (status == 5) {
                memoryDetail?.visibility = View.GONE
                boxSDCard?.visibility = View.GONE
            }
        } else if (dp.key == "117") {
            val progress = (dp.value as Double).toInt()
            if (progress == 100) {
                tvEnumFormatCard?.text = ""
            } else {
                tvEnumFormatCard?.text = progress.toString().plus("%")
            }
        } else if (dp.key == "119") {
            val memoryData = dp.value as String
            val memArr = memoryData.split("|")
            if (memArr.size > 2) {
                edtValueFreeCapacity?.text =
                    bytesToHumanReadableSize((memArr[2].toDouble()))
                edtValueUsedCapacity?.text =
                    bytesToHumanReadableSize((memArr[1].toDouble()))
                edtValueTotalCapacity?.text =
                    bytesToHumanReadableSize((memArr[0].toDouble()))
            }
        }
    }

    init {
        val view = inflate(context, R.layout.device_mgt_memory_card_dp, this)
        tvEnumRecordingMode = view.findViewById(R.id.tvEnumRecordingMode)
        edtValueFreeCapacity = view.findViewById(R.id.edtValueFreeCapacity)
        edtValueUsedCapacity = view.findViewById(R.id.edtValueUsedCapacity)
        edtValueTotalCapacity = view.findViewById(R.id.edtValueTotalCapacity)
        edtValueMemoryCardStatus = view.findViewById(R.id.edtValueMemoryCardStatus)
        switchEnableRecord = view.findViewById(R.id.switchEnableRecord)
        boxSDCard = view.findViewById(R.id.boxSDCard)
        contentFormat = view.findViewById(R.id.contentFormat)
        contentRecord = view.findViewById(R.id.contentRecord)
        tvEnumFormatCard = view.findViewById(R.id.tvEnumFormatCard)

        deviceBean?.let { bean ->
            if (bean.dps?.containsKey("110") == true) {
                val value = bean.dps?.get("110") as Int
                edtValueMemoryCardStatus?.text = getValue(value)
                if (value == 5) {
                    memoryDetail?.visibility = View.GONE
                    boxSDCard?.visibility = View.GONE
                }
            } else {
                memoryDetail?.visibility = View.GONE
                boxSDCard?.visibility = View.GONE
            }
            if (bean.dps?.containsKey("109") == true) {
                val memoryData = bean.dps?.get("109") as String
                val memArr = memoryData.split("|")
                if (memArr.size > 2) {
                    edtValueFreeCapacity?.text =
                        bytesToHumanReadableSize((memArr[2].toDouble()))
                    edtValueUsedCapacity?.text =
                        bytesToHumanReadableSize((memArr[1].toDouble()))
                    edtValueTotalCapacity?.text =
                        bytesToHumanReadableSize((memArr[0].toDouble()))
                }
            }

            if (bean.dps?.containsKey("150") == true) {
                val isRecordEnable = bean.dps?.containsKey("150") as Boolean
                switchEnableRecord?.isChecked = isRecordEnable
                switchEnableRecord?.setOnCheckedChangeListener { buttonView, isChecked ->
                    if (buttonView.isPressed) {
                        val map = HashMap<String, Any>()
                        map["150"] = isChecked
                        Timber.e(
                            "Code : ${
                                TuyaHomeSdk.getDataInstance().getSchema(deviceBean.devId)
                                    ?.get("150")?.code
                            }"
                        )
                        EventUtils.sendEvent(
                            EventUtils.TYPE_EVENT,
                            EventGeneralDeviceUtils.TUYA_PANEL_GANERAL_DEVICE_DP.plus(
                                TuyaHomeSdk.getDataInstance().getSchema(deviceBean.devId)
                                    ?.get("150")?.code
                            ),
                            isChecked.toString()
                        )
                        publish(device, map)
                    }
                }
            }

            /*val formattingState = bean.dps?.get("117") as Int
            Timber.e("formattingState : $formattingState")*/
            if (bean.dps?.containsKey("151") == true) {
                val mBuilder: AlertDialog.Builder = AlertDialog.Builder(context)
                mBuilder.setTitle("Choose an item")

                TuyaHomeSdk.getDataInstance().getSchema(bean.devId)?.let { map ->
                    val enumSchemaBean = SchemaMapper.toEnumSchema(map["151"]?.property)
                    recordList = enumSchemaBean.range.toList()
                }

                val recordType = bean.dps?.get("151") as String
                val itemsString = ArrayList<String>()
                recordList.forEachIndexed { index, element ->
                    itemsString.add(element)
                    if (recordType == element) {
                        selectRecordPosition = index
                    }
                }

                tvEnumRecordingMode?.text = recordArr[selectRecordPosition]
                contentRecord?.setOnClickListener {

                    mBuilder.setSingleChoiceItems(
                        recordArr.toTypedArray(), selectRecordPosition
                    ) { dialogInterface, i ->
                        selectRecordPosition = i
                        val map = HashMap<String, Any>()
                        map["151"] = recordList[selectRecordPosition]
                        JSONObject.toJSONString(map)?.let {
                            Timber.e(
                                "Code : ${
                                    TuyaHomeSdk.getDataInstance().getSchema(deviceBean.devId)
                                        ?.get("151")?.code
                                }"
                            )
                            EventUtils.sendEvent(
                                EventUtils.TYPE_EVENT,
                                EventGeneralDeviceUtils.TUYA_PANEL_GANERAL_DEVICE_DP.plus(
                                    TuyaHomeSdk.getDataInstance().getSchema(deviceBean.devId)
                                        ?.get("151")?.code
                                ),
                                recordList[selectRecordPosition]
                            )
                            device?.publishDps(it, object : IResultCallback {
                                override fun onSuccess() {
                                    tvEnumRecordingMode?.text =
                                        recordArr[selectRecordPosition]
                                }

                                override fun onError(code: String?, error: String?) {
                                }

                            })
                        }
                        dialogInterface.dismiss()
                    }
                    val mDialog: AlertDialog = mBuilder.create()
                    mDialog.show()
                }
            }
            if (bean.dps?.containsKey("111") == true) {
                contentFormat?.setOnClickListener {
                    supportFragmentManager.let {
                        val notices = ConfirmDialogFragment(
                            context.getString(R.string.alert_micro_sd_deleted),
                            null,
                            object : AlertUtil.OnSuccessListener {
                                override fun onSuccess() {
                                    val map = HashMap<String, Any>()
                                    map["111"] = true
                                    Timber.e(
                                        "Code : ${
                                            TuyaHomeSdk.getDataInstance()
                                                .getSchema(deviceBean.devId)?.get("111")?.code
                                        }"
                                    )
                                    EventUtils.sendEvent(
                                        EventUtils.TYPE_EVENT,
                                        EventGeneralDeviceUtils.TUYA_PANEL_GANERAL_DEVICE_DP.plus(
                                            TuyaHomeSdk.getDataInstance()
                                                .getSchema(deviceBean.devId)?.get("111")?.code
                                        ),
                                        "true"
                                    )
                                    publish(device, map)
                                }
                            },
                            null,
                            R.color.red
                        )
                        notices.isCancelable = false
                        notices.show(
                            it,
                            "showAlertNotices"
                        )
                    }
                }
            }
        }
    }

    fun getValue(value: Int): String {
        return when (value) {
            1 -> {
                "Normal"
            }
            2 -> {
                "Abnormal"
            }
            3 -> {
                "Insufficient Space"
            }
            4 -> {
                "Formatting"
            }
            else -> {
                "No SD Card Inserted"
            }
        }
    }
    //เลือกโหมด (event record , continuous record)

    fun bytesToHumanReadableSize(byte: Double): String {
        Timber.e("$byte")
        val bytes = byte * (1024)
        if (bytes == 0.0) {
            return "0.0 GB"
        }
        return when {
            bytes < 1024L -> "$bytes B"
            bytes <= 0xfffccccccccccccL shr 40 -> "%.1f KB".format(bytes / (0x1 shl 10))
            bytes <= 0xfffccccccccccccL shr 30 -> "%.1f MB".format(bytes / (0x1 shl 20))
            bytes <= 0xfffccccccccccccL shr 20 -> "%.1f GB".format(bytes / (0x1 shl 30))
            bytes <= 0xfffccccccccccccL shr 10 -> "%.1f TB".format(bytes / (0x1 shl 40))
            bytes <= 0xfffccccccccccccL -> "%.1f PiB".format((bytes) / (0x1 shl 40))
            else -> "%.1f EiB".format((bytes) / (0x1 shl 40))
        }
    }

    private fun bytesIntoHumanReadable(bytes: Long): String {
        val kilobyte: Long = 1024
        val megabyte = kilobyte * 1024
        val gigabyte = megabyte * 1024
        val terabyte = gigabyte * 1024
        return when {
            bytes in 0 until kilobyte -> {
                "$bytes B"
            }
            bytes in kilobyte until megabyte -> {
                (bytes / kilobyte).toString() + " KB"
            }
            bytes in megabyte until gigabyte -> {
                (bytes / megabyte).toString() + " MB"
            }
            bytes in gigabyte until terabyte -> {
                (bytes / gigabyte).toString() + " GB"
            }
            bytes >= terabyte -> {
                (bytes / terabyte).toString() + " TB"
            }
            else -> {
                "$bytes Bytes"
            }
        }
    }


    fun publish(device: ITuyaDevice?, map: HashMap<String, Any>) {
        JSONObject.toJSONString(map)?.let {

            device?.publishDps(it, object : IResultCallback {
                override fun onSuccess() {
                    //tvEnumName?.text = items[selectPosition].capitalize()
                }

                override fun onError(code: String?, error: String?) {
                    //   Log.e("DpEnumItem", "$code --> $error")
                }

            })
        }
    }
}