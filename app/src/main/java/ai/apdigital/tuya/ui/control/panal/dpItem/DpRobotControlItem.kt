/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2021 Tuya Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ai.apdigital.tuya.ui.control.panal.dpItem

import ai.apdigital.tuya.R
import ai.apdigital.tuya.ui.control.camera.utils.DPConstants
import ai.apdigital.tuya.utils.EventGeneralDeviceUtils
import ai.apdigital.tuya.utils.EventUtils
import android.content.Context
import android.content.res.ColorStateList
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.ContextCompat
import androidx.core.widget.ImageViewCompat
import com.alibaba.fastjson.JSONObject
import com.tuya.smart.android.device.bean.SchemaBean
import com.tuya.smart.sdk.api.IResultCallback
import com.tuya.smart.sdk.api.ITuyaDevice
import timber.log.Timber


/**
 * Data point(DP) Integer type item
 *
 * @author qianqi <a href="mailto:developer@tuya.com"/>
 * @since 2021/1/21 3:06 PM
 */
class DpRobotControlItem @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    schemaBean: SchemaBean,
    value: Any?,
    device: ITuyaDevice?, name: String?
) : FrameLayout(context, attrs, defStyle), View.OnTouchListener {
    var mDevice: ITuyaDevice? = device
    var mSchemaBean: SchemaBean = schemaBean

    private var colorStateBlack: ColorStateList? = null
    private var colorStateGray: ColorStateList? = null
    private var colorStateYellow: ColorStateList? = null
    private var colorStateGrayPTZ: ColorStateList? = null
    private var btnSlideUp: AppCompatImageView? = null
    private var btnSlideDown: AppCompatImageView? = null
    private var btnSlideLeft: AppCompatImageView? = null
    private var btnSlideRight: AppCompatImageView? = null

    init {
        val view = inflate(context, R.layout.device_mgt_item_robot_control, this)

        colorStateGray =
            ColorStateList.valueOf(ContextCompat.getColor(context, R.color.gray_camera))
        colorStateBlack = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.black))
        colorStateYellow = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.yellow))
        colorStateGrayPTZ =
            ColorStateList.valueOf(ContextCompat.getColor(context, R.color.gray_ptz))

        btnSlideUp = view.findViewById(R.id.btnSlideUp)
        btnSlideDown = view.findViewById(R.id.btnSlideDown)
        btnSlideLeft = view.findViewById(R.id.btnSlideLeft)
        btnSlideRight = view.findViewById(R.id.btnSlideRight)


        btnSlideUp?.setOnTouchListener(this)
        btnSlideDown?.setOnTouchListener(this)
        btnSlideLeft?.setOnTouchListener(this)
        btnSlideRight?.setOnTouchListener(this)
        //val slDp = findViewById<AppCompatSeekBar>(R.id.slDp)

        //val valueSchemaBean = SchemaMapper.toValueSchema(schemaBean.property)

        /*if (valueSchemaBean.max > 1000) {
            edtValue?.isEnabled = true
            edtValue?.isClickable = true
            edtValue.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    curValue = v.text.toString().toDouble()
                    changeValue(calValue(scale, curValue, valueSchemaBean), device, schemaBean)
                }
                false
            })
        }*/
        /*slDp.value = curValue.toFloat()
         slDp.stepSize = (valueSchemaBean.step.toDouble() / scale).toFloat()
         slDp.valueFrom = valueSchemaBean.min.toFloat()
         slDp.valueTo = valueSchemaBean.max.toFloat()*/

    }

    private var lastTouchId: View? = null
    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        when (event?.action) {
            MotionEvent.ACTION_DOWN -> {
                lastTouchId = v
                lastTouchId?.let {
                    ImageViewCompat.setImageTintList(it as ImageView, colorStateYellow)
                }
//[\"forward\",\"backward\",\"turn_left\",\"turn_right\",\"stop\"]
                when (v?.id) {
                    R.id.btnSlideUp -> {
                        publishDps("forward", mDevice, mSchemaBean)
                    }
                    R.id.btnSlideDown -> {
                        publishDps("backward", mDevice, mSchemaBean)
                    }
                    R.id.btnSlideLeft -> {
                        publishDps("turn_left", mDevice, mSchemaBean)
                    }
                    R.id.btnSlideRight -> {
                        publishDps("turn_right", mDevice, mSchemaBean)
                    }
                }
            }
            MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> {
                lastTouchId?.let {
                    ImageViewCompat.setImageTintList(it as ImageView, colorStateGrayPTZ)
                }
                publishDps("stop", mDevice, mSchemaBean)
            }
        }
        return true
    }

    private fun publishDps(
        sValue: String,
        device: ITuyaDevice?, schemaBean: SchemaBean
    ) {
        val map = HashMap<String, Any>()
        map[schemaBean.id] = sValue
        EventUtils.sendEvent(
            EventUtils.TYPE_EVENT,
            EventGeneralDeviceUtils.TUYA_PANEL_GANERAL_DEVICE_DP.plus(schemaBean.code),
            sValue.toString()
        )
        JSONObject.toJSONString(map)?.let {
            device?.publishDps(it, object : IResultCallback {
                override fun onSuccess() {
                }

                override fun onError(code: String?, error: String?) {
                }
            })
        }
    }

}