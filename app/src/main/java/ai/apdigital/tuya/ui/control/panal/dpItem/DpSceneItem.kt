/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2021 Tuya Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ai.apdigital.tuya.ui.control.panal.dpItem

/*import com.bigkoo.pickerview.MyOptionsPickerView*/
import ai.apdigital.tuya.R
import ai.apdigital.tuya.utils.EventGeneralDeviceUtils
import ai.apdigital.tuya.utils.EventUtils
import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.alibaba.fastjson.JSONObject
import com.tuya.smart.android.device.bean.SchemaBean
import com.tuya.smart.sdk.api.IResultCallback
import com.tuya.smart.sdk.api.ITuyaDevice

/**
 * Data point(DP) Enum type item
 *
 * @author qianqi <a href="mailto:developer@tuya.com"/>
 * @since 2021/1/21 3:06 PM
 */
class DpSceneItem @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    schemaBean: SchemaBean,
    value: String,
    device: ITuyaDevice?, name: String?
) : FrameLayout(context, attrs, defStyle) {

    var tvDpName: AppCompatTextView? = null
    var tvEnumName: AppCompatTextView? = null
    var content: ConstraintLayout? = null
    var ivArrowRight: AppCompatImageView? = null

    var selectPosition = 0
    fun updateValue(dp: Map.Entry<*, *>) {
        getValue(dp.value as String)
    }

    var arrSceneItem: MutableList<SceneData>? = mutableListOf()

    inner class SceneData(
        var schemaName: String? = "",
        var color: String? = null,
        var schemaCode: String? = ""
    )

    init {

        val view = inflate(context, R.layout.device_mgt_item_dp_enum, this)

        val mBuilder: AlertDialog.Builder = AlertDialog.Builder(context)
        mBuilder.setTitle("Choose an item")

        //val enumSchemaBean = SchemaMapper.toEnumSchema(schemaBean.property)
        arrSceneItem = mutableListOf()
        arrSceneItem?.add(
            SceneData(
                "Read",
                schemaCode = "010e0d0000000000000003e801f4",
                color = "fffcf70168ffff"
            )
        )
        arrSceneItem?.add(
            SceneData(
                "Work",
                schemaCode = "020e0d0000000000000003e803e8",
                color = "cf38000168ffff"
            )
        )
        arrSceneItem?.add(
            SceneData(
                "Casual",
                schemaCode = "030e0d0000000000000001f401f4",
                color = "3855b40168ffff"
            )
        )
        arrSceneItem?.add(
            SceneData(
                "Good Night",
                schemaCode = "000e0d0000000000000000c80000",
                color = "bd76000168ffff"
            )
        )

        tvDpName = view.findViewById(R.id.tvDpName)
        content = view.findViewById(R.id.content)
        tvEnumName = view.findViewById(R.id.tvEnumName)

        tvDpName?.text = name
        getValue(value)

        if (schemaBean.mode.contains("w")) {
            content?.setOnClickListener {
                if (arrSceneItem?.isNotEmpty() == true) {
                    val reName: MutableList<String> = mutableListOf()
                    arrSceneItem?.forEach { name ->
                        val key = name.schemaName ?: ""
                        reName.add(key)
                    }

                    mBuilder.setSingleChoiceItems(
                        reName.toTypedArray(), selectPosition
                    ) { dialogInterface, i ->
                        selectPosition = i
                        val map = HashMap<String, Any>()
                        map[schemaBean.id] = arrSceneItem?.get(selectPosition)?.schemaCode ?: ""
                        EventUtils.sendEvent(
                            EventUtils.TYPE_EVENT,
                            EventGeneralDeviceUtils.TUYA_PANEL_GANERAL_DEVICE_DP.plus(schemaBean.code),
                            arrSceneItem?.get(selectPosition)?.schemaCode ?: ""
                        )
                        JSONObject.toJSONString(map)?.let {
                            device?.publishDps(it, object : IResultCallback {
                                override fun onSuccess() {
                                    tvEnumName?.text = arrSceneItem?.get(selectPosition)?.schemaName
                                }

                                override fun onError(code: String?, error: String?) {
                                    //   Log.e("DpEnumItem", "$code --> $error")
                                }

                            })
                        }

                        dialogInterface.dismiss()
                    }
                    val mDialog: AlertDialog = mBuilder.create()
                    mDialog.show()
                }
            }
            val itemsString = ArrayList<String>()
            arrSceneItem?.forEachIndexed { index, element ->
                itemsString.add(element.schemaName ?: "")
                if (value == element.schemaCode)
                    selectPosition = index
            }
        } else {
            ivArrowRight?.visibility = View.INVISIBLE
        }
    }

    fun truncate(str: String, len: Int): String {
        return if (str.length > len) {
            str.substring(0, len) + "..."
        } else {
            str
        }
    }

    fun getValue(value: String) {
        arrSceneItem?.forEach { element ->
            if (value == element.schemaCode)
                tvEnumName?.text = element.schemaName
            return@forEach
        }
    }
}