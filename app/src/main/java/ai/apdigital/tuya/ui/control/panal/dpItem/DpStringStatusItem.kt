/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2021 Tuya Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ai.apdigital.tuya.ui.control.panal.dpItem

import ai.apdigital.tuya.R
import android.content.Context
import android.util.AttributeSet
import android.widget.EditText
import android.widget.FrameLayout
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import com.tuya.smart.android.device.bean.SchemaBean
import com.tuya.smart.sdk.api.ITuyaDevice
import timber.log.Timber


/**
 * Data point(DP) Enum type item
 *
 * @author qianqi <a href="mailto:developer@tuya.com"/>
 * @since 2021/1/21 3:06 PM
 */
class DpStringStatusItem @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    schemaBean: SchemaBean,
    value: String,
    device: ITuyaDevice?, name: String?
) : FrameLayout(context, attrs, defStyle) {
    fun updateValue(dp: Map.Entry<Any?, Any?>) {
        updateStatus(dp.value as String)
    }

    var tvDpName: AppCompatTextView? = null
    var tvUnit: AppCompatTextView? = null
    var ivIcon: AppCompatImageView? = null

    init {

        val view = inflate(context, R.layout.device_mgt_item_dp_string_status, this)
        tvDpName = view.findViewById(R.id.tvDpName)
        tvUnit = view.findViewById(R.id.tvUnit)
        ivIcon = view.findViewById(R.id.ivIcon)
        //if (schemaBean.code.startsWith("pir", ignoreCase = true)) {
        updateStatus(value)
        tvDpName?.text = "STATUS"

    }

    private fun updateStatus(value: String) {
        tvUnit?.text = getValue(value)
        if (value != "none") {
            //presence_state
            ivIcon?.setImageResource(R.drawable.ic_motion_sensor_active)
        } else {
            ivIcon?.setImageResource(R.drawable.ic_motion_sensor)
        }
    }

    fun getValue(value: String): String {

        var key = value
        if (key.contains("_")) {
            val dataSplit = key.split("_")
            key = ""
            dataSplit.forEach {
                Timber.e(it.capitalize())
                key += " ".plus(it.capitalize())

            }
        } else {
            key = key.capitalize()
        }
        return key
    }
}