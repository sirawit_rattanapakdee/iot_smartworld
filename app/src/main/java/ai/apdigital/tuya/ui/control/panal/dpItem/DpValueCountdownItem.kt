/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2021 Tuya Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ai.apdigital.tuya.ui.control.panal.dpItem

import ai.apdigital.tuya.R
import ai.apdigital.tuya.utils.EventGeneralDeviceUtils
import ai.apdigital.tuya.utils.EventUtils
import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.akexorcist.snaptimepicker.SnapTimePickerDialog
import com.akexorcist.snaptimepicker.TimeRange
import com.akexorcist.snaptimepicker.TimeValue
import com.alibaba.fastjson.JSONObject
import com.suke.widget.SwitchButton
import com.tuya.smart.android.device.bean.SchemaBean
import com.tuya.smart.android.device.bean.ValueSchemaBean
import com.tuya.smart.home.sdk.utils.SchemaMapper
import com.tuya.smart.sdk.api.IResultCallback
import com.tuya.smart.sdk.api.ITuyaDevice
import timber.log.Timber
import kotlin.math.pow

/**
 * Data point(DP) Integer type item
 *
 * @author qianqi <a href="mailto:developer@tuya.com"/>
 * @since 2021/1/21 3:06 PM
 */
class DpValueCountdownItem @JvmOverloads constructor(
    context: AppCompatActivity,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    schemaBean: SchemaBean,
    value: Int,
    device: ITuyaDevice?, name: String?
) : FrameLayout(context, attrs, defStyle) {
    var valueReal = 0
    var tvDpName: AppCompatTextView? = null
    var tvHourValue: AppCompatTextView? = null
    var tvMinuteValue: AppCompatTextView? = null
    var switchEnable: SwitchButton? = null
    var content: ConstraintLayout? = null
    fun updateValue(dp: Map.Entry<*, *>) {
        // edtValue?.setText(dp.value.toString())
    }

    init {
        val view = inflate(context, R.layout.device_mgt_item_dp_timer, this)
        tvDpName = view.findViewById(R.id.tvDpName)
        switchEnable = view.findViewById(R.id.switchEnable)
        tvHourValue = view.findViewById(R.id.tvHourValue)
        tvMinuteValue = view.findViewById(R.id.tvMinuteValue)
        //val slDp = findViewById<AppCompatSeekBar>(R.id.slDp)

        val valueSchemaBean = SchemaMapper.toValueSchema(schemaBean.property)

        val scale = 10.0.pow(valueSchemaBean.scale.toDouble())
        valueReal = value
        var curValue = (value * valueSchemaBean.step + valueSchemaBean.min).toFloat() / scale
        if (curValue > valueSchemaBean.max) {
            curValue = valueSchemaBean.max.toDouble()
        } else if (curValue < valueSchemaBean.min) {
            curValue = valueSchemaBean.min.toDouble()
        }
        //edtValue?.setText(curValue.toInt().toString().plus("\u2103"))
        tvDpName?.text = name//"${schemaBean.name}(${valueSchemaBean.unit})"

        /*slDp.value = curValue.toFloat()
         slDp.stepSize = (valueSchemaBean.step.toDouble() / scale).toFloat()
         slDp.valueFrom = valueSchemaBean.min.toFloat()
         slDp.valueTo = valueSchemaBean.max.toFloat()*/

        if (valueReal < 60) {
            switchEnable?.isChecked = false
        } else {

            tvHourValue?.setTextColor(ContextCompat.getColor(context, R.color.black))
            tvMinuteValue?.setTextColor(ContextCompat.getColor(context, R.color.black))
            setValue(valueReal)
            switchEnable?.isChecked = true
        }

        switchEnable?.setOnCheckedChangeListener { view, isChecked ->
            if (isChecked) {
                tvHourValue?.setTextColor(ContextCompat.getColor(context, R.color.black))
                tvMinuteValue?.setTextColor(ContextCompat.getColor(context, R.color.black))
            } else {
                tvHourValue?.setTextColor(ContextCompat.getColor(context, R.color.gray_camera))
                tvMinuteValue?.setTextColor(ContextCompat.getColor(context, R.color.gray_camera))
                changeValue(0, device, schemaBean)
            }
        }
        content?.setOnClickListener {
            if (switchEnable?.isChecked == true) {
                SnapTimePickerDialog.Builder().apply {

                    val hours: Int = valueReal / 3600
                    val minutes: Int = valueReal % 3600 / 60
                    setPreselectedTime(TimeValue(hours, minutes))
                    //setSelectableTimeRange(TimeRange(TimeValue(2, 15), TimeValue(14, 30)))
                    setTitle(R.string.title_select_time)
                    setPrefix(R.string.time_prefix)
                    setSuffix(R.string.time_suffix)
                    setThemeColor(R.color.yellow)
                    setTitleColor(R.color.white)
                    setNegativeButtonColor(R.color.yellow)
                    setPositiveButtonColor(R.color.yellow)
                    setButtonTextAllCaps(false)
                }.build().apply {
                    setListener { hour, minute -> onTimePicked(hour, minute, device, schemaBean) }
                }.show(context.supportFragmentManager, SnapTimePickerDialog.TAG)
            }
        }

    }

    fun setValue(value: Int) {
        val hours: Int = value / 3600
        val minutes: Int = value % 3600 / 60
        valueReal = value
        tvHourValue?.text = if (hours < 10)
            "0".plus(hours) else hours.toString()

        tvMinuteValue?.text = if (minutes < 10)
            "0".plus(minutes) else minutes.toString()

    }


    private fun onTimePicked(
        selectedHour: Int, selectedMinute: Int,
        device: ITuyaDevice?, schemaBean: SchemaBean
    ) {
        /* val hour = selectedHour.toString().padStart(2, '0')
         val minute = selectedMinute.toString().padStart(2, '0')*/

        val hour = selectedHour * 3600
        val minute = selectedMinute * 60

        val total = hour + minute
        setValue(total)
        changeValue(total, device, schemaBean)
        /*binding.textViewTime.text =
            String.format(getString(R.string.selected_time_format, hour, minute))*/
    }

    private fun changeValue(
        sValue: Int,
        device: ITuyaDevice?, schemaBean: SchemaBean
    ) {

        val map = HashMap<String, Any>()
        map[schemaBean.id] = sValue
        Timber.e(JSONObject.toJSONString(map))
        EventUtils.sendEvent(
            EventUtils.TYPE_EVENT,
            EventGeneralDeviceUtils.TUYA_PANEL_GANERAL_DEVICE_DP.plus(schemaBean.code),
            sValue.toString()
        )
        JSONObject.toJSONString(map)?.let {
            device?.publishDps(it, object : IResultCallback {
                override fun onSuccess() {
                    setValue(sValue)
                }

                override fun onError(code: String?, error: String?) {

                }

            })
        }
    }

}