/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2021 Tuya Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ai.apdigital.tuya.ui.control.panal.dpItem

import ai.apdigital.tuya.R
import ai.apdigital.tuya.utils.EventGeneralDeviceUtils
import ai.apdigital.tuya.utils.EventUtils
import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.FrameLayout
import android.widget.TextView.OnEditorActionListener
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import com.alibaba.fastjson.JSONObject
import com.google.gson.Gson
import com.tuya.smart.android.device.bean.SchemaBean
import com.tuya.smart.android.device.bean.ValueSchemaBean
import com.tuya.smart.home.sdk.utils.SchemaMapper
import com.tuya.smart.sdk.api.IDevListener
import com.tuya.smart.sdk.api.IResultCallback
import com.tuya.smart.sdk.api.ITuyaDevice

import timber.log.Timber
import kotlin.math.pow


/**
 * Data point(DP) Integer type item
 *
 * @author qianqi <a href="mailto:developer@tuya.com"/>
 * @since 2021/1/21 3:06 PM
 */
class DpValueIntItem @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    schemaBean: SchemaBean,
    value: Int,
    device: ITuyaDevice?, name: String?
) : FrameLayout(context, attrs, defStyle) {
    var tvDpName: AppCompatTextView? = null
    var tvUnit: AppCompatTextView? = null
    var edtValue: AppCompatEditText? = null
    var ivAdd: AppCompatImageView? = null
    var ivSub: AppCompatImageView? = null


    fun updateValue(dp: Map.Entry<*, *>) {
        edtValue?.setText(dp.value.toString())
    }

    init {
        val view = inflate(context, R.layout.device_mgt_item_dp_integer, this)
        tvDpName = view.findViewById(R.id.tvDpName)
        tvUnit = view.findViewById(R.id.tvUnit)
        edtValue = view.findViewById(R.id.edtValue)
        ivSub = view.findViewById(R.id.ivSub)
        ivAdd = view.findViewById(R.id.ivAdd)
        //val slDp = findViewById<AppCompatSeekBar>(R.id.slDp)

        val valueSchemaBean = SchemaMapper.toValueSchema(schemaBean.property)

        val scale = 10.0.pow(valueSchemaBean.scale.toDouble())

        var curValue = (value * valueSchemaBean.step + valueSchemaBean.min).toFloat() / scale
        if (curValue > valueSchemaBean.max) {
            curValue = valueSchemaBean.max.toDouble()
        } else if (curValue < valueSchemaBean.min) {
            curValue = valueSchemaBean.min.toDouble()
        }
        if (valueSchemaBean.unit != null && !valueSchemaBean.unit.isNullOrEmpty()) {
            tvUnit?.visibility = View.VISIBLE
            tvUnit?.text = valueSchemaBean.unit
        }
        edtValue?.setText(curValue.toInt().toString())
        tvDpName?.text = name//"${schemaBean.name}(${valueSchemaBean.unit})"
        /*if (valueSchemaBean.max > 1000) {
            edtValue?.isEnabled = true
            edtValue?.isClickable = true
            edtValue.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    curValue = v.text.toString().toDouble()
                    changeValue(calValue(scale, curValue, valueSchemaBean), device, schemaBean)
                }
                false
            })
        }*/
        /*slDp.value = curValue.toFloat()
         slDp.stepSize = (valueSchemaBean.step.toDouble() / scale).toFloat()
         slDp.valueFrom = valueSchemaBean.min.toFloat()
         slDp.valueTo = valueSchemaBean.max.toFloat()*/

        if (schemaBean.mode.contains("w")) {
            ivAdd?.visibility = View.VISIBLE
            ivSub?.visibility = View.VISIBLE
            edtValue?.background = ContextCompat.getDrawable(context, R.drawable.background_et)
            ivSub?.setOnClickListener {
                if (curValue <= valueSchemaBean.min.toDouble())
                    return@setOnClickListener
                curValue -= valueSchemaBean.step
                edtValue?.setText(curValue.toInt().toString())
                changeValue(calValue(scale, curValue, valueSchemaBean), device, schemaBean)
            }
            ivAdd?.setOnClickListener {
                if (curValue >= valueSchemaBean.max.toDouble())
                    return@setOnClickListener
                curValue += valueSchemaBean.step
                edtValue?.setText(curValue.toInt().toString())
                changeValue(calValue(scale, curValue, valueSchemaBean), device, schemaBean)
            }
        }

    }

    private fun calValue(scale: Double, sValue: Double, valueSchemaBean: ValueSchemaBean): Int {
        return (((sValue * scale) - valueSchemaBean.min) / valueSchemaBean.step).toInt()
    }

    private fun changeValue(
        sValue: Int,
        device: ITuyaDevice?, schemaBean: SchemaBean
    ) {

        val map = HashMap<String, Any>()
        map[schemaBean.id] = sValue
        Timber.e(JSONObject.toJSONString(map))
        EventUtils.sendEvent(
            EventUtils.TYPE_EVENT,
            EventGeneralDeviceUtils.TUYA_PANEL_GANERAL_DEVICE_DP.plus(schemaBean.code),
            sValue.toString()
        )
        JSONObject.toJSONString(map)?.let {
            device?.publishDps(it, object : IResultCallback {
                override fun onSuccess() {
                }

                override fun onError(code: String?, error: String?) {

                }

            })
        }
    }

}