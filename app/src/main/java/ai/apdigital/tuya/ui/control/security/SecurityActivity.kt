package ai.apdigital.tuya.ui.control.security

import ai.apdigital.tuya.HomeModel
import ai.apdigital.tuya.R
import ai.apdigital.tuya.TuyaFcmListenerService
import ai.apdigital.tuya.TuyaSmartWorldModule
import ai.apdigital.tuya.base.BaseActivity
import ai.apdigital.tuya.ui.DeviceContainer
import ai.apdigital.tuya.ui.control.DPMapper
import ai.apdigital.tuya.ui.control.DeviceDetailActivity
import ai.apdigital.tuya.base.Constants
import ai.apdigital.tuya.ui.MainActivity
import ai.apdigital.tuya.ui.control.panal.MultiplePanelFragment
import ai.apdigital.tuya.ui.control.panal.SinglePanelFragment
import ai.apdigital.tuya.ui.control.panal.WidgetPack
import ai.apdigital.tuya.ui.control.panal.dpItem.*
import ai.apdigital.tuya.utils.EventUtils
import ai.apdigital.tuya.utils.PageUtils
import ai.apdigital.tuya.utils.SchemaConverter
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import com.tuya.smart.android.device.bean.*
import com.tuya.smart.android.device.enums.DataTypeEnum
import com.tuya.smart.home.sdk.TuyaHomeSdk
import com.tuya.smart.home.sdk.utils.SchemaMapper
import com.tuya.smart.sdk.api.IDevListener
import com.tuya.smart.sdk.api.ITuyaDevice
import kotlinx.android.synthetic.main.device_mgt_activity_control.*
import kotlinx.android.synthetic.main.toolbar_back_with_control_panel.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.*
import kotlin.collections.HashMap

class SecurityActivity : BaseActivity() {
    private var dpPoints: HashMap<String, String>? = null
    private var deviceContainer: DeviceContainer? = null
    private var ignoreDPs: MutableList<String>? = mutableListOf()
    private var dpsWidget: MutableList<WidgetPack>? = mutableListOf()
    private var deviceId: String? = null

    companion object {
        var isSmartLockForeground = false
    }

    //var mDevice: ITuyaDevice? = null
    override fun getLayoutView() = R.layout.device_mgt_activity_security

    override fun bindView() {
    }

    override fun setupInstance() {
    }

    override fun setupView() {
        setupToolbar(
            getString(R.string.smart_home),
            showButtonBack = true,
            showRightMenu = true
        )
        ivBack?.setOnClickListener { openMainActivity() }
    }

    private val startForDeviceDetailControlResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                setResult(Activity.RESULT_OK, result.data)
                when {
                    intent?.getStringExtra("ACTION") == "REMOVE" -> {
                        finish()
                    }
                    intent?.getStringExtra("ACTION") == "RENAME" -> {
                        tvTitle?.text = intent.getStringExtra("NAME")
                        isUpdate = true
                    }
                }
            }
        }
    var dataPush: Bundle? = null
    var dataFCM: String? = null
    override fun initialize(savedInstanceState: Bundle?) {
        this.dpPoints = DPMapper.getDPFromAsset(this@SecurityActivity, "dp.json")
        dataPush = intent.getBundleExtra(Constants.INTENT_NOTIFICATION_DATA)
        dataFCM = intent.getStringExtra(Constants.INTENT_NOTIFICATION_ALERT)
        ignoreDPs = intent.getStringArrayListExtra(Constants.INTENT_IGNORE_DPS) ?: mutableListOf()
        deviceId = intent.getStringExtra(Constants.INTENT_DEV_ID)

        /*val isHasNotification =
            intent.getBooleanExtra(TuyaSmartWorldModule.EXTRA_IS_HAS_NOTIFICATION, false)*/


        deviceContainer = SchemaConverter.convertSchemaDevice(deviceId ?: "")
        EventUtils.sendEvent(
            EventUtils.TYPE_PAGE,
            PageUtils.TUYA_PANEL_GENERAL_DEVICE,
            deviceContainer?.device?.deviceCategory
        )

        menuEdit?.setOnClickListener {
            val intent = Intent(this@SecurityActivity, DeviceDetailActivity::class.java)
            intent.putExtra(Constants.INTENT_DEV_ID, deviceId)
            startForDeviceDetailControlResult.launch(intent)
        }

        when {
            dataPush != null -> {
                replaceFragmentToActivity(
                    R.id.mainPanel,
                    SecurityFragment.newInstance(deviceContainer, dataPush)
                )
            }
            dataFCM != null -> {
                replaceFragmentToActivity(
                    R.id.mainPanel,
                    SecurityFragment.newInstance(
                        deviceContainer, Gson().fromJson(
                            intent.getStringExtra(Constants.INTENT_NOTIFICATION_ALERT) ?: "",
                            RemoteMessage::class.java
                        )
                    )
                )
            }
            else -> replaceFragmentToActivity(
                R.id.mainPanel,
                SecurityFragment.newInstance(deviceContainer)
            )
        }
        tvTitle?.text = "Home Security"
    }

    override fun onDestroy() {

        /* mDevice?.registerDevListener(devListener)
         mDevice?.onDestroy()*/
        /*TuyaSmartWorldModule.RN_CALLBACK?.invoke()
        TuyaSmartWorldModule.RN_CALLBACK = null
        TuyaHomeSdk.getHomeManagerInstance().onDestroy()
        TuyaHomeSdk.closeService()
        TuyaHomeSdk.onDestroy()*/
        dpPoints = null
        super.onDestroy()
    }


    override fun onResume() {
        super.onResume()
        isSmartLockForeground = true
    }

    override fun onPause() {
        super.onPause()
        isSmartLockForeground = false
    }

    override fun onBackPressed() {
        super.onBackPressed()
        openMainActivity()
    }

    private fun openMainActivity() {
        if (dataPush != null) {
            val intent = Intent(
                this@SecurityActivity,
                MainActivity::class.java
            )
            /* intent.putExtra(
                 TuyaSmartWorldModule.EXTRA_LANGUAGE,
                 language
             )
             intent.putExtra(
                 Constants.INTENT_NOTIFICATION_DATA,
                 dataPush
             )*/
            startActivity(
                intent
            )

        }
        finish()
    }
}