package ai.apdigital.tuya.ui.control.security

import ai.apdigital.tuya.R
import ai.apdigital.tuya.base.BaseFragment
import ai.apdigital.tuya.base.Constants
import ai.apdigital.tuya.ui.DeviceContainer
import ai.apdigital.tuya.ui.viewmodel.NotificationViewModel
import ai.apdigital.tuya.utils.DateHelper
import ai.apdigital.tuya.utils.EventGeneralDeviceUtils
import ai.apdigital.tuya.utils.EventUtils
import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.lifecycle.Observer
import com.alibaba.fastjson.JSONObject
import com.bumptech.glide.Glide
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import com.tuya.smart.android.device.bean.BoolSchemaBean
import com.tuya.smart.android.device.bean.EnumSchemaBean
import com.tuya.smart.android.device.enums.DataTypeEnum
import com.tuya.smart.home.sdk.TuyaHomeSdk
import com.tuya.smart.sdk.api.IDevListener
import com.tuya.smart.sdk.api.IResultCallback
import com.tuya.smart.sdk.api.ITuyaDevice
import kotlinx.android.synthetic.main.device_mgt_item_security_dps.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class SecurityFragment : BaseFragment() {
    //   private val notificationVM: NotificationViewModel? by viewModel()
    private var deviceContainer: DeviceContainer? = null
    private var deviceInstance: ITuyaDevice? = null
    private var isNotification = false
    private var notificationData: Bundle? = null
    private var remoteMessage: RemoteMessage? = null

    companion object {
        fun newInstance(deviceContainer: DeviceContainer?) =
            SecurityFragment()
                .apply {
                    this.deviceContainer = deviceContainer
                    this.isNotification = false
                    arguments = Bundle().apply {
                    }
                }

        fun newInstance(
            deviceContainer: DeviceContainer?,
            pushRaw: Bundle?
        ) =
            SecurityFragment()
                .apply {
                    this.deviceContainer = deviceContainer
                    this.isNotification = true
                    this.notificationData = pushRaw
                    arguments = Bundle().apply {
                    }
                }

        fun newInstance(
            deviceContainer: DeviceContainer?,
            remoteMessage: RemoteMessage?
        ) =
            SecurityFragment()
                .apply {
                    this.deviceContainer = deviceContainer
                    this.isNotification = true
                    this.remoteMessage = remoteMessage
                    arguments = Bundle().apply {
                    }
                }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {

    }

    override fun getLayoutView() = R.layout.device_mgt_item_security_dps

    override fun setupInstance() {

    }

    private fun stopEventReceiver() {
        try {
            activity?.unregisterReceiver(mLocationReceiver)
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        }
    }

    override fun onResume() {
        super.onResume()
        startEventReceiver()
    }

    override fun onPause() {
        super.onPause()
        stopEventReceiver()
    }

    private fun startEventReceiver() {
        try {
            val filter = IntentFilter()
            filter.addAction(Constants.ACTION_NOTIFICATION)
            activity?.registerReceiver(mLocationReceiver, filter)
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        }
    }

    private val mLocationReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            val notification = Gson().fromJson(
                intent.getStringExtra(Constants.INTENT_NOTIFICATION_ALERT) ?: "",
                RemoteMessage::class.java
            )
            boxNotification?.visibility = View.VISIBLE
            val dateTime: String

            var time = ""
            notification?.data?.get("ts")?.let {
                time = DateHelper.convertSystemTimeDateToStringWithFormat(
                    it.toLong(),
                    "hh:mm:ss"
                )
            }
            val body = notification?.notification?.body

            dateTime = time.plus(" | ").plus(body)
            val alert: String = notification?.notification?.title ?: ""
            try {
                tvDeviceName?.text = body?.split(":")?.get(0)
            } catch (e: Exception) {
                Timber.e(e.toString())
            }

            setData(alert, dateTime)
        }
    }


    override fun setupView() {

    }

    override fun initialize() {
        val entity = deviceContainer?.device
        tvDeviceName?.text = entity?.name
        ivDevice?.let { iv ->
            context?.let {
                Glide
                    .with(it)
                    .load(entity?.iconUrl)
                    .into(iv)
            }
        }
        if (isNotification) {
            boxNotification?.visibility = View.VISIBLE
            val dateTime: String
            val alert: String
            if (notificationData != null) {
                val notification = notificationData?.getBundle("notification_data")
               // val data = notificationData?.getBundle("data")
                var time = ""
                notificationData?.get("ts")?.let {
                    time = DateHelper.convertSystemTimeDateToStringWithFormat(
                        it.toString().toLong(),
                        "hh:mm:ss"
                    )
                }
                val body = notification?.getString("body")

                dateTime = time.plus(" | ").plus(body)
                alert = notification?.getString("title").toString()
                try {
                    tvDeviceName?.text = body?.split(":")?.get(0)
                } catch (e: Exception) {
                    Timber.e(e.toString())
                }
            } else {
                var time = ""
                remoteMessage?.data?.get("ts")?.let {
                    time = DateHelper.convertSystemTimeDateToStringWithFormat(
                        it.toLong(),
                        "hh:mm:ss"
                    )
                }
                val body = remoteMessage?.notification?.body
                dateTime = time.plus(" | ").plus(body)
                alert = remoteMessage?.notification?.title ?: ""
                try {
                    tvDeviceName?.text = body?.split(":")?.get(0)
                } catch (e: Exception) {
                    Timber.e(e.toString())
                }
            }
            setData(alert, dateTime)

        } else {
            boxNotification?.visibility = View.GONE
        }
        val roomBean =
            TuyaHomeSdk.getDataInstance().getDeviceRoomBean(deviceContainer?.device?.devId ?: "")

        roomBean?.let {
            tvRoom?.text = it.name
        }
        GlobalScope.launch {
            deviceInstance = TuyaHomeSdk.newDeviceInstance(entity?.devId)

            TuyaHomeSdk.getDataInstance().getSchema(entity?.devId)?.let { map ->
                for (bean in map.values) {
                    if (bean.type == DataTypeEnum.OBJ.type) {
                        if (bean.schemaType == EnumSchemaBean.type) {
                            if (bean.code?.contains("S_Mode") == true) {
                                updateViewMode(deviceContainer?.device?.dps?.get(bean.id) as String)
                            }
                        } else if (bean.schemaType == BoolSchemaBean.type) {
                            if (bean.code?.contains("SOS") == true) {
                                updateSOSView(deviceContainer?.device?.dps?.get(bean.id) as Boolean)
                            }
                        }
                    }
                }
            }

            deviceInstance?.registerDevListener(object : IDevListener {
                override fun onDpUpdate(devId: String?, dpStr: String?) {
                    val dps: Map<*, *>? = Gson().fromJson(dpStr, Map::class.java)

                    dps?.forEach { dp ->
                        val dpKey = dp.key as String
                        if (dpKey == "S_Mode" || dpKey == "116") {
                            updateViewMode(dp.value as String)
                        } else if (/*dpKey == "SOS" || dpKey == "104" || */dpKey == "S_LocalSiren" || dpKey == "128") {
                            updateSOSView(dp.value as Boolean)
                        }
                    }
                }

                override fun onRemoved(devId: String?) {

                }

                override fun onStatusChanged(devId: String?, online: Boolean) {

                    /*recyclerListDevice?.findViewHolderForLayoutPosition(
                        deviceContainer?.position ?: 0
                    )?.let {
                        if (deviceContainer != null) {
                          deviceAdapter?.updateView(
                               it, deviceContainer
                           )

                    }*/
                }

                override fun onNetworkStatusChanged(devId: String?, status: Boolean) {

                }

                override fun onDevInfoUpdate(devId: String?) {

                }
            })
        }
        boxRecordMode?.setOnClickListener {
            val intent =
                Intent(context, SecurityAllPanelActivity::class.java)
            intent.putExtra(Constants.INTENT_DEV_ID, entity?.devId)
            startForResult.launch(intent)
        }


        ivSwitch1?.setOnClickListener {
            sendDPPoint("116", "Arm")
            //  updateViewMode("Arm")
        }
        ivSwitch2?.setOnClickListener {
            sendDPPoint("116", "Disarm")
            //   updateViewMode("Disarm")
        }

        ivIcon?.setOnClickListener {
            sendDPPoint("128", !(deviceContainer?.device?.dps?.get("128") as Boolean))
            //  updateSOSView(!(deviceContainer?.device?.dps?.get("104") as Boolean))
        }
    }

    fun setData(alarmType: String, dateTime: String) {
        tvAlarmType?.text = alarmType
        tvDateTime?.text = dateTime
    }

    fun updateViewMode(value: String) {
        if (value.equals("arm", true)) {
            ivSwitch2?.setImageResource(R.drawable.disarm_inactive)
            ivSwitch1?.setImageResource(R.drawable.arm_active)
        } else {
            ivSwitch2?.setImageResource(R.drawable.disarm_active)
            ivSwitch1?.setImageResource(R.drawable.arm_inactive)
        }
    }

    val red = "#F12532"
    val white = "#ffffff"
    val black = "#000000"
    val gray = "#979797"

    private fun updateSOSView(value: Boolean) {
        if (value) {
            boxSos?.setCardBackgroundColor(Color.parseColor(red))
            tvDpStatus?.setTextColor(Color.parseColor(white))
            tvDpName?.setTextColor(Color.parseColor(white))
            tvDpStatus?.text = "Turn off sound the siren."
            ivIcon?.setImageResource(R.drawable.alarm_active)
        } else {
            boxSos?.setCardBackgroundColor(Color.parseColor(white))
            tvDpStatus?.setTextColor(Color.parseColor(gray))
            tvDpName?.setTextColor(Color.parseColor(black))
            ivIcon?.setImageResource(R.drawable.alarm_inactive)
            tvDpStatus?.text = "Turn on sound the siren."
        }
    }

    val startForResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                Timber.e("startForResult : getHomeUpdate")
            }
        }

    fun sendDPPoint(dp: String, value: Any) {
        val map = HashMap<String, Any>()
        map[dp] = value
        JSONObject.toJSONString(map)?.let { param ->
            Timber.e(param)
            EventUtils.sendEvent(
                EventUtils.TYPE_EVENT,
                EventGeneralDeviceUtils.TUYA_PANEL_GANERAL_DEVICE_SW,
                "".plus(value)
            )

            deviceInstance
                ?.publishDps(param, object : IResultCallback {
                    override fun onError(code: String?, error: String?) {
                        Toast.makeText(
                            context,
                            "Failed to switch ".plus(value),
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }

                    override fun onSuccess() {
                        /* Toast.makeText(
                             context,
                             switchStr,
                             Toast.LENGTH_SHORT
                         ).show()*/
                    }
                })
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        deviceContainer = null
        deviceInstance?.onDestroy()
        deviceInstance = null
    }
}