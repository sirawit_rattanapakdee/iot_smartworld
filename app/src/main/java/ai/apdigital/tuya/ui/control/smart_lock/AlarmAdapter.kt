package ai.apdigital.tuya.ui.control.smart_lock

import ai.apdigital.tuya.R
import ai.apdigital.tuya.ui.control.smart_lock.model.RecordContainer
import android.content.Context
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.tuya.smart.optimus.lock.api.bean.Record.DataBean
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap


class AlarmAdapter(val context: Context, val icon: Int,val dpPoints: HashMap<String, String>?) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    interface OnItemClickListener {
        fun onRecyclerItemClick(view: View, position: Int, entity: DataBean?)
    }

    override fun getItemViewType(position: Int): Int {
        return if (data[position].isHeader) 2 else 1
    }

    private var data: MutableList<RecordContainer> = arrayListOf()
    private var mListener: OnItemClickListener? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        if (viewType == 1) {
            return AdapterViewHolder(
                layoutInflater.inflate(
                    R.layout.item_record,
                    parent,
                    false
                ), this
            )
        } else {
            return AdapterHeaderViewHolder(
                layoutInflater.inflate(
                    R.layout.item_record_header,
                    parent,
                    false
                ), this
            )
        }
    }


    fun setOnItemClickListener(onEntryClickListener: OnItemClickListener) {
        mListener = onEntryClickListener
    }

    override fun getItemCount(): Int = data.size


    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        if (viewHolder.itemViewType == 2) {
            viewHolder as AdapterHeaderViewHolder
            viewHolder.tvDate?.text = data[position].date
        } else {
            viewHolder as AdapterViewHolder
            viewHolder.holdItem = data[position].records
            viewHolder.holdItem?.let { entity ->
                viewHolder.ivAction?.setImageResource(icon)

                val dateFormatter = SimpleDateFormat("HH:mm")
                val key = entity.dpCodeMap?.keys?.elementAt(0) as String
                /*if (key?.contains("_") == true) {
                    val dataSplit = key.split("_")
                    key = ""
                    dataSplit.forEach {
                        key += " ".plus(it.capitalize())
                    }
                }*/
                dateFormatter.timeZone = TimeZone.getDefault()
                viewHolder.tvTime?.text = dateFormatter.format(entity.createTime)
                viewHolder.tvAction?.text = dpPoints?.get(key)
                    ?: convertDPToName(key)
            }
        }
    }


    private fun convertDPToName(value: String): String {
        var key = value
        return if (key.contains("_")) {
            val dataSplit = key.split("_")
            key = ""
            dataSplit.forEach {
                //Timber.e(it.capitalize())
                key += it.capitalize().plus(" ")

            }
            key
        } else {
            key.capitalize()
        }
    }

    //{"avatarUrl":"https://s3-us-west-2.amazonaws.com/airtake-public-data/smart/user_res/avatar/scale/user_icon_default.png","createTime":1629629166000,"devId":"14610040e8db84b8e815","dpCodeMap":{"unlock_password":"1"},"dpData":{"2":"1"},"tags":0,"unlockRelation":{"passwordNumber":1,"unlockType":"unlock_password"},"userId":"0","userName":"unknown"}{"avatarUrl":"https://s3-us-west-2.amazonaws.com/airtake-public-data/smart/user_res/avatar/scale/user_icon_default.png","createTime":1629629166000,"devId":"14610040e8db84b8e815","dpCodeMap":{"unlock_password":"1"},"dpData":{"2":"1"},"tags":0,"unlockRelation":{"passwordNumber":1,"unlockType":"unlock_password"},"userId":"0","userName":"unknown"}

    fun updateData(items: List<RecordContainer>?) {
        this.data = mutableListOf()

        items?.let { it ->
            this.data = it.toMutableList()
        }

        notifyDataSetChanged()
    }

    inner class AdapterViewHolder(
        itemView: View,
        val adapterDevice: AlarmAdapter
    ) :
        RecyclerView.ViewHolder(itemView),
        View.OnClickListener, View.OnLongClickListener, View.OnTouchListener {

        var holdItem: DataBean? = null
        private var container: ConstraintLayout? = itemView.findViewById(R.id.container)
        internal var tvTime: AppCompatTextView? = itemView.findViewById(R.id.tvTime)
        internal var ivAction: AppCompatImageView? = itemView.findViewById(R.id.ivAction)
        internal var tvAction: AppCompatTextView? = itemView.findViewById(R.id.tvAction)


        init {
            container?.setOnClickListener(this)
            container?.setOnLongClickListener(this)
            container?.setOnTouchListener(this)
        }

        override fun onClick(v: View?) {
            mListener?.onRecyclerItemClick(itemView, layoutPosition, holdItem)
        }

        override fun onLongClick(v: View?): Boolean {
            mListener?.onRecyclerItemClick(itemView, layoutPosition, holdItem)
            return true
        }

        override fun onTouch(v: View?, event: MotionEvent?): Boolean {
            when (event?.action) {
                MotionEvent.ACTION_DOWN -> {
                    v?.alpha = 0.5f
                }
                MotionEvent.ACTION_UP
                -> {
                    v?.alpha = 1f
                }
                MotionEvent.ACTION_CANCEL -> {
                    v?.alpha = 1f
                }
            }
            return false
        }
    }


    inner class AdapterHeaderViewHolder(
        itemView: View,
        val adapterDevice: AlarmAdapter
    ) :
        RecyclerView.ViewHolder(itemView) {

        private var container: ConstraintLayout? = itemView.findViewById(R.id.container)
        internal var tvDate: AppCompatTextView? = itemView.findViewById(R.id.tvDate)

        init {
        }
    }
}