package ai.apdigital.tuya.ui.control.smart_lock

import ai.apdigital.tuya.R
import ai.apdigital.tuya.base.BaseActivity
import ai.apdigital.tuya.networking.Resource
import ai.apdigital.tuya.networking.Status
import ai.apdigital.tuya.ui.control.DPMapper
import ai.apdigital.tuya.base.Constants
import ai.apdigital.tuya.ui.viewmodel.TuyaDoorLockViewModel
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.tuya.smart.optimus.lock.api.bean.Record
import kotlinx.android.synthetic.main.activity_list.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class AlarmDoorLockActivity : BaseActivity() {
    private var dpPoints: HashMap<String, String>? = null
    var deviceId: String? = ""
    private val tuyaDoorLockVM: TuyaDoorLockViewModel? by viewModel()
    var password: String? = ""
    var typeOfDoorLock: TypeDoorLock = TypeDoorLock.WIFI
    private var alarmAdapter: AlarmAdapter? = null

    override fun getLayoutView() = R.layout.activity_list

    override fun bindView() {

    }

    override fun setupInstance() {
        setupToolbar(
            getString(R.string.title_alarm),
            showButtonBack = true,
            showRightMenu = true
        )
    }

    override fun setupView() {
        this.dpPoints = DPMapper.getDPFromAsset(this@AlarmDoorLockActivity, "dp.json")
        shimmerViewContainer = shimmer_container
        contentContainer = content

        deviceId = intent.getStringExtra(Constants.INTENT_DEV_ID)
        typeOfDoorLock =
            intent.getSerializableExtra(Constants.INTENT_DOOR_LOCK_TYPE) as TypeDoorLock
        recyclerList?.let {
            alarmAdapter =
                AlarmAdapter(
                    this, R.drawable.ic_alarm_alert, dpPoints
                )
            val layoutManager = LinearLayoutManager(this)
            layoutManager.orientation = LinearLayoutManager.VERTICAL
            it.layoutManager = layoutManager
            it.adapter = alarmAdapter
            it.setHasFixedSize(true)

            alarmAdapter?.setOnItemClickListener(object : AlarmAdapter.OnItemClickListener {
                override fun onRecyclerItemClick(
                    view: View,
                    position: Int,
                    entity: Record.DataBean?
                ) {
                }
            })
            it.addOnScrollListener(scrollListener)
        }

        swipeContainer?.isRefreshing = false
        swipeContainer?.setOnRefreshListener {
            dataQuery = null
            getRecord()
        }
        tuyaDoorLockVM?.getAlarmListener()?.observe(this, alarmResp)
        getRecord()
    }

    private val scrollListener: RecyclerView.OnScrollListener =
        object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                dataQuery?.let {
                    if (it.hasNext)
                        if (recyclerList?.canScrollVertically(it.datas.size) == false) {
                            offset++
                            getRecord()
                        }
                }
            }
        }

    var offset = 0
    private fun getRecord() {
        tuyaDoorLockVM?.getAlarmList(deviceId, typeOfDoorLock, offset, 30)
    }

    override fun initialize(savedInstanceState: Bundle?) {

    }

    var dataQuery: Record? = null

    private val alarmResp = Observer<Resource<Record>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                swipeContainer?.isRefreshing = false
                hideShimmerLoading()
                if (dataQuery == null)
                    dataQuery = resource.data
                else {
                    dataQuery?.totalCount = resource.data?.totalCount
                    dataQuery?.hasNext = resource.data?.hasNext
                    resource.data?.datas?.let { dataQuery?.datas?.addAll(it) }
                }

                alarmAdapter?.updateData(ConvertHeader.convertObjectHeader(dataQuery))
                if (resource.data?.datas?.size == 0)
                    tvNotFoundData?.visibility = View.VISIBLE
                //enableUnlock(false)
            }

            Status.ERROR -> {
                swipeContainer?.isRefreshing = false
                hideShimmerLoading()
                showToastErrorAlertNotices(
                    resource.message?.message ?: getString(R.string.msg_failure), null
                )
            }
            Status.LOADING -> {
                if (offset == 0)
                    showShimmerLoading()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        this.dpPoints = null
        this.alarmAdapter = null
    }
}