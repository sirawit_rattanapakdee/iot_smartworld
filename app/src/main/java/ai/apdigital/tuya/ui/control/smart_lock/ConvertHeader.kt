package ai.apdigital.tuya.ui.control.smart_lock

import ai.apdigital.tuya.ui.control.smart_lock.model.RecordContainer
import com.tuya.smart.optimus.lock.api.bean.Record
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*

object ConvertHeader {

    fun convertObjectHeader(record: Record?): MutableList<RecordContainer> {

        val dateFormatter = SimpleDateFormat("E, dd MMMM yyyy",Locale.US)

        dateFormatter.timeZone = TimeZone.getDefault()
        val dataRecords: MutableList<RecordContainer> = mutableListOf()
        record?.datas?.forEach {
            var recordData = RecordContainer()
            if (dataRecords.isEmpty()) {
                recordData.isHeader = true
                recordData.date = dateFormatter.format(it.createTime)
                dataRecords.add(recordData)
            } else {
                Timber.e("${dataRecords.last().date} : ${dateFormatter.format(it.createTime)}")
                if (dataRecords.last().date != dateFormatter.format(it.createTime)) {
                    recordData.isHeader = true
                    recordData.date = dateFormatter.format(it.createTime)
                    dataRecords.add(recordData)
                }
            }
            recordData = RecordContainer()
            recordData.isHeader = false
            recordData.records = it
            recordData.date = dateFormatter.format(it.createTime)
            dataRecords.add(recordData)
        }
        return dataRecords
    }
}