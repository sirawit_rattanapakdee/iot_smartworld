package ai.apdigital.tuya.ui.control.smart_lock

import ai.apdigital.tuya.R
import ai.apdigital.tuya.base.BaseActivity
import ai.apdigital.tuya.ui.control.DeviceDetailActivity
import ai.apdigital.tuya.base.Constants
import ai.apdigital.tuya.ui.control.panal.ControlPanelActivity
import ai.apdigital.tuya.ui.control.smart_lock.temporary.TemporaryDoorLockActivity
import ai.apdigital.tuya.ui.control.smart_lock.unlock.DoorLockPagerAdapter
import ai.apdigital.tuya.utils.EventDoorLockUtils
import ai.apdigital.tuya.utils.EventUtils
import ai.apdigital.tuya.utils.PageUtils
import ai.apdigital.tuya.utils.SchemaConverter
import android.Manifest
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import com.google.android.material.tabs.TabLayoutMediator
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.tuya.smart.android.device.enums.DataTypeEnum
import com.tuya.smart.home.sdk.TuyaHomeSdk
import com.tuya.smart.home.sdk.callback.ITuyaResultCallback
import com.tuya.smart.optimus.lock.api.ITuyaLockManager
import com.tuya.smart.optimus.lock.api.bean.Record
import com.tuya.smart.optimus.lock.api.bean.TempPassword
import com.tuya.smart.optimus.lock.api.bean.WifiLockUser
import com.tuya.smart.optimus.sdk.TuyaOptimusSdk
import com.tuya.smart.sdk.optimus.lock.bean.ble.BLELockUser
import kotlinx.android.synthetic.main.activity_smart_lock.*
import kotlinx.android.synthetic.main.toolbar_back_with_control_panel.*

import timber.log.Timber
import kotlin.random.Random


class SmartLockActivity : BaseActivity() {
    var deviceId: String? = ""
    var typeOfDoorLock: TypeDoorLock = TypeDoorLock.WIFI
    var tuyaLockManager: ITuyaLockManager? = null
    override fun getLayoutView(): Int = R.layout.activity_smart_lock

    override fun bindView() {
    }

    override fun setupInstance() {
        setupToolbar(
            getString(R.string.title_smart_lock),
            showButtonBack = true,
            showRightMenu = true
        )
    }

    override fun setupView() {

        menuEdit?.setOnClickListener {

            EventUtils.sendEvent(EventUtils.TYPE_EVENT, EventDoorLockUtils.TUYA_PANEL_DOOR_LOCK_DETAIL, "")
            val intent = Intent(this@SmartLockActivity, DeviceDetailActivity::class.java)
            intent.putExtra(Constants.INTENT_DEV_ID, deviceId)
            startForResult.launch(intent)
        }/*

        tvSetting?.setOnClickListener {
            val intent = Intent(this@SmartLockActivity, DeviceDetailActivity::class.java)
            intent.putExtra(Constants.INTENT_DEV_ID, deviceId)
            startForResult.launch(intent)
        }*/
    }

    val startForResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                val intent = result.data
                if (intent?.getStringExtra("ACTION") == "REMOVE")
                    finish()
                else if (intent?.getStringExtra("ACTION") == "RENAME")
                    tvTitle?.text = intent.getStringExtra("NAME")
            }
        }

    override fun initialize(savedInstanceState: Bundle?) {
        EventUtils.sendEvent(EventUtils.TYPE_PAGE, PageUtils.TUYA_PANEL_DOOR_LOCK, "")

        deviceId = intent.getStringExtra(Constants.INTENT_DEV_ID)
        tuyaLockManager = TuyaOptimusSdk.getManager(
            ITuyaLockManager::class.java
        )
        val deviceContainer = SchemaConverter.convertSchemaDevice(deviceId ?: "")

        tvTitle?.text = deviceContainer.device?.name

        var haveDynamicPassword = false
        var haveTemporaryPassword = false
        TuyaHomeSdk.getDataInstance().getSchema(deviceId)?.let { map ->
            //val deviceBean = TuyaHomeSdk.getDataInstance().getDeviceBean(deviceId)
            for (bean in map.values) {
                Timber.e("${bean.type} : ${bean.code} : ${bean.id}")
                if (bean.type == DataTypeEnum.OBJ.type) {
                    if (bean.code?.startsWith("unlock_dynamic") == true)
                        haveDynamicPassword = true
                    if (bean.code?.startsWith("unlock_temporary") == true)
                        haveTemporaryPassword = true
                }
            }
        }

        when {
            deviceContainer.device?.categoryCode?.startsWith("ble") == true -> {
                Dexter.withContext(this@SmartLockActivity)
                    .withPermissions(
                        Manifest.permission.BLUETOOTH,
                        Manifest.permission.BLUETOOTH_ADMIN,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ).withListener(object : MultiplePermissionsListener {
                        override fun onPermissionsChecked(report: MultiplePermissionsReport) {

                            typeOfDoorLock = TypeDoorLock.BLE
                            viewPager.adapter =
                                DoorLockPagerAdapter(
                                    this@SmartLockActivity,
                                    deviceContainer,
                                    typeOfDoorLock, if (haveDynamicPassword) 2 else 1
                                )
                        }

                        override fun onPermissionRationaleShouldBeShown(
                            permissions: List<PermissionRequest?>?,
                            token: PermissionToken?
                        ) {

                        }
                    }).check()
            }
            deviceContainer.device?.categoryCode?.startsWith(
                "wf"
            ) == true -> {
                typeOfDoorLock = TypeDoorLock.WIFI
                viewPager.adapter =
                    DoorLockPagerAdapter(
                        this,
                        deviceContainer,
                        typeOfDoorLock,
                        if (haveDynamicPassword) 2 else 1
                    )
            }
            else -> {
                typeOfDoorLock = TypeDoorLock.ZIGBEE
                viewPager.adapter =
                    DoorLockPagerAdapter(
                        this,
                        deviceContainer,
                        typeOfDoorLock,
                        if (haveDynamicPassword) 2 else 1
                    )
            }
        }
        if (haveDynamicPassword) {
            TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            }.attach()
        }

        boxMore?.setOnClickListener {
            EventUtils.sendEvent(EventUtils.TYPE_EVENT, EventDoorLockUtils.TUYA_PANEL_DOOR_LOCK_MORE, "")
            val intent = Intent(this@SmartLockActivity, ControlPanelActivity::class.java)
            intent.putExtra(Constants.INTENT_DEV_ID, deviceId)
            intent.putExtra(Constants.INTENT_DOOR_LOCK_TYPE, typeOfDoorLock)
            startActivity(intent)
        }

        if (haveTemporaryPassword) {
            btnTempPassword?.setOnClickListener {
                EventUtils.sendEvent(EventUtils.TYPE_EVENT, EventDoorLockUtils.TUYA_PANEL_DOOR_LOCK_TEMP_PW, "")
                val intent = Intent(this@SmartLockActivity, TemporaryDoorLockActivity::class.java)
                intent.putExtra(Constants.INTENT_DEV_ID, deviceId)
                intent.putExtra(Constants.INTENT_DOOR_LOCK_TYPE, typeOfDoorLock)
                startTempPasswordForResult.launch(intent)
            }
        } else {
            btnTempPassword?.isEnabled = false
            btnTempPassword?.alpha = 0.3f
        }


        btnRecord?.setOnClickListener {
            EventUtils.sendEvent(EventUtils.TYPE_EVENT, EventDoorLockUtils.TUYA_PANEL_DOOR_LOCK_RECORD, "")
            val intent = Intent(this@SmartLockActivity, RecordDoorLockActivity::class.java)
            intent.putExtra(Constants.INTENT_DEV_ID, deviceId)
            intent.putExtra(Constants.INTENT_DOOR_LOCK_TYPE, typeOfDoorLock)
            startRecordForResult.launch(intent)
        }

        btnAlarm?.setOnClickListener {
            EventUtils.sendEvent(EventUtils.TYPE_EVENT, EventDoorLockUtils.TUYA_PANEL_DOOR_LOCK_ALARM, "")
            val intent = Intent(this@SmartLockActivity, AlarmDoorLockActivity::class.java)
            intent.putExtra(Constants.INTENT_DEV_ID, deviceId)
            intent.putExtra(Constants.INTENT_DOOR_LOCK_TYPE, typeOfDoorLock)
            startAlarmForResult.launch(intent)
        }
    }

    val startRecordForResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                val intent = result.data

            }
        }

    val startAlarmForResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                val intent = result.data

            }
        }

    val startTempPasswordForResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                val intent = result.data

            }
        }


    private fun wifiMenu() {
        val tuyaLockDevice = tuyaLockManager?.getWifiLock(deviceId)
        val dpCodes: ArrayList<String> = ArrayList()
        dpCodes.add("alarm_lock")
        dpCodes.add("hijack")
        dpCodes.add("doorbell")
        tuyaLockDevice?.getRecords(dpCodes, 0, 10, object : ITuyaResultCallback<Record> {
            override fun onError(code: String, message: String) {
                Log.e("TAG", "get unlock records failed: code = $code  message = $message")
            }

            override fun onSuccess(recordBean: Record) {
                Log.i("TAG", "get unlock records success: recordBean = $recordBean")
            }
        })
        tuyaLockDevice?.getTempPasswords(object : ITuyaResultCallback<List<TempPassword?>> {
            override fun onError(code: String, message: String) {
                Log.e("TAG", "get lock temp passwords failed: code = $code  message = $message")
            }

            override fun onSuccess(tempPasswords: List<TempPassword?>) {
                tempPasswords.forEach { pass ->
                    Log.e(
                        "TAG",
                        "get lock temp passwords success: tempPasswords${pass?.id} ${pass?.name}"
                    )
                }
            }
        })
/*
             tuyaLockDevice.deleteTempPassword(1529652,object :ITuyaResultCallback<Boolean?> {
                 override fun onSuccess(result: Boolean?) {
                     Log.e("TAG", "Delete 1529652")
                 }

                 override fun onError(errorCode: String?, errorMessage: String?) {
                     Log.e("TAG", "Fail 1529652")
                 }
             })

             tuyaLockDevice.deleteTempPassword(1494856,object :ITuyaResultCallback<Boolean?> {
                 override fun onSuccess(result: Boolean?) {
                     Log.e("TAG", "Delete 1494856")
                 }

                 override fun onError(errorCode: String?, errorMessage: String?) {
                     Log.e("TAG", "Fail 1494856")
                 }
             })

             tuyaLockDevice.getTempPasswords(object : ITuyaResultCallback<List<TempPassword?>> {
                 override fun onError(code: String, message: String) {
                     Log.e("TAG", "PSS : code = $code  message = $message")
                 }

                 override fun onSuccess(tempPasswords: List<TempPassword?>) {
                     tempPasswords.forEach { pass->
                         Log.i("TAG", "PSS ${pass?.id} ${pass?.name}")
                     }
                 }
             })*/

        val password = Random.nextInt(1000000, 9999999).toString()

        Timber.e("randomValues AF : $password")


        tuyaLockDevice?.getLockUsers(object : ITuyaResultCallback<List<WifiLockUser>> {


            override fun onSuccess(result: List<WifiLockUser>?) {
                result?.forEach {
                    Timber.e("get lock users success: lockUserBean = ${it.userName} : ${it.userType} ${it.contact} ")
                }

            }

            override fun onError(errorCode: String?, errorMessage: String?) {
                Timber.e("get lock users failed: code = " + errorCode + "  message = " + errorMessage)
            }
        });

    }


    private fun bleMenu() {

        val tuyaLockDevice = tuyaLockManager?.getBleLock(deviceId)
        tuyaLockDevice?.getLockUsers(object : ITuyaResultCallback<List<BLELockUser>> {


            override fun onSuccess(result: List<BLELockUser>?) {
                result?.forEach {
                    Timber.e("get lock users success: lockUserBean = ${it.nickName} : ${it.userType} ${it.lockUserId} ")
                }

            }

            override fun onError(errorCode: String?, errorMessage: String?) {
                Timber.e("get lock users failed: code = " + errorCode + "  message = " + errorMessage)
            }
        });

        tuyaLockDevice?.getAlarmRecords(0, 20, object : ITuyaResultCallback<Record> {
            override fun onError(code: String, message: String) {
                Log.e("TAG", "get lock records failed: code = $code  message = $message")
            }

            override fun onSuccess(recordBean: Record) {
                Log.i("TAG", "get lock records success: recordBean = $recordBean")
            }
        })
    }
}