package ai.apdigital.tuya.ui.control.smart_lock

enum class TypeDoorLock {
    WIFI,BLE,ZIGBEE
}