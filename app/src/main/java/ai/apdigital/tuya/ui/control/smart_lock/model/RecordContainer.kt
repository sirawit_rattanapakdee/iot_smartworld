package ai.apdigital.tuya.ui.control.smart_lock.model

import com.tuya.smart.optimus.lock.api.bean.Record

data class RecordContainer(
    var isHeader: Boolean = false,
    var date: String? = null,
    var records: Record.DataBean? = null
)