package ai.apdigital.tuya.ui.control.smart_lock.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TemporaryPasswordEntity(
    var name: String? = null,
    var password: String? = null,
    var startDate:Long? = 0L,
    var endDate:Long? = 0L
):Parcelable