package ai.apdigital.tuya.ui.control.smart_lock.temporary

import ai.apdigital.tuya.R
import ai.apdigital.tuya.base.BaseActivity
import ai.apdigital.tuya.base.SmartWorldPreferences
import ai.apdigital.tuya.model.TicketEntity
import ai.apdigital.tuya.model.TokenEntity
import ai.apdigital.tuya.networking.Resource
import ai.apdigital.tuya.networking.Status
import ai.apdigital.tuya.base.Constants
import ai.apdigital.tuya.ui.control.smart_lock.TypeDoorLock
import ai.apdigital.tuya.ui.control.smart_lock.model.TemporaryPasswordEntity
import ai.apdigital.tuya.ui.viewmodel.TuyaDoorLockViewModel
import ai.apdigital.tuya.ui.viewmodel.TuyaIRViewModel
import ai.apdigital.tuya.utils.alerts.AlertUtil
import android.app.Activity
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import com.tuya.smart.optimus.lock.api.ITuyaLockManager
import com.tuya.smart.optimus.sdk.TuyaOptimusSdk
import kotlinx.android.synthetic.main.activity_door_lock_confirm_temporary_password.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class TemporaryConfirmCreatePasswordDoorLockActivity : BaseActivity() {
    var preference: SmartWorldPreferences? = null
    var deviceId: String? = ""
    var tempPass: TemporaryPasswordEntity? = null
    private val tuyaDoorLockVM: TuyaDoorLockViewModel? by viewModel()
    private val tuyaIRVM: TuyaIRViewModel? by viewModel()
    var typeOfDoorLock: TypeDoorLock = TypeDoorLock.WIFI
    private val tuyaLockManager: ITuyaLockManager? = TuyaOptimusSdk.getManager(
        ITuyaLockManager::class.java
    )

    override fun getLayoutView() = R.layout.activity_door_lock_confirm_temporary_password

    override fun bindView() {
        preference = SmartWorldPreferences(this)
    }

    override fun setupInstance() {
        setupToolbar(
            getString(R.string.title_temporary_pw),
            showButtonBack = true,
            showRightMenu = true
        )
    }

    private fun setClipboard(context: Context?, text: String?) {
        val clipboard = context?.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText("Copied Text", text)
        clipboard.setPrimaryClip(clip)
        showToastSuccessAlertNotices(getString(R.string.copied_to_the_phone_clipboard), null)
    }

    var pw = ""
    override fun setupView() {
        tempPass = intent.getParcelableExtra(Constants.INTENT_DOOR_LOCK_TEMP_PASS)
        deviceId = intent.getStringExtra(Constants.INTENT_DEV_ID)
        typeOfDoorLock =
            intent.getSerializableExtra(Constants.INTENT_DOOR_LOCK_TYPE) as TypeDoorLock
        btnCopyPassword?.setOnClickListener {
            setClipboard(this, "")
        }

        pw = ""
        tempPass?.password?.toCharArray()?.forEach { v ->
            pw = pw.plus(v).plus(" ")
        }

        tvPassword?.text = pw
        btnButtom?.setOnClickListener {
            showAlertConfirmWithActionClick(
                getString(R.string.alert_need_to_create_password),
                getString(R.string.input_password).plus(" ").plus(pw),
                object : AlertUtil.OnSuccessListener {
                    override fun onSuccess() {
                        when (typeOfDoorLock) {
                            TypeDoorLock.WIFI -> {
                                tuyaDoorLockVM?.createTemporaryWIFIPassword(
                                    deviceId,
                                    tempPass
                                )
                            }
                            TypeDoorLock.BLE -> {
                                tuyaDoorLockVM?.createTemporaryBLEPassword(
                                    deviceId,
                                    tempPass
                                )
                            }
                            else -> {
                                tuyaIRVM?.getToken()
                            }
                        }
                    }
                }, R.color.black
            )
        }

        tuyaIRVM?.getTokenListener()?.observe(this, tokenResp)
        tuyaDoorLockVM?.ticketDoorLockPasswordListener()?.observe(this, ticketResp)
        tuyaDoorLockVM?.createTemporaryPasswordListener()?.observe(this, createTempResp)
        enableSubmitPassword(false)

        chkBackup?.setOnCheckedChangeListener { buttonView, isChecked ->
            enableSubmitPassword(isChecked)
        }
        tvCheckBackup?.setOnClickListener {
            chkBackup?.isChecked = chkBackup?.isChecked != true
            enableSubmitPassword(chkBackup?.isChecked == true)
        }
    }

    fun enableSubmitPassword(isEnabled: Boolean) {
        btnButtom?.isEnabled = isEnabled
        btnButtom?.isClickable = isEnabled
    }

    override fun initialize(savedInstanceState: Bundle?) {

    }

    private val tokenResp = Observer<Resource<TokenEntity>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                //hideProgressView()
                tuyaDoorLockVM?.getTicket(deviceId ?: "")
            }

            Status.ERROR -> {
                hideProgressView()
                /*showToastErrorAlertNotices(
                    resource.message?.message ?: getString(R.string.msg_failure), null
                )*/
            }
            Status.LOADING -> {
                showProgressView()
            }
        }
    }

    var ticket: TicketEntity? = null
    private val ticketResp = Observer<Resource<TicketEntity>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                //hideProgressView()
                ticket = resource.data

                tuyaDoorLockVM?.createTemporaryZigbeePassword(
                    deviceId,
                    tempPass,
                    ticket
                )
            }

            Status.ERROR -> {
                hideProgressView()
                /*showToastErrorAlertNotices(
                    resource.message?.message ?: getString(R.string.msg_failure), null
                )*/
            }
            Status.LOADING -> {
                showProgressView()
            }
        }
    }

    private val createTempResp = Observer<Resource<Boolean>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                hideProgressView()
                showToastSuccessAlertNotices(
                    getString(R.string.msg_successfully),
                    object : AlertUtil.OnSuccessListener {
                        override fun onSuccess() {
                            val intent = Intent()
                            setResult(Activity.RESULT_OK, intent)
                            finish()
                        }
                    })
            }

            Status.ERROR -> {
                hideProgressView()
                showToastErrorAlertNotices(
                    getString(R.string.msg_failure), null
                )
            }
            Status.LOADING -> {
                showProgressView()
            }
        }
    }
}