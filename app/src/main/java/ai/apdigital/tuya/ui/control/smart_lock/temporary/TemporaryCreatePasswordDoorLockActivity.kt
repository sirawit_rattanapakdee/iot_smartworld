package ai.apdigital.tuya.ui.control.smart_lock.temporary

import ai.apdigital.tuya.R
import ai.apdigital.tuya.base.BaseActivity
import ai.apdigital.tuya.base.SmartWorldPreferences
import ai.apdigital.tuya.networking.Resource
import ai.apdigital.tuya.networking.Status
import ai.apdigital.tuya.base.Constants
import ai.apdigital.tuya.ui.control.smart_lock.TypeDoorLock
import ai.apdigital.tuya.ui.control.smart_lock.model.TemporaryPasswordEntity
import ai.apdigital.tuya.ui.viewmodel.TuyaDoorLockViewModel
import ai.apdigital.tuya.utils.alerts.AlertUtil
import ai.apdigital.tuya.utils.alerts.EditRoomDialogFragment
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.widget.TextView
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.akexorcist.snaptimepicker.SnapTimePickerDialog
import com.akexorcist.snaptimepicker.TimeValue
import com.niwattep.materialslidedatepicker.SlideDatePickerDialog
import com.niwattep.materialslidedatepicker.SlideDatePickerDialogCallback
import kotlinx.android.synthetic.main.activity_door_lock_temporary_password_create.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*
import kotlin.random.Random


class TemporaryCreatePasswordDoorLockActivity : BaseActivity(), SlideDatePickerDialogCallback {
    var preference: SmartWorldPreferences? = null
    var deviceId: String? = ""
    private val tuyaDoorLockVM: TuyaDoorLockViewModel? by viewModel()
    var typeOfDoorLock: TypeDoorLock = TypeDoorLock.WIFI
    var selectDate: SelectDate = SelectDate.START

    enum class SelectDate {
        START, END
    }

    var startDate: Long = 0L
    var endDate: Long = 0L
    var startTime: Int = 0
    var endTime: Int = 0

    var sh = 0
    var eh = 0
    var sm = 0
    var em = 0
    var noOfField = 7

    override fun getLayoutView() = R.layout.activity_door_lock_temporary_password_create

    override fun bindView() {
        preference = SmartWorldPreferences(this)
    }

    override fun setupInstance() {
        setupToolbar(
            getString(R.string.title_temporary_pw),
            showButtonBack = true,
            showRightMenu = true
        )
    }

    override fun setupView() {
        deviceId = intent.getStringExtra(Constants.INTENT_DEV_ID)
        typeOfDoorLock =
            intent.getSerializableExtra(Constants.INTENT_DOOR_LOCK_TYPE) as TypeDoorLock
        btnRandom?.setOnClickListener {
            val password = Random.nextInt(1000000, 9999999).toString()
            squareFieldPassword?.setText(password)
            checkValidData()
        }

        if (typeOfDoorLock == TypeDoorLock.WIFI) {
            squareFieldPassword?.numberOfFields = 7
            noOfField = 7
        } else {
            squareFieldPassword?.numberOfFields = 6
            noOfField = 6
        }
        squareFieldPassword?.inputType = InputType.TYPE_CLASS_NUMBER

        btnButtom?.setOnClickListener {
            val temPass = TemporaryPasswordEntity(
                password = squareFieldPassword?.text?.toString(),
                name = tvName?.text?.toString(),
                startDate = startDate + startTime,
                endDate = endDate + endTime
            )
            val intent = Intent(
                this,
                TemporaryConfirmCreatePasswordDoorLockActivity::class.java
            )
            intent.putExtra(Constants.INTENT_DEV_ID, deviceId)
            intent.putExtra(Constants.INTENT_DOOR_LOCK_TYPE, typeOfDoorLock)
            intent.putExtra(Constants.INTENT_DOOR_LOCK_TEMP_PASS, temPass)

            startConfirmTempPasswordForResult.launch(intent)
        }

        createNameBox?.setOnClickListener {
            supportFragmentManager.let {
                val notices = EditRoomDialogFragment(
                    tvTitlePassword?.text.toString(),
                    tvName.text.toString(),
                    object : EditRoomDialogFragment.OnEditNameListener {
                        override fun onSaveClick(name: String) {
                            tvName.text = name
                            checkValidData()
                        }
                    })
                notices.isCancelable = false
                notices.show(
                    it,
                    "showAlertNotices"
                )
            }
        }

        tuyaDoorLockVM?.createTemporaryPasswordListener()?.observe(this, createTempResp)

        edtStart?.setOnClickListener {
            selectDate = SelectDate.START
            SlideDatePickerDialog.Builder()
                .setLocale(getCurrentLanguage())
                //.setYearModifier(543)
                .setHeaderDateFormat("EE dd MMM")
                .setCancelText(getString(R.string.action_cancel))
                .setConfirmText(getString(R.string.action_ok))
                .setEndDate(Calendar.getInstance().apply {
                    add(Calendar.YEAR, 10)
                })
                .setStartDate(Calendar.getInstance().apply {
                    set(Calendar.DATE, this.get(Calendar.DATE) - 0)
                })
                .setHeaderTextColor(R.color.yellow)
                .setThemeColor(ContextCompat.getColor(this, R.color.yellow))
                .setPreselectedDate(Calendar.getInstance())
                .build()
                .show(supportFragmentManager, "TAG")
        }

        edtEnd?.setOnClickListener {
            selectDate = SelectDate.END
            SlideDatePickerDialog.Builder()
                .setLocale(getCurrentLanguage())
                // .setYearModifier(543)
                .setHeaderDateFormat("EE dd MMM")
                .setCancelText(getString(R.string.action_cancel))
                .setConfirmText(getString(R.string.action_ok))
                .setEndDate(Calendar.getInstance().apply {
                    add(Calendar.YEAR, 10)
                })
                .setStartDate(Calendar.getInstance().apply {
                    set(Calendar.DATE, this.get(Calendar.DATE) - 0)
                })
                .setHeaderTextColor(R.color.yellow)
                .setThemeColor(ContextCompat.getColor(this, R.color.yellow))
                .setPreselectedDate(Calendar.getInstance())
                .build()
                .show(supportFragmentManager, "TAG")
        }

        edtTimeEnd?.setOnClickListener {
            SnapTimePickerDialog.Builder().apply {

                setPreselectedTime(TimeValue(eh, em))
                setTitle(R.string.title_select_time)
                setPrefix(R.string.time_prefix)
                setSuffix(R.string.time_suffix)
                setThemeColor(R.color.yellow)
                setTitleColor(R.color.white)
                setNegativeButtonColor(R.color.yellow)
                setPositiveButtonColor(R.color.yellow)
                setButtonTextAllCaps(false)
            }.build().apply {
                setListener { hour, minute ->
                    em = minute
                    eh = hour
                    endTime = (hour * 60 * 60 * 1000) + (minute * 60 * 1000)
                    val h = if (hour < 10) "0".plus(hour) else hour.toString()
                    val m = if (minute < 10) "0".plus(minute) else minute

                    findViewById<TextView>(R.id.edtTimeEnd)?.text = h.plus(" : ").plus(m)
                    checkValidData()
                }
            }.show(supportFragmentManager, SnapTimePickerDialog.TAG)

        }

        edtTimeStart?.setOnClickListener {
            SnapTimePickerDialog.Builder().apply {
                setPreselectedTime(TimeValue(sh, sm))
                setTitle(R.string.title_select_time)
                setPrefix(R.string.time_prefix)
                setSuffix(R.string.time_suffix)
                setThemeColor(R.color.yellow)
                setTitleColor(R.color.white)
                setNegativeButtonColor(R.color.yellow)
                setPositiveButtonColor(R.color.yellow)
                setButtonTextAllCaps(false)
            }.build().apply {
                setListener { hour, minute ->
                    sm = minute
                    sh = hour
                    startTime = (hour * 60 * 60 * 1000) + (minute * 60 * 1000)
                    val h = if (hour < 10) "0".plus(hour) else hour.toString()
                    val m = if (minute < 10) "0".plus(minute) else minute

                    findViewById<TextView>(R.id.edtTimeStart)?.text = h.plus(" : ").plus(m)
                    checkValidData()
                }
            }.show(supportFragmentManager, SnapTimePickerDialog.TAG)

        }
        enableSubmitPassword(false)
    }

    private fun checkValidData() {
        if (startDate != 0L && endDate != 0L && tvName?.text?.length ?: 0 > 0 && squareFieldPassword?.text?.length ?: 0 >= noOfField)
            enableSubmitPassword(true)
    }

    fun enableSubmitPassword(isEnabled: Boolean) {
        btnButtom?.isEnabled = isEnabled
        btnButtom?.isClickable = isEnabled
    }

    override fun initialize(savedInstanceState: Bundle?) {

    }

    val startConfirmTempPasswordForResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                setResult(Activity.RESULT_OK, result.data)
                finish()
            }
        }


    private val createTempResp = Observer<Resource<Boolean>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                hideProgressView()
                val start = (startDate + startTime).toString().dropLast(4)
                val end = (endDate + endTime).toString().dropLast(4)
                preference?.setPasswordLock(
                    tvName?.text?.toString().plus(start).plus(end),
                    squareFieldPassword?.text?.toString() ?: ""
                )
                showToastSuccessAlertNotices(
                    getString(R.string.msg_successfully),
                    object : AlertUtil.OnSuccessListener {
                        override fun onSuccess() {
                            val intent = Intent()
                            setResult(Activity.RESULT_OK, intent)
                            finish()
                        }
                    })
            }

            Status.ERROR -> {
                hideProgressView()
                showToastErrorAlertNotices(
                    resource.message?.message ?: getString(R.string.msg_failure), null
                )
            }
            Status.LOADING -> {
                showProgressView()
            }
        }
    }

    override fun onPositiveClick(day: Int, month: Int, year: Int, calendar: Calendar) {
        val time = SimpleDateFormat("dd MMM yyyy").format(calendar.time)
        calendar.set(
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH),
            0,
            0,
            0
        )
        if (selectDate == SelectDate.START) {
            edtStart.text = time
            startDate = calendar.timeInMillis
        } else {
            edtEnd.text = time
            endDate = calendar.timeInMillis
        }

        checkValidData()

        Timber.e(
            "${System.currentTimeMillis()} : ".plus((System.currentTimeMillis() + (5 * 60 * 1000)))
        )
        Timber.e("Start = ${startDate + startTime} | End = ${endDate + endTime}")
    }
}