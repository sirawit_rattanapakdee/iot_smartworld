package ai.apdigital.tuya.ui.control.smart_lock.temporary

import ai.apdigital.tuya.R
import ai.apdigital.tuya.base.BaseActivity
import ai.apdigital.tuya.base.SmartWorldPreferences
import ai.apdigital.tuya.model.TokenEntity
import ai.apdigital.tuya.model.temp.TemporaryPassword
import ai.apdigital.tuya.networking.Resource
import ai.apdigital.tuya.networking.Status
import ai.apdigital.tuya.base.Constants
import ai.apdigital.tuya.ui.control.smart_lock.TypeDoorLock
import ai.apdigital.tuya.ui.viewmodel.TuyaDoorLockViewModel
import ai.apdigital.tuya.ui.viewmodel.TuyaIRViewModel
import ai.apdigital.tuya.utils.EventDoorLockUtils
import ai.apdigital.tuya.utils.EventUtils
import ai.apdigital.tuya.utils.PageUtils
import ai.apdigital.tuya.utils.alerts.AlertUtil
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.tuya.smart.optimus.lock.api.bean.TempPassword
import com.tuya.smart.sdk.optimus.lock.bean.ble.TempPasswordBeanV3
import kotlinx.android.synthetic.main.activity_list_with_button.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*

class TemporaryDoorLockActivity : BaseActivity() {

    private val tuyaDoorLockVM: TuyaDoorLockViewModel? by viewModel()
    private val tuyaIRVM: TuyaIRViewModel? by viewModel()
    var deviceId: String? = ""
    var password: String? = ""
    var typeOfDoorLock: TypeDoorLock = TypeDoorLock.WIFI

    private var temporaryAdapter: TemporaryPasswordAdapter? = null
    var temporaryBLEAdapter: TemporaryPasswordBLEAdapter? = null
    var temporaryZigbeeAdapter: TemporaryPasswordZigbeeAdapter? = null

    override fun getLayoutView() = R.layout.activity_list_with_button

    override fun bindView() {

        shimmerViewContainer = shimmer_container
        contentContainer = content

    }

    override fun setupInstance() {
        setupToolbar(
            getString(R.string.title_temporary_pw),
            showButtonBack = true,
            showRightMenu = true
        )
    }

    override fun setupView() {
        deviceId = intent.getStringExtra(Constants.INTENT_DEV_ID)
        typeOfDoorLock =
            intent.getSerializableExtra(Constants.INTENT_DOOR_LOCK_TYPE) as TypeDoorLock

        tuyaIRVM?.getTokenListener()?.observe(this, tokenResp)
        tvNotFoundData.text = "No Temporary Password"
        when (typeOfDoorLock) {
            TypeDoorLock.WIFI -> {
                tuyaDoorLockVM?.getTemporaryPasswordListener()?.observe(this, tempResp)
                temporaryAdapter =
                    TemporaryPasswordAdapter(
                        this, 0, SmartWorldPreferences(this)
                    )
                temporaryAdapter?.setOnItemClickListener(object :
                    TemporaryPasswordAdapter.OnItemClickListener {
                    override fun onRecyclerItemClick(
                        view: View,
                        position: Int,
                        entity: TempPassword?
                    ) {

                    }

                    override fun onDeleteItemClick(
                        view: View,
                        position: Int,
                        entity: TempPassword?
                    ) {

                        showConfirmDelete(
                            entity?.id ?: 0,
                            0
                        )
                    }
                })
            }
            TypeDoorLock.BLE -> {
                tuyaDoorLockVM?.getTemporaryBLEPasswordListener()?.observe(this, tempBLEResp)
                temporaryBLEAdapter =
                    TemporaryPasswordBLEAdapter(
                        this, 0
                    )
                temporaryBLEAdapter?.setOnItemClickListener(object :
                    TemporaryPasswordBLEAdapter.OnItemClickListener {

                    override fun onRecyclerItemClick(
                        view: View,
                        position: Int,
                        entity: TempPasswordBeanV3?
                    ) {

                    }

                    override fun onDeleteItemClick(
                        view: View,
                        position: Int,
                        entity: TempPasswordBeanV3?
                    ) {
                        showConfirmDelete(
                            entity?.passwordId ?: 0,
                            entity?.sn ?: 0
                        )
                    }
                })
            }
            else -> {
                tuyaDoorLockVM?.getTemporaryZigbeePasswordListener()?.observe(this, tempZigbeeResp)
                temporaryZigbeeAdapter =
                    TemporaryPasswordZigbeeAdapter(
                        this, 0
                    )
                temporaryZigbeeAdapter?.setOnItemClickListener(object :
                    TemporaryPasswordZigbeeAdapter.OnItemClickListener {

                    override fun onRecyclerItemClick(
                        view: View,
                        position: Int,
                        entity: TemporaryPassword?
                    ) {

                    }

                    override fun onDeleteItemClick(
                        view: View,
                        position: Int,
                        entity: TemporaryPassword?
                    ) {
                        showConfirmDelete(
                            entity?.password_id ?: 0,
                            0
                        )
                    }
                })
            }
        }

        recyclerList?.let {
            val layoutManager = LinearLayoutManager(this)
            layoutManager.orientation = LinearLayoutManager.VERTICAL
            it.layoutManager = layoutManager
            it.adapter =
                if (typeOfDoorLock == TypeDoorLock.WIFI) temporaryAdapter else if (typeOfDoorLock == TypeDoorLock.BLE) temporaryBLEAdapter else temporaryZigbeeAdapter
            it.setHasFixedSize(true)
        }
        swipeContainer?.isRefreshing = false
        swipeContainer?.setOnRefreshListener {
            getTemporaryPassword(false)
        }

        getTemporaryPassword(false)
        btnButtom?.text = getString(R.string.action_create_new_password)
        btnButtom?.setOnClickListener {
            EventUtils.sendEvent(EventUtils.TYPE_EVENT, EventDoorLockUtils.TUYA_PANEL_DOOR_LOCK_TEMP_PW_CREATE, "")
            val intent = Intent(
                this@TemporaryDoorLockActivity,
                TemporaryCreatePasswordDoorLockActivity::class.java
            )
            intent.putExtra(Constants.INTENT_DEV_ID, deviceId)
            intent.putExtra(Constants.INTENT_DOOR_LOCK_TYPE, typeOfDoorLock)
            startCreateTempPasswordForResult.launch(intent)
        }
        tuyaDoorLockVM?.deleteTemporaryBLEPasswordListener()?.observe(this, deleteTempResp)

    }

    fun enableCommit(status: Boolean) {
        btnButtom?.isEnabled = status
        btnButtom?.isClickable = status
    }

    fun showConfirmDelete(id: Int, sn: Int) {
        showAlertConfirmWithActionClick(
            getString(R.string.alert_do_you_want_to_delete_password),
            null,
            object : AlertUtil.OnSuccessListener {
                override fun onSuccess() {
                    tuyaDoorLockVM?.deleteTemporaryPassword(
                        deviceId,
                        typeOfDoorLock, id,
                        sn
                    )
                }
            }, R.color.red
        )
    }

    private fun getTemporaryPassword(isUpdate: Boolean) {

        /*Handler(Looper.getMainLooper()).postDelayed(
            {*/
                if (typeOfDoorLock == TypeDoorLock.ZIGBEE)
                    tuyaIRVM?.getToken()
                else
                    tuyaDoorLockVM?.getTemporaryPasswordList(deviceId, typeOfDoorLock)
         /*   },
            if (isUpdate) 2000 else 0 // value in milliseconds
        )*/

    }

    override fun initialize(savedInstanceState: Bundle?) {
     EventUtils.sendEvent(EventUtils.TYPE_PAGE, PageUtils.TUYA_PANEL_DOOR_LOCK_TEMP, "")
    }

    val startCreateTempPasswordForResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                getTemporaryPassword(true)
            }
        }


    private val tempResp = Observer<Resource<List<TempPassword?>>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                swipeContainer?.isRefreshing = false
                hideShimmerLoading()
                var isHaveData = false
                resource.data?.forEach { temp ->
                    if (temp?.status == 3 || temp?.status == 2) {
                        isHaveData = true
                        return@forEach
                    }
                }
                tvNotFoundData?.visibility = if (!isHaveData) View.VISIBLE else View.GONE
                temporaryAdapter?.updateData(resource.data)
            }

            Status.ERROR -> {
                swipeContainer?.isRefreshing = false
                hideShimmerLoading()
                showToastErrorAlertNotices(
                    resource.message?.message ?: getString(R.string.msg_failure), null
                )
            }
            Status.LOADING -> {
                showShimmerLoading()
            }
        }
    }

    private val tokenResp = Observer<Resource<TokenEntity>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                tuyaDoorLockVM?.getTemporaryPasswordList(deviceId, typeOfDoorLock)
            }

            Status.ERROR -> {
                hideProgressView()
                /*showToastErrorAlertNotices(
                    resource.message?.message ?: getString(R.string.msg_failure), null
                )*/
            }
            Status.LOADING -> {
                showProgressView()
            }
        }
    }

    private val tempBLEResp = Observer<Resource<List<TempPasswordBeanV3?>>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                swipeContainer?.isRefreshing = false
                hideShimmerLoading()
                temporaryBLEAdapter?.updateData(resource.data)
                if (resource.data?.size == 0)
                    tvNotFoundData?.visibility = View.VISIBLE
                else
                    tvNotFoundData?.visibility = View.GONE
            }

            Status.ERROR -> {
                swipeContainer?.isRefreshing = false
                hideShimmerLoading()
                showToastErrorAlertNotices(
                    resource.message?.message ?: getString(R.string.msg_failure), null
                )
            }
            Status.LOADING -> {
                showShimmerLoading()
            }
        }
    }


    private val tempZigbeeResp = Observer<Resource<List<TemporaryPassword?>>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                swipeContainer?.isRefreshing = false
                hideShimmerLoading()
                temporaryZigbeeAdapter?.updateData(resource.data)
                if (resource.data?.size == 0)
                    tvNotFoundData?.visibility = View.VISIBLE
                else
                    tvNotFoundData?.visibility = View.GONE
            }

            Status.ERROR -> {
                swipeContainer?.isRefreshing = false
                hideShimmerLoading()
                showToastErrorAlertNotices(
                    resource.message?.message ?: getString(R.string.msg_failure), null
                )
            }
            Status.LOADING -> {
                showShimmerLoading()
            }
        }
    }

    private val deleteTempResp = Observer<Resource<Boolean>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                hideProgressView()
                showToastSuccessAlertNotices(getString(R.string.msg_successfully), success)
            }

            Status.ERROR -> {
                hideProgressView()
                showToastErrorAlertNotices(
                    resource.message?.message ?: getString(R.string.msg_failure), null
                )
            }
            Status.LOADING -> {
                showProgressView()
            }
        }
    }

    val success = object : AlertUtil.OnSuccessListener {
        override fun onSuccess() {
            getTemporaryPassword(true)
        }
    }
}