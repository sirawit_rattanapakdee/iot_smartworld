package ai.apdigital.tuya.ui.control.smart_lock.temporary

import ai.apdigital.tuya.R
import ai.apdigital.tuya.base.SmartWorldPreferences
import android.content.Context
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.tuya.smart.optimus.lock.api.bean.TempPassword
import kotlinx.android.synthetic.main.item_door_lock_password.*
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*


class TemporaryPasswordAdapter(
    val context: Context,
    val type: Int,
    var preference: SmartWorldPreferences?
) :
    RecyclerView.Adapter<TemporaryPasswordAdapter.AdapterViewHolder>() {
    interface OnItemClickListener {
        fun onRecyclerItemClick(view: View, position: Int, entity: TempPassword?)
        fun onDeleteItemClick(view: View, position: Int, entity: TempPassword?)
    }

    private var data: MutableList<TempPassword?> = arrayListOf()
    private var mListener: OnItemClickListener? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        return AdapterViewHolder(
            layoutInflater.inflate(
                R.layout.item_temporary_password,
                parent,
                false
            ), this
        )
    }


    fun setOnItemClickListener(onEntryClickListener: OnItemClickListener) {
        mListener = onEntryClickListener
    }

    override fun getItemCount(): Int = data.size
    override fun onBindViewHolder(
        viewHolder: AdapterViewHolder,
        position: Int
    ) {

        viewHolder.holdItem = data[position]
        viewHolder.holdItem?.let { entity ->

            val dateFormatter = SimpleDateFormat("dd MMM yyyy HH:mm")
            dateFormatter.timeZone = TimeZone.getDefault()
            viewHolder.tvDesc?.text = entity.name

            viewHolder.tvDate?.text = dateFormatter.format(entity.effectiveTime).plus(" - ")
                .plus(dateFormatter.format(entity.invalidTime))

            /*val psw = preference?.getPasswordLock(
                entity.name.plus(entity.effectiveTime.toString().dropLast(4))
                    .plus(entity.invalidTime.toString().dropLast(4))
            )
            var pw = ""
            psw?.toCharArray()?.forEach { v ->
                pw = pw.plus(v).plus(" ")
            }
            viewHolder.tvTitle?.text = pw*/

            //Timber.e(Gson().toJson(entity))
        }
    }

    fun updateData(items: List<TempPassword?>?) {
        this.data = mutableListOf()

        items?.let { it ->
            it.forEach { temp ->
                if (temp?.status == 3 || temp?.status == 2) {
                    this.data.add(temp)
                }
            }
            //this.data = it.toMutableList()
        }

        notifyDataSetChanged()
    }

    inner class AdapterViewHolder(
        itemView: View,
        val adapterDevice: TemporaryPasswordAdapter
    ) :
        RecyclerView.ViewHolder(itemView),
        View.OnClickListener, View.OnLongClickListener, View.OnTouchListener {

        var holdItem: TempPassword? = null
        private var container: ConstraintLayout? = itemView.findViewById(R.id.container)
        internal var tvDesc: AppCompatTextView? = itemView.findViewById(R.id.tvDesc)
        internal var tvDate: AppCompatTextView? = itemView.findViewById(R.id.tvDate)
        internal var tvDelete: AppCompatTextView? = itemView.findViewById(R.id.tvDelete)

        init {
            container?.setOnClickListener(this)
            container?.setOnLongClickListener(this)
            container?.setOnTouchListener(this)
            val allSceneString = SpannableString(tvDelete?.text)
            allSceneString.setSpan(UnderlineSpan(), 0, allSceneString.length, 0)
            tvDelete?.text = allSceneString
            tvDelete?.setOnClickListener {
                mListener?.onDeleteItemClick(
                    it,
                    layoutPosition,
                    holdItem
                )
            }
        }

        override fun onClick(v: View?) {
            mListener?.onRecyclerItemClick(itemView, layoutPosition, holdItem)
        }

        override fun onLongClick(v: View?): Boolean {
            mListener?.onRecyclerItemClick(itemView, layoutPosition, holdItem)
            return true
        }

        override fun onTouch(v: View?, event: MotionEvent?): Boolean {
            when (event?.action) {
                MotionEvent.ACTION_DOWN -> {
                    v?.alpha = 0.5f
                }
                MotionEvent.ACTION_UP
                -> {
                    v?.alpha = 1f
                }
                MotionEvent.ACTION_CANCEL -> {
                    v?.alpha = 1f
                }
            }
            return false
        }
    }
}