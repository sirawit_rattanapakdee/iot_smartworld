package ai.apdigital.tuya.ui.control.smart_lock.temporary

import ai.apdigital.tuya.R
import ai.apdigital.tuya.model.temp.TemporaryPassword
import android.content.Context
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.tuya.smart.sdk.optimus.lock.bean.ble.TempPasswordBeanV3
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*


class TemporaryPasswordZigbeeAdapter(val context: Context, val type: Int) :
    RecyclerView.Adapter<TemporaryPasswordZigbeeAdapter.AdapterViewHolder>() {

    interface OnItemClickListener {
        fun onRecyclerItemClick(view: View, position: Int, entity: TemporaryPassword?)
        fun onDeleteItemClick(view: View, position: Int, entity: TemporaryPassword?)
    }

    private var data: MutableList<TemporaryPassword?> = arrayListOf()
    private var mListener: OnItemClickListener? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        return AdapterViewHolder(
            layoutInflater.inflate(
                R.layout.item_temporary_password,
                parent,
                false
            ), this
        )
    }


    fun setOnItemClickListener(onEntryClickListener: OnItemClickListener) {
        mListener = onEntryClickListener
    }

    override fun getItemCount(): Int = data.size
    override fun onBindViewHolder(
        viewHolder: AdapterViewHolder,
        position: Int
    ) {

        viewHolder.holdItem = data[position]
        viewHolder.holdItem?.let { entity ->

            val dateFormatter = SimpleDateFormat("dd MMM yyyy HH:mm")
            dateFormatter.timeZone = TimeZone.getDefault()
            //viewHolder.tvTitle?.text = entity.
            viewHolder.tvDesc?.text = entity.name

            viewHolder.tvDate?.text = dateFormatter.format(entity.effective_time.toString().plus("000").toLong()).plus("-")
                .plus(dateFormatter.format(entity.invalid_time.toString().plus("000").toLong()))

            //   Timber.e(Gson().toJson(entity))
        }
    }

    //{"avatarUrl":"https://s3-us-west-2.amazonaws.com/airtake-public-data/smart/user_res/avatar/scale/user_icon_default.png","createTime":1629629166000,"devId":"14610040e8db84b8e815","dpCodeMap":{"unlock_password":"1"},"dpData":{"2":"1"},"tags":0,"unlockRelation":{"passwordNumber":1,"unlockType":"unlock_password"},"userId":"0","userName":"unknown"}{"avatarUrl":"https://s3-us-west-2.amazonaws.com/airtake-public-data/smart/user_res/avatar/scale/user_icon_default.png","createTime":1629629166000,"devId":"14610040e8db84b8e815","dpCodeMap":{"unlock_password":"1"},"dpData":{"2":"1"},"tags":0,"unlockRelation":{"passwordNumber":1,"unlockType":"unlock_password"},"userId":"0","userName":"unknown"}

    fun updateData(items: List<TemporaryPassword?>?) {
        this.data = mutableListOf()
        items?.let { it ->
            this.data = it.toMutableList()
        }

        notifyDataSetChanged()
    }

    inner class AdapterViewHolder(
        itemView: View,
        val adapterDevice: TemporaryPasswordZigbeeAdapter
    ) :
        RecyclerView.ViewHolder(itemView),
        View.OnClickListener, View.OnLongClickListener, View.OnTouchListener {

        var holdItem: TemporaryPassword? = null
        private var container: ConstraintLayout? = itemView.findViewById(R.id.container)
        internal var tvDesc: AppCompatTextView? = itemView.findViewById(R.id.tvDesc)
        internal var tvDate: AppCompatTextView? = itemView.findViewById(R.id.tvDate)
        internal var tvDelete: AppCompatTextView? = itemView.findViewById(R.id.tvDelete)

        init {
            container?.setOnClickListener(this)
            container?.setOnLongClickListener(this)
            container?.setOnTouchListener(this)
            val allSceneString = SpannableString(tvDelete?.text)
            allSceneString.setSpan(UnderlineSpan(), 0, allSceneString.length, 0)
            tvDelete?.text = allSceneString
            tvDelete?.setOnClickListener {
                mListener?.onDeleteItemClick(
                    it,
                    layoutPosition,
                    holdItem
                )
            }
        }

        override fun onClick(v: View?) {
            mListener?.onRecyclerItemClick(itemView, layoutPosition, holdItem)
        }

        override fun onLongClick(v: View?): Boolean {
            mListener?.onRecyclerItemClick(itemView, layoutPosition, holdItem)
            return true
        }

        override fun onTouch(v: View?, event: MotionEvent?): Boolean {
            when (event?.action) {
                MotionEvent.ACTION_DOWN -> {
                    v?.alpha = 0.5f
                }
                MotionEvent.ACTION_UP
                -> {
                    v?.alpha = 1f
                }
                MotionEvent.ACTION_CANCEL -> {
                    v?.alpha = 1f
                }
            }
            return false
        }
    }
}