package ai.apdigital.tuya.ui.control.smart_lock.unlock

import ai.apdigital.tuya.R
import ai.apdigital.tuya.base.BaseFragment
import ai.apdigital.tuya.ui.DeviceContainer
import ai.apdigital.tuya.utils.EventDoorLockUtils
import ai.apdigital.tuya.utils.EventUtils
import ai.apdigital.tuya.utils.SchemaConverter
import android.Manifest
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import com.bumptech.glide.Glide
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.tuya.smart.optimus.lock.api.ITuyaBleLock
import com.tuya.smart.optimus.lock.api.ITuyaLockManager
import com.tuya.smart.optimus.sdk.TuyaOptimusSdk

import kotlinx.android.synthetic.main.item_door_lock_unlock.*
import timber.log.Timber


class BLEUnlockDoorFragment : BaseFragment(), View.OnTouchListener {
    private var deviceContainer: DeviceContainer? = null

    companion object {
        fun newInstance(deviceContainer: DeviceContainer?) =
            BLEUnlockDoorFragment()
                .apply {
                    this.deviceContainer = deviceContainer
                    arguments = Bundle().apply {
                    }
                }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {

    }

    override fun getLayoutView() = R.layout.item_door_lock_unlock

    override fun setupInstance() {

    }

    override fun setupView() {
        enableUnlock(false)
    }

    override fun initialize() {

        Dexter.withContext(context)
            .withPermissions(
                Manifest.permission.BLUETOOTH,
                Manifest.permission.BLUETOOTH_ADMIN,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            ).withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    initBLE()
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest?>?,
                    token: PermissionToken?
                ) {
                    token?.continuePermissionRequest()
                }
            }).check()

    }

    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        when (event?.action) {
            MotionEvent.ACTION_DOWN -> {
                v?.alpha = 0.5f
            }
            MotionEvent.ACTION_UP
            -> {
                v?.alpha = 1f
            }
            MotionEvent.ACTION_CANCEL -> {
                v?.alpha = 1f
            }
        }
        return false
    }

    var tuyaLockDevice: ITuyaBleLock? = null
    fun initBLE() {
        bg?.setOnTouchListener(this)
        val tuyaLockManager = TuyaOptimusSdk.getManager(
            ITuyaLockManager::class.java
        )

        tuyaLockDevice = tuyaLockManager.getBleLock(deviceContainer?.device?.devId)

        if (tuyaLockDevice?.isBLEConnected == true)
            enableUnlock(true)
        else
            tuyaLockDevice?.connect { online ->
                Timber.e("onStatusChanged  online: $online")
                if (online)
                    enableUnlock(true)
            }

        bg?.setOnClickListener {
            val online = tuyaLockDevice?.isBLEConnected
            Timber.e("online: $online")
            EventUtils.sendEvent(EventUtils.TYPE_EVENT, EventDoorLockUtils.TUYA_PANEL_DOOR_LOCK_UNLOCK, "")
            if (online == true) {
                tuyaLockDevice?.unlock("1")
                showToastAlertNotices(getString(R.string.alert_unlock_door), R.drawable.ic_unlock_key, null)
            }
        }

        context?.let {
            Glide
                .with(it)
                .load(deviceContainer?.device?.iconUrl)
                .into(ivDevice)
        }
        val battery = SchemaConverter.checkBattery(deviceContainer?.device?.devId ?: "")
        if (battery != 1000) {
            ivBattery?.setImageResource(SchemaConverter.convertPercentToDrawable(battery))
            ivBattery?.visibility = View.VISIBLE
            tvPercent?.text = battery.toString().plus("%")
        } else {
            ivBattery?.visibility = View.GONE
            tvPercent?.visibility = View.GONE
        }
        tvTapToUnlock?.text = getString(R.string.action_tap_unlock_door)
    }

    override fun onDestroy() {
        super.onDestroy()
        tuyaLockDevice?.onDestroy()
    }

    private fun enableUnlock(enable: Boolean) {
        bg?.isClickable = enable
        bg?.isEnabled = enable

        //bg?.setImageResource(if (enable) R.drawable.ic_unlock_available else R.drawable.ic_unlock_normal)// = enable
    }
}