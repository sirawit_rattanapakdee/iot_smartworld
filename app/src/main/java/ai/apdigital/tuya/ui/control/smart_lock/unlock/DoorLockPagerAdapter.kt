package ai.apdigital.tuya.ui.control.smart_lock.unlock

import ai.apdigital.tuya.ui.DeviceContainer
import ai.apdigital.tuya.ui.control.smart_lock.TypeDoorLock
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter

class DoorLockPagerAdapter(
    fa: FragmentActivity,
    val deviceContainer: DeviceContainer,
    var typeDoorLock: TypeDoorLock, var item: Int
) :
    FragmentStateAdapter(fa) {
    override fun getItemCount(): Int = item

    override fun createFragment(position: Int): Fragment =
        if (position == 0) {
            when (typeDoorLock) {
                TypeDoorLock.WIFI -> WifiUnlockDoorFragment.newInstance(
                    deviceContainer
                )
                TypeDoorLock.BLE -> BLEUnlockDoorFragment.newInstance(
                    deviceContainer
                )
                else -> ZigbeeUnlockDoorFragment.newInstance(
                    deviceContainer
                )
            }

        } else
            UnlockDoorDynamicPasswordFragment.newInstance(
                deviceContainer, typeDoorLock
            )
}