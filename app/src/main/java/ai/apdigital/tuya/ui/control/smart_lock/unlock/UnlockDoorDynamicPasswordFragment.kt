package ai.apdigital.tuya.ui.control.smart_lock.unlock

import ai.apdigital.tuya.R
import ai.apdigital.tuya.base.BaseFragment
import ai.apdigital.tuya.model.TokenEntity
import ai.apdigital.tuya.networking.Resource
import ai.apdigital.tuya.networking.Status
import ai.apdigital.tuya.ui.DeviceContainer
import ai.apdigital.tuya.ui.control.smart_lock.TypeDoorLock
import ai.apdigital.tuya.ui.viewmodel.TuyaDoorLockViewModel
import ai.apdigital.tuya.ui.viewmodel.TuyaIRViewModel
import ai.apdigital.tuya.utils.EventDoorLockUtils
import ai.apdigital.tuya.utils.EventUtils
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.view.MotionEvent
import android.view.View
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.item_door_lock_password.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class UnlockDoorDynamicPasswordFragment : BaseFragment(), View.OnTouchListener {
    private val tuyaDoorLockVM: TuyaDoorLockViewModel? by viewModel()
    private val tuyaIRVM: TuyaIRViewModel? by viewModel()
    private var deviceContainer: DeviceContainer? = null
    var password: String? = ""
    var typeOfDoorLock: TypeDoorLock = TypeDoorLock.WIFI

    companion object {
        fun newInstance(deviceContainer: DeviceContainer?, typeOfDoorLock: TypeDoorLock) =
            UnlockDoorDynamicPasswordFragment()
                .apply {
                    this.deviceContainer = deviceContainer
                    this.typeOfDoorLock = typeOfDoorLock
                    arguments = Bundle().apply {
                    }
                }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {

    }

    override fun getLayoutView() = R.layout.item_door_lock_password
    override fun setupInstance() {

        val allSceneString = SpannableString(tvCopyPassword?.text)
        allSceneString.setSpan(UnderlineSpan(), 0, allSceneString.length, 0)
        tvCopyPassword?.text = allSceneString
    }

    override fun setupView() {
        tuyaIRVM?.getTokenListener()?.observe(this, tokenResp)
        enableUnlock(true)
        val allSceneString = SpannableString(tvCopyPassword?.text)
        allSceneString.setSpan(UnderlineSpan(), 0, allSceneString.length, 0)
        tvCopyPassword?.text = allSceneString
    }


    private val tokenResp = Observer<Resource<TokenEntity>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                //hideProgressView()
                tuyaDoorLockVM?.tabToGetDynamicZigbeeDoorLock(deviceContainer?.device?.devId)
            }

            Status.ERROR -> {
                hideProgressView()
                /*showToastErrorAlertNotices(
                    resource.message?.message ?: getString(R.string.msg_failure), null
                )*/
            }
            Status.LOADING -> {
                showProgressView()
            }
        }
    }

    override fun initialize() {
        val entity = deviceContainer?.device

        tuyaDoorLockVM?.getDynamicPasswordListener()?.observe(this, dynamicPasswordRes)
        bg?.setOnClickListener {
            EventUtils.sendEvent(
                EventUtils.TYPE_EVENT,
                EventDoorLockUtils.TUYA_PANEL_DOOR_LOCK_DYN_PW,
                ""
            )
            if (typeOfDoorLock == TypeDoorLock.ZIGBEE)
                tuyaIRVM?.getToken()
            else
                tuyaDoorLockVM?.tabToGetDynamicDoorLock(entity?.devId, typeOfDoorLock)
        }
        bg?.setOnTouchListener(this)
        tvCopyPassword?.setOnClickListener {
            setClipboard(context, password)
        }
        tvCopyPassword?.visibility = View.GONE
    }

    private fun setClipboard(context: Context?, text: String?) {
        val clipboard = context?.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText("Copied Text", text)
        clipboard.setPrimaryClip(clip)
        showToastSuccessAlertNotices(getString(R.string.copied_to_the_phone_clipboard), null)
    }

    private val dynamicPasswordRes = Observer<Resource<String>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                hideProgressView()
                resource.data?.let {
                    password = it
                    var pw = ""
                    it.toCharArray().forEach { v ->
                        pw = pw.plus(v).plus(" ")
                    }
                    tvPassword?.text = pw
                    tvTapToUnlock?.text =
                        getString(R.string.text_remote_dynamic_password_invalid_after_5)
                    tvCopyPassword?.visibility = View.VISIBLE
                    enableUnlock(false)
                }
            }

            Status.ERROR -> {
                showErrorAlertNotices(
                    resource.message, getString(R.string.action_ok)
                )
            }
            Status.LOADING -> {
                showProgressView()
            }
        }
    }

    private fun enableUnlock(enable: Boolean) {
        bg?.isClickable = enable
        bg?.isEnabled = enable
    }


    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        when (event?.action) {
            MotionEvent.ACTION_DOWN -> {
                v?.alpha = 0.5f
            }
            MotionEvent.ACTION_UP
            -> {
                v?.alpha = 1f
            }
            MotionEvent.ACTION_CANCEL -> {
                v?.alpha = 1f
            }
        }
        return false
    }
}