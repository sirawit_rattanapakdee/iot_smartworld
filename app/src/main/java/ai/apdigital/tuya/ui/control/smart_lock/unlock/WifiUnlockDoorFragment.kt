package ai.apdigital.tuya.ui.control.smart_lock.unlock

import ai.apdigital.tuya.R
import ai.apdigital.tuya.base.BaseFragment
import ai.apdigital.tuya.networking.Resource
import ai.apdigital.tuya.networking.Status
import ai.apdigital.tuya.ui.DeviceContainer
import ai.apdigital.tuya.ui.control.panal.dpItem.*
import ai.apdigital.tuya.ui.viewmodel.TuyaDoorLockViewModel
import ai.apdigital.tuya.utils.EventDoorLockUtils
import ai.apdigital.tuya.utils.EventUtils
import ai.apdigital.tuya.utils.SchemaConverter
import ai.apdigital.tuya.utils.alerts.AlertUtil
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.tuya.smart.home.sdk.TuyaHomeSdk
import com.tuya.smart.sdk.api.IDevListener
import com.tuya.smart.sdk.api.ITuyaDevice

import kotlinx.android.synthetic.main.item_door_lock_unlock.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class WifiUnlockDoorFragment : BaseFragment(), View.OnTouchListener {
    private var deviceContainer: DeviceContainer? = null
    private val tuyaDoorLockVM: TuyaDoorLockViewModel? by viewModel()
    var mDevice: ITuyaDevice? = null

    companion object {
        fun newInstance(deviceContainer: DeviceContainer?) =
            WifiUnlockDoorFragment()
                .apply {
                    this.deviceContainer = deviceContainer
                    arguments = Bundle().apply {
                    }
                }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {

    }

    override fun getLayoutView() = R.layout.item_door_lock_unlock

    override fun setupInstance() {

    }

    override fun setupView() {
        enableUnlock(false)
    }

    override fun initialize() {
        val entity = deviceContainer?.device
        val battery = SchemaConverter.checkBattery(deviceContainer?.device?.devId ?: "")
        if (battery != 1000) {
            ivBattery?.setImageResource(SchemaConverter.convertPercentToDrawable(battery))
            ivBattery?.visibility = View.VISIBLE
            tvPercent?.text = battery.toString().plus(" %")
        } else {
            ivBattery?.visibility = View.INVISIBLE
            tvPercent?.visibility = View.INVISIBLE
        }


        tvCountdown?.visibility = View.INVISIBLE
        tuyaDoorLockVM?.registerUnlockListener()?.observe(this, registerDoorListenerResp)
        tuyaDoorLockVM?.remoteUnlockListener()?.observe(this, unlockResp)
        tuyaDoorLockVM?.registerUnLockListener(entity?.devId)

        context?.let {
            Glide
                .with(it)
                .load(entity?.iconUrl)
                .into(ivDevice)
        }

        bg?.setOnClickListener {
            EventUtils.sendEvent(EventUtils.TYPE_EVENT, EventDoorLockUtils.TUYA_PANEL_DOOR_LOCK_UNLOCK, "")
            tuyaDoorLockVM?.replyUnlock(true, entity?.devId)
        }

        bg?.setOnTouchListener(this)

        mDevice = TuyaHomeSdk.newDeviceInstance(entity?.devId)
        mDevice?.registerDevListener(object : IDevListener {
            override fun onDpUpdate(devId: String?, dpStr: String?) {
                Timber.e("$dpStr")
                //if (!isShowUnlock) {
                val dps: Map<*, *>? = Gson().fromJson(dpStr, Map::class.java)
                if (dps?.size == 1)
                    dps.forEach exitDPS@{ dp ->
                        TuyaHomeSdk.getDataInstance().getSchema(devId)?.let { map ->
                            for (bean in map.values) {
                                if (bean.id == dp.key)
                                    if (bean.code == "doorbell") {
                                        alertToOpenDoor()
                                        return@exitDPS
                                    }
                            }
                        }
                    }
                //  }
            }

            override fun onRemoved(devId: String?) {
            }

            override fun onStatusChanged(devId: String?, online: Boolean) {

            }

            override fun onNetworkStatusChanged(devId: String?, status: Boolean) {

            }

            override fun onDevInfoUpdate(devId: String?) {

            }
        })
    }

    //var isShowUnlock = false
    private fun alertToOpenDoor() {
        //isShowUnlock = true
        showConfirmAndOKAndFail(
            getString(R.string.is_this_openning_allowed),
            getString(R.string.please_response),
            object : AlertUtil.OnSuccessListener {
                override fun onSuccess() {
                    //   isShowUnlock = false
                    tuyaDoorLockVM?.replyUnlock(true, deviceContainer?.device?.devId)
                }
            }, object : AlertUtil.OnCancelListener {
                override fun onCancel() {
                    // isShowUnlock = false
                }
            },
            R.color.black
        )
    }

    private val registerDoorListenerResp = Observer<Resource<Int>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                hideProgressView()
                resource.data?.let {
                    enableUnlock(it != 0)
                    tvCountdown?.text = it.toString().plus(" S")
                }
            }

            Status.ERROR -> {
                /*showErrorAlertNotices(
                    resource.message, getString(R.string.action_ok)
                )*/
            }
            Status.LOADING -> {
                //showProgressView()
            }
        }
    }

    private val unlockResp = Observer<Resource<Boolean>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                hideProgressView()
                enableUnlock(false)
            }

            Status.ERROR -> {
                /*showErrorAlertNotices(
                    resource.message, getString(R.string.action_ok)
                )*/
            }
            Status.LOADING -> {
                //showProgressView()
            }
        }
    }

    private fun enableUnlock(enable: Boolean) {
        bg?.isClickable = enable
        bg?.isEnabled = enable
        if (enable)
            tvTapToUnlock.text = getString(R.string.action_tap_unlock_door)
        else
            tvTapToUnlock.text = getString(R.string.text_no_request)

        bg?.setImageResource(if (enable) R.drawable.bg_doorlock_unlock else R.drawable.bg_doorlock_normal)
        tvCountdown?.visibility = if (enable) View.VISIBLE else View.INVISIBLE
    }


    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        when (event?.action) {
            MotionEvent.ACTION_DOWN -> {
                v?.alpha = 0.5f
            }
            MotionEvent.ACTION_UP
            -> {
                v?.alpha = 1f
            }
            MotionEvent.ACTION_CANCEL -> {
                v?.alpha = 1f
            }
        }
        return false
    }

    override fun onDestroy() {
        super.onDestroy()
        mDevice?.unRegisterDevListener()
    }
}