package ai.apdigital.tuya.ui.control.smart_lock.unlock

import ai.apdigital.tuya.R
import ai.apdigital.tuya.base.BaseFragment
import ai.apdigital.tuya.model.TicketEntity
import ai.apdigital.tuya.model.TokenEntity
import ai.apdigital.tuya.networking.Resource
import ai.apdigital.tuya.networking.Status
import ai.apdigital.tuya.ui.DeviceContainer
import ai.apdigital.tuya.ui.viewmodel.TuyaDoorLockViewModel
import ai.apdigital.tuya.ui.viewmodel.TuyaIRViewModel
import ai.apdigital.tuya.utils.EventDoorLockUtils
import ai.apdigital.tuya.utils.EventUtils
import ai.apdigital.tuya.utils.SchemaConverter
import android.Manifest
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.gson.Gson
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.tuya.smart.home.sdk.TuyaHomeSdk
import com.tuya.smart.sdk.api.IDevListener
import com.tuya.smart.sdk.api.ITuyaDevice
import com.tuya.smart.utils.AES
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.item_door_lock_unlock.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber


class ZigbeeUnlockDoorFragment : BaseFragment(), View.OnTouchListener {
    private var deviceContainer: DeviceContainer? = null
    private val tuyaDoorLockVM: TuyaDoorLockViewModel? by viewModel()
    private val tuyaIRVM: TuyaIRViewModel? by viewModel()
    var dialogNumber: BottomSheetDialog? = null
    var mDevice: ITuyaDevice? = null
    var edtPassword: AppCompatEditText? = null

    companion object {
        fun newInstance(deviceContainer: DeviceContainer?) =
            ZigbeeUnlockDoorFragment()
                .apply {
                    this.deviceContainer = deviceContainer
                    arguments = Bundle().apply {
                    }
                }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {

    }

    override fun getLayoutView() = R.layout.item_door_lock_unlock

    override fun setupInstance() {

    }

    override fun setupView() {
    //    enableUnlock(true)
    }

    override fun initialize() {

        Dexter.withContext(context)
            .withPermissions(
                Manifest.permission.BLUETOOTH,
                Manifest.permission.BLUETOOTH_ADMIN,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            ).withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    initZigbee()
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest?>?,
                    token: PermissionToken?
                ) {

                }
            }).check()
        bg?.setOnTouchListener(this)
        val battery = SchemaConverter.checkBattery(deviceContainer?.device?.devId ?: "")
        if (battery != 1000) {
            ivBattery?.setImageResource(SchemaConverter.convertPercentToDrawable(battery))
            ivBattery?.visibility = View.VISIBLE
            tvPercent?.text = battery.toString().plus(" %")
        } else {
            ivBattery?.visibility = View.GONE
            tvPercent?.visibility = View.GONE
        }

    }

    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        when (event?.action) {
            MotionEvent.ACTION_DOWN -> {
                v?.alpha = 0.5f
            }
            MotionEvent.ACTION_UP
            -> {
                v?.alpha = 1f
            }
            MotionEvent.ACTION_CANCEL -> {
                v?.alpha = 1f
            }
        }
        return false
    }

    private val tokenResp = Observer<Resource<TokenEntity>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                //hideProgressView()
                tuyaDoorLockVM?.getTicket(deviceContainer?.device?.devId ?: "")
            }

            Status.ERROR -> {
                hideProgressView()
                /*showToastErrorAlertNotices(
                    resource.message?.message ?: getString(R.string.msg_failure), null
                )*/
            }
            Status.LOADING -> {
                showProgressView()
            }
        }
    }

    var ticket: TicketEntity? = null
    private val ticketResp = Observer<Resource<TicketEntity>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                //hideProgressView()
                ticket = resource.data
                val originalKey =
                    AES.decryptString(ticket?.ticket_key, "b69691b0c658452da8357c0e16889a7d")
                val password = AES.encryptString(edtPassword?.text.toString(), originalKey)
                dialogNumber?.dismiss()
                tuyaDoorLockVM?.openZigbeeDoor(
                    devId = deviceContainer?.device?.devId,
                    "ticket",
                    password, ticketId = ticket?.ticket_id
                )
            }

            Status.ERROR -> {
                hideProgressView()
                /*showToastErrorAlertNotices(
                    resource.message?.message ?: getString(R.string.msg_failure), null
                )*/
            }
            Status.LOADING -> {
                showProgressView()
            }
        }
    }


    //var tuyaLockDevice: ITuyaBleLock? = null
    fun initZigbee() {
        tuyaIRVM?.getTokenListener()?.observe(this, tokenResp)
        tuyaDoorLockVM?.ticketDoorLockPasswordListener()?.observe(this, ticketResp)
        tuyaDoorLockVM?.openDoorLockPasswordListener()?.observe(this, responseUnLockZigbee)
        val dialogView: View? =
            layoutInflater.inflate(R.layout.layout_number_zigbee_bottom_sheet, null)
        dialogView?.let { view ->
            dialogNumber = context?.let { it1 -> BottomSheetDialog(it1) }
            dialogNumber?.setContentView(view)
            val btn1 = view.findViewById<LinearLayoutCompat?>(R.id.btn1)
            val btn2 = view.findViewById<LinearLayoutCompat?>(R.id.btn2)
            val btn3 = view.findViewById<LinearLayoutCompat?>(R.id.btn3)
            val btn4 = view.findViewById<LinearLayoutCompat?>(R.id.btn4)
            val btn5 = view.findViewById<LinearLayoutCompat?>(R.id.btn5)
            val btn6 = view.findViewById<LinearLayoutCompat?>(R.id.btn6)
            val btn7 = view.findViewById<LinearLayoutCompat?>(R.id.btn7)
            val btn8 = view.findViewById<LinearLayoutCompat?>(R.id.btn8)
            val btn9 = view.findViewById<LinearLayoutCompat?>(R.id.btn9)
            val btn0 = view.findViewById<LinearLayoutCompat?>(R.id.btn0)
            val btnDelete = view.findViewById<AppCompatImageView?>(R.id.btnDelete)
            val btnBack = view.findViewById<AppCompatImageView?>(R.id.btnBack)
            val tvEnter = view.findViewById<AppCompatTextView?>(R.id.tvEnter)
            edtPassword = view.findViewById(R.id.edtPassword)

            btn1?.setOnClickListener { typePassword("1") }
            btn2?.setOnClickListener { typePassword("2") }
            btn3?.setOnClickListener { typePassword("3") }
            btn4?.setOnClickListener { typePassword("4") }
            btn5?.setOnClickListener { typePassword("5") }
            btn6?.setOnClickListener { typePassword("6") }
            btn7?.setOnClickListener { typePassword("7") }
            btn8?.setOnClickListener { typePassword("8") }
            btn9?.setOnClickListener { typePassword("9") }
            btn0?.setOnClickListener { typePassword("0") }
            btnDelete?.setOnClickListener { typeDelete() }
            btnBack?.setOnClickListener {
                dialogNumber?.dismiss()
            }
            tvEnter?.setOnClickListener { submitPassword() }
        }


        context?.let {
            Glide
                .with(it)
                .load(deviceContainer?.device?.iconUrl)
                .into(ivDevice)
        }

        bg?.isEnabled = true
        bg?.isClickable = true
        bg?.setOnClickListener {
            dialogNumber?.show()
        }
        tvTapToUnlock?.text = getString(R.string.action_tap_unlock_door)
        GlobalScope.launch {
            mDevice = TuyaHomeSdk.newDeviceInstance(deviceContainer?.device?.devId)
            mDevice?.registerDevListener(deviceListener)
        }
    }

    fun typePassword(value: String) {
        edtPassword?.setText(edtPassword?.text.toString().plus(value))
    }

    fun typeDelete() {
        var pass = edtPassword?.text.toString()
        if (pass.isNotEmpty()) {
            pass = pass.dropLast(1)
            edtPassword?.setText(pass)
        }
    }

    fun submitPassword() {
        EventUtils.sendEvent(EventUtils.TYPE_EVENT, EventDoorLockUtils.TUYA_PANEL_DOOR_LOCK_UNLOCK, "")
        tuyaIRVM?.getToken()
    }

    private var responseUnLockZigbee = Observer<Resource<Boolean>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                hideProgressView()
                showToastAlertNotices("UNLOCK REQUEST SENT", R.drawable.ic_unlock_key, null)
                edtPassword?.setText("")
                //enableUnlock(true)
            }

            Status.ERROR -> {
                /*showErrorAlertNotices(
                    resource.message, getString(R.string.action_ok)
                )*/
            }
            Status.LOADING -> {
                //showProgressView()
            }
        }
    }

    private fun enableUnlock(enable: Boolean) {
        bg?.isClickable = enable
        bg?.isEnabled = enable

        bg?.setImageResource(if (enable) R.drawable.bg_doorlock_unlock else R.drawable.bg_doorlock_normal)// = enable
    }

    private var deviceListener: IDevListener? = object : IDevListener {
        override fun onDpUpdate(devId: String?, dpStr: String?) {
            //override fun onDpUpdate(devId: String?, dpStr: MutableMap<String, Any>?) {
            val dps: Map<*, *>? = Gson().fromJson(dpStr, Map::class.java)
            dps?.forEach { dp ->
                // Timber.e("dp.key : ${dp.key}, value : ${dp.value}")
                if (dp.key == "22") {
                    // Timber.e("Alert : showAlertUnlock")
                    showAlertUnlock(dp.value as Boolean)
                    return@forEach
                }
            }
        }

        override fun onRemoved(devId: String?) {
            Timber.e("onRemoved $devId $devId")
        }

        override fun onStatusChanged(devId: String?, online: Boolean) {


        }

        override fun onNetworkStatusChanged(devId: String?, status: Boolean) {
            Timber.e("onNetworkStatusChanged {$devId")
        }


        override fun onDevInfoUpdate(devId: String?) {
            Timber.e("onDevInfoUpdate : $devId")
        }

    }

    fun showAlertUnlock(status: Boolean) {
        if (status)
            showToastAlertNotices("Unlock successful", R.drawable.ic_alert_success, null)
        else
            showToastAlertNotices("Wrong password", R.drawable.ic_alert_error, null)
    }

    override fun onDestroy() {
        super.onDestroy()
        mDevice?.onDestroy()
    }
}