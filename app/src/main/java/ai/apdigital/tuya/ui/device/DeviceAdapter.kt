package ai.apdigital.tuya.ui.device

import ai.apdigital.tuya.R
import android.content.Context
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.tuya.smart.android.device.bean.*
import com.tuya.smart.home.sdk.TuyaHomeSdk
import com.tuya.smart.home.sdk.bean.RoomBean
import com.tuya.smart.sdk.bean.DeviceBean
import timber.log.Timber


class DeviceAdapter(val context: Context, val type: Int) :
    RecyclerView.Adapter<DeviceAdapter.AdapterViewHolder>() {


    interface OnItemClickListener {
        fun onRecyclerItemClick(view: View, position: Int, entity: DeviceBean?)
        fun onRecyclerItemLongClick(view: View, position: Int, entity: DeviceBean?)

    }

    private var data: MutableList<DeviceBean> = arrayListOf()
    private var mListener: OnItemClickListener? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        return AdapterViewHolder(
            layoutInflater.inflate(
                R.layout.item_device,
                parent,
                false
            ), this
        )
    }


    fun setOnItemClickListener(onEntryClickListener: OnItemClickListener) {
        mListener = onEntryClickListener
    }

    override fun getItemCount(): Int = data.size
    override fun onBindViewHolder(
        viewHolder: AdapterViewHolder,
        position: Int
    ) {

        viewHolder.holdItem = data[position]
        viewHolder.holdItem?.let { entity ->
            viewHolder.ivDevice?.let { iv ->
                Glide
                    .with(context)
                    .load(entity.iconUrl)
                    .into(iv)
            }
            viewHolder.tvDeviceName?.text = entity.name
            if (type == 2) {
                viewHolder.ivNext?.visibility = View.VISIBLE
                viewHolder.tvRoom?.visibility = View.VISIBLE
                viewHolder.tvRoom?.text = "-"
                val roomBean = TuyaHomeSdk.getDataInstance().getDeviceRoomBean(entity.devId)
                roomBean?.let {
                    viewHolder.tvRoom?.text =
                        context.getString(R.string.room).plus(": ").plus(it.name)
                }
            }
        }
    }

    fun updateData(items: List<DeviceBean>?) {
        this.data = mutableListOf()
        items?.let { it ->
            this.data = it.toMutableList()
        }
        notifyDataSetChanged()
    }

    inner class AdapterViewHolder(
        itemView: View,
        val adapterDevice: DeviceAdapter
    ) :
        RecyclerView.ViewHolder(itemView),
        View.OnClickListener, View.OnLongClickListener, View.OnTouchListener {

        var holdItem: DeviceBean? = null
        private var container: ConstraintLayout? = itemView.findViewById(R.id.container)
        internal var tvDeviceName: AppCompatTextView? = itemView.findViewById(R.id.tvDeviceName)
        internal var ivDevice: AppCompatImageView? = itemView.findViewById(R.id.ivDevice)
        internal var tvRoom: AppCompatTextView? = itemView.findViewById(R.id.tvRoom)
        internal var ivNext: AppCompatImageView? = itemView.findViewById(R.id.ivNext)


        init {
            container?.setOnClickListener(this)
            container?.setOnLongClickListener(this)
            container?.setOnTouchListener(this)
        }

        override fun onClick(v: View?) {
            mListener?.onRecyclerItemClick(itemView, layoutPosition, holdItem)
        }

        override fun onLongClick(v: View?): Boolean {
            mListener?.onRecyclerItemLongClick(itemView, layoutPosition, holdItem)
            return true
        }

        override fun onTouch(v: View?, event: MotionEvent?): Boolean {
            when (event?.action) {
                MotionEvent.ACTION_DOWN -> {
                    v?.alpha = 0.5f
                }
                MotionEvent.ACTION_UP
                -> {
                    v?.alpha = 1f
                }
                MotionEvent.ACTION_CANCEL -> {
                    v?.alpha = 1f
                }
            }
            return false
        }
    }
}