package ai.apdigital.tuya.ui.device

import ai.apdigital.tuya.HomeModel
import ai.apdigital.tuya.R
import ai.apdigital.tuya.base.BaseActivity
import ai.apdigital.tuya.base.ExtraName
import ai.apdigital.tuya.networking.Resource
import ai.apdigital.tuya.networking.Status
import ai.apdigital.tuya.ui.viewmodel.TuyaViewModel
import ai.apdigital.tuya.utils.alerts.AlertUtil
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.tuya.smart.home.sdk.TuyaHomeSdk
import com.tuya.smart.home.sdk.bean.HomeBean
import com.tuya.smart.home.sdk.bean.RoomBean
import com.tuya.smart.sdk.bean.DeviceBean
import kotlinx.android.synthetic.main.activity_room_create.*
import kotlinx.android.synthetic.main.toolbar_back.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class DeviceListActivity : BaseActivity() {
    private val tuyaVM: TuyaViewModel? by viewModel()
    private var deviceAdapter: DeviceAdapter? = null
    private var devicesAll: MutableList<DeviceBean>? = null
    private var roomBean: RoomBean? = null

    override fun getLayoutView() = R.layout.activity_device_list
    override fun bindView() {
        setupToolbar(
            getString(R.string.title_device_list),
            showButtonBack = true,
            showRightMenu = false
        )
    }

    override fun setupInstance() {
        val homeId = HomeModel.INSTANCE.getCurrentHome(this)
        devicesAll = TuyaHomeSdk.getDataInstance().getHomeDeviceList(homeId)

        intent.getLongExtra(ExtraName.ROOM_ID, 0L).let {
            if (it != 0L)
                roomBean = TuyaHomeSdk.getDataInstance().getRoomBean(it)
        }

    }

    override fun setupView() {
        recyclerListDevice?.let {
            deviceAdapter =
                DeviceAdapter(
                    this, 2
                )
            val layoutManager = LinearLayoutManager(this)
            layoutManager.orientation = LinearLayoutManager.VERTICAL
            it.layoutManager = layoutManager
            it.adapter = deviceAdapter
            it.setHasFixedSize(true)
            deviceAdapter?.setOnItemClickListener(object : DeviceAdapter.OnItemClickListener {

                override fun onRecyclerItemClick(
                    view: View,
                    position: Int,
                    entity: DeviceBean?
                ) {
                    roomBean?.let { room ->
                        showAlertConfirmWithActionClick(
                            "ต้องการเพิ่มอุปกรณ์นี้ใช่หรือไม่",
                            "อุปกรณ์ ".plus(entity?.name),
                            object : AlertUtil.OnSuccessListener {
                                override fun onSuccess() {
                                    tuyaVM?.addDeviceToRoom()
                                        ?.observe(this@DeviceListActivity, deviceAddRes)
                                    tuyaVM?.addDeviceToRoom(room.roomId, entity?.devId ?: "")
                                }
                            }, R.color.black
                        )
                    }
                }

                override fun onRecyclerItemLongClick(
                    view: View,
                    position: Int,
                    entity: DeviceBean?
                ) {

                }
            })
        }
        devicesAll?.sortBy { it.displayOrder }
        deviceAdapter?.updateData(devicesAll)
    }

    override fun initialize(savedInstanceState: Bundle?) {

    }

    private val deviceAddRes = Observer<Resource<Long>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                showToastSuccessAlertNotices(
                    getString(R.string.msg_successfully),
                    object : AlertUtil.OnSuccessListener {
                        override fun onSuccess() {
                            setResult(Activity.RESULT_OK, Intent())
                            finish()
                        }
                    })
            }

            Status.ERROR -> showToastErrorAlertNotices(
                resource.message?.message ?: getString(R.string.msg_failure), null
            )
            Status.LOADING -> showProgressView()

        }
    }

    override fun onDestroy() {
        deviceAdapter = null
        devicesAll = null
        roomBean = null
        super.onDestroy()
    }
}