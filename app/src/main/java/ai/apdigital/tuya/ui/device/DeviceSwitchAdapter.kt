package ai.apdigital.tuya.ui.device

import ai.apdigital.tuya.R
import ai.apdigital.tuya.ui.DeviceContainer
import ai.apdigital.tuya.ui.DeviceSwitchValue
import ai.apdigital.tuya.ui.control.camera.utils.ToastUtil
import android.content.Context
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.AppCompatToggleButton
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.tuya.smart.android.device.bean.BoolSchemaBean
import com.tuya.smart.android.device.bean.EnumSchemaBean
import com.tuya.smart.android.device.enums.DataTypeEnum

import com.tuya.smart.home.sdk.TuyaHomeSdk
import kotlinx.android.synthetic.main.item_device_main_multi_switch.view.*
import timber.log.Timber


class DeviceSwitchAdapter(var context: Context) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    interface OnItemClickListener {
        fun onRecyclerItemClick(view: View, position: Int, entity: DeviceContainer?)
        fun onRecyclerSwitchClick(
            view: View?,
            position: Int,
            deviceSwitch: DeviceSwitchValue?,
            entity: DeviceContainer?
        )

        fun onRecyclerSOSModeChanged(
            view: View?,
            position: Int,
            dp: String,
            value: String
        )

        fun onAlarmSwitchClick(
            view: View?, position: Int,
            dp: String,
            value: Boolean
        )

        fun onRecyclerInfo(view: View, position: Int, entity: DeviceContainer?)
    }

    var data: MutableList<DeviceContainer>? = arrayListOf()
    var mListener: OnItemClickListener? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        /*  if (viewType == 1) {
              val inflatedView = layoutInflater.inflate(
                  R.layout.item_device_main_single_switch,
                  parent,
                  false
              )

              return AdapterSingleSwitchViewHolder(inflatedView, this)
          } else {*/
        val inflatedView = layoutInflater.inflate(
            R.layout.item_device_main_multi_switch,
            parent,
            false
        )

        return AdapterMultiSwitchViewHolder(inflatedView, this)
        //}
    }

    override fun getItemViewType(position: Int): Int {
        val entity = data?.get(position)
        return if (entity?.switchCount ?: 0 < 2) 1 else 2
    }

    fun setOnItemClickListener(onEntryClickListener: OnItemClickListener) {
        mListener = onEntryClickListener
    }

    override fun getItemCount(): Int = data?.size ?: 0

    override fun onBindViewHolder(
        viewHolder: RecyclerView.ViewHolder,
        position: Int
    ) {
        data?.get(position)?.position = position
        viewHolder as AdapterMultiSwitchViewHolder
        viewHolder.holdItem = data?.get(position)
        viewHolder.holdItem?.let { content ->
            val entity = content.device
            viewHolder.ivDevice?.let { iv ->
                Glide
                    .with(context)
                    .load(entity?.iconUrl)
                    .into(iv)
            }
            viewHolder.tvDeviceName?.text = entity?.name
            viewHolder.tvRoom?.visibility = View.VISIBLE
            viewHolder.tvRoom?.text = "-"
            val roomBean = TuyaHomeSdk.getDataInstance().getDeviceRoomBean(entity?.devId)
            roomBean?.let {
                viewHolder.tvRoom?.text = it.name
            }
            viewHolder.container?.isEnabled = true
            viewHolder.container?.isClickable = true

            if (content.switchCount > 0) {
                updateSwitchView(viewHolder, content)
            } else {
                when (content.deviceType) {
                    "PIR", "presence_state", "pir_state" -> {
                        updateViewPIR(viewHolder, content)
                    }
                    "DOOR_CONTACT" -> {
                        updateViewDoorContact(viewHolder, content)
                    }
                    "ALARM_SWITCH" -> {
                        updateViewAlarmSwitch(viewHolder, content)
                    }
                    "SOS" -> {
                        updateViewSOSSwitch(viewHolder, content)
                    }
                    else -> {
                        updateViewNormal(viewHolder, content)
                    }
                }
            }
        }
    }

    fun updateViewDoorContact(
        viewHolder: RecyclerView.ViewHolder,
        content: DeviceContainer?
    ) {
        Timber.e("updateViewDoorContact : ${content?.device?.name}")
        viewHolder.itemView.let { holder ->
            //holder as AdapterMultiSwitchViewHolder
            holder.boxSwitch3?.visibility = View.GONE
            holder.boxDoorContact?.visibility = View.INVISIBLE
            holder.boxAlarmSwitch?.visibility = View.GONE
            holder.boxSwitch2?.visibility = View.GONE
            holder.boxSwitch1?.visibility = View.GONE
            var doorStatus = false

            TuyaHomeSdk.getDataInstance().getSchema(content?.device?.devId)?.let { map ->
                if (content?.device?.categoryCode != "inf_qt") {
                    for (bean in map.values) {
                        if (bean.type == DataTypeEnum.OBJ.type) {
                            if (bean.schemaType == BoolSchemaBean.type) {
                                if (bean.code?.contains("doorcontact") == true) {
                                    doorStatus = content?.device?.dps?.get(bean.id) as Boolean
                                    holder.boxDoorContact?.visibility =
                                        if (doorStatus) View.VISIBLE else View.INVISIBLE
                                }
                            }
                        }
                    }
                }
            }

            if (doorStatus && content?.device?.isOnline == true) {
                holder.container.background =
                    ContextCompat.getDrawable(context, R.drawable.box_shadow_yellow)
                holder.tvRoom?.setTextColor(ContextCompat.getColor(context, R.color.yellow))
                holder.tvDeviceName?.setTextColor(ContextCompat.getColor(context, R.color.yellow))
            } else {
                holder.tvRoom?.setTextColor(ContextCompat.getColor(context, R.color.gray))
                holder.tvDeviceName?.setTextColor(ContextCompat.getColor(context, R.color.black))
                holder.container.background =
                    ContextCompat.getDrawable(context, R.drawable.box_shadow)
            }
            setOnline(holder.container, content?.device?.isOnline)
        }
    }

    fun setOnline(container: View, isOnline: Boolean?) {
        if (isOnline == true) {
            container.alpha = 1f
        } else {
            container.alpha = 0.3f
        }
    }

    fun updateViewNormal(
        viewHolder: RecyclerView.ViewHolder,
        content: DeviceContainer?
    ) {
        Timber.e("updateViewNormal : ${content?.device?.name}")
        viewHolder.itemView.let { holder ->
            //holder as AdapterMultiSwitchViewHolder
            holder.boxSwitch3?.visibility = View.INVISIBLE
            holder.boxSwitch2?.visibility = View.INVISIBLE
            holder.boxSwitch1?.visibility = View.INVISIBLE
            holder.boxDoorContact?.visibility = View.GONE
            holder.boxAlarmSwitch?.visibility = View.GONE

            setOnline(holder.container, content?.device?.isOnline)
        }
    }

    fun updateViewSOSSwitch(
        viewHolder: RecyclerView.ViewHolder,
        content: DeviceContainer?
    ) {
        val entity = TuyaHomeSdk.getDataInstance().getDeviceBean(content?.device?.devId)

        viewHolder.itemView.let { holder ->
            //holder as AdapterMultiSwitchViewHolder
            holder.boxSwitch3?.visibility = View.VISIBLE
            holder.boxSwitch2?.visibility = View.VISIBLE
            holder.boxSwitch1?.visibility = View.INVISIBLE
            holder.boxDoorContact?.visibility = View.GONE
            holder.boxAlarmSwitch?.visibility = View.GONE

            holder.ivSwitch2?.tag = "Arm"
            holder.ivSwitch3?.tag = "Disarm"

            Timber.e("updateSwitchView : ${content?.device?.name} | isOnline ${entity?.isOnline == true}")
            holder.ivSwitch3?.setImageResource(R.drawable.disarm_inactive)
            holder.ivSwitch2?.setImageResource(R.drawable.arm_inactive)

            if (entity?.isOnline == true) {
                holder.boxSwitch2?.visibility = View.VISIBLE
                holder.boxSwitch3?.visibility = View.VISIBLE

                TuyaHomeSdk.getDataInstance().getSchema(content?.device?.devId)?.let { map ->
                    if (content?.device?.categoryCode != "inf_qt") {
                        for (bean in map.values) {
                            if (bean.type == DataTypeEnum.OBJ.type) {
                                if (bean.schemaType == EnumSchemaBean.type) {
                                    if (bean.code?.contains("S_Mode") == true) {
                                        val doorStatus =
                                            content?.device?.dps?.get(bean.id) as String
                                        if (doorStatus.equals("arm", true)) {
                                            holder.ivSwitch3?.setImageResource(R.drawable.disarm_inactive)
                                            holder.ivSwitch2?.setImageResource(R.drawable.arm_active)
                                        } else {
                                            holder.ivSwitch3?.setImageResource(R.drawable.disarm_active)
                                            holder.ivSwitch2?.setImageResource(R.drawable.arm_inactive)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            } else {

                holder.tvRoom?.setTextColor(ContextCompat.getColor(context, R.color.gray))
                holder.tvDeviceName?.setTextColor(ContextCompat.getColor(context, R.color.black))
                holder.container.background =
                    ContextCompat.getDrawable(context, R.drawable.box_shadow)
            }
            setOnline(holder.container, content?.device?.isOnline)
        }
    }

    fun updateViewAlarmSwitch(
        viewHolder: RecyclerView.ViewHolder,
        content: DeviceContainer?
    ) {
        Timber.e("updateViewAlarmSwitch  : ${content?.device?.name}")
        viewHolder.itemView.let { holder ->
            //holder as AdapterMultiSwitchViewHolder
            holder.boxSwitch3?.visibility = View.GONE
            holder.boxDoorContact?.visibility = View.GONE
            holder.boxAlarmSwitch?.visibility = View.VISIBLE
            holder.boxSwitch2?.visibility = View.GONE
            holder.boxSwitch1?.visibility = View.GONE
            var alarmStatus = false
            TuyaHomeSdk.getDataInstance().getSchema(content?.device?.devId)?.let { map ->
                if (content?.device?.categoryCode != "inf_qt") {
                    for (bean in map.values) {
                        if (bean.type == DataTypeEnum.OBJ.type) {
                            if (bean.schemaType == BoolSchemaBean.type) {
                                if (bean.code?.contains("alarm_switch") == true) {
                                    alarmStatus = content?.device?.dps?.get(bean.id) as Boolean
                                    holder.switchAlarm?.setOnCheckedChangeListener { buttonView, isChecked ->
                                        if (buttonView.isPressed) {
                                            mListener?.onAlarmSwitchClick(
                                                buttonView, viewHolder.adapterPosition,
                                                bean.id,
                                                isChecked
                                            )
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            holder.switchAlarm?.isChecked = alarmStatus
            if (alarmStatus && content?.device?.isOnline == true) {
                holder.container.background =
                    ContextCompat.getDrawable(context, R.drawable.box_shadow_yellow)
                holder.tvRoom?.setTextColor(ContextCompat.getColor(context, R.color.yellow))
                holder.tvDeviceName?.setTextColor(ContextCompat.getColor(context, R.color.yellow))
            } else {
                holder.tvRoom?.setTextColor(ContextCompat.getColor(context, R.color.gray))
                holder.tvDeviceName?.setTextColor(ContextCompat.getColor(context, R.color.black))
                holder.container.background =
                    ContextCompat.getDrawable(context, R.drawable.box_shadow)
            }
            setOnline(holder.container, content?.device?.isOnline)
        }
    }

    fun updateViewPIR(
        viewHolder: RecyclerView.ViewHolder,
        content: DeviceContainer?
    ) {
        Timber.e("updateViewPIR : ${content?.device?.name}")
        viewHolder.itemView.let { holder ->
            holder.boxSwitch3?.visibility = View.VISIBLE
            holder.boxSwitch2?.visibility = View.INVISIBLE
            holder.boxAlarmSwitch?.visibility = View.GONE
            holder.boxDoorContact?.visibility = View.GONE
            holder.boxSwitch1?.visibility = View.INVISIBLE
            var status = false
            holder.ivSwitch3?.setImageResource(R.drawable.ic_motion_sensor)
            TuyaHomeSdk.getDataInstance().getSchema(content?.device?.devId)?.let { map ->
                if (content?.device?.categoryCode != "inf_qt") {
                    for (bean in map.values) {
                        Timber.e("${bean.schemaType} == ${bean.code}")
                        if (bean.type == DataTypeEnum.OBJ.type) {
                            if (bean.schemaType == BoolSchemaBean.type && bean.code?.startsWith(
                                    "pir",
                                    ignoreCase = true
                                ) == true
                            ) {
                                status = content?.device?.dps?.get(bean.id) as Boolean
                                Timber.e("PIR : $status")
                            } else if (bean.schemaType == EnumSchemaBean.type && bean.code?.startsWith(
                                    "presence_state",
                                    ignoreCase = true
                                ) == true || bean.code?.startsWith(
                                    "pir_state",
                                    ignoreCase = true
                                ) == true
                            ) {
                                status =
                                    content?.device?.dps?.get(bean.id) as String != "none"
                                Timber.e("presence_state : $status")
                            }
                        }
                    }
                }
            }
            holder.ivSwitch3?.setImageResource(
                if (status) R.drawable.ic_motion_sensor_active else R.drawable.ic_motion_sensor
            )

            if (status && content?.device?.isOnline == true) {
                holder.container.background =
                    ContextCompat.getDrawable(context, R.drawable.box_shadow_yellow)

                holder.tvRoom?.setTextColor(ContextCompat.getColor(context, R.color.yellow))
                holder.tvDeviceName?.setTextColor(ContextCompat.getColor(context, R.color.yellow))
            } else {
                holder.tvRoom?.setTextColor(ContextCompat.getColor(context, R.color.gray))
                holder.tvDeviceName?.setTextColor(ContextCompat.getColor(context, R.color.black))
                holder.container.background =
                    ContextCompat.getDrawable(context, R.drawable.box_shadow)
            }
            setOnline(holder.container, content?.device?.isOnline)
        }
    }

    fun updateSwitchView(
        viewHolder: RecyclerView.ViewHolder,
        content: DeviceContainer?
    ) {
        val entity = TuyaHomeSdk.getDataInstance().getDeviceBean(content?.device?.devId)

        viewHolder.itemView.let { holder ->
            //holder as AdapterMultiSwitchViewHolder
            holder.boxSwitch3?.visibility = View.INVISIBLE
            holder.boxSwitch2?.visibility = View.INVISIBLE
            holder.boxSwitch1?.visibility = View.INVISIBLE
            holder.boxDoorContact?.visibility = View.GONE
            holder.boxAlarmSwitch?.visibility = View.GONE
            var isEnableSwitch = false


            Timber.e("updateSwitchView : ${content?.device?.name} | isOnline ${entity?.isOnline == true}")
            if (entity?.isOnline == true) {
                content?.switchSchema?.asReversed()?.forEachIndexed { index, bean ->
                    val value = bean.switchEnable

                    if (!isEnableSwitch)
                        isEnableSwitch = value == true
                    val tag = (content.switchSchema?.size ?: 0) - (index + 1)
                    // Timber.e("TAG : $tag == ${content.switchSchema?.size} - ${(index+1)}")
                    //if()
                    when (index) {
                        0 -> {
                            holder.ivSwitch3?.tag = tag
                            switchEnable(
                                value, holder.ivSwitch3,
                                holder.boxSwitch3
                            )
                        }
                        1 -> {
                            holder.ivSwitch2?.tag = tag
                            switchEnable(
                                value, holder.ivSwitch2,
                                holder.boxSwitch2
                            )
                        }
                        2 -> {
                            holder.ivSwitch1?.tag = tag
                            switchEnable(
                                value, holder.ivSwitch1,
                                holder.boxSwitch1
                            )
                        }
                    }
                }
                if (isEnableSwitch) {
                    holder.container.background =
                        ContextCompat.getDrawable(context, R.drawable.box_shadow_yellow)
                    /*holder.container?.setElevationShadowColor(
                        ContextCompat.getColor(
                            context,
                            R.color.yellow
                        )
                    )*/
                    //holder.container?.invalidate()
                    holder.tvRoom?.setTextColor(ContextCompat.getColor(context, R.color.yellow))
                    holder.tvDeviceName?.setTextColor(
                        ContextCompat.getColor(
                            context,
                            R.color.yellow
                        )
                    )
                } else {
                    holder.tvRoom?.setTextColor(ContextCompat.getColor(context, R.color.gray))
                    holder.tvDeviceName?.setTextColor(
                        ContextCompat.getColor(
                            context,
                            R.color.black
                        )
                    )
                    holder.container.background =
                        ContextCompat.getDrawable(context, R.drawable.box_shadow)
                }
            } else {
                holder.tvRoom?.setTextColor(ContextCompat.getColor(context, R.color.gray))
                holder.tvDeviceName?.setTextColor(ContextCompat.getColor(context, R.color.black))
                holder.container.background =
                    ContextCompat.getDrawable(context, R.drawable.box_shadow)
            }

            if (content?.deviceType == "DOOR_CONTACT") {
                holder.boxDoorContact?.visibility = View.INVISIBLE
                holder.boxAlarmSwitch?.visibility = View.GONE
                holder.boxSwitch1?.visibility = View.GONE
                var doorStatus = false

                TuyaHomeSdk.getDataInstance().getSchema(content.device?.devId)?.let { map ->
                    if (content.device?.categoryCode != "inf_qt") {
                        for (bean in map.values) {
                            if (bean.type == DataTypeEnum.OBJ.type) {
                                if (bean.schemaType == BoolSchemaBean.type) {
                                    if (bean.code?.contains("doorcontact") == true) {
                                        doorStatus = content.device?.dps?.get(bean.id) as Boolean
                                        holder.boxDoorContact?.visibility =
                                            if (doorStatus) View.VISIBLE else View.INVISIBLE
                                    }
                                }
                            }
                        }
                    }
                }

                if (doorStatus && content.device?.isOnline == true) {
                    holder.container.background =
                        ContextCompat.getDrawable(context, R.drawable.box_shadow_yellow)
                }
            }
            setOnline(holder.container, entity?.isOnline)
        }
    }

    fun updateData(items: List<DeviceContainer>?) {
        //this.data = mutableListOf()
        //items?.let { it ->
        this.data = items?.toMutableList()
        if (this.data == null)
            this.data = mutableListOf()
        //}


        notifyDataSetChanged()
    }

    fun switchEnable(
        enable: Boolean,
        switchIcon: AppCompatImageView?,
        box: LinearLayoutCompat?
    ) {

        box?.visibility = View.VISIBLE

        switchIcon?.setImageResource(
            if (enable) {
                R.drawable.btn_power_on
            } else {
                R.drawable.btn_power_off
            }
        )
    }

    fun clear() {
        data?.clear()
        notifyDataSetChanged()
    }

    inner class AdapterMultiSwitchViewHolder(
        itemView: View,
        val adapterDevice: DeviceSwitchAdapter
    ) :
        RecyclerView.ViewHolder(itemView), View.OnTouchListener {

        var holdItem: DeviceContainer? = null
        internal var container: ConstraintLayout? = itemView.findViewById(R.id.container)
        //internal var container: ConstraintLayout? = itemView.findViewById(R.id.container)

        internal var tvDeviceName: AppCompatTextView? =
            itemView.findViewById(R.id.tvDeviceName)
        internal var ivDevice: AppCompatImageView? = itemView.findViewById(R.id.ivDevice)
        internal var tvRoom: AppCompatTextView? = itemView.findViewById(R.id.tvRoom)
        internal var ivInfo: AppCompatImageView? = itemView.findViewById(R.id.ivInfo)
        internal var ivSwitch1: AppCompatImageView? = itemView.findViewById(R.id.ivSwitch1)
        internal var ivSwitch2: AppCompatImageView? = itemView.findViewById(R.id.ivSwitch2)
        internal var ivSwitch3: AppCompatImageView? = itemView.findViewById(R.id.ivSwitch3)

        //internal var switchAlarm: AppCompatToggleButton? = itemView.findViewById(R.id.switchAlarm)
        init {
            //if (holdItem?.device?.isOnline != false)
            container?.setOnTouchListener(this)

            container?.setOnClickListener {
                mListener?.onRecyclerItemClick(
                    itemView,
                    layoutPosition,
                    holdItem
                )
            }
            ivInfo?.setOnClickListener {
                mListener?.onRecyclerInfo(
                    itemView,
                    layoutPosition,
                    holdItem
                )
            }
            ivSwitch1?.setOnClickListener { v ->
                if ((holdItem?.switchCount ?: 0) > 2) {
                    mListener?.onRecyclerSwitchClick(
                        itemView,
                        layoutPosition, holdItem?.switchSchema?.get(v.tag.toString().toInt()),
                        holdItem
                    )
                }
            }

            ivSwitch2?.setOnClickListener { v ->
                if ((holdItem?.switchCount ?: 0) > 1) {
                    mListener?.onRecyclerSwitchClick(
                        itemView,
                        layoutPosition, holdItem?.switchSchema?.get(v.tag.toString().toInt()),
                        holdItem
                    )
                } else {
                    if (holdItem?.deviceType == "SOS") {
                        mListener?.onRecyclerSOSModeChanged(
                            itemView,
                            layoutPosition, "116", v.tag.toString()
                        )
                    }
                }
            }

            ivSwitch3?.setOnClickListener { v ->
                if ((holdItem?.switchCount ?: 0) > 0) {
                    mListener?.onRecyclerSwitchClick(
                        itemView,
                        layoutPosition, holdItem?.switchSchema?.get(v.tag.toString().toInt()),
                        holdItem
                    )
                } else {
                    if (holdItem?.deviceType == "SOS") {
                        mListener?.onRecyclerSOSModeChanged(
                            itemView,
                            layoutPosition, "116", v.tag.toString()
                        )
                    }
                }
            }
        }

        override fun onTouch(v: View?, event: MotionEvent?): Boolean {
            if (holdItem?.device?.isOnline != true && holdItem?.device?.categoryCode?.startsWith("ble") == false) {
                when (event?.action) {
                    MotionEvent.ACTION_DOWN -> {
                        v?.alpha = 0.3f
                    }
                    MotionEvent.ACTION_UP
                    -> {
                        v?.alpha = 0.3f
                    }
                    MotionEvent.ACTION_CANCEL -> {
                        v?.alpha = 0.3f
                    }
                }
            } else {
                when (event?.action) {
                    MotionEvent.ACTION_DOWN -> {
                        v?.alpha = 0.5f
                    }
                    MotionEvent.ACTION_UP
                    -> {
                        v?.alpha = 1f
                    }
                    MotionEvent.ACTION_CANCEL -> {
                        v?.alpha = 1f
                    }
                }
            }
            return false
        }
    }

}