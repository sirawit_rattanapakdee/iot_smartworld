package ai.apdigital.tuya.ui.room

import ai.apdigital.tuya.R
import ai.apdigital.tuya.utils.drag.ItemTouchHelperAdapter
import ai.apdigital.tuya.utils.drag.ItemTouchHelperViewHolder
import ai.apdigital.tuya.utils.drag.OnStartDragListener
import android.content.Context
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.MotionEventCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.tuya.smart.android.device.bean.*

import com.tuya.smart.sdk.bean.DeviceBean
import timber.log.Timber
import java.util.*

class ReorderDeviceAdapter(
    val context: Context,
    val type: Int,
    private val dragStartListener: OnStartDragListener,
    private val reorderd: () -> Unit
) :
    RecyclerView.Adapter<ReorderDeviceAdapter.AdapterViewHolder>(), ItemTouchHelperAdapter {
    private var mListener: OnItemClickListener? = null
    private var data: MutableList<DeviceBean> = arrayListOf()

    interface OnItemClickListener {
        fun onRecyclerItemClick(view: View, position: Int, entity: DeviceBean?)
        fun onRecyclerItemLongClick(view: View, position: Int, entity: DeviceBean?)
        fun onSorted(data: MutableList<DeviceBean>?)
    }

    fun updateData(items: List<DeviceBean>?) {
        this.data = mutableListOf()
        items?.let { it ->
            this.data = it.toMutableList()
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapterViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context).inflate(
            R.layout.item_device_drag,
            parent,
            false
        )

        return AdapterViewHolder(layoutInflater, reorderd, this, dragStartListener)
    }


    fun setOnItemClickListener(onEntryClickListener: OnItemClickListener) {
        mListener = onEntryClickListener
    }

    override fun getItemCount(): Int = data.size

    override fun onItemMove(fromPosition: Int, toPosition: Int): Boolean {
        Collections.swap(data, fromPosition, toPosition)
        notifyItemMoved(fromPosition, toPosition)
        mListener?.onSorted(data)
        return true
    }

    override fun onItemDismiss(position: Int) {

    }

    inner class AdapterViewHolder(
        itemView: View,
        val reordered: () -> Unit,
        val adapterDevice: ReorderDeviceAdapter,
        private val dragStartListener: OnStartDragListener? = null
    ) :
        RecyclerView.ViewHolder(itemView),
        View.OnClickListener, View.OnLongClickListener, ItemTouchHelperViewHolder,
        View.OnTouchListener {

        var holdItem: DeviceBean? = null
        private var container: ConstraintLayout? = itemView.findViewById(R.id.container)
        internal var tvDeviceName: AppCompatTextView? = itemView.findViewById(R.id.tvDeviceName)
        internal var ivDevice: AppCompatImageView? = itemView.findViewById(R.id.ivDevice)
        internal var tvRoom: AppCompatTextView? = itemView.findViewById(R.id.tvRoom)
        private var ivDrag: AppCompatImageView? = itemView.findViewById(R.id.ivDrag)


        init {
            container?.setOnClickListener(this)
            container?.setOnLongClickListener(this)
            container?.setOnTouchListener(this)
            ivDrag?.setOnTouchListener { v, event ->
                if (MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_DOWN) {
                    dragStartListener?.onStartDrag(this@AdapterViewHolder)
                }
                false
            }
        }

        override fun onClick(v: View?) {
            mListener?.onRecyclerItemClick(itemView, layoutPosition, holdItem)
        }

        override fun onLongClick(v: View?): Boolean {
            mListener?.onRecyclerItemLongClick(itemView, layoutPosition, holdItem)
            return true
        }

        override fun onItemSelected() {

        }

        override fun onItemClear() {
            reordered()
        }

        override fun onTouch(v: View?, event: MotionEvent?): Boolean {
            when (event?.action) {
                MotionEvent.ACTION_DOWN -> {
                    v?.alpha = 0.5f
                }
                MotionEvent.ACTION_UP
                -> {
                    v?.alpha = 1f
                }
                MotionEvent.ACTION_CANCEL -> {
                    v?.alpha = 1f
                }
            }
            return false
        }
    }

    override fun onBindViewHolder(viewHolder: AdapterViewHolder, position: Int) {
        viewHolder.holdItem = data[position]
        viewHolder.holdItem?.let { entity ->
            viewHolder.ivDevice?.let { iv ->
                Glide
                    .with(context)
                    .load(entity.iconUrl)
                    .into(iv)
            }
            viewHolder.tvDeviceName?.text = entity.name
        }
    }

}