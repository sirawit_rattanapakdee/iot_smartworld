package ai.apdigital.tuya.ui.room

import ai.apdigital.tuya.HomeModel
import ai.apdigital.tuya.R
import ai.apdigital.tuya.base.BaseActivity
import ai.apdigital.tuya.base.ExtraName
import ai.apdigital.tuya.ui.viewmodel.CommandType
import ai.apdigital.tuya.utils.EventRoomUtils
import ai.apdigital.tuya.utils.EventUtils
import ai.apdigital.tuya.utils.PageUtils
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.recyclerview.widget.LinearLayoutManager
import com.tuya.smart.home.sdk.TuyaHomeSdk
import com.tuya.smart.home.sdk.bean.RoomBean
import kotlinx.android.synthetic.main.toolbar_back_with_control_panel.*
import kotlinx.android.synthetic.main.activity_list_with_button.*

class RoomActivity : BaseActivity() {
    private var roomAdapter: RoomAdapter? = null
    private var roomList: MutableList<RoomBean>? = null

    //private var homeBean: HomeBean? = null
    var homeId: Long = 0
    override fun getLayoutView() = R.layout.activity_list_with_button

    override fun bindView() {
    }

    override fun setupInstance() {
        setupToolbar(
            getString(R.string.title_room_management),
            showButtonBack = true,
            showRightMenu = false
        )
    }

    override fun setupView() {
        homeId = HomeModel.INSTANCE.getCurrentHome(this)
        shimmerViewContainer = shimmer_container
        contentContainer = content
        menuEdit?.visibility = View.GONE
    }

    override fun initialize(savedInstanceState: Bundle?) {
        EventUtils.sendEvent(EventUtils.TYPE_PAGE, PageUtils.TUYA_ROOM_MANAGE, "")
        recyclerList?.let {
            roomAdapter =
                RoomAdapter(
                    this, 0
                )
            val layoutManager = LinearLayoutManager(this)
            layoutManager.orientation = LinearLayoutManager.VERTICAL
            it.layoutManager = layoutManager
            it.adapter = roomAdapter
            it.setHasFixedSize(true)

            roomAdapter?.setOnItemClickListener(object : RoomAdapter.OnItemClickListener {
                override fun onRecyclerItemClick(
                    view: View,
                    position: Int,
                    entity: RoomBean?
                ) {
                    EventUtils.sendEvent(EventUtils.TYPE_EVENT, EventRoomUtils.TUYA_MANAGE_ROOM_EDIT_ROOM, "")
                    val intent = Intent(this@RoomActivity, RoomFormActivity::class.java)
                    intent.putExtra(ExtraName.COMMAND, CommandType.UPDATE)
                    intent.putExtra(ExtraName.ROOM_ID, entity?.roomId)
                    startForResult.launch(intent)
                }
            })
        }
        btnButtom.text = getString(R.string.action_create_room)
        btnButtom?.setOnClickListener {
            EventUtils.sendEvent(EventUtils.TYPE_EVENT, EventRoomUtils.TUYA_MANAGE_ROOM_CREATE, "")
            val intent = Intent(this, RoomFormActivity::class.java)
            intent.putExtra(ExtraName.COMMAND, CommandType.CREATE)
            intent.putExtra(ExtraName.ROOM_ID, 0L)
            startForResult.launch(intent)
        }
        val dataRoom = TuyaHomeSdk.getDataInstance().getHomeRoomList(homeId)
        if (dataRoom.size > 0)
            roomAdapter?.updateData(TuyaHomeSdk.getDataInstance().getHomeRoomList(homeId))
        else
            tvNotFoundData?.visibility = View.VISIBLE


        swipeContainer?.isRefreshing = false
        swipeContainer?.setOnRefreshListener {
            getHomeDetail(homeId)
        }
    }

    var isRoomUpdate = false

    val startForResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                isRoomUpdate = true
                getHomeDetail(homeId)
            }
        }

    private fun getHomeDetail(homeId: Long) {
        swipeContainer?.isRefreshing = false
        roomAdapter?.updateData(TuyaHomeSdk.getDataInstance().getHomeRoomList(homeId))
    }

    override fun onDestroy() {
        roomAdapter = null
        roomList = null
        contentContainer = null
        shimmerViewContainer = null
        super.onDestroy()
    }
}