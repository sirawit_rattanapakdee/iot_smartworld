package ai.apdigital.tuya.ui.room

import ai.apdigital.tuya.R
import android.content.Context
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.tuya.smart.home.sdk.bean.RoomBean


class RoomAdapter(val context: Context, val type: Int) :
    RecyclerView.Adapter<RoomAdapter.AdapterViewHolder>() {

    interface OnItemClickListener {
        fun onRecyclerItemClick(view: View, position: Int, entity: RoomBean?)
    }

    private var data: List<RoomBean> = mutableListOf()
    private var mListener: OnItemClickListener? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        val inflatedView = if (type == 1) {
            layoutInflater.inflate(
                R.layout.item_room,
                parent,
                false
            )
        } else {
            layoutInflater.inflate(
                R.layout.item_room,
                parent,
                false
            )
        }
        return AdapterViewHolder(inflatedView, this)
    }


    fun setOnItemClickListener(onEntryClickListener: OnItemClickListener) {
        mListener = onEntryClickListener
    }

    override fun getItemCount(): Int = data.size
    override fun onBindViewHolder(
        viewHolder: AdapterViewHolder,
        position: Int
    ) {

        viewHolder.holdItem = data[position]
        viewHolder.holdItem?.let { entity ->

            viewHolder.tvTitle?.text = entity.name
        }

    }

    fun updateData(items: List<RoomBean>?) {
        this.data = mutableListOf()
        items?.let { it ->
            this.data = it.toMutableList()
        }
        notifyDataSetChanged()
    }

    inner class AdapterViewHolder(
        itemView: View,
        val adapterHome: RoomAdapter
    ) :
        RecyclerView.ViewHolder(itemView),
        View.OnClickListener, View.OnTouchListener  {

        var holdItem: RoomBean? = null
        private var container: RelativeLayout? = itemView.findViewById(R.id.container)
        internal var tvTitle: AppCompatTextView? = itemView.findViewById(R.id.tvTitle)
        /*internal var tvDesc: AppCompatTextView? = itemView.findViewById(R.id.tvDesc)
        internal var ivCover: AppCompatImageView? = itemView.findViewById(R.id.ivCover)
        internal var ivEdit: AppCompatImageView? = itemView.findViewById(R.id.ivEdit)*/

        init {
            container?.setOnClickListener(this)
            container?.setOnTouchListener(this)
        }

        override fun onClick(v: View?) {
            mListener?.onRecyclerItemClick(itemView, layoutPosition, holdItem)
        }

        override fun onTouch(v: View?, event: MotionEvent?): Boolean {
            when (event?.action) {
                MotionEvent.ACTION_DOWN -> {
                    v?.alpha = 0.5f
                }
                MotionEvent.ACTION_UP
                -> {
                    v?.alpha = 1f
                }
                MotionEvent.ACTION_CANCEL -> {
                    v?.alpha = 1f
                }
            }
            return false
        }
    }
}