package ai.apdigital.tuya.ui.room

import ai.apdigital.tuya.HomeModel
import ai.apdigital.tuya.R
import ai.apdigital.tuya.base.BaseActivity
import ai.apdigital.tuya.base.Constants
import ai.apdigital.tuya.base.ExtraName
import ai.apdigital.tuya.networking.Resource
import ai.apdigital.tuya.networking.Status
import ai.apdigital.tuya.ui.control.DeviceDetailActivity
import ai.apdigital.tuya.ui.device.DeviceAdapter
import ai.apdigital.tuya.ui.device.DeviceListActivity
import ai.apdigital.tuya.ui.viewmodel.CommandType
import ai.apdigital.tuya.ui.viewmodel.TuyaViewModel
import ai.apdigital.tuya.utils.EventRoomUtils
import ai.apdigital.tuya.utils.EventUtils
import ai.apdigital.tuya.utils.PageUtils
import ai.apdigital.tuya.utils.alerts.AlertChooseDeviceDialogFragment
import ai.apdigital.tuya.utils.alerts.AlertUtil
import ai.apdigital.tuya.utils.alerts.EditRoomDialogFragment
import ai.apdigital.tuya.utils.drag.OnStartDragListener
import ai.apdigital.tuya.utils.drag.ReorderHelperCallback
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.view.MotionEvent
import android.view.View

import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.tuya.smart.api.router.UrlBuilder
import com.tuya.smart.api.router.UrlRouter
import com.tuya.smart.home.sdk.TuyaHomeSdk
import com.tuya.smart.home.sdk.bean.DeviceAndGroupInRoomBean
import com.tuya.smart.home.sdk.bean.RoomBean
import com.tuya.smart.interior.enums.BizParentTypeEnum
import com.tuya.smart.sdk.bean.DeviceBean
import kotlinx.android.synthetic.main.activity_room_create.*
import kotlinx.android.synthetic.main.toolbar_back.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber


class RoomFormActivity : BaseActivity(), OnStartDragListener, View.OnTouchListener {
    private val tuyaVM: TuyaViewModel? by viewModel()
    private var deviceAdapter: DeviceAdapter? = null
    private var commandType: CommandType? = null
    private var roomBean: RoomBean? = null
    private var homeId: Long = 0L
    private var roomId: Long = 0L
    private var mItemTouchHelper: ItemTouchHelper? = null
    private var sortDeviceChanged: Boolean = false
    private var roomNameChanged: Boolean = false
    private var deviceRoomBeanSort: MutableList<DeviceAndGroupInRoomBean>? = null

    override fun getLayoutView() = R.layout.activity_room_create
    override fun bindView() {
        setupToolbar(
            getString(R.string.title_room_create),
            showButtonBack = true,
            showRightMenu = false
        )
    }

    override fun setupInstance() {
        homeId = HomeModel.INSTANCE.getCurrentHome(this)

        intent.getLongExtra(ExtraName.ROOM_ID, 0L).let {
            roomId = it
            if (it != 0L)
                roomBean = TuyaHomeSdk.getDataInstance().getRoomBean(it)
        }

        commandType =
            intent.getSerializableExtra(ExtraName.COMMAND) as CommandType? ?: CommandType.CREATE
    }

    override fun setupView() {
        setUpViewAdapter()

        enableDeviceList(roomBean?.deviceList?.isNotEmpty() == true)

        if (commandType == CommandType.UPDATE) updateRoom() else createRoom()

        ivAdd?.setOnClickListener {
            openAddDeviceToRoom()
        }

        nameOfRoomBox?.setOnClickListener {
            supportFragmentManager.let {
                val notices = EditRoomDialogFragment(
                    getString(R.string.room_name),
                    tvRoomName.text.toString(),
                    object : EditRoomDialogFragment.OnEditNameListener {
                        override fun onSaveClick(name: String) {
                            if (commandType == CommandType.CREATE) {
                                EventUtils.sendEvent(
                                    EventUtils.TYPE_EVENT,
                                    EventRoomUtils.TUYA_CREATE_ROOM_SET_NAME,
                                    ""
                                )
                                btnSave?.isEnabled = true
                            }
                            roomNameChanged = true
                            enableSaveButton(true)
                            tvRoomName?.text = name
                            EventUtils.sendEvent(
                                EventUtils.TYPE_EVENT,
                                EventRoomUtils.TUYA_MANAGE_ROOM_EDIT_ROOM_ADD_DEVICE,
                                ""
                            )
                        }
                    })
                notices.isCancelable = false
                notices.show(
                    it,
                    "showAlertNotices"
                )
            }
        }
        addDeviceBox?.setOnClickListener {
            openAddDeviceToRoom()
        }
        nameOfRoomBox?.setOnTouchListener(this)
        btnSave?.setOnTouchListener(this)
        addDeviceBox?.setOnTouchListener(this)

        tuyaVM?.removeDeviceToRoom()
            ?.observe(this@RoomFormActivity, deviceDeleteRes)
        tuyaVM?.roomAddNew()
            ?.observe(this@RoomFormActivity, addRoomRes)
    }

    private fun setUpViewAdapter() {
        recyclerListDevice?.let {
            deviceAdapter =
                DeviceAdapter(
                    this, 1
                )
            val layoutManager = LinearLayoutManager(this)
            layoutManager.orientation = LinearLayoutManager.VERTICAL
            it.layoutManager = layoutManager
            it.adapter = deviceAdapter
            it.setHasFixedSize(true)

            deviceAdapter?.setOnItemClickListener(object :
                DeviceAdapter.OnItemClickListener {

                override fun onRecyclerItemClick(
                    view: View,
                    position: Int,
                    entity: DeviceBean?
                ) {
                    /*val intent = Intent(this@RoomFormActivity, DeviceDetailActivity::class.java)
                    intent.putExtra(Constants.INTENT_DEV_ID, entity?.devId)
                    startForDeviceDetailMainResult.launch(intent)*/
                    /*val urlBuilders = UrlBuilder(this@RoomFormActivity, "panelMore")
                    val bundle = Bundle()
                    bundle.putString("extra_panel_dev_id", entity?.devId)
                    bundle.putString("extra_panel_name", entity?.name)
                    bundle.putLong("extra_panel_group_id", -1)
                    urlBuilders.putExtras(bundle)
                    UrlRouter.execute(urlBuilders)*/

                }

                override fun onRecyclerItemLongClick(
                    view: View,
                    position: Int,
                    entity: DeviceBean?
                ) {
                    roomBean?.roomId?.let { roomId ->
                        entity?.devId?.let { devId ->
                            showAlertConfirmWithActionClick(
                                getString(R.string.action_do_you_want_to_remove_your_device),
                                null,
                                object : AlertUtil.OnSuccessListener {
                                    override fun onSuccess() {
                                        tuyaVM?.removeDeviceToRoom(roomId, devId)
                                    }
                                }, R.color.red
                            )

                        }
                    }
                }/*

                override fun onSorted(data: MutableList<DeviceBean>?) {
                    deviceRoomBeanSort = mutableListOf()
                    data?.forEach { dev ->
                        val devMove = DeviceAndGroupInRoomBean()
                        devMove.id = dev.devId
                        devMove.type = BizParentTypeEnum.DEVICE.getType()
                        deviceRoomBeanSort?.add(devMove)
                    }
                    enableSaveButton(true)
                    sortDeviceChanged = true
                }*/
            })
            /*val callback: ItemTouchHelper.Callback = ReorderHelperCallback(deviceAdapter!!)
            mItemTouchHelper = ItemTouchHelper(callback)
            mItemTouchHelper?.attachToRecyclerView(it)*/
        }
    }

    private fun updateRoom() {
        contentDevice?.visibility = View.VISIBLE
        val allSceneString = SpannableString(tvDelete?.text)
        allSceneString.setSpan(UnderlineSpan(), 0, allSceneString.length, 0)
        tvDelete?.text = allSceneString

        if (roomBean != null) {
            roomBean?.deviceList?.sortBy { it.displayOrder }
            deviceAdapter?.updateData(roomBean?.deviceList)
            tvRoomName?.text = roomBean?.name
        }
        tvLongClickDelete?.visibility = View.VISIBLE
        btnSave?.visibility = View.GONE
        tvDelete?.visibility = View.VISIBLE
        tvDelete?.setOnClickListener {
            roomBean?.roomId?.let { roomId ->
                tuyaVM?.roomRemove()?.observe(this, deleteRoomRes)
                showAlertConfirmWithActionClick(
                    getString(R.string.alert_room_delete),
                    null,
                    object : AlertUtil.OnSuccessListener {
                        override fun onSuccess() {
                            tuyaVM?.roomRemove(homeId, roomId)
                        }
                    }, R.color.red
                )
            }
        }

        tvSave?.visibility = View.VISIBLE
        enableSaveButton(false)
        tvSave?.setOnClickListener {
            roomBean?.roomId?.let {
                if (tvRoomName?.text?.length ?: 0 > 0) {
                    showAlertConfirmWithActionClick(
                        getString(R.string.alert_do_you_want_save_change),
                        null,
                        object : AlertUtil.OnSuccessListener {
                            override fun onSuccess() {
                                if (sortDeviceChanged) {
                                    deviceRoomBeanSort?.let { devices ->
                                        tuyaVM?.roomSortDeviceList()
                                            ?.observe(this@RoomFormActivity, updateSortDevice)
                                        tuyaVM?.sortDeviceInRoom(it, devices)
                                    }
                                }
                                if (roomNameChanged) {
                                    tuyaVM?.roomEit()?.observe(this@RoomFormActivity, updateRoomRes)
                                    tuyaVM?.editRoom(it, tvRoomName?.text.toString())
                                }
                            }
                        }, R.color.black
                    )
                }
            }
        }
    }

    private fun enableSaveButton(enable: Boolean) {
        tvSave?.isEnabled = enable
    }


    private fun createRoom() {
        contentDevice?.visibility = View.GONE
        btnSave?.visibility = View.VISIBLE
        tvDelete?.visibility = View.GONE
        btnSave?.isEnabled = false
        btnSave?.setOnClickListener {

            if (tvRoomName?.text?.length ?: 0 > 0) {
                EventUtils.sendEvent(
                    EventUtils.TYPE_EVENT,
                    EventRoomUtils.TUYA_CREATE_ROOM_SAVE_ROOM,
                    ""
                )
                showAlertConfirmWithActionClick(
                    getString(R.string.alert_create_room), null,
                    object : AlertUtil.OnSuccessListener {
                        override fun onSuccess() {
                            tuyaVM?.addRoom(homeId, tvRoomName?.text.toString())
                        }
                    }, R.color.black
                )
            }
        }
    }

    private fun enableDeviceList(isEnable: Boolean) {
        tvLongClickDelete?.visibility = if (isEnable) View.VISIBLE else View.GONE
        recyclerListDevice?.visibility = if (isEnable) View.VISIBLE else View.GONE
        addDeviceBox?.visibility = if (isEnable) View.GONE else View.VISIBLE
    }


    private fun openAddDeviceToRoom() {
        val intent = Intent(this, DeviceListActivity::class.java)
        intent.putExtra(ExtraName.ROOM_ID, roomId)
        startForResult.launch(intent)
    }

    override fun initialize(savedInstanceState: Bundle?) {
        EventUtils.sendEvent(
            EventUtils.TYPE_PAGE,
            if (commandType == CommandType.UPDATE) PageUtils.TUYA_ROOM_MANAGE_EDIT_ROOM else PageUtils.TUYA_CREATE_ROOM,
            ""
        )

    }

    val startForResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                EventUtils.sendEvent(
                    EventUtils.TYPE_EVENT,
                    if (commandType == CommandType.UPDATE) EventRoomUtils.TUYA_MANAGE_ROOM_EDIT_ROOM_ADD_DEVICE else EventRoomUtils.TUYA_CREATE_ROOM_POPUP_ADD_DEVICE,
                    ""
                )
                updateDevice()
            }
        }

    fun updateDevice() {
        roomBean = TuyaHomeSdk.getDataInstance().getRoomBean(roomId)
        enableDeviceList(roomBean?.deviceList?.isNotEmpty() == true)
        roomBean?.deviceList?.sortBy { it.displayOrder }
        deviceAdapter?.updateData(roomBean?.deviceList)
    }

    private val addRoomRes = Observer<Resource<RoomBean>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                showToastSuccessAlertNotices(
                    getString(R.string.msg_successfully),
                    object : AlertUtil.OnSuccessListener {
                        override fun onSuccess() {
                            roomNameChanged = false
                            isUpdate = true
                            roomBean = resource.data
                            roomId = resource.data?.roomId ?: 0L
                            Timber.e("Room ID $roomId")
                            updateRoom()
                            EventUtils.sendEvent(
                                EventUtils.TYPE_PAGE,
                                EventRoomUtils.TUYA_CREATE_ROOM_POPUP_ADD_DEVICE,
                                ""
                            )
                            supportFragmentManager.let {
                                val notices = AlertChooseDeviceDialogFragment(
                                    object :
                                        AlertChooseDeviceDialogFragment.OnChooseDeviceRoomListener {
                                        override fun onAddDeviceClick() {

                                            tvLongClickDelete?.visibility = View.VISIBLE
                                            openAddDeviceToRoom()
                                        }

                                        override fun onNotNowClick() {
                                            setResult(Activity.RESULT_OK, Intent())
                                            finish()
                                        }
                                    })
                                notices.isCancelable = false
                                notices.show(
                                    it,
                                    "showAlertNotices"
                                )
                            }
                        }
                    })
            }

            Status.ERROR -> showToastErrorAlertNotices(
                resource.message?.message ?: getString(R.string.msg_failure), null
            )
            Status.LOADING -> showProgressView()

        }
    }


    private val updateRoomRes = Observer<Resource<Long>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                enableSaveButton(false)
                roomNameChanged = false
                isUpdate = true
                showToastSuccessAlertNotices(getString(R.string.msg_successfully), null)
            }

            Status.ERROR -> {
                roomNameChanged = false
                showToastErrorAlertNotices(
                    resource.message?.message ?: getString(R.string.msg_failure), null
                )
            }
            Status.LOADING -> showProgressView()

        }
    }

    private val deleteRoomRes = Observer<Resource<Long>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                EventUtils.sendEvent(
                    EventUtils.TYPE_EVENT,
                    EventRoomUtils.TUYA_MANAGE_ROOM_EDIT_ROOM_DELETE,
                    ""
                )
                showToastSuccessAlertNotices(
                    getString(R.string.msg_successfully),
                    object : AlertUtil.OnSuccessListener {
                        override fun onSuccess() {
                            setResult(Activity.RESULT_OK, Intent())
                            finish()
                        }
                    })
            }

            Status.ERROR -> showToastErrorAlertNotices(
                resource.message?.message ?: getString(R.string.msg_failure), null
            )
            Status.LOADING -> showProgressView()

        }
    }

    private val deviceDeleteRes = Observer<Resource<Long>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                hideProgressView()
                isUpdate = true
                updateDevice()
            }

            Status.ERROR -> showToastErrorAlertNotices(
                resource.message?.message ?: getString(R.string.msg_failure), null
            )
            Status.LOADING -> showProgressView()

        }
    }

    private val updateSortDevice = Observer<Resource<Long>> { resource ->

        when (resource?.status) {
            Status.SUCCESS -> {
                sortDeviceChanged = false
                isUpdate = true
                enableSaveButton(false)
                showToastSuccessAlertNotices(getString(R.string.msg_successfully), null)
            }

            Status.ERROR -> {
                sortDeviceChanged = false
                showToastErrorAlertNotices(
                    resource.message?.message ?: getString(R.string.msg_failure), null
                )
            }
            Status.LOADING -> showProgressView()
        }
    }

    override fun onStartDrag(viewHolder: RecyclerView.ViewHolder?) {
        viewHolder?.let {
            mItemTouchHelper?.startDrag(it)
        }
    }

    override fun onBackPressed() {
        if (sortDeviceChanged || roomNameChanged) {
            showAlertConfirmWithActionClick(
                getString(R.string.alert_leave_not_save),
                null,
                object : AlertUtil.OnSuccessListener {
                    override fun onSuccess() {
                        sortDeviceChanged = false
                        roomNameChanged = false
                        onBackMain()
                    }
                }, R.color.red
            )
        } else {
            onBackMain()
        }
    }

    override fun onDestroy() {
        roomBean = null
        deviceRoomBeanSort = null
        deviceAdapter = null
        super.onDestroy()
    }

    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        when (event?.action) {
            MotionEvent.ACTION_DOWN -> {
                v?.alpha = 0.5f
            }
            MotionEvent.ACTION_UP
            -> {
                v?.alpha = 1f
            }
            MotionEvent.ACTION_CANCEL -> {
                v?.alpha = 1f
            }
        }
        return false
    }


}