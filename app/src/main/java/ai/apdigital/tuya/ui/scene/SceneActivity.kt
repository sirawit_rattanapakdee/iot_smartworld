package ai.apdigital.tuya.ui.scene

import ai.apdigital.tuya.HomeModel
import ai.apdigital.tuya.R
import ai.apdigital.tuya.base.BaseActivity
import ai.apdigital.tuya.networking.Resource
import ai.apdigital.tuya.networking.Status
import ai.apdigital.tuya.ui.viewmodel.TuyaViewModel
import ai.apdigital.tuya.utils.EventHomeUtils
import ai.apdigital.tuya.utils.EventSceneUtils
import ai.apdigital.tuya.utils.EventUtils
import ai.apdigital.tuya.utils.PageUtils
import ai.apdigital.tuya.utils.alerts.AlertUtil
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.tuya.smart.api.MicroContext


import com.tuya.smart.home.sdk.TuyaHomeSdk
import com.tuya.smart.home.sdk.bean.scene.SceneBean
import com.tuya.smart.scene.business.api.ITuyaSceneBusinessService
import com.tuya.smart.sdk.api.ISmartUpdateListener
import kotlinx.android.synthetic.main.activity_list_with_button.*
import kotlinx.android.synthetic.main.toolbar_back_with_control_panel.*

import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber


class SceneActivity : BaseActivity() {
    private val tuyaVM: TuyaViewModel? by viewModel()
    var homeId: Long = 0
    private var sceneAdapter: SceneAdapter? = null
    private var sceneList: List<SceneBean>? = null

    override fun getLayoutView() = R.layout.activity_list_with_button

    override fun bindView() {
        menuEdit?.visibility = View.GONE
    }

    override fun setupInstance() {
        EventUtils.sendEvent(EventUtils.TYPE_PAGE, PageUtils.TUYA_SCENE, "")
    }

    override fun setupView() {
        homeId = HomeModel.INSTANCE.getCurrentHome(this)
        /*intent.getStringExtra(ExtraName.SCENE)?.let {
            sceneList = Gson().fromJson<List<SceneBean>>(
                it,
                object : TypeToken<List<SceneBean>>() {}.type
            )
        }
        intent.getLongExtra(ExtraName.HOME_ID, 0).let {
            homeId = it
        }*/

        //TuyaHomeSdk.getDataInstance().scec(homeId)
        //sceneList = TuyaHomeSdk.getSceneManagerInstance().sceneCaches
        setupToolbar(
            getString(R.string.title_scene_and_automation),
            showButtonBack = true,
            showRightMenu = false
        )
        btnButtom.text = getString(R.string.action_scene_amp_automation)

        shimmerViewContainer = shimmer_container
        contentContainer = content
        /*ivAdd?.setOnClickListener { openAddDevice() }
        ivNotice?.setOnClickListener {
            UrlRouter.execute(UrlBuilder(this, "messageCenter"))
        }*/
        swipeContainer?.isRefreshing = false
        swipeContainer?.setOnRefreshListener { // Your code to refresh the list here.
            getScene(homeId)
        }
        btnButtom?.setOnTouchListener { v, event ->
            when (event?.action) {
                MotionEvent.ACTION_DOWN -> {
                    v?.alpha = 0.5f
                }
                MotionEvent.ACTION_UP
                -> {
                    v?.alpha = 1f
                }
                MotionEvent.ACTION_CANCEL -> {
                    v?.alpha = 1f
                }
            }
            false
        }
        // btnButtom?.visibility = View.GONE
    }

    //var service: AbsBizBundleFamilyService? = null
    override fun initialize(savedInstanceState: Bundle?) {
        tuyaVM?.getSceneList()?.observe(this, sceneRes)
        tuyaVM?.deleteSceneListener()?.observe(this, deleteResp)
        tuyaVM?.setEnableScene()?.observe(this, sceneEnableRes)

        shimmerViewContainer = shimmer_container
        contentContainer = content

        recyclerList?.let { it ->
            sceneAdapter =
                SceneAdapter(
                    this, 1
                )
            val layoutManager = LinearLayoutManager(this)
            layoutManager.orientation = LinearLayoutManager.VERTICAL
            it.layoutManager = layoutManager
            it.adapter = sceneAdapter
            it.setHasFixedSize(true)

            sceneAdapter?.setOnItemClickListener(object : SceneAdapter.OnItemClickListener {

                override fun onRecyclerItemClick(
                    view: View,
                    position: Int,
                    entity: SceneBean?
                ) {
                    entity?.let {
                        if (it.conditions == null) {
                            EventUtils.sendEvent(
                                EventUtils.TYPE_EVENT,
                                EventSceneUtils.TUYA_SHOW_ALL_SCENE_RUN,
                                it.name
                            )
                            tuyaVM?.tapToRubScene(it)
                        }
                    }
                }

                override fun onDeleteItemClick(view: View, position: Int, entity: SceneBean?) {
                    /*showAlertConfirmWithActionClick(
                        "Do you want to delete Scene?",
                        "Scene ".plus(entity?.name),
                        object : AlertUtil.OnSuccessListener {
                            override fun onSuccess() {
                                tuyaVM?.deleteScene(entity?.id)
                            }
                        }, R.color.red
                    )*/
                    EventUtils.sendEvent(
                        EventUtils.TYPE_EVENT,
                        EventSceneUtils.TUYA_SHOW_ALL_EDIT,
                        ""
                    )
                    val iTuyaSceneBusinessService: ITuyaSceneBusinessService =
                        MicroContext.findServiceByInterface(
                            ITuyaSceneBusinessService::class.java.name
                        )
                    iTuyaSceneBusinessService.editScene(
                        this@SceneActivity,
                        homeId, entity,
                        12121
                    )
                }

                override fun onEnableItemClick(
                    view: View,
                    position: Int,
                    entity: SceneBean?,
                    isChecked: Boolean
                ) {
                    entity?.let {
                        EventUtils.sendEvent(
                            EventUtils.TYPE_EVENT,
                            EventSceneUtils.TUYA_SHOW_ALL_AUTOMATIC_TOGGLE,
                            it.name
                        )
                        tuyaVM?.enableScene(isChecked, it)
                    }
                }
            })
        }

        if (sceneList != null) {
            sceneAdapter?.updateData(sceneList)
        } else {
            getScene(homeId)
        }

        btnButtom?.setOnClickListener {
            EventUtils.sendEvent(
                EventUtils.TYPE_EVENT,
                EventSceneUtils.TUYA_SHOW_ALL_CREATE,
                ""
            )
            val iTuyaSceneBusinessService: ITuyaSceneBusinessService =
                MicroContext.findServiceByInterface(
                    ITuyaSceneBusinessService::class.java.name
                )
            iTuyaSceneBusinessService.addScene(
                this,
                homeId,
                12120
            )
        }
    }

    override fun onResume() {
        super.onResume()
        TuyaHomeSdk.getSceneManagerInstance().registerSmartUpdateListener(mSmartUpdateListener)
    }

    private var mSmartUpdateListener = object : ISmartUpdateListener {
        override fun onSmartUpdateListener() {
            //getScene(homeId)
            //haveSenceUpdateMain = true
        }

        override fun onCollectionsUpdateListener() {}
    }

    override fun onPause() {
        super.onPause()
        TuyaHomeSdk.getSceneManagerInstance().unRegisterSmartUpdateListener(mSmartUpdateListener)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Timber.e("requestCode $requestCode _ resultCode $resultCode")

        if (requestCode == 12121 || requestCode == 12120) {

            isUpdate = true
            data?.let {
                getScene(homeId)
            }
        }
    }

    private fun getScene(homeId: Long) {
        Timber.e("getScene: $homeId")
        tuyaVM?.getScene(homeId)
    }

    private val deleteResp = Observer<Resource<Boolean>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                hideProgressView()
                resource.data?.let {
                    showToastSuccessAlertNotices(
                        getString(R.string.action_delete).plus(" ").plus(
                            getString(R.string.msg_successfully)
                        ), null
                    )
                    getScene(homeId)
                    isUpdate = true
                }
            }

            Status.ERROR -> {
                showToastErrorAlertNotices(
                    resource.message?.message ?: getString(R.string.msg_failure), null
                )
            }
            Status.LOADING -> {
                showProgressView()
            }
        }
    }

    private val sceneEnableRes = Observer<Resource<SceneBean>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                resource.data?.let {
                    val str =
                        if (it.isEnabled) getString(R.string.enabled) else getString(R.string.disabled)
                    showToastSuccessAlertNotices(
                        str.plus(" ").plus(
                            getString(R.string.msg_successfully)
                        ), null
                    )
                }
            }

            Status.ERROR -> {
                showToastErrorAlertNotices(
                    resource.message?.message ?: getString(R.string.msg_failure), null
                )
            }
            Status.LOADING -> {
                // showProgressView()
            }
        }
    }

    private val sceneRes = Observer<Resource<List<SceneBean>>> { resource ->
        when (resource?.status) {
            Status.SUCCESS -> {
                swipeContainer?.isRefreshing = false
                hideShimmerLoading()
                /*showToastSuccessAlertNotices(getString(R.string.msg_successfully), object : AlertUtil.OnClickListener {
                    override fun onOKClick() {*/
                resource.data?.let { data ->
                    if (data.isNotEmpty()) {
                        sceneList = data
                        sceneAdapter?.updateData(sceneList)
                    }
                }/*
                    }
                })*/
            }

            Status.ERROR -> {
                swipeContainer?.isRefreshing = false
                hideShimmerLoading()
                showToastErrorAlertNotices(
                    resource.message?.message ?: getString(R.string.msg_failure), null
                )
            }
            Status.LOADING -> {
                showShimmerLoading()
            }
        }
    }
/*
    private fun openAddDevice() {
        TuyaDeviceActivatorManager.startDeviceActiveAction(this, homeId)
        TuyaDeviceActivatorManager.setListener(
            object : ITuyaDeviceActiveListener {
                override fun onDevicesAdd(list: List<String?>?) {
                    Timber.e("onDevicesAdd")
                    //haveSenceUpdateMain = true
                }

                override fun onRoomDataUpdate() {
                    Timber.e("onRoomDataUpdate")
                }

                override fun onOpenDevicePanel(s: String?) {
                    Timber.e("onOpenDevicePanel")
                }

                override fun onExitConfigBiz() {
                    Timber.e("onExitConfigBiz")
                }
            })
    }*/

    override fun onDestroy() {
        super.onDestroy()
        sceneAdapter = null
        sceneList = null

        contentContainer = null
        shimmerViewContainer = null

    }
}