package ai.apdigital.tuya.ui.scene

import ai.apdigital.tuya.R
import android.content.Context
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.AppCompatToggleButton
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.suke.widget.SwitchButton
import com.tuya.smart.home.sdk.bean.scene.SceneBean
import timber.log.Timber


class SceneAdapter(val context: Context, val type: Int) :
    RecyclerView.Adapter<SceneAdapter.AdapterViewHolder>() {

    interface OnItemClickListener {
        fun onRecyclerItemClick(view: View, position: Int, entity: SceneBean?)
        fun onDeleteItemClick(view: View, position: Int, entity: SceneBean?)
        fun onEnableItemClick(view: View, position: Int, entity: SceneBean?, isChecked: Boolean)
    }

    private var data: List<SceneBean> = mutableListOf()
    private var mListener: OnItemClickListener? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        val inflatedView = if (type == 1) {
            layoutInflater.inflate(
                R.layout.item_scene,
                parent,
                false
            )
        } else {
            layoutInflater.inflate(
                R.layout.item_scene_main,
                parent,
                false
            )
        }
        return AdapterViewHolder(inflatedView, this)
    }


    fun setOnItemClickListener(onEntryClickListener: OnItemClickListener) {
        mListener = onEntryClickListener
    }

    override fun getItemCount(): Int = data.size
    override fun onBindViewHolder(
        viewHolder: AdapterViewHolder,
        position: Int
    ) {

        viewHolder.holdItem = data[position]
        viewHolder.holdItem?.let { entity ->
            if (entity.background == null || entity.background.isEmpty()) {
                viewHolder.cardItem?.setCardBackgroundColor(Color.parseColor("#".plus(entity.displayColor)))
            } else {
                viewHolder.ivCover?.let { iv ->
                    Glide
                        .with(context)
                        .load(entity.background)
                        .centerCrop()
                        .into(iv)
                }
            }
            viewHolder.tvTitle?.text = entity.name
            if (entity.conditions == null) {
                viewHolder.tvDesc?.text = context.getString(R.string.tap_to_run)
                viewHolder.tvDesc?.visibility = View.VISIBLE
                viewHolder.switchEnable?.visibility = View.INVISIBLE
                viewHolder.ivCover?.alpha = 1f
            } else {
                if (entity.isEnabled) {
                    viewHolder.opacity?.setBackgroundColor(
                        ContextCompat.getColor(
                            context,
                            R.color.black_alpha30
                        )
                    )
                    viewHolder.ivCover?.alpha = 1f
                    viewHolder.tvDesc?.text = context.getString(R.string.enabled)
                } else {
                    viewHolder.opacity?.setBackgroundColor(
                        ContextCompat.getColor(
                            context,
                            R.color.black_alpha20
                        )
                    )
                    viewHolder.ivCover?.alpha = 0.4f
                    viewHolder.tvDesc?.text = context.getString(
                        R.string.disabled
                    )
                }

                viewHolder.switchEnable?.isChecked = entity.isEnabled
                viewHolder.switchEnable?.visibility = View.VISIBLE
                viewHolder.switchEnable?.setOnCheckedChangeListener { buttonView, isChecked ->
                    if (buttonView.isPressed) {
                        viewHolder.holdItem?.isEnabled = isChecked
                        mListener?.onEnableItemClick(
                            viewHolder.itemView,
                            position,
                            viewHolder.holdItem, isChecked
                        )

                        if (isChecked) {
                            viewHolder.opacity?.setBackgroundColor(
                                ContextCompat.getColor(
                                    context,
                                    R.color.black_alpha30
                                )
                            )
                            viewHolder.ivCover?.alpha = 1f
                            viewHolder.tvDesc?.text = context.getString(R.string.enabled)
                        } else {
                            viewHolder.opacity?.setBackgroundColor(
                                ContextCompat.getColor(
                                    context,
                                    R.color.black_alpha20
                                )
                            )
                            viewHolder.ivCover?.alpha = 0.4f
                            viewHolder.tvDesc?.text = context.getString(
                                R.string.disabled
                            )
                        }
                    }
                }
            }
            if (type == 1)
                viewHolder.ivDelete?.setOnClickListener {
                    mListener?.onDeleteItemClick(
                        viewHolder.itemView,
                        position,
                        viewHolder.holdItem
                    )
                }
        }
    }

    fun updateData(items: List<SceneBean>?) {
        this.data = mutableListOf()
        items?.let { it ->
            this.data = it.toMutableList()
        }
        notifyDataSetChanged()
    }

    inner class AdapterViewHolder(
        itemView: View,
        val adapterHome: SceneAdapter
    ) :
        RecyclerView.ViewHolder(itemView),
        View.OnClickListener, View.OnTouchListener {

        var holdItem: SceneBean? = null
        private var container: RelativeLayout? = itemView.findViewById(R.id.container)
        internal var tvTitle: AppCompatTextView? = itemView.findViewById(R.id.tvTitle)
        internal var tvDesc: AppCompatTextView? = itemView.findViewById(R.id.tvDesc)
        internal var ivCover: AppCompatImageView? = itemView.findViewById(R.id.ivCover)
        internal var cardItem: CardView? = itemView.findViewById(R.id.cardItem)

        internal var ivDelete: AppCompatImageView? = itemView.findViewById(R.id.ivDelete)
        internal var switchEnable: AppCompatToggleButton? = itemView.findViewById(R.id.switchEnable)
        internal var opacity: View? = itemView.findViewById(R.id.opacity)

        init {
            container?.setOnClickListener(this)
            container?.setOnTouchListener(this)
        }

        override fun onClick(v: View?) {
            mListener?.onRecyclerItemClick(itemView, layoutPosition, holdItem)
        }

        override fun onTouch(v: View?, event: MotionEvent?): Boolean {
            when (event?.action) {
                MotionEvent.ACTION_DOWN -> {
                    v?.alpha = 0.5f
                }
                MotionEvent.ACTION_UP
                -> {
                    v?.alpha = 1f
                }
                MotionEvent.ACTION_CANCEL -> {
                    v?.alpha = 1f
                }
            }
            return false
        }
    }
}