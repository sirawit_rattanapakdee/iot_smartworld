package ai.apdigital.tuya.ui.viewmodel

import ai.apdigital.tuya.base.BaseViewModel
import ai.apdigital.tuya.base.SmartWorldPreferences
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.messaging.RemoteMessage

class NotificationViewModel(
/*private val userRepository: UserRepository,
private val responseHandler: ResponseHandler,*/
    private val sharedPreferences: SmartWorldPreferences
) : BaseViewModel() {
    private var registerNotificationCallback = MutableLiveData<RemoteMessage>()
    fun receiverNotification(): LiveData<RemoteMessage> =
        registerNotificationCallback

    fun sendNotificationData(remoteMessage: RemoteMessage) {
        registerNotificationCallback.value = remoteMessage
    }
}