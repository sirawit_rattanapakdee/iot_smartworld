package ai.apdigital.tuya.ui.viewmodel

import ai.apdigital.tuya.base.BaseViewModel
import ai.apdigital.tuya.base.SmartWorldPreferences
import ai.apdigital.tuya.base.SignUtils
import ai.apdigital.tuya.model.DynamicPasswordEntity
import ai.apdigital.tuya.model.TicketEntity
import ai.apdigital.tuya.model.temp.TemporaryPassword
import ai.apdigital.tuya.networking.Resource
import ai.apdigital.tuya.networking.ResponseHandler
import ai.apdigital.tuya.repository.request.ZigbeeUnlock
import ai.apdigital.tuya.repository.TuyaDoorLockRepository
import ai.apdigital.tuya.repository.TuyaIRRepository
import ai.apdigital.tuya.repository.request.CreateTemporaryPasswordRequest
import ai.apdigital.tuya.ui.control.smart_lock.TypeDoorLock
import ai.apdigital.tuya.ui.control.smart_lock.model.TemporaryPasswordEntity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.tuya.smart.home.sdk.callback.ITuyaResultCallback
import com.tuya.smart.optimus.lock.api.ITuyaLockManager
import com.tuya.smart.optimus.lock.api.TempPasswordBuilder
import com.tuya.smart.optimus.lock.api.bean.Record
import com.tuya.smart.optimus.lock.api.bean.TempPassword
import com.tuya.smart.optimus.lock.api.callback.TemporaryPasswordListener
import com.tuya.smart.optimus.sdk.TuyaOptimusSdk
import com.tuya.smart.sdk.api.ITuyaDataCallback
import com.tuya.smart.sdk.optimus.lock.bean.ble.ScheduleBean
import com.tuya.smart.sdk.optimus.lock.bean.ble.TempPasswordBeanV3
import com.tuya.smart.utils.AES
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import timber.log.Timber


class TuyaDoorLockViewModel(
/*private val userRepository: UserRepository,
private val responseHandler: ResponseHandler,*/
    private val tuyaIRRepository: TuyaIRRepository,
    private val doorLock: TuyaDoorLockRepository,
    private val responseHandler: ResponseHandler,
    private val sharedPreferences: SmartWorldPreferences
) : BaseViewModel() {

    private fun getSign(t: Long): String {
        return SignUtils.bizSign(
            "rsqhsxvaalfaqz2nualb",
            sharedPreferences.getToken()?.access_token,
            "b69691b0c658452da8357c0e16889a7d", t
        )
    }

    private var temporaryPasswordDoorLock = MutableLiveData<Resource<List<TempPassword?>>>()
    fun getTemporaryPasswordListener(): LiveData<Resource<List<TempPassword?>>> =
        temporaryPasswordDoorLock

    private var temporaryPasswordBLEDoorLock =
        MutableLiveData<Resource<List<TempPasswordBeanV3?>>>()

    fun getTemporaryBLEPasswordListener(): LiveData<Resource<List<TempPasswordBeanV3?>>> =
        temporaryPasswordBLEDoorLock


    private var temporaryPasswordZigbeeDoorLock =
        MutableLiveData<Resource<List<TemporaryPassword?>>>()

    fun getTemporaryZigbeePasswordListener(): LiveData<Resource<List<TemporaryPassword?>>> =
        temporaryPasswordZigbeeDoorLock


    private var temporaryDeletePasswordBLEDoorLock =
        MutableLiveData<Resource<Boolean>>()

    fun deleteTemporaryBLEPasswordListener(): LiveData<Resource<Boolean>> =
        temporaryDeletePasswordBLEDoorLock

    private var temporaryCreatePasswordDoorLock =
        MutableLiveData<Resource<Boolean>>()

    fun createTemporaryPasswordListener(): LiveData<Resource<Boolean>> =
        temporaryCreatePasswordDoorLock

    private var recordDoorLock = MutableLiveData<Resource<Record>>()
    fun getRecordListener(): LiveData<Resource<Record>> = recordDoorLock

    private var alarmDoorLock = MutableLiveData<Resource<Record>>()
    fun getAlarmListener(): LiveData<Resource<Record>> = alarmDoorLock

    private var dynamicDoorLock = MutableLiveData<Resource<String>>()
    fun getDynamicPasswordListener(): LiveData<Resource<String>> = dynamicDoorLock

    private var unLockListener = MutableLiveData<Resource<Int>>()
    fun registerUnlockListener(): LiveData<Resource<Int>> = unLockListener

    private var remoteUnlock = MutableLiveData<Resource<Boolean>>()
    fun remoteUnlockListener(): LiveData<Resource<Boolean>> = remoteUnlock


    private val tuyaLockManager: ITuyaLockManager? = TuyaOptimusSdk.getManager(
        ITuyaLockManager::class.java
    )

    fun registerUnLockListener(devId: String?) {
        val tuyaLockDevice = tuyaLockManager?.getWifiLock(devId)
        tuyaLockDevice?.setRemoteUnlockListener { devId, second ->
            Timber.e("$devId - $second : remote unlock request onReceive")
            unLockListener.value = responseHandler.handleSuccess(second)
        }
    }

    fun replyUnlock(unlock: Boolean, devId: String?) {
        val tuyaLockDevice = tuyaLockManager?.getWifiLock(devId)
        remoteUnlock.value = Resource.loading(null)
        tuyaLockDevice?.replyRemoteUnlock(unlock, object : ITuyaResultCallback<Boolean?> {
            override fun onError(code: String, message: String) {
                remoteUnlock.value = responseHandler.handleError(code, message)
                Timber.e("reply remote unlock failed: code = $code  message = $message")
            }

            override fun onSuccess(result: Boolean?) {
                remoteUnlock.value = responseHandler.handleSuccess(result)
                Timber.e("reply remote unlock success")
            }
        })
    }

    fun tabToGetDynamicDoorLock(devId: String?, type: TypeDoorLock) {
        dynamicDoorLock.value = Resource.loading(null)
        if (type == TypeDoorLock.WIFI) {
            val tuyaLockDevice = tuyaLockManager?.getWifiLock(devId)
            tuyaLockDevice?.getDynamicPassword(object : ITuyaResultCallback<String> {
                override fun onError(code: String, message: String) {
                    dynamicDoorLock.value = responseHandler.handleError(code, message)
                }

                override fun onSuccess(dynamicPassword: String) {
                    dynamicDoorLock.value = responseHandler.handleSuccess(dynamicPassword)
                }
            })
        } else {
            val tuyaLockDevice = tuyaLockManager?.getBleLock(devId)
            tuyaLockDevice?.getDynamicPassword(object : ITuyaResultCallback<String> {
                override fun onError(code: String, message: String) {
                    dynamicDoorLock.value = responseHandler.handleError(code, message)
                }

                override fun onSuccess(dynamicPassword: String) {
                    dynamicDoorLock.value = responseHandler.handleSuccess(dynamicPassword)
                }
            })
        }
    }

    fun getRecordList(devId: String?, type: TypeDoorLock, offset: Int, limit: Int) {
        recordDoorLock.value = Resource.loading(null)
        val dpCodes: ArrayList<String> = ArrayList()
        dpCodes.add("unlock_dynamic")
        dpCodes.add("closed_opened")
        dpCodes.add("unlock_temporary")
        dpCodes.add("unlock_password")
        dpCodes.add("unlock_fingerprint")
        dpCodes.add("unlock_card")
        dpCodes.add("unlock_face")
        dpCodes.add("open_inside")
        dpCodes.add("unlock_key")
        dpCodes.add("unlock_request")
        dpCodes.add("unlock_hand")
        dpCodes.add("reply_unlock_request")
        dpCodes.add("battery_state")
        dpCodes.add("residual_electricity")
        dpCodes.add("reverse_lock")
        dpCodes.add("child_lock")
        dpCodes.add("message")
        dpCodes.add("unlock_eye")
        dpCodes.add("unlock_finger_vein")
        dpCodes.add("unlock_offline_pd")
        dpCodes.add("unlock_offline_clear")
        dpCodes.add("unlock_offline_clear_single")
        dpCodes.add("unlock_app")

        if (type == TypeDoorLock.WIFI ||type == TypeDoorLock.ZIGBEE  ) {
            val tuyaLockDevice = tuyaLockManager?.getWifiLock(devId)
            tuyaLockDevice?.getRecords(
                dpCodes,
                offset,
                limit,
                object : ITuyaResultCallback<Record> {
                    override fun onError(code: String, message: String) {
                        recordDoorLock.value = responseHandler.handleError(code, message)
                    }

                    override fun onSuccess(recordBean: Record) {
                        recordDoorLock.value = responseHandler.handleSuccess(recordBean)
                    }
                })

        } else {
            val tuyaLockDevice = tuyaLockManager?.getBleLock(devId)
            tuyaLockDevice?.getRecords(dpCodes, offset,
                limit, object : ITuyaResultCallback<Record> {
                    override fun onError(code: String, message: String) {
                        recordDoorLock.value = responseHandler.handleError(code, message)
                    }

                    override fun onSuccess(recordBean: Record) {
                        recordDoorLock.value = responseHandler.handleSuccess(recordBean)
                    }
                })
        }

    }


    fun getAlarmList(devId: String?, type: TypeDoorLock, offset: Int, limit: Int) {
        alarmDoorLock.value = Resource.loading(null)
        val dpCodes: ArrayList<String> = ArrayList()
        dpCodes.add("alarm_lock")
        dpCodes.add("hijack")
        dpCodes.add("doorbell")
        if (type == TypeDoorLock.WIFI ||type == TypeDoorLock.ZIGBEE  ) {
            val tuyaLockDevice = tuyaLockManager?.getWifiLock(devId)

            tuyaLockDevice?.getRecords(
                dpCodes,
                offset,
                limit,
                object : ITuyaResultCallback<Record> {
                    override fun onError(code: String, message: String) {
                        alarmDoorLock.value = responseHandler.handleError(code, message)
                    }

                    override fun onSuccess(recordBean: Record) {
                        alarmDoorLock.value = responseHandler.handleSuccess(recordBean)
                    }
                })

        } else {
            val tuyaLockDevice = tuyaLockManager?.getBleLock(devId)
            tuyaLockDevice?.getRecords(dpCodes,
                offset,
                limit, object : ITuyaResultCallback<Record> {
                    override fun onError(code: String, message: String) {
                        alarmDoorLock.value = responseHandler.handleError(code, message)
                    }

                    override fun onSuccess(recordBean: Record) {
                        alarmDoorLock.value = responseHandler.handleSuccess(recordBean)
                    }
                })
        }
    }

    fun getTemporaryPasswordList(devId: String?, type: TypeDoorLock) {
        when (type) {
            TypeDoorLock.WIFI -> {
                temporaryPasswordDoorLock.value = Resource.loading(null)
                val tuyaLockDevice = tuyaLockManager?.getWifiLock(devId)
                tuyaLockDevice?.getTempPasswords(object : ITuyaResultCallback<List<TempPassword?>> {
                    override fun onError(code: String, message: String) {
                        temporaryPasswordDoorLock.value = responseHandler.handleError(code, message)
                    }

                    override fun onSuccess(tempPasswords: List<TempPassword?>) {
                        temporaryPasswordDoorLock.value =
                            responseHandler.handleSuccess(tempPasswords)
                    }
                })
            }
            TypeDoorLock.BLE -> {
                temporaryPasswordBLEDoorLock.value = Resource.loading(null)
                val tuyaLockDevice = tuyaLockManager?.getBleLock(devId)
                tuyaLockDevice?.getTempPasswordList(
                    0,
                    object : ITuyaDataCallback<List<TempPasswordBeanV3?>> {
                        override fun onSuccess(result: List<TempPasswordBeanV3?>) {
                            temporaryPasswordBLEDoorLock.value =
                                responseHandler.handleSuccess(result)
                        }

                        override fun onError(code: String, message: String) {
                            temporaryPasswordBLEDoorLock.value =
                                responseHandler.handleError(code, message)
                        }
                    })
            }
            else -> {
                temporaryPasswordZigbeeDoorLock.value = Resource.loading(null)

                val t = System.currentTimeMillis()
                val sign = getSign(t)

                doorLock.getTemporaryPassword(
                    sharedPreferences.getToken()?.access_token ?: "",
                    sign,
                    t, devId
                ).subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .let { it ->
                        addToDisposable(
                            it.subscribe({
                                temporaryPasswordZigbeeDoorLock.value = it
                            }, { error ->
                                temporaryPasswordZigbeeDoorLock.value =
                                    responseHandler.handleException(error)
                            })
                        )
                    }
            }
        }
    }

    fun deleteTemporaryPassword(devId: String?, type: TypeDoorLock, passwordId: Int, sn: Int) {
        temporaryDeletePasswordBLEDoorLock.value = Resource.loading(null)
        when (type) {
            TypeDoorLock.WIFI -> {
                val tuyaLockDevice = tuyaLockManager?.getWifiLock(devId)
                tuyaLockDevice?.deleteTempPassword(
                    passwordId,
                    object : ITuyaResultCallback<Boolean?> {
                        override fun onSuccess(result: Boolean?) {
                            temporaryDeletePasswordBLEDoorLock.value =
                                responseHandler.handleSuccess(result)
                        }

                        override fun onError(code: String?, message: String?) {
                            temporaryDeletePasswordBLEDoorLock.value =
                                responseHandler.handleError(code, message)
                        }
                    })
            }
            TypeDoorLock.BLE -> {
                val tuyaLockDevice = tuyaLockManager?.getBleLock(devId)
                tuyaLockDevice?.deleteTempPassword(passwordId, sn)
                temporaryDeletePasswordBLEDoorLock.value = responseHandler.handleSuccess(true)
            }
            else -> {

                val t = System.currentTimeMillis()
                val sign = getSign(t)
                doorLock.deleteTemporaryPassword(
                    sharedPreferences.getToken()?.access_token ?: "",
                    sign,
                    t, devId, passwordId
                ).subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .let { it ->
                        addToDisposable(
                            it.subscribe({
                                temporaryDeletePasswordBLEDoorLock.value = it
                            }, { error ->
                                temporaryDeletePasswordBLEDoorLock.value =
                                    responseHandler.handleException(error)
                            })
                        )
                    }
            }
        }
    }

    fun createTemporaryWIFIPassword(devId: String?, tempPass: TemporaryPasswordEntity?) {
        val tempPasswordBuilder = TempPasswordBuilder()
            .name(tempPass?.name)
            .password(tempPass?.password)
            .effectiveTime(tempPass?.startDate ?: 0L)
            .invalidTime(tempPass?.endDate ?: 0L)

        temporaryCreatePasswordDoorLock.value = Resource.loading(null)
        val tuyaLockDevice = tuyaLockManager?.getWifiLock(devId)
        tuyaLockDevice?.createTempPassword(
            tempPasswordBuilder,
            object : ITuyaResultCallback<Boolean?> {
                override fun onSuccess(result: Boolean?) {
                    temporaryCreatePasswordDoorLock.value = responseHandler.handleSuccess(result)
                }

                override fun onError(code: String?, message: String?) {
                    temporaryCreatePasswordDoorLock.value =
                        responseHandler.handleError(code, message)
                }
            })
    }


    fun createTemporaryBLEPassword(devId: String?, tempPass: TemporaryPasswordEntity?) {
        temporaryCreatePasswordDoorLock.value = Resource.loading(null)
        val tuyaLockDevice = tuyaLockManager?.getBleLock(devId)
        tuyaLockDevice?.setTemporaryPasswordListener(object :
            TemporaryPasswordListener {
            override fun onSuccess(p0: String?, p1: Int, p2: Int) {
                temporaryCreatePasswordDoorLock.value = responseHandler.handleSuccess(true)

            }

            override fun onFailed(p0: String?, p1: Int) {
                temporaryCreatePasswordDoorLock.value =
                    responseHandler.handleError(p1.toString(), p0)

            }
        })
        val scheduleBean = ScheduleBean()
        scheduleBean.allDay = true
        scheduleBean.dayOfWeeks.add(ScheduleBean.DayOfWeek.MONDAY)
        scheduleBean.dayOfWeeks.add(ScheduleBean.DayOfWeek.TUESDAY)
        scheduleBean.dayOfWeeks.add(ScheduleBean.DayOfWeek.WEDNESDAY)
        scheduleBean.dayOfWeeks.add(ScheduleBean.DayOfWeek.THURSDAY)
        scheduleBean.dayOfWeeks.add(ScheduleBean.DayOfWeek.FRIDAY)
        scheduleBean.dayOfWeeks.add(ScheduleBean.DayOfWeek.SATURDAY)
        scheduleBean.dayOfWeeks.add(ScheduleBean.DayOfWeek.SUNDAY)

        tuyaLockDevice?.createTempPassword(
            0,
            tempPass?.name,
            tempPass?.password,
            null,
            "",
            "",
            tempPass?.startDate ?: 0L,
            tempPass?.endDate ?: 0L/*
                                System.currentTimeMillis(),
                                System.currentTimeMillis() + 24 * 60 * 60 * 1000*/
        )
    }

    private var openDoorLock = MutableLiveData<Resource<Boolean>>()
    fun openDoorLockPasswordListener(): LiveData<Resource<Boolean>> = openDoorLock

    fun openZigbeeDoor(devId: String?, passType: String?, pass: String?, ticketId: String?) {
        openDoorLock.value = Resource.loading(null)

        val zigbee = ZigbeeUnlock(
            password_type = passType,
            ticket_id = ticketId,
            password = pass
        )
        val t = System.currentTimeMillis()
        val sign = getSign(t)

        doorLock.unlockDoor(
            sharedPreferences.getToken()?.access_token ?: "",
            sign,
            t, devId, zigbee
        ).subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .let { it ->
                addToDisposable(
                    it.subscribe({
                        openDoorLock.value = it
                    }, { error ->
                        openDoorLock.value = responseHandler.handleException(error)
                    })
                )
            }
    }

    fun tabToGetDynamicZigbeeDoorLock(devId: String?) {
        openDoorLock.value = Resource.loading(null)

        /*val zigbee = ZigbeeUnlock(
            password_type = passType,
            ticket_id = ticketId,
            password = pass
        )*/
        val t = System.currentTimeMillis()
        val sign = getSign(t)

        doorLock.dynamicPassword(
            sharedPreferences.getToken()?.access_token ?: "",
            sign,
            t, devId
        ).subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .let { it ->
                addToDisposable(
                    it.subscribe({
                        dynamicDoorLock.value =
                            responseHandler.handleSuccess(it.data?.dynamic_password ?: "")
                    }, { error ->
                        dynamicDoorLock.value = responseHandler.handleException(error)
                    })
                )
            }
    }

    private var ticketDoorLock = MutableLiveData<Resource<TicketEntity>>()
    fun ticketDoorLockPasswordListener(): LiveData<Resource<TicketEntity>> = ticketDoorLock
    fun getTicket(devId: String) {
        ticketDoorLock.value = Resource.loading(null)

        val t = System.currentTimeMillis()
        val sign = getSign(t)


        doorLock.getTicket(
            sharedPreferences.getToken()?.access_token ?: "",
            sign,
            t, devId
        ).subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .let { it ->
                addToDisposable(
                    it.subscribe({
                        ticketDoorLock.value = it
                    }, { error ->
                        sharedPreferences.setToken(null)
                        ticketDoorLock.value = responseHandler.handleException(error)
                    })
                )
            }
    }


    fun createTemporaryZigbeePassword(
        devId: String?,
        tempPass: TemporaryPasswordEntity?,
        ticket: TicketEntity?
    ) {
        temporaryCreatePasswordDoorLock.value = Resource.loading(null)

        val t = System.currentTimeMillis()
        val sign = getSign(t)

        val originalKey =
            AES.decryptString(ticket?.ticket_key, "b69691b0c658452da8357c0e16889a7d")
        val password = AES.encryptString(tempPass?.password, originalKey)

        val createParam = CreateTemporaryPasswordRequest(
            device_id = devId,
            name = tempPass?.name,
            password = password,
            type = 0,
            password_type = "ticket",
            ticket_id = ticket?.ticket_id,
            invalid_time = tempPass?.endDate?.toString()?.take(10)?.toLong(),
            effective_time = tempPass?.startDate?.toString()?.take(10)?.toLong()
        )
        doorLock.createTemporaryPassword(
            sharedPreferences.getToken()?.access_token ?: "",
            sign,
            t, devId, createParam
        ).subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .let { it ->
                addToDisposable(
                    it.subscribe({
                        temporaryCreatePasswordDoorLock.value = responseHandler.handleSuccess(true)
                    }, { error ->
                        sharedPreferences.setToken(null)
                        temporaryCreatePasswordDoorLock.value =
                            responseHandler.handleException(error)
                    })
                )
            }
    }
}