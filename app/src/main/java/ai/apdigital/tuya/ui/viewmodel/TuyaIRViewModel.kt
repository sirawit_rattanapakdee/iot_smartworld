package ai.apdigital.tuya.ui.viewmodel

import ai.apdigital.tuya.base.BaseViewModel
import ai.apdigital.tuya.base.SmartWorldPreferences
import ai.apdigital.tuya.base.SignUtils
import ai.apdigital.tuya.model.*
import ai.apdigital.tuya.networking.Resource
import ai.apdigital.tuya.networking.ResponseHandler
import ai.apdigital.tuya.repository.TuyaIRRepository
import ai.apdigital.tuya.repository.request.*
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import timber.log.Timber


class TuyaIRViewModel(
/*
private val responseHandler: ResponseHandler,*/
    private val tuyaIRRepository: TuyaIRRepository,
    private val responseHandler: ResponseHandler,
    private val sharedPreferences: SmartWorldPreferences
) : BaseViewModel() {

    private var updateSwitchName = MutableLiveData<Resource<Boolean>>()
    fun switchUpdateListener(): LiveData<Resource<Boolean>> = updateSwitchName

    private var getToken = MutableLiveData<Resource<TokenEntity>>()
    fun getTokenListener(): LiveData<Resource<TokenEntity>> = getToken

    private var getRemotes = MutableLiveData<Resource<MutableList<RemoteDeviceEntity>>>()
    fun getRemotesListener(): LiveData<Resource<MutableList<RemoteDeviceEntity>>> = getRemotes

    /*private var getCategories = MutableLiveData<Resource<MutableList<CategoryEntity>>>()
    fun getCategoriesListener(): LiveData<Resource<MutableList<CategoryEntity>>> = getCategories*/

    private var getBrands = MutableLiveData<Resource<MutableList<BrandEntity>>>()
    fun getBrandsListener(): LiveData<Resource<MutableList<BrandEntity>>> = getBrands

    private var getRemotedIndex = MutableLiveData<Resource<MutableList<RemoteIndexEntity>>>()
    fun getRemotedIndexListener(): LiveData<Resource<MutableList<RemoteIndexEntity>>> =
        getRemotedIndex

    private var sendRemoteTest = MutableLiveData<Resource<TokenEntity>>()
    fun getRemoteTestListener(): LiveData<Resource<TokenEntity>> = sendRemoteTest

    private var sendAirTest = MutableLiveData<Resource<TokenEntity>>()
    fun getAirTestListener(): LiveData<Resource<TokenEntity>> = sendAirTest

    private var sendAirCommand = MutableLiveData<Resource<Boolean>>()
    fun getAirCommandListener(): LiveData<Resource<Boolean>> = sendAirCommand

    private var sendRemoteCommand = MutableLiveData<Resource<Boolean>>()
    fun getRemoteCommandListener(): LiveData<Resource<Boolean>> = sendRemoteCommand

    private var airStatus = MutableLiveData<Resource<AirStatusEntity>>()
    fun getAirStatusListener(): LiveData<Resource<AirStatusEntity>> = airStatus

    private var addDevice = MutableLiveData<Resource<AddRemoteEntity>>()
    fun addRemoteListener(): LiveData<Resource<AddRemoteEntity>> = addDevice

    private var renameDevice = MutableLiveData<Resource<Boolean>>()
    fun renameRemoteListener(): LiveData<Resource<Boolean>> = renameDevice

    private var removeDevice = MutableLiveData<Resource<Boolean>>()
    fun removeRemoteListener(): LiveData<Resource<Boolean>> = removeDevice


    /* fun callCategories(devId: String) {
         getCategories.value = Resource.loading(null)
         val t = System.currentTimeMillis()
         val sign = getSign(t)

         tuyaRepository.getCategories(
             sharedPreferences.getToken()?.access_token ?: "",
             sign,
             t,
             devId
         )
             ?.subscribeOn(Schedulers.newThread())
             ?.observeOn(AndroidSchedulers.mainThread())
             ?.let { it ->
                 addToDisposable(
                     it.subscribe({
                         getCategories.value = it
                     }, { error ->
                         getCategories.value = responseHandler.handleException(error)
                     })
                 )
             }
     }*/
    private fun getSign(t: Long): String {
        return SignUtils.bizSign(
            "rsqhsxvaalfaqz2nualb",
            sharedPreferences.getToken()?.access_token,
            "b69691b0c658452da8357c0e16889a7d", t
        )
    }

    private fun getSignToken(t: Long): String {
        return SignUtils.tokenSign(
            "rsqhsxvaalfaqz2nualb",
            "b69691b0c658452da8357c0e16889a7d", t
        )
    }

    fun getToken() {
        val t = System.currentTimeMillis()
        val sign = getSignToken(t)
        val token = sharedPreferences.getToken()
        if (token != null) {
            if (token.expire_at < System.currentTimeMillis()) {
                getToken.value = Resource.loading(null)
                tuyaIRRepository.getRefreshToken(
                    token.refresh_token ?: "",
                    sign,
                    t
                )
                    ?.subscribeOn(Schedulers.newThread())
                    ?.observeOn(AndroidSchedulers.mainThread())
                    ?.let { it ->
                        addToDisposable(
                            it.subscribe({
                                val expiredAt =
                                    System.currentTimeMillis() + ((it.data?.expire_time
                                        ?: 0) * 1000)
                                it.data?.expire_at = expiredAt
                                sharedPreferences.setToken(it.data)
                                getToken.value = it
                            }, { error ->
                                getToken.value = responseHandler.handleException(error)
                            })
                        )
                    }
            } else {
                getToken.value = Resource.success(token)
            }
        } else {
            getToken.value = Resource.loading(null)
            tuyaIRRepository.getToken("", sign, t)
                ?.subscribeOn(Schedulers.newThread())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.let { it ->
                    addToDisposable(
                        it.subscribe({
                            val expiredAt =
                                System.currentTimeMillis() + ((it.data?.expire_time ?: 0) * 1000)
                            it.data?.expire_at = expiredAt
                            sharedPreferences.setToken(it.data)
                            getToken.value = it
                        }, { error ->
                            getToken.value = responseHandler.handleException(error)
                        })
                    )
                }
        }
    }

    fun callRemotes(devId: String) {
        getRemotes.value = Resource.loading(null)
        val t = System.currentTimeMillis()
        val sign = getSign(t)

        tuyaIRRepository.getRemote(sharedPreferences.getToken()?.access_token ?: "", sign, t, devId)
            ?.subscribeOn(Schedulers.newThread())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.let { it ->
                addToDisposable(
                    it.subscribe({
                        getRemotes.value = it
                    }, { error ->
                        getRemotes.value = responseHandler.handleException(error)
                    })
                )
            }
    }

    fun callBrands(devId: String, categoryId: String) {
        getBrands.value = Resource.loading(null)
        val t = System.currentTimeMillis()
        val sign = getSign(t)
        tuyaIRRepository.getBrands(
            sharedPreferences.getToken()?.access_token ?: "",
            sign,
            t,
            devId,
            categoryId
        )
            ?.subscribeOn(Schedulers.newThread())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.let { it ->
                addToDisposable(
                    it.subscribe({
                        getBrands.value = it
                    }, { error ->
                        getBrands.value = responseHandler.handleException(error)
                    })
                )
            }
    }

    fun callRemotedIndex(devId: String, categoryId: String, brandId: String) {
        getRemotedIndex.value = Resource.loading(null)
        val t = System.currentTimeMillis()
        val sign = getSign(t)
        tuyaIRRepository.getRemoteIndexes(
            sharedPreferences.getToken()?.access_token ?: "",
            sign,
            t,
            devId,
            categoryId,
            brandId
        )
            ?.subscribeOn(Schedulers.newThread())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.let { it ->
                addToDisposable(
                    it.subscribe({
                        getRemotedIndex.value = it
                    }, { error ->
                        getRemotedIndex.value = responseHandler.handleException(error)
                    })
                )
            }
    }

    fun callTestAirCommand(
        devId: String,
        cateId: String,
        key: String,
        value: Int,
        remote_index: String?
    ) {
        sendAirTest.value = Resource.loading(null)
        val t = System.currentTimeMillis()
        val sign = getSign(t)


        val airCommand = AirTestRequest()
        airCommand.infrared_id = devId
        airCommand.category_id = cateId
        airCommand.code = key
        airCommand.value = value
        airCommand.remote_index = remote_index

        tuyaIRRepository.testSendAirCommand(
            sharedPreferences.getToken()?.access_token ?: "",
            sign,
            t,
            devId, airCommand
        )
            ?.subscribeOn(Schedulers.newThread())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.let { it ->
                addToDisposable(
                    it.subscribe({
                        sendAirTest.value = it
                    }, { error ->
                        sendAirTest.value = responseHandler.handleException(error)
                    })
                )
            }
    }

    fun callAirCommand(
        devId: String,
        remoteId: String,
        airCommand: AirSendCommandRequest
    ) {
        sendAirCommand.value = Resource.loading(null)
        val t = System.currentTimeMillis()
        val sign = getSign(t)
        tuyaIRRepository.sendAirCommand(
            sharedPreferences.getToken()?.access_token ?: "",
            sign,
            t,
            devId,
            remoteId,
            airCommand
        )
            ?.subscribeOn(Schedulers.newThread())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.let { it ->
                addToDisposable(
                    it.subscribe({
                        sendAirCommand.value = it
                    }, { error ->
                        sendAirCommand.value = responseHandler.handleException(error)
                    })
                )
            }
    }


    fun callAirCCommand(
        devId: String,
        remoteId: String,
        code: String, value: Int
    ) {
        sendAirCommand.value = Resource.loading(null)
        val t = System.currentTimeMillis()
        val sign = getSign(t)
        tuyaIRRepository.sendAirCCommand(
            sharedPreferences.getToken()?.access_token ?: "",
            sign,
            t,
            devId,
            remoteId,
            AirCommandRequest(code, value)
        )
            ?.subscribeOn(Schedulers.newThread())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.let { it ->
                addToDisposable(
                    it.subscribe({
                        sendAirCommand.value = it
                    }, { error ->
                        sendAirCommand.value = responseHandler.handleException(error)
                    })
                )
            }
    }

    fun callTestRemoteCommand(
        devId: String,
        category_id: String,
        command: String,
        remoteIndex: String?
    ) {

        sendRemoteTest.value = Resource.loading(null)
        val t = System.currentTimeMillis()
        val sign = getSign(t)
        tuyaIRRepository.testSendRemoteCommand(
            sharedPreferences.getToken()?.access_token ?: "",
            sign,
            t,
            devId,
            TVTestRequest(category_id = category_id, remote_index = remoteIndex, key = command)
        )
            ?.subscribeOn(Schedulers.newThread())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.let { it ->
                addToDisposable(
                    it.subscribe({
                        sendRemoteTest.value = it
                    }, { error ->
                        sendRemoteTest.value = responseHandler.handleException(error)
                    })
                )
            }
    }

    fun callRemoteCommand(
        devId: String,
        remoteId: String,
        command: String
    ) {
        sendRemoteCommand.value = Resource.loading(null)
        val t = System.currentTimeMillis()
        val sign = getSign(t)
        tuyaIRRepository.sendRemoteCommand(
            sharedPreferences.getToken()?.access_token ?: "",
            sign,
            t,
            devId,
            remoteId,
            TVSendCommandRequest(command)
        )
            ?.subscribeOn(Schedulers.newThread())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.let { it ->
                addToDisposable(
                    it.subscribe({
                        sendRemoteCommand.value = it
                    }, { error ->
                        sendRemoteCommand.value = responseHandler.handleException(error)
                    })
                )
            }
    }

    fun callAirStatus(devId: String, remoteId: String) {
        airStatus.value = Resource.loading(null)
        val t = System.currentTimeMillis()
        val sign = getSign(t)
        Timber.e("callAirStatus $devId $remoteId")
        tuyaIRRepository.getAirStatus(
            sharedPreferences.getToken()?.access_token ?: "",
            sign,
            t,
            devId,
            remoteId
        )
            ?.subscribeOn(Schedulers.newThread())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.let { it ->
                addToDisposable(
                    it.subscribe({
                        airStatus.value = it
                    }, { error ->
                        airStatus.value = responseHandler.handleException(error)
                    })
                )
            }
    }

    fun callAddRemote(
        devId: String,
        category_id: String,
        brandId: String,
        brandName: String?,
        index: String?,
        name: String?
    ) {
        addDevice.value = Resource.loading(null)
        val t = System.currentTimeMillis()
        val sign = getSign(t)

        tuyaIRRepository.addDevice(
            sharedPreferences.getToken()?.access_token ?: "",
            sign,
            t,
            devId,
            AddRemoteRequest(
                category_id = category_id,
                brand_id = brandId,
                brand_name = brandName,
                remote_index = index,
                remote_name = name
            )
        )
            ?.subscribeOn(Schedulers.newThread())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.let { it ->
                addToDisposable(
                    it.subscribe({
                        addDevice.value = it
                    }, { error ->
                        addDevice.value = responseHandler.handleException(error)
                    })
                )
            }
    }

    fun renameDevice(devId: String?, remoteId: String?, remoteName: String) {
        renameDevice.value = Resource.loading(null)
        val t = System.currentTimeMillis()
        val sign = getSign(t)

        tuyaIRRepository.renameRemote(
            sharedPreferences.getToken()?.access_token ?: "",
            sign,
            t,
            devId, remoteId, remoteName
        )
            ?.subscribeOn(Schedulers.newThread())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.let { it ->
                addToDisposable(
                    it.subscribe({
                        renameDevice.value = it
                    }, { error ->
                        renameDevice.value = responseHandler.handleException(error)
                    })
                )
            }
    }

    fun removeDevice(devId: String?, remoteId: String?) {
        removeDevice.value = Resource.loading(null)
        val t = System.currentTimeMillis()
        val sign = getSign(t)

        tuyaIRRepository.deleteRemote(
            sharedPreferences.getToken()?.access_token ?: "",
            sign,
            t,
            devId, remoteId
        )
            ?.subscribeOn(Schedulers.newThread())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.let { it ->
                addToDisposable(
                    it.subscribe({
                        removeDevice.value = it
                    }, { error ->
                        removeDevice.value = responseHandler.handleException(error)
                    })
                )
            }
    }


    fun updateSwitchName(devId: String?, code: String?, name: String?) {
        updateSwitchName.value = Resource.loading(null)
        val t = System.currentTimeMillis()
        val sign = getSign(t)

        tuyaIRRepository.updateNameSwitch(
            sharedPreferences.getToken()?.access_token ?: "",
            sign,
            t,
            devId, code, name
        )
            ?.subscribeOn(Schedulers.newThread())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.let { it ->
                addToDisposable(
                    it.subscribe({
                        updateSwitchName.value = it
                    }, { error ->
                        updateSwitchName.value = responseHandler.handleException(error)
                    })
                )
            }
    }

}