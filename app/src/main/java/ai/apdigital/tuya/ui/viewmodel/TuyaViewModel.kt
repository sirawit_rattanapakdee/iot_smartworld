package ai.apdigital.tuya.ui.viewmodel

import ai.apdigital.tuya.base.BaseViewModel
import ai.apdigital.tuya.base.SmartWorldPreferences
import ai.apdigital.tuya.networking.Resource
import ai.apdigital.tuya.networking.ResponseHandler
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.tuya.smart.home.sdk.TuyaHomeSdk
import com.tuya.smart.home.sdk.bean.DeviceAndGroupInHomeBean
import com.tuya.smart.home.sdk.bean.DeviceAndGroupInRoomBean
import com.tuya.smart.home.sdk.bean.HomeBean
import com.tuya.smart.home.sdk.bean.RoomBean
import com.tuya.smart.home.sdk.bean.scene.SceneBean
import com.tuya.smart.home.sdk.callback.ITuyaHomeResultCallback
import com.tuya.smart.home.sdk.callback.ITuyaResultCallback
import com.tuya.smart.home.sdk.callback.ITuyaRoomResultCallback
import com.tuya.smart.sdk.api.IResultCallback
import timber.log.Timber


class TuyaViewModel(
/*private val userRepository: UserRepository,
private val responseHandler: ResponseHandler,*/
    private val responseHandler: ResponseHandler,
    private val sharedPreferences: SmartWorldPreferences
) : BaseViewModel() {

    private var scenes = MutableLiveData<Resource<List<SceneBean>>>()
    fun getSceneList(): LiveData<Resource<List<SceneBean>>> = scenes

    private var sceneDelete = MutableLiveData<Resource<Boolean>>()
    fun deleteSceneListener(): LiveData<Resource<Boolean>> = sceneDelete

    private var homeDetail = MutableLiveData<Resource<HomeBean>>()
    fun getHomeDetail(): LiveData<Resource<HomeBean>> = homeDetail

    private var roomAdd = MutableLiveData<Resource<RoomBean>>()
    fun roomAddNew(): LiveData<Resource<RoomBean>> = roomAdd

    private var roomEdit = MutableLiveData<Resource<Long>>()
    fun roomEit(): LiveData<Resource<Long>> = roomEdit

    private var roomRemove = MutableLiveData<Resource<Long>>()
    fun roomRemove(): LiveData<Resource<Long>> = roomRemove

    private var deviceAddToRoom = MutableLiveData<Resource<Long>>()
    fun addDeviceToRoom(): LiveData<Resource<Long>> = deviceAddToRoom

    private var deviceRemoveToRoom = MutableLiveData<Resource<Long>>()
    fun removeDeviceToRoom(): LiveData<Resource<Long>> = deviceRemoveToRoom

    private var enableScene = MutableLiveData<Resource<SceneBean>>()
    fun setEnableScene(): LiveData<Resource<SceneBean>> = enableScene

    private var supportOTA = MutableLiveData<Resource<Boolean>>()
    fun isSupportUpgrade(): LiveData<Resource<Boolean>> = supportOTA

    private var roomSortDevice = MutableLiveData<Resource<Long>>()
    fun roomSortDeviceList(): LiveData<Resource<Long>> = roomSortDevice

    private var homeSortDevice = MutableLiveData<Resource<Long>>()
    fun homeSortDeviceList(): LiveData<Resource<Long>> = homeSortDevice

    fun getScene(homeID: Long) {
        scenes.value = Resource.loading(null)
        TuyaHomeSdk.getSceneManagerInstance()
            .getSceneList(homeID, object : ITuyaResultCallback<List<SceneBean>> {
                override fun onSuccess(result: List<SceneBean>) {
                    scenes.value = responseHandler.handleSuccess(result)
                }

                override fun onError(errorCode: String, errorMessage: String) {
                    scenes.value = responseHandler.handleError(errorCode, errorMessage)
                }
            })
    }

/*
    fun deleteScene(sceneId: String?) {
        scenes.value = Resource.loading(null)
        Timber.e("sceneId : $sceneId")
        TuyaHomeSdk.newSceneInstance(sceneId).deleteScene(object : IResultCallback {
            override fun onSuccess() {
                Timber.e("Success : $sceneId")
                sceneDelete.value = responseHandler.handleSuccess(true)
            }

            override fun onError(errorCode: String, errorMessage: String) {
                Timber.e("Fail : $sceneId")
                sceneDelete.value = responseHandler.handleError(errorCode, errorMessage)
            }
        })

    }*/


    fun getHomeDetail(homeID: Long) {
        homeDetail.value = Resource.loading(null)
        TuyaHomeSdk.newHomeInstance(homeID).getHomeDetail(object : ITuyaHomeResultCallback {
            override fun onError(errorCode: String, errorMsg: String) {
                homeDetail.value = responseHandler.handleError(errorCode, errorMsg)
            }

            override fun onSuccess(home: HomeBean) {
                homeDetail.value = responseHandler.handleSuccess(home)
            }
        })
    }

    fun addRoom(homeId: Long, roomName: String) {
        roomAdd.value = Resource.loading(null)
        TuyaHomeSdk.newHomeInstance(homeId).addRoom(roomName, object : ITuyaRoomResultCallback {
            override fun onError(errorCode: String, errorMsg: String) {
                roomAdd.value = responseHandler.handleError(errorCode, errorMsg)
            }

            override fun onSuccess(bean: RoomBean) {
                roomAdd.value = responseHandler.handleSuccess(bean)
            }
        })
    }


    fun editRoom(roomId: Long, roomName: String) {
        roomEdit.value = Resource.loading(null)
        TuyaHomeSdk.newRoomInstance(roomId).updateRoom(roomName, object : IResultCallback {
            override fun onError(code: String?, error: String?) {
                roomEdit.value = responseHandler.handleError(code, error)
            }

            override fun onSuccess() {
                roomEdit.value = responseHandler.handleSuccess(roomId)
            }
        })
    }

    fun roomRemove(homeId: Long?, roomId: Long) {
        roomRemove.value = Resource.loading(null)
        TuyaHomeSdk.newHomeInstance(homeId ?: 0L).removeRoom(roomId, object : IResultCallback {
            override fun onError(code: String, error: String) {
                roomRemove.value = responseHandler.handleError(code, error)
            }

            override fun onSuccess() {
                roomRemove.value = responseHandler.handleSuccess(roomId)
            }
        })
    }

    fun addDeviceToRoom(roomId: Long, devId: String) {
        deviceAddToRoom.value = Resource.loading(null)
        TuyaHomeSdk.newRoomInstance(roomId).addDevice(devId, object : IResultCallback {
            override fun onError(code: String, error: String) {
                deviceAddToRoom.value = responseHandler.handleError(code, error)
            }

            override fun onSuccess() {
                deviceAddToRoom.value = responseHandler.handleSuccess(roomId)
            }
        })
    }


    fun removeDeviceToRoom(roomId: Long, devId: String) {
        deviceRemoveToRoom.value = Resource.loading(null)
        TuyaHomeSdk.newRoomInstance(roomId).removeDevice(devId, object : IResultCallback {
            override fun onError(code: String, error: String) {
                deviceRemoveToRoom.value = responseHandler.handleError(code, error)
            }

            override fun onSuccess() {
                deviceRemoveToRoom.value = responseHandler.handleSuccess(roomId)
            }
        })
    }


    fun enableScene(isEnable: Boolean, scene: SceneBean) {
        enableScene.value = Resource.loading(null)

        val enableListener = object : IResultCallback {
            override fun onSuccess() {
                enableScene.value = responseHandler.handleSuccess(scene)
            }

            override fun onError(errorCode: String, errorMessage: String) {
                enableScene.value = responseHandler.handleError("0", null)

            }
        }
        if (isEnable) {
            TuyaHomeSdk.newSceneInstance(scene.id)
                .enableScene(scene.id, enableListener)
        } else {
            TuyaHomeSdk.newSceneInstance(scene.id)
                .disableScene(scene.id, enableListener)
        }
    }

    fun tapToRubScene(scene: SceneBean) {
        enableScene.value = Resource.loading(null)
        TuyaHomeSdk.newSceneInstance(scene.id)
            .executeScene(object : IResultCallback {
                override fun onSuccess() {
                    enableScene.value = responseHandler.handleSuccess(scene)
                }

                override fun onError(
                    errorCode: String?,
                    errorMessage: String?
                ) {
                    enableScene.value = responseHandler.handleError("0", null)
                }
            })
    }

    fun sortDeviceInRoom(roomId: Long, devs: MutableList<DeviceAndGroupInRoomBean>) {
        roomSortDevice.value = Resource.loading(null)
        TuyaHomeSdk.newRoomInstance(roomId)
            .sortDevInRoom(devs, object : IResultCallback {
                override fun onError(code: String, error: String) {
                    roomSortDevice.value = responseHandler.handleError("0", null)
                }

                override fun onSuccess() {
                    roomSortDevice.value = responseHandler.handleSuccess(roomId)
                }
            })
    }


    fun sortDeviceInHome(homeId: Long, devs: MutableList<DeviceAndGroupInHomeBean>?) {
        homeSortDevice.value = Resource.loading(null)
        TuyaHomeSdk.newHomeInstance(homeId)
            .sortDevInHome(homeId.toString(), devs?.reversed(), object : IResultCallback {
                override fun onError(code: String, error: String) {
                    homeSortDevice.value = responseHandler.handleError("0", null)
                }

                override fun onSuccess() {
                    homeSortDevice.value = responseHandler.handleSuccess(homeId)
                }
            })
    }

}