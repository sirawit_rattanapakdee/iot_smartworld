package ai.apdigital.tuya.utils


import android.annotation.SuppressLint
import android.os.Build
import androidx.annotation.RequiresApi
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object DateHelper {
    private var SECOND_IN_MILLIS = 1000L
    private var MINUTE_IN_MILLIS = 60000L
    private var HOUR_IN_MILLIS = 3600000L
    private var DAY_IN_MILLIS = 86400000L
    private var WEEK_IN_MILLIS = 604800000L
    private var YEAR_IN_MILLIS = 31449600000L

    private var SECOND_MILLIS = 1000
    private var MINUTE_MILLIS = 60 * SECOND_MILLIS
    private var HOUR_MILLIS = 60 * MINUTE_MILLIS
    private var DAY_MILLIS = 24 * HOUR_MILLIS

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun convertDateToStringWithFormat(date: Date, format: String): String {
        val sdf = SimpleDateFormat(
            format,
            Locale.forLanguageTag("en")
        )
        return sdf.format(date)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun convertSystemTimeDateToStringWithFormat(dateString: Long, format: String): String {
        val simpleDateFormat = SimpleDateFormat(format)
        return simpleDateFormat.format(dateString)
        return "-"
    }


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun convertStringToDateWithFormat(dateString: String?, format: String): Date? {
        val sdf = SimpleDateFormat(
            format,
            Locale.getDefault()
        )
        try {
            sdf.timeZone = TimeZone.getTimeZone("GMT")
            return sdf.parse(dateString)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return null
    }

    @SuppressLint("SimpleDateFormat")
    fun dateToStringForServer(date: Date, format: String): String {
        val sdf = SimpleDateFormat(format, Locale("en", "US"))
        return sdf.format(date)
    }

    @SuppressLint("SimpleDateFormat")
    fun stringToDateForServer(dateString: String, format: String): Date? {
        val sdf = SimpleDateFormat(format)

        try {
            return sdf.parse(dateString)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return null
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun getDateWithFormat(dateString: String, format: String): String {
        val date = convertStringToDateWithFormat(dateString, "yyyy-MM-dd HH:mm:ss")
        return if (date != null) {
            convertDateToStringWithFormat(date, format)
        } else {
            dateString
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun getDateWithFormat(dateString: String, format: String, mainFormat: String): String {
        val date = convertStringToDateWithFormat(dateString, mainFormat)
        return if (date != null) {
            convertDateToStringWithFormat(date, format)
        } else {
            dateString
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun getFullDateWithTime(dateString: String?, format: String): String {
        return if (dateString == null) {
            ""
        } else {
            val date = convertStringToDateWithFormat(dateString, format)
            convertDateToStringWithFormat(
                date!!, "dd MMMM yyyy, HH:mm"
            )
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun getShortDateWithTime(dateString: String?): String {
        return try {
            if (dateString == null) {
                ""
            } else {
                val date = convertStringToDateWithFormat(dateString, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                convertDateToStringWithFormat(
                    date!!, "dd MMMM yyyy"
                )
            }
        } catch (e: Exception) {
            ""
        }
    }


    fun getShortTime(dateString: String?): String? {
        return try {
            if (dateString == null) {
                ""
            } else {
                val date = convertStringToDateWithFormat(dateString, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                date?.let {
                    convertDateToStringWithFormat(
                        it, "hh:mm aa"
                    )
                }
            }
        } catch (e: Exception) {
            ""
        }
    }


    fun getShortDayAndMonth(dateString: String?): String? {
        return try {
            if (dateString == null) {
                "-"
            } else {
                val date = convertStringToDateWithFormat(dateString, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                date?.let {
                    convertDateToStringWithFormat(
                        it, "MM/dd"
                    )
                }
            }
        } catch (e: Exception) {
            ""
        }
    }


    fun getFromFormat(dateString: String?, format: String): String? {
        return try {
            if (dateString == null) {
                "-"
            } else {
                val date = convertStringToDateWithFormat(dateString, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                date?.let {
                    convertDateToStringWithFormat(
                        it, format
                    )
                }
            }
        } catch (e: Exception) {
            ""
        }
    }

    /*  fun getTimeAgo(context: Context, time: Long): String {

          var timeDate = time
          if (timeDate < 1000000000000L) {
              timeDate *= 1000
          }

          var now = Date().time
          if (time > now || time <= 0) {
              return ""
          }

          var diff = now - time
          return when {
              diff <= MINUTE_IN_MILLIS -> context.getString(R.string.just_now_title)
              diff <= HOUR_IN_MILLIS -> ((diff / MINUTE_MILLIS)).toString() + " " + context.getString(
                  R.string.minute_ago_title
              )
              diff <= DAY_IN_MILLIS -> ((diff / HOUR_MILLIS)).toString() + " " + context.getString(R.string.hour_ago_title)
              diff <= WEEK_IN_MILLIS -> ((diff / DAY_MILLIS)).toString() + " " + context.getString(R.string.days_ago_title)
              diff <= (WEEK_IN_MILLIS * 4) -> (diff / WEEK_IN_MILLIS).toString() + " " + context.getString(
                  R.string.week_ago
              )
              diff <= YEAR_IN_MILLIS -> (diff / (WEEK_IN_MILLIS * 4)).toString() + " " + context.getString(
                  R.string.month_ago
              )
              else -> (diff / YEAR_IN_MILLIS).toString() + " " + context.getString(R.string.year_ago)

              *//*diff < MINUTE_MILLIS -> AppController.applicationContext().getString(R.string.just_now_title)
            diff < 2 * MINUTE_MILLIS -> AppController.applicationContext().getString(R.string.a_minute_ago_title)
            diff < 50 * MINUTE_MILLIS -> (diff / MINUTE_MILLIS).toString() + " " + AppController.applicationContext().getString(R.string.minute_ago_title)
            diff < 90 * MINUTE_MILLIS -> AppController.applicationContext().getString(R.string.a_hour_ago_title)
            diff < 24 * HOUR_MILLIS -> (diff / HOUR_MILLIS).toString() + " " + AppController.applicationContext().getString(R.string.hour_ago_title)
            diff < 24 * HOUR_MILLIS -> AppController.applicationContext().getString(R.string.yesterday_title)
            diff < WEEK_IN_MILLIS -> (diff / DAY_MILLIS).toString() + " " + AppController.applicationContext().getString(R.string.days_ago_title)
            diff < 30 * DAY_MILLIS ->   (diff / WEEK_IN_MILLIS).toString() + " " + AppController.applicationContext().getString(R.string.week_ago)
            diff < YEAR_IN_MILLIS -> (diff / (DAY_MILLIS * 30)).toString() + " " + AppController.applicationContext().getString(R.string.month_ago)
            else -> (diff / YEAR_IN_MILLIS).toString() + " " + AppController.applicationContext().getString(R.string.year_ago)*//*
        }
    }*/
}