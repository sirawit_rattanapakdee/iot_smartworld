package ai.apdigital.tuya.utils

import ai.apdigital.tuya.R
import ai.apdigital.tuya.utils.alerts.AlertUtil
import android.app.Dialog

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import timber.log.Timber


object DialogAlertHelper {
    private var mDialog: Dialog? = null
    private var tvMessage: TextView? = null
    private var ivIcon: ImageView? = null
    private var handler: Handler? = null
    private var runnable: Runnable? = null


    fun showProgress(
        context: Context,
        resId: Int,
        message: String,
        successListener: AlertUtil.OnSuccessListener?
    ) {
        try {
            mDialog?.dismiss()
            runnable?.let {
                handler?.removeCallbacks(it)
            Looper.myLooper()?.let { loop->
                handler = Handler(loop)
                runnable = Runnable {
                    hideProgress()
                    successListener?.onSuccess()
                }
                handler?.postDelayed(it, 1200L)
            }
            }
            mDialog = Dialog(context, R.style.NewDialog)
            mDialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            mDialog?.window?.setBackgroundDrawableResource(android.R.color.transparent)
            mDialog?.setContentView(R.layout.layout_alert_notice)

            // indicator view
            tvMessage = mDialog?.findViewById(R.id.tvMessage)
            ivIcon = mDialog?.findViewById(R.id.progressIcon)
            ivIcon?.setImageResource(resId)
            tvMessage?.text = message
            //mDialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            mDialog?.setCancelable(true)
            mDialog?.setCanceledOnTouchOutside(true)
            mDialog?.show()

        } catch (e: Exception) {
            Timber.e("Error $e")
            hideProgress()
            successListener?.onSuccess()
        }
    }

    fun hideProgress() {
        try {
            ivIcon = null
            tvMessage = null
            mDialog?.dismiss()
            mDialog = null
        } catch (e: Exception) {
            Timber.e("Error $e")
        }
    }
}