package ai.apdigital.tuya.utils

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Window
import ai.apdigital.tuya.R


object DialogCircleLoadingHelper {
    private var mDialog: Dialog? = null


    fun showProgress(context: Context) {
        mDialog?.dismiss()
        mDialog = Dialog(context)
        mDialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        mDialog?.setContentView(R.layout.layout_progress_loading)

        mDialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialog?.setCancelable(true)
        mDialog?.setCanceledOnTouchOutside(false)
        //show dialog
        //hideProgress()
        mDialog?.show()
    }

    fun hideProgress() {
        mDialog?.dismiss()
        mDialog = null
    }
}