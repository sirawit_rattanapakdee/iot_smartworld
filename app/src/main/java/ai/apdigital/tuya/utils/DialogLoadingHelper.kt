package ai.apdigital.tuya.utils

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.view.Window
import android.widget.TextView
import ai.apdigital.tuya.R
import com.wang.avi.AVLoadingIndicatorView


object DialogLoadingHelper {
    private var indicatorView: AVLoadingIndicatorView? = null
    private var mDialog: Dialog? = null
    private var tvMessage: TextView? = null


    fun showProgress(context: Context) {
        mDialog?.dismiss()
        mDialog = Dialog(context)
        mDialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        mDialog?.setContentView(R.layout.layout_progress_hud)

        // indicator view
        indicatorView = mDialog?.findViewById(R.id.progressbar)
        indicatorView?.visibility = View.VISIBLE
        tvMessage = mDialog?.findViewById(R.id.tvMessage)
        mDialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        //mDialog?.setCancelable(false)
        mDialog?.setCanceledOnTouchOutside(false)
        //show dialog
        //hideProgress()
        mDialog?.show()
    }

    fun hideProgress() {
        mDialog?.dismiss()
        tvMessage = null
        indicatorView = null
        mDialog = null
    }
}