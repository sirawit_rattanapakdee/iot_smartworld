package ai.apdigital.tuya.utils;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;

import ai.apdigital.tuya.TuyaSmartWorldModule;

public class EventSenderUtil {

    public static String PAGE = "page";
    public static String EVENT = "event";
    public static String TUYA_PANEL_CCTV_PAGE = "Tuya_panel_cctv";
    public static String TUYA_PANEL_CCTV_PAGE_PLAYBACK = "Tuya_panel_cctv_playback";


    public static String TUYA_PANEL_CCTV_DEVICE_DETAIL = "tuya_panel_cctv_device_detail";
    public static String TUYA_PANEL_CCTV_CONTROL = "tuya_panel_cctv_control";
    public static String TUYA_PANEL_CCTV_SCREENSHOT = "tuya_panel_cctv_screenshot";
    public static String TUYA_PANEL_CCTV_PLAYBACK = "tuya_panel_cctv_playback";
    public static String TUYA_PANEL_CCTV_PRESS_TO_SPEAK = "tuya_panel_cctv_press_to_speak";


    public static String TUYA_PANEL_CCTV_PLAYBACK_DATE = "tuya_panel_cctv_playback_date";
    public static String TUYA_PANEL_CCTV_PLAYBACK_PAUSE = "tuya_panel_cctv_playback_pause";
    public static String TUYA_PANEL_CCTV_PLAYBACK_SOUND = "tuya_panel_cctv_playback_sound";
    public static String TUYA_PANEL_CCTV_PLAYBACK_SCREENSHOT = "tuya_panel_cctv_playback_screenshot";
    public static String TUYA_PANEL_CCTV_PLAYBACK_RECORD = "tuya_panel_cctv_playback_record";


    public static String TUYA_PANEL_CCTV_MORE = "tuya_panel_cctv_more";


    public static void sendEvent(String type, String contentMenu, String value) {
        try {
            WritableMap payload = Arguments.createMap();
            payload.putString("type", type);
            payload.putString("contentMenu", contentMenu);
            payload.putString("value", value);
            if (TuyaSmartWorldModule.reactContextApp != null)
                TuyaSmartWorldModule.reactContextApp.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit("onEngagementCallBack", payload);
        } catch (Exception ignored) {
        }
    }
}
