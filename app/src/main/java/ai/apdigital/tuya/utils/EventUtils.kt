package ai.apdigital.tuya.utils

import ai.apdigital.tuya.TuyaSmartWorldModule
import com.facebook.react.bridge.Arguments
import timber.log.Timber

import com.facebook.react.bridge.WritableMap
import com.facebook.react.modules.core.DeviceEventManagerModule.RCTDeviceEventEmitter
import java.lang.Exception


object EventUtils {
    val TYPE_PAGE = "page"
    val TYPE_EVENT = "event"
    fun sendEvent(type: String?, contentMenu: String?, value: String?) {
        Timber.e("$type ___ $contentMenu ___ $value")
        // Create map for params
        try {
            val payload: WritableMap? = Arguments.createMap()
            payload?.putString("type", type)
            payload?.putString("contentMenu", contentMenu)
            payload?.putString("value", value)
            TuyaSmartWorldModule.reactContextApp.getJSModule(RCTDeviceEventEmitter::class.java)
                ?.emit("onEngagementCallBack", payload)
        } catch (e: Exception) {
        }
    }
}

object PageUtils {

    val TUYA_HOME = "Tuya_home"
    val TUYA_SCENE = "Tuya_showall_scene"
    val TUYA_ROOM_MANAGE = "Tuya_manage_room"
    val TUYA_CREATE_ROOM = "Tuya_create_room"
    val TUYA_PANEL_GANERAL_DEVICE_DETAIL = "Tuya_panel_general_device_detail"

    val TUYA_PANEL_DOOR_LOCK = "Tuya_panel_doorlock"
    val TUYA_PANEL_DOOR_LOCK_TEMP = "Tuya_panel_door_lock_temporary_pw"
    val TUYA_PANEL_GENERAL_DEVICE = "Tuya_panel_general_device"
    val TUYA_CREATE_ROOM_POPUP_ADD_DEVICE = "Tuya_create_room_popup_add_device"


    val TUYA_ROOM_MANAGE_EDIT_ROOM = "Tuya_manage_room_edit_room"
    val TUYA_SORT_DEVICE = "Tuya_sortt_device"
    val TUYA_PANEL_IR_ADD_REMOTE = "Tuya_ir_add_remote"
    val TUYA_PANEL_IR = "Tuya_panel_ir_"
    val TUYA_PANEL_IR_P = "Tuya_panel_ir"
    val TUYA_PANEL_IR_MATCH = "Tuya_panel_ir_match_"
}

object EventHomeUtils {
    val TUYA_HOME_NOTI_CENTER = "tuya_home_noti_center"
    val TUYA_HOME_ADD_DEVICE = "tuya_home_add_device"
    val TUYA_HOME_SHOW_ALL_SCENE = "tuya_home_showall_scene"
    val TUYA_HOME_SCENE = "tuya_home_scene"
    val TUYA_HOME_SELECT_ROOM = "tuya_home_select_room"
    val TUYA_HOME_MANAGE_ROOM = "tuya_home_manage_room"
    val TUYA_HOME_SORT_DEVICE = "tuya_home_sort_device"
    val TUYA_HOME_DEVICE_DETAIL = "tuya_home_device_detail"
    val TUYA_HOME_DEVICE_PANEL = "tuya_home_device_panel"

    val TUYA_HOME_QUICK_TOGGLE_SW = "tuya_home_quick_toggle_sw"

}

object EventSceneUtils {
    val TUYA_SHOW_ALL_EDIT = "tuya_showall_edit"
    val TUYA_SHOW_ALL_SCENE_RUN = "tuya_showall_scene_run"
    val TUYA_SHOW_ALL_AUTOMATIC_TOGGLE = "tuya_showall_automation_toggle"
    val TUYA_SHOW_ALL_CREATE = "tuya_showall_create"
}

object EventRoomUtils {
    val TUYA_MANAGE_ROOM_EDIT_ROOM = "tuya_manage_room_edit_room"
    val TUYA_MANAGE_ROOM_CREATE = "tuya_manage_room_create"
    val TUYA_CREATE_ROOM_SET_NAME = "tuya_create_room_set_name"
    val TUYA_CREATE_ROOM_SAVE_ROOM = "tuya_create_room_save_room"
    val TUYA_MANAGE_ROOM_EDIT_ROOM_DELETE = "tuya_manage_room_edit_room_delete"
    val TUYA_MANAGE_ROOM_EDIT_ROOM_ADD_DEVICE = "tuya_manage_room_edit_room_add_device"
    val TUYA_CREATE_ROOM_POPUP_ADD_DEVICE = "tuya_create_room_popup_add_device"
}

object EventGeneralDeviceUtils {
    val TUYA_PANEL_GANERAL_DEVICE_DEVICE_DETAIL = "tuya_panel_general_device_device_detail"
    val TUYA_PANEL_GANERAL_DEVICE_DP = "tuya_panel_general_device_"
    val TUYA_PANEL_GANERAL_DEVICE_SW = "tuya_panel_general_device_sw"
    val TUYA_PANEL_GANERAL_DEVICE_EDIT_NAME = "tuya_panel_general_device_edit_name"
    val TUYA_PANEL_GANERAL_DEVICE_ALLON = "tuya_panel_general_device_allon"
    val TUYA_PANEL_GANERAL_DEVICE_ALLOFF = "tuya_panel_general_device_alloff"
    val TUYA_PANEL_GANERAL_DEVICE_SM_BULB_COLOR = "tuya_panel_general_device_sm_bulb_color"

}

object EventDoorLockUtils {
    val TUYA_PANEL_DOOR_LOCK_DETAIL = "tuya_panel_doorlock_device_detail"
    val TUYA_PANEL_DOOR_LOCK_UNLOCK = "tuya_panel_doorlock_remote_unlock"
    val TUYA_PANEL_DOOR_LOCK_DYN_PW = "tuya_panel_doorlock_get_dynamic_pass"
    val TUYA_PANEL_DOOR_LOCK_ALARM = "tuya_panel_doorlock_alarm"
    val TUYA_PANEL_DOOR_LOCK_RECORD = "tuya_panel_doorlock_record"
    val TUYA_PANEL_DOOR_LOCK_TEMP_PW = "tuya_panel_doorlock_temporary_pw"
    val TUYA_PANEL_DOOR_LOCK_MORE = "tuya_panel_doorlock_more"
    val TUYA_PANEL_DOOR_LOCK_TEMP_PW_CREATE = "tuya_panel_door_lock_temporary_pw_create"
}

object EventIRTVUtils {
    val TUYA_PANEL_IR_DETAIL = "tuya_panel_ir_device_detail"
    val TUYA_PANEL_IR_ADD_REMOTTE = "tuya_panel_ir_add_remote"
    val TUYA_PANEL_IR_ADD_REMOTTE_ITEM = "tuya_ir_add_remote"
    val TUYA_PANEL_IR_TV_DEVICE_DETAIL = "tuya_panel_ir_tv_device_detail"


    val TUYA_PANEL_IR_MATCH_TV = "tuya_panel_ir_match_tv"
    val TUYA_PANEL_IR_TV = "tuya_panel_ir_tv"
    val TUYA_PANEL_IR_MATCH_TV_NEXT = "tuya_panel_ir_match_tv_next"
    val TUYA_PANEL_IR_MATCH_TV_CAN_CONTROL = "tuya_panel_ir_match_tv_can"

}

object EventIRAirUtils {
    val TUYA_PANEL_IR_MATCH_AIR = "tuya_panel_ir_match_air"
    val TUYA_PANEL_IR_AIR = "tuya_panel_ir_air"
    val TUYA_PANEL_IR_MATCH_AIR_NEXT = "tuya_panel_ir_match_air_next"
    val TUYA_PANEL_IR_MATCH_AIR_CAN_CONTROL = "tuya_panel_ir_match_air_can"
    val TUYA_PANEL_IR_AIR_DEVICE_DETAIL = "tuya_panel_ir_air_device_detail"

}