package ai.apdigital.tuya.utils

import android.view.View

interface OnItemDeleteClickListener<T> {

    fun onDeleteItemClick(view: View?, position: Int, isLongClick: Boolean, entity: T?)
}