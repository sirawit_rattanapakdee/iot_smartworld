package ai.apdigital.tuya.utils

import ai.apdigital.tuya.R
import ai.apdigital.tuya.ui.DeviceContainer
import ai.apdigital.tuya.ui.DeviceSwitchValue
import com.tuya.smart.android.device.bean.BoolSchemaBean
import com.tuya.smart.android.device.enums.DataTypeEnum
import com.tuya.smart.home.sdk.TuyaHomeSdk
import com.tuya.smart.sdk.api.ITuyaDevice
import com.tuya.smart.sdk.bean.DeviceBean
import timber.log.Timber
import java.util.*

object SchemaConverter {

    fun convertSchemaDevice(devId: String): DeviceContainer {

        var countSwitch = 0
        val deviceContainer = DeviceContainer()
        var deviceBean: DeviceBean? = null
        TuyaHomeSdk.getDataInstance().getSchema(devId)?.let { map ->
            deviceBean = TuyaHomeSdk.getDataInstance().getDeviceBean(devId)
            if (deviceBean?.categoryCode != "inf_qt") {

                map.values.forEachIndexed { index, bean ->
                    if (bean.type == DataTypeEnum.OBJ.type) {
                        val value = deviceBean?.dps?.get(bean.id)
                        when {

                            bean.code?.startsWith("SOS") == true -> {
                                deviceContainer.deviceTypeDP = bean.id
                                deviceContainer.deviceType =
                                    "SOS"
                            }
                            bean.code?.startsWith("PIR") == true -> {
                                deviceContainer.deviceTypeDP = bean.id
                                deviceContainer.deviceType =
                                    "PIR"
                            }
                            bean.code?.startsWith("presence_state") == true -> {
                                deviceContainer.deviceTypeDP = bean.id
                                deviceContainer.deviceType =
                                    "presence_state"
                            }
                            bean.code?.startsWith("pir_state") == true -> {
                                deviceContainer.deviceTypeDP = bean.id
                                deviceContainer.deviceType =
                                    "pir_state"
                            }
                            bean.code?.contains("doorcontact") == true -> {
                                deviceContainer.deviceTypeDP = bean.id
                                deviceContainer.deviceType =
                                    "DOOR_CONTACT"
                            }
                            bean.code?.contains("alarm_switch") == true -> {
                                deviceContainer.deviceTypeDP = bean.id
                                deviceContainer.deviceType =
                                    "ALARM_SWITCH"
                            }
                        }
                        if (value is Boolean) {
                            if (bean.getSchemaType() == BoolSchemaBean.type && bean.mode?.contains("w") == true && (bean.code?.startsWith(
                                    "switch"
                                ) == true || bean.code?.startsWith("led_switch") == true)
                            ) {
                                deviceContainer.switchSchema?.add(
                                    DeviceSwitchValue(
                                        value,
                                        index,
                                        bean
                                    )
                                )
                                countSwitch++
                            }
                        }
                    }
                }
            }
        }
/*
        if (countSwitch > 0)
            deviceContainer.deviceType = "SWITCH"
*/

        deviceContainer.switchCount = countSwitch
        deviceContainer.device = deviceBean


        return deviceContainer
    }

    val battery = arrayListOf(
        "battery_percentage",
        "battary",
        "battery",
        "Battery",
        "residual_electricity",
        "wireless_electricity"
    )

    fun checkBattery(devId: String): Int {
        TuyaHomeSdk.getDataInstance().getSchema(devId)?.let { map ->
            val deviceBean = TuyaHomeSdk.getDataInstance().getDeviceBean(devId)
            for (bean in map.values) {
                if (battery.contains(bean.code)) {
                    val value = deviceBean?.dps?.get(bean.id)
                    if (value is Int) {
                        return value.toInt()
                    }
                }
            }
        }
        return 1000
    }

    fun convertPercentToDrawable(percent: Int): Int {
        when {
            percent <= 0 -> {
                return R.drawable.ic_battery_0
            }
            percent < 21 -> {
                return R.drawable.ic_battery_20
            }
            percent < 41 -> {
                return R.drawable.ic_battery_40
            }
            percent <= 61 -> {
                return R.drawable.ic_battery_60
            }
            percent <= 81 -> {
                return R.drawable.ic_battery_80
            }
            else -> {
                return R.drawable.ic_battery_100
            }
        }
    }
}