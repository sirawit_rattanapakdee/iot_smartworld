package ai.apdigital.tuya.utils.alerts

import ai.apdigital.tuya.R
import ai.apdigital.tuya.utils.widgets.MaterialDialogFragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.alert_room_choose_device.*


class AlertChooseDeviceDialogFragment(
    private val clickListener: OnChooseDeviceRoomListener?
) : MaterialDialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.alert_room_choose_device, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnNotNow?.setOnClickListener {
            clickListener?.onNotNowClick()
            dismissAllowingStateLoss()
        }
        btnAddDevice?.setOnClickListener {
            clickListener?.onAddDeviceClick()
            dismissAllowingStateLoss()
        }
    }

    interface OnChooseDeviceRoomListener {
        fun onAddDeviceClick()
        fun onNotNowClick()
    }

}