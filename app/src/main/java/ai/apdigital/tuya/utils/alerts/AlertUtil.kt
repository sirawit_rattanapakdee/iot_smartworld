package ai.apdigital.tuya.utils.alerts

import ai.apdigital.tuya.networking.ResponseDescription
import androidx.fragment.app.FragmentManager

/**
 * Created by Waheed on 04,November,2019
 */

object AlertUtil {

    interface OnSuccessListener {
        fun onSuccess()
    }

    interface OnCancelListener {
        fun onCancel()
    }

    fun showAlertConfirmSFWithActionOKClick(
        title: String?,
        fragmentManager: FragmentManager?,
        message: String?, successListener: OnSuccessListener?, cancelListener: OnCancelListener?,
        confirmColor: Int
    ) {
        fragmentManager?.let {
            val notices =
                ConfirmDialogFragment(title, message, successListener, cancelListener, confirmColor)
            notices.isCancelable = false
            notices.show(
                it,
                "showAlertNotices"
            )
        }
    }

    fun showAlertConfirmWithActionOKClick(
        title: String?,
        fragmentManager: FragmentManager?,
        message: String?, successListener: OnSuccessListener?,
        confirmColor: Int
    ) {
        fragmentManager?.let {
            val notices = ConfirmDialogFragment(title, message, successListener, null, confirmColor)
            notices.isCancelable = false
            notices.show(
                it,
                "showAlertNotices"
            )
        }
    }

    fun showAlertNoticesWithActionOKClick(
        alertType: AlertTypeEnum,
        fragmentManager: FragmentManager?,
        message: ResponseDescription?, successListener: OnSuccessListener?, buttonName: String
    ) {
        fragmentManager?.let {
            val notices = NoticeDialogFragment(alertType, message, buttonName, successListener)
            notices.isCancelable = false
            notices.show(
                it,
                "showAlertNotices"
            )
        }
    }
/*

    fun showAlertNotices(
        context: Context?,
        message: ResponseDescription?
    ) {
        try {


            context?.let { c ->

                val con = ContextThemeWrapper(c, R.style.MaterialTheme)
                MaterialAlertDialogBuilder(con)
                    .setTitle(c.getString(R.string.text_notices))
                    .setMessage(getMessageByCode(context, message))
                    .setPositiveButton(c.getString(R.string.action_ok)) { _, _ ->
                    }.setCancelable(false)
                    .show()

            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun showAlert(
        context: Context?,
        message: ResponseDescription?
    ) {
        try {
            context?.let { c ->
                val con = ContextThemeWrapper(c, R.style.MaterialTheme)
                MaterialAlertDialogBuilder(con)
                    .setView(R.layout.dialog_alert)
                    .setTitle(c.getString(R.string.text_notices))
                    .setMessage(getMessageByCode(context, message))
                    .setPositiveButton(c.getString(R.string.action_ok)) { _, _ ->
                    }.setCancelable(false)
                    .show()

            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
*/
}