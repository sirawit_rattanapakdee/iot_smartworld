package ai.apdigital.tuya.utils.alerts

import ai.apdigital.tuya.R
import ai.apdigital.tuya.utils.widgets.MaterialDialogFragment
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import com.prolificinteractive.materialcalendarview.CalendarDay
import com.prolificinteractive.materialcalendarview.MaterialCalendarView
import kotlinx.android.synthetic.main.alert_calendar.*
import java.lang.NullPointerException
import java.text.ParseException
import java.text.SimpleDateFormat

import org.threeten.bp.LocalDate
import timber.log.Timber
import java.util.*

class CalendarDialogFragment(
    private val listener: OnCalendarClickListener
) : MaterialDialogFragment() {

    interface OnCalendarClickListener {
        fun onDateSelected(name: String)

        fun onMonthChanged(name: String)

        fun onCancelClick()
    }

    val DATE_FORMAT = "yyyy-MM-dd"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.alert_calendar, container, false)
    }

    var calendar: CalendarDay? = null

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        calendarView?.showOtherDates = MaterialCalendarView.SHOW_ALL

        val c = Calendar.getInstance()
        c.add(Calendar.MONTH, 1)
        var year = c.get(Calendar.YEAR)
        var month = c.get(Calendar.MONTH)
        var day = c.get(Calendar.DAY_OF_MONTH)
        val max = getLocalDate("$year-$month-$day")


        c.add(Calendar.MONTH, -5)
        year = c.get(Calendar.YEAR)
        month = c.get(Calendar.MONTH)
        day = c.get(Calendar.DAY_OF_MONTH)
        val min = getLocalDate("$year-$month-$day")

/*
        val sdf = SimpleDateFormat("yyyy-MM-dd")
        val currentDate = sdf.format(Date())
*/
        calendarView?.setAllowClickDaysOutsideCurrentMonth(true)
        /* final LocalDate max = getLocalDate("2019-12-30");*/
        calendarView?.state()?.edit()?.setMinimumDate(min)?.setMaximumDate(max)?.commit()
        calendarView?.setOnMonthChangedListener { widget, date ->
            date?.let {
                val yearC = it.year
                val monthC = it.month
                var mStr = monthC.toString()
                if (monthC < 10) mStr = "0$monthC"
                val dateSelect = "$yearC-$mStr"
                Timber.e("Date : $it")
                listener.onMonthChanged(dateSelect)
            }
        }

        calendarView?.setOnDateChangedListener { widget, date, selected ->
            if (selected) {
                calendar = date
                calendar?.let {

                    val yearC = it.year
                    val monthC = it.month
                    val dayC = it.day

                    var mStr = monthC.toString()
                    if (monthC < 10) mStr = "0$monthC"

                    var dStr: String = dayC.toString()
                    if (dayC < 10) dStr = "0$dayC"
                    val dateSelect = "$yearC-$mStr-$dStr"
                    Timber.e("Date : $dateSelect")
                    dismissAllowingStateLoss()
                    listener.onDateSelected(dateSelect)
                    calendar = null
                }
            }
        }

        btnOK?.setOnClickListener {
            calendar?.let {

                val yearC = it.year
                val monthC = it.month
                val dayC = it.day

                var mStr = monthC.toString()
                if (monthC < 10) mStr = "0$monthC"

                var dStr: String = dayC.toString()
                if (dayC < 10) dStr = "0$dayC"
                val dateSelect = "$yearC-$mStr-$dStr"
                Timber.e("Date : $dateSelect")
                dismissAllowingStateLoss()
                listener.onDateSelected(dateSelect)
                calendar = null
            }
        }
        btnCancel?.setOnClickListener {
            dismissAllowingStateLoss()
            listener.onCancelClick()
        }
        calendarView?.invalidateDecorators()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun setEvent(dateList: List<String?>) {
        val localDateList: MutableList<LocalDate> = ArrayList<LocalDate>()
        for (string in dateList) {
            val calendar: LocalDate? = getLocalDate(string)
            calendar?.let {
                localDateList.add(calendar)
            }
        }
        val datesLeft: MutableList<CalendarDay> = ArrayList()
        val datesCenter: MutableList<CalendarDay> = ArrayList()
        val datesRight: MutableList<CalendarDay> = ArrayList()
        val datesIndependent: MutableList<CalendarDay> = ArrayList()
        for (localDate in localDateList) {
            var right = false
            var left = false
            for (day1 in localDateList) {
                if (localDate.isEqual(day1.plusDays(1))) {
                    left = true
                }
                if (day1.isEqual(localDate.plusDays(1))) {
                    right = true
                }
            }
            if (left && right) {
                datesCenter.add(CalendarDay.from(localDate))
            } else if (left) {
                datesLeft.add(CalendarDay.from(localDate))
            } else if (right) {
                datesRight.add(CalendarDay.from(localDate))
            } else {
                datesIndependent.add(CalendarDay.from(localDate))
            }
        }
        /*if (color == pink) {
            setDecor(datesCenter, R.drawable.p_center)
            setDecor(datesLeft, R.drawable.p_left)
            setDecor(datesRight, R.drawable.p_right)
            setDecor(datesIndependent, R.drawable.p_independent)
        } else {*/
        setDecor(datesCenter, R.drawable.g_center)
        setDecor(datesLeft, R.drawable.g_left)
        setDecor(datesRight, R.drawable.g_right)
        setDecor(datesIndependent, R.drawable.g_independent)
        /*}*/
    }

    private fun setDecor(calendarDayList: List<CalendarDay>?, drawable: Int) {
        calendarView.addDecorators(
            EventDecorator(
                context, drawable, calendarDayList
            )
        )
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun getLocalDate(date: String?): LocalDate? {
        val sdf = SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH)
        return try {
            val input = sdf.parse(date)
            val cal = Calendar.getInstance()
            cal.time = input
            LocalDate.of(
                cal[Calendar.YEAR],
                cal[Calendar.MONTH] + 1,
                cal[Calendar.DAY_OF_MONTH]
            )
        } catch (e: NullPointerException) {
            null
        } catch (e: ParseException) {
            null
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun updateView(highlight: MutableList<String>) {
        setEvent(highlight)
        calendarView?.invalidateDecorators()
    }
}