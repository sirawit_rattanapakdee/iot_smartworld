package ai.apdigital.tuya.utils.alerts

import ai.apdigital.tuya.R
import ai.apdigital.tuya.utils.widgets.MaterialDialogFragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.alert_confirm.*

class ConfirmDialogFragment(
    private val title: String?,
    private val message: String?,
    private val successListener: AlertUtil.OnSuccessListener?, private val cancelListener: AlertUtil.OnCancelListener?, private val confirmColor: Int
) : MaterialDialogFragment() {


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.alert_confirm, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tvMessage?.visibility = View.GONE
        message?.let {
            tvMessage?.visibility = View.VISIBLE
            tvMessage?.text = message
        }

        tvTitle?.text = title
        context?.let { ContextCompat.getColor(it, confirmColor) }?.let { color ->
            btnConfirm?.setTextColor(color)
        }
        btnConfirm?.setOnClickListener {
            successListener?.onSuccess()
            dismissAllowingStateLoss()
        }
        btnCancel?.setOnClickListener {
            cancelListener?.onCancel()
            dismissAllowingStateLoss()
        }
    }
}