package ai.apdigital.tuya.utils.alerts

import ai.apdigital.tuya.R
import ai.apdigital.tuya.utils.widgets.MaterialDialogFragment
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.alert_room_name.*


class EditRoomDialogFragment(
    val title: String?,
    val name: String?,
    private val clickListener: OnEditNameListener?
) : MaterialDialogFragment() {

    interface OnEditNameListener {
        fun onSaveClick(name: String)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.alert_room_name, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        edtRoomName?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                btnSave?.isEnabled = s?.length ?: 0 > 0
            }
        })

        tvTitle.text = title
        edtRoomName?.setText(name ?: "")
        btnSave?.setOnClickListener {
            clickListener?.onSaveClick(edtRoomName?.text.toString())
            dismissAllowingStateLoss()
        }
        btnCancel?.setOnClickListener {
            dismissAllowingStateLoss()
        }
    }
}