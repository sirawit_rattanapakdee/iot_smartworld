package ai.apdigital.tuya.utils.alerts

import ai.apdigital.tuya.R
import ai.apdigital.tuya.networking.ResponseDescription
import ai.apdigital.tuya.utils.widgets.MaterialDialogFragment
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.alert_notices.*

class NoticeDialogFragment(
    private val alertType: AlertTypeEnum,
    private val message: ResponseDescription?,
    private val buttonName: String?,
    private val successListener: AlertUtil.OnSuccessListener?
) : MaterialDialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.alert_notices, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        /* when (alertType) {
             AlertTypeEnum.ERROR -> {
                 ivNoticeSymbol?.setImageResource(R.drawable.ic_notices_error)
             }
             AlertTypeEnum.SUCCESS -> {
                 ivNoticeSymbol?.setImageResource(R.drawable.ic_notices_success)
             }
             else -> {
                 ivNoticeSymbol?.setImageResource(R.drawable.ic_notices_information)
             }
         }*/
        tvMessage?.text = getMessageByCode(context, message)
        btnClose?.text = buttonName
        btnClose?.setOnClickListener {
            successListener?.onSuccess()
            dismissAllowingStateLoss()
        }
    }


    private fun getMessageByCode(
        context: Context?,
        message: ResponseDescription?
    ): String {
        context?.let { c ->
            return when (message?.code) {
                101 -> {
                    c.getString(R.string.error_internet_or_host)
                }
                401 -> {
                    c.getString(R.string.error_username_or_password_incorrect)
                }
                else -> {
                    message?.message.toString()
                }
            }
        }
        return message?.message.toString()
    }

}