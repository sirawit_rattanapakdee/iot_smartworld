package ai.apdigital.tuya.utils.exception

open class NotSetLayoutException : RuntimeException() {
    init {
        RuntimeException("getLayoutView() not return 0")
    }
}