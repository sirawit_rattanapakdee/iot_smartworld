package ai.apdigital.tuya.utils.keyboard

import ai.apdigital.tuya.R
import android.content.Context
import android.graphics.Rect
import android.util.TypedValue
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlin.math.roundToInt

fun AppCompatActivity.getRootView(): View {
    return findViewById(R.id.container)
}

fun Context.convertDpToPx(dp: Float): Float {
    return TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP,
        dp,
        this.resources.displayMetrics
    )
}

fun AppCompatActivity.isKeyboardOpen(height: Int): Boolean {
    val visibleBounds = Rect()
    this.getRootView().getWindowVisibleDisplayFrame(visibleBounds)
    val heightNew = if (getRootView().height >= height) {
        KeyboardEventListener.height = getRootView().height
        getRootView().height - visibleBounds.height()

    } else
        height - visibleBounds.height()
    val marginOfError = this.convertDpToPx(50F).roundToInt()
    return heightNew > marginOfError
}
